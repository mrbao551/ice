-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2023 at 12:39 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jwt_api_ci3`
--

-- --------------------------------------------------------

--
-- Table structure for table `ask`
--

CREATE TABLE `ask` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT 0,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `diem` tinyint(2) NOT NULL DEFAULT 1,
  `time_exam` int(5) NOT NULL,
  `time_created` int(11) NOT NULL DEFAULT 0,
  `time_end` int(11) DEFAULT 0,
  `show_reply` tinyint(1) NOT NULL DEFAULT 0,
  `stt` int(11) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ask`
--

INSERT INTO `ask` (`id`, `class_id`, `title`, `type`, `file`, `content`, `diem`, `time_exam`, `time_created`, `time_end`, `show_reply`, `stt`, `active`) VALUES
(4, 1, 'Question 1', 0, 'source/files/Lung Cu.jpg', '<span xss=removed><span xss=\"removed\"><strong><span xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\">Where is this? </span></span></span></span></strong></span></span>', 0, 0, 1681352142, 1681352210, 1, 0, 1),
(5, 1, 'Question 2', 0, '', '<p><span xss=removed><strong><span xss=\"removed\"><span xss=\"removed\">What is the real name of “Trạng Trình”?</span></span></strong></span></p>\n', 0, 0, 0, 1681352252, 1, 0, 1),
(6, 1, 'Question 3', 0, '', '<p><span xss=\"removed\"><strong><span xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\">Which of the following is the cultural symbol of the civilization of the Hồng River Delta?</span></span></span></strong></span></p>\n', 0, 0, 0, 1681353316, 1, 0, 1),
(7, 1, 'Question 4', 0, '', '<span xss=removed><strong><span xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\">How many countries are there in the Association</span></span></span><span xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\"> of South East Asian Nations?</span></span></span></strong></span>', 0, 0, 0, 0, 0, 0, 1),
(8, 1, 'Question 5', 0, '', '<span xss=removed><strong><span xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\">Which continent has the most Covid -19 cases?</span></span></span></strong></span>', 0, 0, 0, 0, 0, 0, 1),
(9, 1, 'Question 6', 0, '', '<span xss=\"removed\"><strong><span xss=\"removed\"><span xss=\"removed\">Which country is known as the land of the</span></span> <span xss=\"removed\"><span xss=\"removed\">rising sun?</span></span></strong></span>', 0, 0, 0, 0, 0, 0, 1),
(10, 1, 'Question 7', 0, '', '<span xss=\"removed\"><strong><span xss=\"removed\"><span xss=\"removed\"><span xss=\"removed\">How many countries in the world without Covid 19 case?</span></span></span></strong></span>', 0, 0, 0, 0, 0, 0, 1),
(11, 1, 'Question 8', 0, '', '<span xss=removed><strong><span xss=\"removed\"><span xss=\"removed\">What is the real name of “Kim Dong”?</span></span></strong></span>', 0, 0, 0, 0, 0, 0, 1),
(12, 1, 'Question 9', 0, 'source/files/WWII.jpg', '<span xss=\"removed\"><strong><span xss=\"removed\"><span xss=\"removed\">Which event is this photo about?</span></span></strong></span>', 0, 0, 0, 0, 0, 0, 1),
(13, 1, 'Question 10', 0, 'source/files/mix_34s (audio-joiner.com).mp3', '<p><span xss=removed><strong><span xss=removed><span xss=removed><span xss=removed>What is this song?</span></span></span></strong></span></p>\n', 0, 0, 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `id` int(11) NOT NULL,
  `title` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `ck_exam_id` int(11) NOT NULL,
  `ask_id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `reply_true` tinyint(1) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `stt` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reply`
--

INSERT INTO `reply` (`id`, `title`, `ck_exam_id`, `ask_id`, `content`, `reply_true`, `active`, `stt`) VALUES
(1, 'A', 1, 1, 'used to', 0, 1, 0),
(2, 'B', 1, 1, 'will', 1, 1, 0),
(3, 'C', 1, 1, 'would', 0, 1, 0),
(4, 'D', 1, 1, 'could', 0, 1, 0),
(5, 'A', 1, 1, 'used to', 0, 1, 0),
(6, 'B', 1, 1, 'will', 0, 1, 0),
(7, 'C', 1, 1, 'would', 0, 1, 0),
(8, 'D', 1, 1, 'could', 0, 1, 0),
(9, 'A', 1, 2, '\"Thanh niên làm theo lời Bác\" or \"Thanh nien lam theo loi Bac\"', 0, 1, 0),
(10, 'B', 1, 2, '\"Khat vong tuoi tre\" or \"Khát vọng tuổi trẻ\"', 0, 1, 0),
(11, 'C', 1, 2, '\"Lên Đàng\" or \"Len Dang\"', 1, 1, 0),
(12, 'D', 1, 2, '\"Tuoi tre the he Ho Chi Minh\" or \"Tuổi trẻ thế hệ Hồ Chí Minh\"', 0, 1, 0),
(13, 'A', 1, 4, 'Lung Cu Flag Tower', 1, 1, 0),
(14, 'B', 1, 4, 'Mai Chau Flag Tower', 0, 1, 0),
(15, 'C', 1, 4, 'Hien Luong Flag Tower ', 0, 1, 0),
(16, 'D', 1, 4, 'Tho Chu Flag Tower', 0, 1, 0),
(17, 'A', 1, 5, 'Chu Van An', 0, 1, 0),
(18, 'B', 1, 5, 'Cao Ba Quat', 0, 1, 0),
(19, 'C', 1, 5, 'Nguyen Binh Khiem', 1, 1, 0),
(20, 'D', 1, 5, 'Nguyen Sieu', 0, 1, 0),
(21, 'A', 1, 6, 'Gongs', 0, 1, 0),
(22, 'B', 1, 6, 'Dong Son bronze drum', 1, 1, 0),
(23, 'C', 1, 6, 'House on stilts', 0, 1, 0),
(24, 'D', 1, 6, 'None of the above', 0, 1, 0),
(25, 'A', 1, 7, '11', 0, 1, 0),
(26, 'B', 1, 7, '12', 0, 1, 0),
(27, 'C', 1, 7, '10', 1, 1, 0),
(28, 'D', 1, 7, '9', 0, 1, 0),
(29, 'A', 1, 8, 'Africa', 0, 1, 0),
(30, 'B', 1, 8, 'South America', 0, 1, 0),
(31, 'C', 1, 8, 'Europe', 0, 1, 0),
(32, 'D', 1, 8, 'Asia', 1, 1, 0),
(33, 'A', 1, 9, 'America', 0, 1, 0),
(34, 'B', 1, 9, 'China', 0, 1, 0),
(35, 'C', 1, 9, 'Japan', 1, 1, 0),
(36, 'D', 1, 9, 'Russia', 0, 1, 0),
(37, 'A', 1, 10, '&lt;50 countries', 0, 1, 0),
(38, 'B', 1, 10, '&lt;20 countries', 0, 1, 0),
(39, 'C', 1, 10, '&lt;10 countries', 0, 1, 0),
(40, 'D', 1, 10, 'No data', 0, 1, 0),
(41, 'A', 1, 11, 'Vu A Dinh', 0, 1, 0),
(42, 'B', 1, 11, 'Nong Van Den', 1, 1, 0),
(43, 'C', 1, 11, 'Le Van Tam', 0, 1, 0),
(44, 'D', 1, 11, 'Be Van Dan', 0, 1, 0),
(45, 'A', 1, 12, 'The World War I', 0, 1, 0),
(46, 'B', 1, 12, 'The World War II', 1, 1, 0),
(47, 'C', 1, 12, 'The Cold War', 0, 1, 0),
(48, 'D', 1, 12, 'The Russian Civil War', 0, 1, 0),
(49, 'A', 1, 13, 'Khat vong tuoi tre', 0, 1, 0),
(50, 'B', 1, 13, 'Tien quan ca', 0, 1, 0),
(51, 'C', 1, 13, 'Len dang', 1, 1, 0),
(52, 'D', 1, 13, 'Bai ca Ho Chi Minh', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reply_member`
--

CREATE TABLE `reply_member` (
  `id` int(11) NOT NULL,
  `bang_thi_id` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `user_luot_thi_id` tinyint(1) DEFAULT 0,
  `ask_id` int(11) NOT NULL DEFAULT 0,
  `reply_id` tinyint(1) NOT NULL DEFAULT 0,
  `reply_id_true` int(11) NOT NULL DEFAULT 0,
  `reply_text` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reply_active` tinyint(1) NOT NULL DEFAULT 0,
  `time_reply` int(11) NOT NULL DEFAULT 0,
  `time_created` int(11) NOT NULL DEFAULT 0,
  `reply_true` tinyint(1) NOT NULL DEFAULT 0,
  `score_reply_exam` int(11) NOT NULL DEFAULT 0,
  `stt` int(11) NOT NULL DEFAULT 0,
  `active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `reply_member`
--

INSERT INTO `reply_member` (`id`, `bang_thi_id`, `user_id`, `user_luot_thi_id`, `ask_id`, `reply_id`, `reply_id_true`, `reply_text`, `reply_active`, `time_reply`, `time_created`, `reply_true`, `score_reply_exam`, `stt`, `active`) VALUES
(7, 2, 4, 0, 6, 0, 22, NULL, 0, 0, 1681900394, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `fullname` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `class_id` tinyint(1) NOT NULL DEFAULT 0,
  `password` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) DEFAULT 'default.jpg',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_admin` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `is_confirmed` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `is_deleted` tinyint(1) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `fullname`, `email`, `class_id`, `password`, `avatar`, `created_at`, `updated_at`, `is_admin`, `is_confirmed`, `is_deleted`) VALUES
(2, 'admin1', NULL, 'admin1@gmail.com', 2, '$2y$10$JhjUqBKzRTx6h8iG8/RH9ewtfmLdpQyzI8XwbQBl5eLmTXiH0VYki', 'default.jpg', '2023-04-18 10:44:22', NULL, 0, 0, 0),
(3, 'admin', NULL, 'admin@gmail.com', 2, '$2y$10$eOQsHh4QqnjPtfG85yPdOO4wYbJ2FUloZscP1HAXz.rYFjpCKjWXa', 'default.jpg', '2023-04-18 10:45:34', NULL, 0, 0, 0),
(4, 'admin2', NULL, 'admin2@gmail.com', 2, '$2y$10$VO4gMItvZkFwWcvKPEwwAOPZWKJ8YBb4KQHghPPhruRujm8T1xox2', 'default.jpg', '2023-04-18 10:53:02', NULL, 0, 0, 0),
(5, 'admin3', NULL, 'admin3@gmail.com', 2, '$2y$10$i8H3IvB5MexeT46a1cLcheMq.c7gXKO.Yxdzz3a74suDHGQmK52xi', 'default.jpg', '2023-04-18 10:53:19', NULL, 0, 0, 0),
(6, 'admin3', NULL, 'admin4@gmail.com', 2, '$2y$10$OxP6gXrpz9Kg9tcUTLStPOi9M.ahZNTtalCbRzF5Lb/zTcfuSmSOq', 'default.jpg', '2023-04-18 10:54:31', NULL, 0, 0, 0),
(7, 'admin5', NULL, 'admin5@gmail.com', 2, '$2y$10$IwAU/1.5/Kn.CsPYrP8Bee7Nc5DyX4kUbkbI6tvLZXETEdunOtEv2', 'default.jpg', '2023-04-18 11:13:13', NULL, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_luot_thi`
--

CREATE TABLE `user_luot_thi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `so_luot` tinyint(1) NOT NULL DEFAULT 0,
  `trang_thai` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_luot_thi`
--

INSERT INTO `user_luot_thi` (`id`, `user_id`, `so_luot`, `trang_thai`) VALUES
(3, 4, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ask`
--
ALTER TABLE `ask`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reply`
--
ALTER TABLE `reply`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reply_member`
--
ALTER TABLE `reply_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_luot_thi`
--
ALTER TABLE `user_luot_thi`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ask`
--
ALTER TABLE `ask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `reply_member`
--
ALTER TABLE `reply_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_luot_thi`
--
ALTER TABLE `user_luot_thi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
