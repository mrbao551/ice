<script src="public/template/backend/vendors/jquery/dist/jquery.min.js"></script>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
      <h3> <small><a href="backend/support/index" style="color:#337ab7" title="quay lai"><i class="fa fa-mail-reply"></i> Quay lại</small></a> &nbsp;&nbsp;Sửa hỗ trợ  </h3>
      </div>
    </div>
    <div class="clearfix"></div>
    <form method="POST" action="" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="x_title">
            <h5>Thông tin chung </h5>                        
            <div class="clearfix"></div>
          </div>
            <div class="x_content">
                <br />
                <center><h4><?php echo common_error(validation_errors()); ?></h4></center>
                <div class="form-group">
                    <label class=" col-md-2 col-sm-2 col-xs-12" for="last-name">Giá trị giải thưởng:</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <input type="text" name="data[title]" id="last-name" value="<?php echo(isset($_post['title']) ? common_value_post($_post['title']): '');?>" required="required" class="form-control col-md-7 col-xs-12">
                    </div>
                </div>
                <div class="form-group">
                    <label class=" col-md-2 col-sm-2 col-xs-12" for="last-name">Ảnh đại diện:</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <div class="input-group col-md-12 col-sm-12 col-xs-12">

                           <input type="text" id="image" name="data[image]"  onclick="openKCFinder(this)" id="img" value="<?php echo(isset($_post['image']) ? common_value_post($_post['image']): '');?>"  class="form-control ">
                        </div>                        
                    </div>
                </div>
                <div class="form-group">
                    <label class=" col-md-2 col-sm-2 col-xs-12" for="last-name">Ảnh liên quan:</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <div class="input-group col-md-12 col-sm-12 col-xs-12">
                            <script>
                                $(document).ready(function(){
                                    var i=<?php echo ($_count_product_image > 1) ? $_count_product_image : 0 ?>;
                                    $("#add_row").click(function(){
                                        $('#addr'+i).html('<td><input name="data_img[title_image][]" type="text" class="form-control input-md"  /> </td><td><textarea class="form-control" name="data_img[src_image_one][]"></textarea> </td><input type="hidden" name="data_img[id_product][]" value="<?php echo $_post['id'] ?>" /><input type="hidden" value="'+i+'" name="data_img[count][]"><td><input id="'+i+'" name="data_img[src_image][]" type="text" class="form-control input-md"></td><td><span class="btn btn-default pull-left" onclick="BrowseServer('+i+')"><a href="javascript:;">Upload ảnh</a></span></td>');

                                        $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
                                        i++;
                                    });
                                    $("#delete_row").click(function(){
                                        if(i>0){
                                            $("#addr"+(i-1)).html('');
                                            i--;
                                        }
                                    });

                                });
                            </script>
                            <table class="table table-bordered table-hover" id="tab_logic">
                                <thead>
                                <tr >

                                    <th class="text-center">
                                        Tiêu đề
                                    </th>
                                    <th class="text-center">
                                        Nội dung
                                    </th> <th class="text-center">
                                        Đường dẫn ảnh
                                    </th>
                                    <th class="text-center">
                                        &nbsp;
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if($_count_product_image > 0){
                                    foreach($_product_image as $key=>$val){  ?>
                                        <tr id='addr<?php echo $key ?>'>
                                            <td>
                                                <input type="text" name='data_img[title_image][]' value="<?php echo $val['title_image'];  ?>" class="form-control"/>
                                                <input type="hidden" name="data_img[id_product][]" value="<?php echo $_post['id'] ?>" />
                                                <input type="hidden" value="" name="data_img[count][]"></td>
                                            </td>
                                            <td>
                                                <textarea class="form-control" name='data_img[src_image_one][]'><?php echo $val['src_image_one'];  ?></textarea>
                                            </td>
                                            <td>
                                                <input type="text" id="<?php echo $key ?>" name="data_img[src_image][]" id="img" value="<?php echo $val['src_image'];  ?>"  class="form-control ">
                                            </td>
                                            <td>
                                                <span class="btn btn-default" id="" onclick="BrowseServer('<?php echo $key ?>')"><a href="javascript:;">Upload ảnh</a></span>
                                                <?php echo isset($val['src_image']) ? '<img width="60" height="40" src="'.BASE_URL.common_value_post($val['src_image']).'" alt="" />' : '' ?>
                                            </td>
                                        </tr>
                                    <?php  } } ?>
                                <tr id='addr<?php echo $_count_product_image ?>'></tr>

                                </tbody>
                            </table>
                            <a id="add_row" class="btn btn-default pull-left">Add Row</a><a id='delete_row' class="pull-right btn btn-default">Delete Row</a>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class=" col-md-2 col-sm-2 col-xs-12" for="last-name">Giải thưởng khác:</label>
                        <div class="col-md-10 col-sm-10 col-xs-12">
                            <input type="text" name="data[hotline]" id="last-name" value="<?php echo(isset($_post['hotline']) ? common_value_post($_post['hotline']): '');?>"  class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                <div class="form-group">
                    <label class=" col-md-2 col-sm-2 col-xs-12" for="last-name">Nội dung chi tiết:</label>
                    <div class="col-md-10 col-sm-10 col-xs-12">
                        <textarea name="data[content]" id="content" rows="9" class="textarea form-control col-md-7 col-xs-12"><?php echo(isset($_post['content']) ? common_value_post($_post['content']): '');?></textarea>
                        <script>
                            CKEDITOR.replace( 'content', {
                                extraPlugins: 'tableresize'
                            });
                        </script>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                        <input type="submit" name="change" class="btn btn-success" value="Thay đổi"/>
                        <button type="reset" class="btn btn-primary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
      </div>
      </form>
    </div>
  </div>
</div>
 