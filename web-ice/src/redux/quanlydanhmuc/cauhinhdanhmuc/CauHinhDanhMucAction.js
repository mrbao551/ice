import * as Constants from './CauHinhDanhMucConstant';
export function setData(CauHinhDanhMucList) {
    return {
        type: Constants.CAUHINHDANHMUC_SET_LIST_DATA,
        CauHinhDanhMucList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.CAUHINHDANHMUC_SET_LIST_META,
        meta
    };
}