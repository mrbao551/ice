import * as Constants from './QuanLyDanhMucConstant';
export function setData(QuanLyDanhMucList) {
    return {
        type: Constants.QUANLYDANHMUC_SET_LIST_DATA,
        QuanLyDanhMucList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.QUANLYDANHMUC_SET_LIST_META,
        meta
    };
}