import * as Constants from './ThongTinHoSoConstant';
export function setData(ThongTinHoSoList) {
    return {
        type: Constants.THONGTINHOSO_SET_LIST_DATA,
        ThongTinHoSoList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.THONGTINHOSO_SET_LIST_META,
        meta
    };
}