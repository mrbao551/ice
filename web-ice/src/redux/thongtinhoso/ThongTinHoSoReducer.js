import * as Constant from './ThongTinHoSoConstant';
const ThongTinHoSo = {
    ThongTinHoSoList: [],
    meta: {
        page: 1,
        page_size: 15,
        sort: {id:'desc'},
        search: '' ,
        filter: {
        },
    }
};
const ThongTinHoSoReducer = (state = ThongTinHoSo, action) => {
    switch (action.type) {
        case Constant.THONGTINHOSO_SET_LIST_DATA:
            return Object.assign({}, state, { ThongTinHoSoList: action.ThongTinHoSoList });
        case Constant.THONGTINHOSO_SET_LIST_META:
        return Object.assign(
            {}, 
            state, 
            { 
                meta: Object.assign(
                    {}, 
                    state.meta, 
                    action.meta
                )
            });
    default:
        return state;
    }
};
export { ThongTinHoSoReducer};