import * as Constants from './CauHinhTaiLieuConstant';
export function setData(CauHinhTaiLieuList) {
    return {
        type: Constants.CAUHINHTAILIEU_SET_LIST_DATA,
        CauHinhTaiLieuList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.CAUHINHTAILIEU_SET_LIST_META,
        meta
    };
}