import * as Constants from './ConfigsConstant';
export function setData(ConfigsList) {
    return {
        type: Constants.CONFIGS_SET_LIST_DATA,
        ConfigsList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.CONFIGS_SET_LIST_META,
        meta
    };
}