import * as Constant from './ConfigsConstant';
const Configs = {
    ConfigsList: [],
    meta: {
        page: 1,
        page_size: 15,
        sort: {id:'desc'},
        search: '' ,
        filter: {
          //isBoQuaKiemTra2:'',
        },
    }
};
const ConfigsReducer = (state = Configs, action) => {
    switch (action.type) {
        case Constant.CONFIGS_SET_LIST_DATA:
            return Object.assign({}, state, { ConfigsList: action.ConfigsList });
        case Constant.CONFIGS_SET_LIST_META:
        return Object.assign(
            {}, 
            state, 
            { 
                meta: Object.assign(
                    {}, 
                    state.meta, 
                    action.meta
                )
            });
    default:
        return state;
    }
};
export { ConfigsReducer};