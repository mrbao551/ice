import * as Constant from './HinhAnhConstant';
const HinhAnh = {
    HinhAnhList: [],
    meta: {
        page: 1,
        page_size: 15,
        sort: {id:'desc'},
        search: '' ,
        filter: {
        },
    }
};
const HinhAnhReducer = (state = HinhAnh, action) => {
    switch (action.type) {
        case Constant.HINHANH_SET_LIST_DATA:
            return Object.assign({}, state, { HinhAnhList: action.HinhAnhList });
        case Constant.HINHANH_SET_LIST_META:
        return Object.assign(
            {}, 
            state, 
            { 
                meta: Object.assign(
                    {}, 
                    state.meta, 
                    action.meta
                )
            });
    default:
        return state;
    }
};
export { HinhAnhReducer};