import * as Constants from './ThanhVienConstant';
export function setData(ThanhVienList) {
    return {
        type: Constants.THANHVIEN_SET_LIST_DATA,
        ThanhVienList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.THANHVIEN_SET_LIST_META,
        meta
    };
}

export function setDataCon(ThanhVienList_Con) {
    return {
        type: Constants.THANHVIEN_SET_LIST_DATA_CON,
        ThanhVienList_Con
    };
}
export function setMetaCon(meta_con) {
    return {
        type: Constants.THANHVIEN_SET_LIST_META_CON,
        meta_con
    };
}

export function setDataVo(ThanhVienList_Vo) {
    return {
        type: Constants.THANHVIEN_SET_LIST_DATA_VO,
        ThanhVienList_Vo
    };
}
export function setMetaVo(meta_vo) {
    return {
        type: Constants.THANHVIEN_SET_LIST_META_VO,
        meta_vo
    };
}