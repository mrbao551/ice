import * as Constants from './DanhSachDanhMucConstant';
export function setData(DanhSachDanhMucList) {
    return {
        type: Constants.DANHSACHDANHMUC_SET_LIST_DATA,
        DanhSachDanhMucList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.DANHSACHDANHMUC_SET_LIST_META,
        meta
    };
}