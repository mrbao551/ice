import * as Constant from './DanhSachDanhMucConstant';
const DanhSachDanhMuc = {
    DanhSachDanhMucList: [],
    meta: {
        page: 1,
        page_size: 15,
        sort: {id:'desc'},
        search: '' ,
        filter: {
        },
    }
};
const DanhSachDanhMucReducer = (state = DanhSachDanhMuc, action) => {
    switch (action.type) {
        case Constant.DANHSACHDANHMUC_SET_LIST_DATA:
            return Object.assign({}, state, { DanhSachDanhMucList: action.DanhSachDanhMucList });
        case Constant.DANHSACHDANHMUC_SET_LIST_META:
        return Object.assign(
            {}, 
            state, 
            { 
                meta: Object.assign(
                    {}, 
                    state.meta, 
                    action.meta
                )
            });
    default:
        return state;
    }
};
export { DanhSachDanhMucReducer};