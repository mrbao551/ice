
import * as Constants from './documentsConstant';

export function setData(documentsList) {
    return {
        type: Constants.DOCUMENTS_SET_LIST_DATA,
        documentsList
    };
}

export function setMeta(meta) {
    return {
        type: Constants.DOCUMENTS_SET_LIST_META,
        meta
    };
}