
import * as Constants from './usersConstant';

export function setData(usersList) {
    return {
        type: Constants.USERS_SET_LIST_DATA,
        usersList
    };
}

export function setMeta(meta) {
    return {
        type: Constants.USERS_SET_LIST_META,
        meta
    };
}