
import * as Constants from './permissionsConstant';

export function setData(permissionsList) {
    return {
        type: Constants.PERMISSIONS_SET_LIST_DATA,
        permissionsList
    };
}

export function setMeta(meta) {
    return {
        type: Constants.PERMISSIONS_SET_LIST_META,
        meta
    };
}