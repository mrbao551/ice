import * as Constant from './groupsConstant';

const groups = {
    groupsList: [],
    meta: {
        page: 1,
        page_size: 15,
        sort: {id:'asc'},
        search: '',
        filter: {                   
        },     
    }
};
const groupsReducer = (state = groups, action) => {
    switch (action.type) {
    case Constant.GROUPS_SET_LIST_DATA:
        return Object.assign({}, state, { groupsList: action.groupsList });
    case Constant.GROUPS_SET_LIST_META:
        return Object.assign(
            {}, 
            state, 
            { 
                meta: Object.assign(
                    {}, 
                    state.meta, 
                    action.meta
                )
            });
    default:
        return state;
    }
};
export  {groupsReducer};