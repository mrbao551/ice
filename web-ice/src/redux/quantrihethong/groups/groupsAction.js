
import * as Constants from './groupsConstant';

export function setData(groupsList) {
    return {
        type: Constants.GROUPS_SET_LIST_DATA,
        groupsList
    };
}

export function setMeta(meta) {
    return {
        type: Constants.GROUPS_SET_LIST_META,
        meta
    };
}