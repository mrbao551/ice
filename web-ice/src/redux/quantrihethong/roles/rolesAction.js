
import * as Constants from './rolesConstant';

export function setData(rolesList) {
    return {
        type: Constants.ROLES_SET_LIST_DATA,
        rolesList
    };
}

export function setMeta(meta) {
    return {
        type: Constants.ROLES_SET_LIST_META,
        meta
    };
}