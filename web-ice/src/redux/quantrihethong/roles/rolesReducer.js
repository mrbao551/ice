import * as Constant from './rolesConstant';

const roles = {
    rolesList: [],
    meta: {
        page: 1,
        page_size: 15,
        sort: {id:'asc'},
        search: '',
        filter: {                   
        },     
    }
};
const rolesReducer = (state = roles, action) => {
    switch (action.type) {
    case Constant.ROLES_SET_LIST_DATA:
        return Object.assign({}, state, { rolesList: action.rolesList });
    case Constant.ROLES_SET_LIST_META:
        return Object.assign(
            {}, 
            state, 
            { 
                meta: Object.assign(
                    {}, 
                    state.meta, 
                    action.meta
                )
            });
    default:
        return state;
    }
};
export  {rolesReducer};