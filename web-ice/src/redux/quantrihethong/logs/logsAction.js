
import * as Constants from './logsConstant';

export function setData(logsList) {
    return {
        type: Constants.LOGS_SET_LIST_DATA,
        logsList
    };
}

export function setMeta(meta) {
    return {
        type: Constants.LOGS_SET_LIST_META,
        meta
    };
}