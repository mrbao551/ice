import * as Constants from './ConfigExcelConstant';
export function setData(ConfigExcelList) {
    return {
        type: Constants.CONFIGEXCEL_SET_LIST_DATA,
        ConfigExcelList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.CONFIGEXCEL_SET_LIST_META,
        meta
    };
}
export function refreshMeta(meta) {
    return {
        type: 'refreshMeta',
        meta
    };
}