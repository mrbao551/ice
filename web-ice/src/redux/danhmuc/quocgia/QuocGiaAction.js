import * as Constants from './QuocGiaConstant';
export function setData(QuocGiaList) {
    return {
        type: Constants.QUOCGIA_SET_LIST_DATA,
        QuocGiaList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.QUOCGIA_SET_LIST_META,
        meta
    };
}