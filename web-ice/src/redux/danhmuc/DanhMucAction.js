import * as Constants from './DanhMucConstant';
export function setData(DanhMucList) {
    return {
        type: Constants.DANHMUC_SET_LIST_DATA,
        DanhMucList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.DANHMUC_SET_LIST_META,
        meta
    };
}