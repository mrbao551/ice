import * as Constants from './TrinhDoConstant';
export function setData(TrinhDoList) {
    return {
        type: Constants.TRINHDO_SET_LIST_DATA,
        TrinhDoList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.TRINHDO_SET_LIST_META,
        meta
    };
}