import * as Constants from './DongHoConstant';
export function setData(DongHoList) {
    return {
        type: Constants.DONGHO_SET_LIST_DATA,
        DongHoList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.DONGHO_SET_LIST_META,
        meta
    };
}