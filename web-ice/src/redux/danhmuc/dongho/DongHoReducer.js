import * as Constant from './DongHoConstant';
const DongHo = {
    DongHoList: [],
    meta: {
        page: 1,
        page_size: 15,
        sort: {id:'desc'},
        search: '' ,
        filter: {
        },
    }
};
const DongHoReducer = (state = DongHo, action) => {
    switch (action.type) {
        case Constant.DONGHO_SET_LIST_DATA:
            return Object.assign({}, state, { DongHoList: action.DongHoList });
        case Constant.DONGHO_SET_LIST_META:
        return Object.assign(
            {}, 
            state, 
            { 
                meta: Object.assign(
                    {}, 
                    state.meta, 
                    action.meta
                )
            });
    default:
        return state;
    }
};
export { DongHoReducer};