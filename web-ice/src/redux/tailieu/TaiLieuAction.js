import * as Constants from './TaiLieuConstant';
export function setData(TaiLieuList) {
    return {
        type: Constants.TAILIEU_SET_LIST_DATA,
        TaiLieuList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.TAILIEU_SET_LIST_META,
        meta
    };
}