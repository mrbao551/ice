import * as Constant from './TrinhDoConstant';
const TrinhDo = {
    TrinhDoList: [],
    meta: {
        page: 1,
        page_size: 15,
        sort: {id:'desc'},
        search: '' ,
        filter: {
        },
    }
};
const TrinhDoReducer = (state = TrinhDo, action) => {
    switch (action.type) {
        case Constant.TRINHDO_SET_LIST_DATA:
            return Object.assign({}, state, { TrinhDoList: action.TrinhDoList });
        case Constant.TRINHDO_SET_LIST_META:
        return Object.assign(
            {}, 
            state, 
            { 
                meta: Object.assign(
                    {}, 
                    state.meta, 
                    action.meta
                )
            });
    default:
        return state;
    }
};
export { TrinhDoReducer};