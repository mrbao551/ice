import * as Constants from './ConfigsObjectSendConstant';
export function setData(ConfigsObjectSendList) {
    return {
        type: Constants.CONFIGSOBJECTSEND_SET_LIST_DATA,
        ConfigsObjectSendList
    };
}
export function setMeta(meta) {
    return {
        type: Constants.CONFIGSOBJECTSEND_SET_LIST_META,
        meta
    };
}