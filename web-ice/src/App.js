
import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { persistor, store } from './redux/store';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { LoadingComponent, RouteGuardComponent as RouteGuard, AlertComponent, ConfirmComponent } from 'shared/components';

import { MainLayout } from './layouts/MainLayout';
import { HomeLayout } from './layouts/HomeLayout';
import OauthCallback from './modules/oauth-callback/OauthCallback';
import { LoginComponent } from './modules/login';
import { LoginSSOComponent } from './modules/loginsso';
import { UnauthorizedComponent } from './modules/Unauthorized';
import { LogoutComponent } from './modules/logout';
import ToastComponent from 'shared/components/behaviors/ToastComponent';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import hotkeys from 'hotkeys-js';

class App extends React.Component {
    componentDidMount() {
        document.addEventListener('contextmenu', this.handleContextMenu);
        hotkeys('alt+s', function (event, handler) {
            // Prevent the default refresh event under WINDOWS system
            event.preventDefault();
            if (document.getElementById("hoanthanhBtn"))
                document.getElementById("hoanthanhBtn").click();
            // console.log('alt+s');
        });
    }
    handleContextMenu = (event) => {
        event.preventDefault();
    }
    componentWillUnmount() {
        document.removeEventListener('contextmenu', this.handleContextMenu);
    }
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={<LoadingComponent loading={true} />} persistor={persistor}>
                    <Router>
                        <Switch>
                            {/* <Route path="/oauth-callback" component={OauthCallback} /> */}
                            <Route path="/login" component={LoginComponent} />
                            <Route path="/logout" component={LogoutComponent} />
                            {/* <Route path="/loginsso" component={LoginSSOComponent} />
                            <Route path="/unauthorized" component={UnauthorizedComponent} /> */}
                            {/* route này sẽ thay bằng Private Router */}
                            {/* <Route path="/admin" component={MainLayout} /> */}
                            <Route path="/" component={HomeLayout} />
                            {/* <RouteGuard path="/" exact component={HomeLayout} />                               */}
                            <RouteGuard component={MainLayout} />

                        </Switch>
                    </Router>
                    <ToastComponent />
                    <ToastContainer position="bottom-right"
                        autoClose={5000}
                        hideProgressBar={false}
                        newestOnTop={false}
                        closeOnClick
                        rtl={false}
                        pauseOnFocusLoss
                        draggable
                        pauseOnHover />
                    <AlertComponent />
                    <ConfirmComponent />
                </PersistGate>
            </Provider>
        );
    }
}
export default App;