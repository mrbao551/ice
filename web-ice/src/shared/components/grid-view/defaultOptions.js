export const defaultPageOptions = {
    page: 1,
    page_size: 15,
    total: 0,
    pageSizeOptions: [15, 30, 50, 100]
};