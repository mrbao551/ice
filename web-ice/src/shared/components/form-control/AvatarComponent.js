import React, { Component, Fragment } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { API_URL } from 'app-setting';
class AvatarComponent extends Component {
  state = {
    isOpen: false,    
    // label: this.props.label,
    // abc: this.props.datacontrolimg,
  }
  
  Openfckfinder() {
    let { datacontrolimg } = this.props;
    document.getElementById(`btnUpload-${datacontrolimg}`).setAttribute('disabled', true);
    var top = window.screen.height - 420;
    top = top > 0 ? top / 2 : 0;
    var left = window.screen.width - 900;
    left = left > 0 ? left / 2 : 0;
    var lch = window.location.origin;
    var new_window = window.open(`${lch}/tckfinder/CkFinder.aspx?hog=${API_URL}&container=${datacontrolimg}&type=nockeditor`, "Upload", "width=1100,height=600,toolbar=no,location=yes,status=no");
    var that = this;
    new_window.onbeforeunload = function () {
      document.getElementById(`btnUpload-${that.props.datacontrolimg}`).disabled = false;
    }
    window.callback = function (resultCallback) {
      if (resultCallback.url) {
        that.props.onChangeImage(resultCallback.url);
        document.getElementById(`btnUpload-${that.props.datacontrolimg}`).disabled = false;
        //document.getElementById(`btnDelete-${that.props.datacontrolimg}`).disabled = false;
      }
    };
  }
  DeleteImage() {
    let { datacontrolimg } = this.props;
    document.getElementById(`img-${datacontrolimg}`).innerHTML = '';
    document.getElementById(`btnUpload-${datacontrolimg}`).disabled = false;
    //document.getElementById(`btnDelete-${datacontrolimg}`).disabled = true;
    this.props.onChangeImage('');    
  }
  render() {
    let { datacontrolimg, src, textButton } = this.props;    
    return (      
      <div>
        <div id={`img-avatar`} >
          <div className="include-library">
            {
              src ? <img name="srcimage" width="150px" alt='' src={src}></img> : ''
            }
          </div>
        </div>
        <Fragment>
          <Button id={`btnUpload-${datacontrolimg}`} className="mr-2" variant="primary" onClick={() => { this.Openfckfinder(); }}>
            {textButton ? textButton : 'Chọn ảnh'}
          </Button>
          <Button id={`btnDelete-${datacontrolimg}`} variant="danger" onClick={() => { this.DeleteImage(); }}>
            Xóa
          </Button>
        </Fragment>
      </div>

    )
  }
}
export default AvatarComponent;