import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal, Card } from 'react-bootstrap';
import { LoadingComponent } from 'shared/components';
import { Context, _HinhAnhService } from './HinhAnhService';
import { QUOCGIA_TITLE } from 'redux/danhmuc//quocgia/QuocGiaConstant';
import { toast } from 'react-toastify';

class ThuVienAnhComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.setInputFocus = this.setInputFocus.bind(this);
        this.state = {
            data: [],
            listGroup: [],
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
        };
        this.subscriptions = {};
    }
    componentDidMount() {        

        
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    fetchData(meta) {
        this.setState({ loading: true });
        _HinhAnhService.getMany(meta).subscribe(res => {
            this.props.setData(res.data || []);          
            this.setState({ meta: newMeta, loading: false });
        }, err => {
            console.log(err);
            this.setState({ loading: false });
        });
    }
    componentDidUpdate(prevProps, prevState) {
        this.setInputFocus();
    }
    setInputFocus() {
        if (this.myInputRef)
            // eslint-disable-next-line react/no-find-dom-node
            ReactDOM.findDOMNode(this.myInputRef).focus();
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        if (this.state.id) {
            this.subscriptions['update'] = this.context.QuocGiaService.update(data, this.state.id).subscribe(() => {
                this.handleClose(true);
                this.setState({
                    loading: false
                });
                toast.success('Cập nhật thành công');
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });
        } else {
            this.subscriptions['create'] = this.context.QuocGiaService.create(data, this.state.id).subscribe(() => {
                this.handleClose(true);
                this.setState({
                    loading: false
                });
                toast.success('Thêm mới thành công');
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });
        }
    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }
    }
    onKeyPress(ev) {
        this.context.QuocGiaService.onKeyPress(ev);
    }
    computedTitle() {
        if (this.state.id) {
            if (this.state.editMode) {
                return 'Chỉnh sửa thông tin ' + this.context.QuocGiaService.lowerCaseStartChar(QUOCGIA_TITLE);
            }
            return 'Chi tiết ' + this.context.QuocGiaService.lowerCaseStartChar(QUOCGIA_TITLE);
        }
        return 'Thêm mới ' + this.context.QuocGiaService.lowerCaseStartChar(QUOCGIA_TITLE);
    }
    render() {
        return (
            <Modal size="lg" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Modal.Header closeButton>
                    <Modal.Title>{this.computedTitle()}</Modal.Title>
                </Modal.Header>
                <Modal.Body >
                    <Card>
                        <Card.Body>
                            <Form.Row>
                                <Form.Group as={Col} md="12" controlId="ma">
                                    
                                </Form.Group>
                            </Form.Row>                           
                        </Card.Body>
                    </Card>
                </Modal.Body>
                <Modal.Footer>
                    {/* {this.state.editMode ?
                        <Button type="submit">Lưu lại</Button>
                        :
                        ''
                    } */}
                    <Button variant="default" onClick={() => { this.handleClose(false); }}>
                        Đóng
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}
export { ThuVienAnhComponent };