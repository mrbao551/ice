﻿const checkContent = (strValue, LstCheck) => {
    var temp = 0;
    if (strValue && LstCheck && LstCheck.length > 0) {
        for (var i = 0; i < LstCheck.length; i++) {
            if (strValue.includes(LstCheck[i].title)) {
                temp += 1;
            }
        }
    }
    if (temp > 0) {
        return true;
    } else {
        return false;

    }
}
const markMatches = (res, lsttk) => {
    if (lsttk.length >0){
        for(var i=0;i<lsttk.length;i++){
            const req = lsttk[i].title;
            if (req) {
                res = res.replaceAll(req,`<span class="mark-color">` + req + `</span>`)
            }
        }
       
    }
  
    return res;
}
export { checkContent, markMatches }
