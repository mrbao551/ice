import { API_URL, AUTHORIZATION_BASE } from '../../app-setting';
import HttpClient from './http/httpClient';
import { jsonToUrlencoded } from './http/htppUtils';
import { loadable } from './loadable';

//utils
export { findByType, generatorGUID } from './findByType';
export { formatDateTime } from './formatDateTime';
export { jsonToUrlencoded, loadable };
export { checkContent } from './checkContent';
export { markMatches } from './checkContent';
export { } from 'shared/services';
export const http = new HttpClient({
    apiUrl: API_URL,
    authorizationBase: AUTHORIZATION_BASE

});
