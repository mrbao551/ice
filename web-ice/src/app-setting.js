export const API_URL = window.appCfg.API_URL;
export const SSO = {
    WEB_ENDPOINT_SSO: "http://localhost:3000/loginsso",
    URL_REDIRECT_SSO: "http://sso.dms.gov.vn/cas/login?service=http://localhost:3000/loginsso",
    URL_LOGOUT_SSO: "https://sso.dms.gov.vn/cas/logout?service=http://localhost:3000",
};
export const VIEW_DOCUMENT_URL = window.appCfg.API_URL + "fileuploadfolder/view";
export const VIEW_LUUTRU_URL = window.appCfg.API_URL+ "fileuploadfolder/viewluutru";
export const TCKFINDER_URL = "http://webadmin.vn/tckfinder/CkFinder.aspx?hog=http://baseapi.vn/&container=anhdaidien-tindang-null&type=ckeditor";
export const CLIENT = {
    client_id: 'EPS',
    client_secret: 'b0udcdl8k80cqiyt63uq',
};
export const AUTHORIZATION_BASE = 'Basic RVBTLklOVEVSTkFMOkl4ckFqRG9hMkZxRWxPN0loclNyVUpFTGhVY2tlUEVQVnBhZVBsU19YYXc=';

export const TYPE_TOAST = {
    SUCCESS: 'success',
    INFO: 'info',
    DANGER: 'danger',
    WARNING: 'warning',
    PRIMARY: 'primary',
    DARK: 'dark',
}

export const PermissonList = {
    NoPer: "1",
    OnlyPer: "2",
    AllPer: "3"
}
export const PrivilegeList = {
    ViewList: "01",
    ViewDetail: "02",
    ViewStatisticDetail: "03",
    ViewStatisticOverview: "04",
    Export: "05",
    ViewGarena: "06",
    Add: "07",
    Edit: "08",
    Delete: "09",
    Approved: "10",
    Hide: "11",
    Close: "12",
    TurnOff: "13",
    Import: "14",
}