import React from 'react';
import { Link } from 'react-router-dom';
import "../../node_modules/bootstrap/dist/js/bootstrap.min.js";
export default class Menu extends React.Component {
    render() {
        return (
            <div id="menu">
                <div className="container">
                    <nav className="navbar navbar-expand-lg navbar-light">
                        <a className="navbar-brand" href="https://tainanganhnguthudo.com/"><img src="https://tainanganhnguthudo.com/source/files/logo-web.png" alt="Tài năng anh ngữ thủ đô" /></a>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon" />
                        </button>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">

                            <ul className="navbar-nav ml-auto text-uppercase font-14">
                                <li className="nav-item active">
                                    <a className="nav-link main-color" href="/">
                                        <i className="fa fa-home" />
                                    </a>
                                </li>
                                <li className="nav-item ">
                                    <a className="nav-link main-color" target='_bank' href="https://tainanganhnguthudo.com/gioi-thieu-art111/">
                                        Giới thiệu
                                    </a>
                                </li>
                                <li className="nav-item ">
                                    <a className="nav-link main-color" target='_bank' href="https://tainanganhnguthudo.com/the-le-art127/">
                                        Thể lệ
                                    </a>
                                </li>
                                <li className="nav-item ">
                                    <a className="nav-link main-color" target='_bank' href="https://tainanganhnguthudo.com/#schedule">
                                        Lịch thi
                                    </a>
                                </li>
                                <li className="nav-item ">
                                    <a className="nav-link main-color" target='_bank' href="https://tainanganhnguthudo.com/tin-tuc-art113/">
                                        Tin tức
                                    </a>
                                </li>
                                <li className="nav-item ">
                                    <a className="nav-link main-color" target='_bank' href="https://tainanganhnguthudo.com/thu-vien-art114/">
                                        Thư viện
                                    </a>
                                </li>
                                <li className="nav-item ">
                                    <a className="nav-link main-color" target='_bank' href="https://tainanganhnguthudo.com/lien-he/">
                                        Liên hệ
                                    </a>
                                </li>
                                <li className="nav-item "><a className="nav-link main-color" href="/result">Lịch sử làm bài thi</a></li>
                            </ul>


                        </div>
                    </nav>
                </div>
            </div>
        );
    }
}
