import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { authService } from 'shared/services';
import { _HomeService } from 'modules/home/HomeService';
class Header extends React.Component {
    logout() {
        authService.logout();
        //window.location.href = '/login';
        if (window.location.pathname != "/login")
            window.location.href = '/login';
    }
    componentDidMount() {
        console.log('USER', this.props.user);
        _HomeService.getUserStep().subscribe(() => {

        },
            (err) => {
                this.logout();
            });


    }
    render() {
        let { data } = this.props.user;
        // console.log('USER', data);
        return (
            <>
                <header id="header">
                    <div className="container header">
                        <div className="row">
                            <div className="col-md-6 col-12 left-header">
                                <span><i className="fa fa-phone" /> 0963.499.365 / 033.905.1197</span>
                                <span className="ml-3"><i className="fa fa-envelope" /> etc.thudo@gmail.com</span>
                            </div>
                            <div className="col-md-6 col-12 right-header">
                                {/* <span className="text-white font-16"><a className target="_blank" href="https://www.youtube.com/@TheEnglishTalentContest/featured"><i style={{ "color": "#fff", "position": "relative", "top": "2px" }} className="fa fa-youtube-play" /></a> | </span> */}
                                {/* <span className="text-white font-16"><a className target="_blank" href="https://fb.com/Etcthudo"><i style={{ "color": "#fff", "position": "relative", "top": "2px" }} className="fa fa-facebook-square" /></a> | </span> */}
                                {
                                    data ?
                                        <><span className="text-white"><a style={{ color: '#fff' }} ><i className="fa fa-user" /> {data.fullname}</a></span> | <a className='text-white' href='#' onClick={this.logout}>Đăng xuất</a></> :
                                        <span className="text-white"><a style={{ color: '#fff' }} href="/login"><i className="fa fa-user" /> Đăng nhập</a></span>

                                }
                            </div>
                        </div>
                    </div>
                </header>                
            </>
        );
    }
}
Header.propTypes = {
    user: PropTypes.object
};
const mapStateToProps = (state) => {
    return {
        user: state.oauth
    };
};
const HeaderComponent = connect(mapStateToProps, {})(Header);
export { HeaderComponent };

