import React from 'react';
import { Link } from 'react-router-dom';
export default class Footer extends React.Component {
    render() {
        return (
            <div id="footer">
                <div className="container">
                    <div className="row pb-2">
                        <div className="col-md-2 pt-3 p-0 contact-info text-center ">
                            <p className="text-center">
                                <strong>ĐƠN VỊ TỔ CHỨC</strong>
                            </p>
                            <img
                                style={{ width: 90 }}
                                src="https://tainanganhnguthudo.com/public/template/frontend/images/Logo-doan.png"
                            />
                            <img
                                style={{ width: 90 }}
                                src="https://tainanganhnguthudo.com/public/template/frontend/images/logo-hoisinhvien.png"
                            />
                            <p className="mt-3">THÀNH PHỐ HÀ NỘI</p>
                        </div>
                        <div className="col-md-4 contact-info">
                            <p className="paddingTopPC" style={{ paddingTop: '40px' }}>
                                <strong>Đoàn TNCS Hồ Chí Minh - Hội Sinh viên Việt Nam</strong>
                                <br />
                                Thành phố Hà Nội
                                <br />- Địa chỉ: 14 Phan Chu Trinh, Hoàn Kiếm, Hà Nội
                                <br />-{" "}
                                <a
                                    href="https://www.facebook.com/hoisinhvienvietnamthanhphohanoi"
                                    target=""
                                >
                                    Fanpage: Fb.com/hoisinhvienvietnamthanhphohanoi
                                </a>
                                <br />- SĐT: 0339 051 197 | 0923 108 668
                            </p>
                        </div>
                        <div className="col-md-2 pt-3 contact-info text-center ">
                            <p className="text-center">
                                <strong>ĐƠN VỊ ĐỒNG HÀNH</strong>
                            </p>
                            <img
                                style={{ width: 150 }}
                                src="https://tainanganhnguthudo.com/public/template/frontend/images/Logo-corp.png"
                            />
                        </div>
                        <div className="col-md-4  contact-info">
                            <p className="paddingTopPC" style={{ paddingTop: '40px' }}>
                                <strong>Hệ thống Anh ngữ Quốc tế ICE IELTS</strong>
                                <br />- Trụ sở chính: Số 20 Đặng Thùy Trâm, Cầu Giấy, Hà Nội
                                <br />- Fanpage:{" "}
                                <a href="https://www.facebook.com/iceielts.vn" target="_blank">
                                    Fb.com/iceielts.vn
                                </a>
                                <br />- SĐT: 0963 499 365 | 1900 633 351
                            </p>
                        </div>
                    </div>

                    <p className="text-white text-right mt-4">Phát triển bởi <a className="text-white" target="_blank" href="http://hcmedia.com.vn/">HCmedia</a></p>
                </div>
            </div>
        );
    }
}
