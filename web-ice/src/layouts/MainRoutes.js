import { loadable } from 'shared/utils';
const routes = [    
    {
        path: '/test',
        component: loadable(() => import('../modules/home'))
    },
    {
        path: '/result',
        component: loadable(() => import('../modules/result'))
    }, 
    {
        path: '/',
        component: loadable(() => import('../modules/home'))
    },   
];
export { routes };