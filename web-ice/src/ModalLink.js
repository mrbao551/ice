import React, { Component, Fragment } from 'react';
import { Button, Modal } from 'react-bootstrap';
import { API_URL } from 'app-setting';
class ModalLink extends Component {
    state = {
        isOpen: false,
        // label: this.props.label,
        // abc: this.props.datacontrolimg,
    }
    Openfckfinder() {

        document.getElementById(`${this.props.idButton}`).setAttribute('disabled', true);
        var top = window.screen.height - 420;
        top = top > 0 ? top / 2 : 0;
        var left = window.screen.width - 900;
        left = left > 0 ? left / 2 : 0;
        var lch = window.location.origin;
        var new_window = window.open(`${lch}/tckfinder/CkFinder.aspx?hog=${API_URL}&container=${this.props.datacontrolimg}&type=nockeditor`, "Upload", "width=1100,height=600,toolbar=no,location=yes,status=no");
        var that = this;
        new_window.onbeforeunload = function () {
            document.getElementById(`btnUpload-${that.props.datacontrolimg}`).disabled = false;
        }
        window.callback = function (resultCallback) {
            if (resultCallback.url) {
                that.props.onChangeImage(resultCallback.url);
                document.getElementById(`btnUpload-${that.props.datacontrolimg}`).disabled = false;
            }
        };
    }
    DeleteImage() {
        document.getElementById(`img-${this.props.datacontrolimg}`).innerHTML = '';
        document.getElementById(`btnUpload-${this.props.datacontrolimg}`).disabled = false;
        this.props.onChangeImage('');
    }
    render() {
        return (

            <Fragment>
                <Button id={this.props.idButton} className="mr-2" variant="primary" onClick={() => { this.Openfckfinder(); }}>
                    Cập nhật ảnh
          </Button>
                <Button variant="danger" onClick={() => { this.DeleteImage(); }}>
                    Xóa ảnh
          </Button>
            </Fragment>
        )
    }
}
export default ModalLink;