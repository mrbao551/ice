import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import { _HomeService } from './HomeService';

import { TestComponent } from './TestComponent';
import { LoadingComponent } from 'shared/components';
// import Scrollspy from 'react-scrollspy'
class Home extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
    }
    constructor(props) {
        super(props);
        this.state = {
            meta: {},
            loading: false,
            status: false,
            data: null,
            type: 1,
            luotthi: null,
            data_user: null,
            config: null

        };
        this.subscriptions = {};
    }

    componentDidMount() {
        this.setState({ data: this.props.user.data });
        this.setState({ loading: true });
        _HomeService.getInfoUser().subscribe(res => {

            this.setState({ data_user: res, loading: false });
        });
        _HomeService.getConfig().subscribe(result => {
            this.setState({ config: result });
            _HomeService.getUserStep().subscribe(res => {
                if (res) {
                    if (parseInt(res.trang_thai) == 0) {
                        let timeLocalStorage = localStorage.getItem('test_' + this.props.user.data.uid + '_' + res.so_luot);
                        //localStorage.setItem('test_' + this.props.user.data.uid + '_' + res.so_luot, res.time_con_lai);
                        if (timeLocalStorage) {
                            localStorage.setItem('test_' + this.props.user.data.uid + '_' + res.so_luot, timeLocalStorage)
                        }
                        else {
                            localStorage.setItem('test_' + this.props.user.data.uid + '_' + res.so_luot, res.time_con_lai)
                        }
                    }
                    else {
                        localStorage.setItem('test_' + this.props.user.data.uid + '_' + (parseInt(res.so_luot) + 1), parseInt(result.thoi_gian) * 60 * 1000)
                    }
                    this.setState({ luotthi: res });
                }
                else {

                    localStorage.setItem('test_' + this.props.user.data.uid + '_1', parseInt(result.thoi_gian) * 60 * 1000)
                }
            },
                (err) => {
                    console.log(err);
                });
        });





    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    setStatus = () => {
        this.setState({ status: true });
    }


    render() {
        let { data, luotthi, data_user, imageUrl, config } = this.state;
        let user_info = (data_user && data_user.user_info) ? JSON.parse(data_user.user_info) : null;
        return (
            <div className='p-4'>
                <h5 id="headertext" className='text-center text-uppercase' style={{ fontWeight: 700, color: 'rgb(3 30 109)' }}>Cuộc thi Tài năng Anh ngữ lần thứ VII (The English Talent Contest)</h5>
                <h6 className='text-center mb-4' >(Bài thi tiếng anh gồm 70 câu trắc nghiệm, 01 bài đọc hiểu)</h6>

                {/* {
                    this.state.status ? <React.Fragment>
                        {
                            data && <TestComponent user={data} />
                        }
                    </React.Fragment> :
                        <div className='text-center p-3'>
                            {
                                data_user && config && (!luotthi || (luotthi && (luotthi.so_luot < parseInt(config.so_lan_thi) || (luotthi.so_luot == parseInt(config.so_lan_thi) && luotthi.trang_thai == 0)))) && <Button variant="primary" size="lg pl-5 pr-5" onClick={this.setStatus}>
                                    {(luotthi && luotthi.time_con_lai != 0 && luotthi.trang_thai == 0) ? luotthi.time_con_lai < (parseInt(config.thoi_gian) * 60 * 1000) ? 'Tiếp tục thi' : 'Bắt đầu thi' : 'Bắt đầu thi'}
                                </Button>
                            }

                        </div>
                } */}


                {
                    !this.state.status && data_user ? <div className='row'>
                        <div className='col-md-6 offset-md-3 border-table'>
                            {
                                data_user.class_id == 2 ?
                                    <table className='table'>
                                        <thead>
                                            <th className='text-center' colSpan={2}>Thông tin thí sinh dự thi bảng A <br /> (Học sinh Trung học phổ thông)</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><strong>Họ và tên</strong>:<br /> {data_user?.fullname}</td>
                                                <td><strong>Thành phố</strong>:<br />  {user_info?.thanh_pho}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Email</strong>:<br />  {data_user?.email}</td>
                                                <td><strong>Quận/huyện</strong>:<br />  {user_info?.quan_huyen}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Số điện thoại</strong>:<br />  {data_user?.phone}</td>
                                                <td><strong>Phường/xã</strong>:<br />  {user_info?.phuong_xa}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Ngày sinh</strong>:<br />  {data_user?.ngay_sinh}</td>
                                                <td><strong>Tên trường</strong>:<br />  {user_info?.ten_truong}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Giới tính</strong>:<br />  {data_user?.gioi_tinh}</td>
                                                <td><strong>Lớp</strong>:<br />  {user_info?.lop}</td>
                                            </tr>
                                        </tbody>
                                    </table> : <table className='table'>
                                        <thead>
                                            <th className='text-center' colSpan={2}>Thông tin thí sinh dự thi bảng B <br /> (Sinh viên các Học viện, trường Đại học,
                                                Cao đẳng)</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><strong>Họ và tên</strong>:<br /> {data_user?.fullname}</td>
                                                <td><strong>Tỉnh/thành phố</strong>:<br /> {user_info?.thanh_pho}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Email</strong>:<br /> {data_user?.email}</td>
                                                <td><strong>Học viện/trường ĐH, CĐ</strong>:<br /> {user_info?.ten_truong}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Số điện thoại</strong>:<br /> {data_user?.phone}</td>
                                                <td><strong>Khoa/viện</strong>:<br /> {user_info?.lop}</td>
                                            </tr>
                                            <tr>
                                                <td><strong>Ngày sinh</strong>:<br /> {data_user?.ngay_sinh}</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Giới tính</strong>:<br /> {data_user?.gioi_tinh}</td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                            }


                        </div>
                    </div> : ''
                }
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
            </div >
        );
    }
}

Home.propTypes = {
    user: PropTypes.object
};
const mapStateToProps = (state) => {
    return {
        user: state.oauth
    };
};
const HomeComponent = connect(mapStateToProps, {})(Home);
export { HomeComponent };