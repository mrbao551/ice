import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, Row, Col, Breadcrumb, Button, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Countdown from "react-countdown";
import { _HomeService } from './HomeService';
import { findIndex } from 'lodash';
import { LoadingComponent } from 'shared/components';
import { NopBaiComponent } from './NopBaiComponent';
import * as htmlToImage from 'html-to-image';

class TuLuanComponent extends Component {
    static propTypes = {
        user: PropTypes.object,
        data: PropTypes.object,
    }
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            tu_luan: null,
            isShow: false
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        if (this.props.data && this.props.data.tu_luan) {
            
            this.setState({
                tu_luan: this.props.data.tu_luan,
            },()=>{
                let tuluan=this.props.data.tu_luan;
                // this.convertToImage("wrap_title_ask_tu_luan", "title_ask_tu_luan");
                // this.convertToImage("wrap_content_ask_tu_luan", "content_ask_tu_luan");
                // if (tuluan.data) {                    
                //     for (let i = 0; i < tuluan.data.length; i++) {
                //         this.convertToImage("wrap_content_ask_" + tuluan.data[i].ask_id, "content_ask_" + tuluan.data[i].ask_id);
                //         if(tuluan.data[i].answers)
                //         {
                //             for (let j = 0; j < tuluan.data[i].answers.length; j++) {                                
                //                 this.convertToImage("wrap_content_answer_"+tuluan.data[i].answers[j].id, "content_answer_"+tuluan.data[i].answers[j].id);                                
                //             }
                //         }                        
                //     }
                // }
            });
        }
    }
    convertToImage(wrapid, selector) {
        debugger;
        let note = document.getElementById(selector);
        note.style.display = 'block';
        const wrap = document.getElementById(wrapid);
        htmlToImage.toPng(note)
            .then(function (dataUrl) {
                const img = new Image();
                img.src = dataUrl;
                if (wrap) {
                    wrap.replaceChildren();
                }
                note.style.display = 'none';
                wrap.appendChild(img); // id của phần tử chứa hình ảnh
            })
            .catch(function (error) {
                console.error('Chuyển đổi sang hình ảnh thất bại: ', error);
            });
    }
    renderer = ({ hours, minutes, seconds, completed }) => {
        if (completed) {
            this.setState({ isShow: true });
            return "Hết giờ";
        } else {
            let timer = (hours * 60 * 60 + minutes * 60 + seconds) * 1000;
            localStorage.setItem('test_' + this.props.user.uid + '_' + this.props.data.so_luot, timer);
            //Viêt thêm Update vào db
            let time_save = 10 * 1000;//10 giây lưu lại 1 lần
            if (timer > 0 && timer % time_save == 0) {
                this.saveTimer(timer, 0);
            }
            const formatNumber = num => String(num).padStart(2, '0');
            return `${formatNumber(hours)}:${formatNumber(minutes)}:${formatNumber(seconds)}`;

        }

    };
    saveTimer(timer, status) {
        let { data } = this.props;
        if (data && data.id && data.id > 0) {
            _HomeService.saveLuotTest({ id: data.id, time_con_lai: timer, trang_thai: status }).subscribe(() => {
            },
                (err) => {
                    console.log(err);
                });
        }
    };
    handleClose(res) {
        this.setState({ isShow: false });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    LoadQuestion(id) {
        return false;
    }
    ChooseAnswer(id, ask_id) {
        let { tu_luan } = this.state;
        let index = findIndex(tu_luan.data, function (o) { return o.ask_id === ask_id; });
        if (index > -1) {
            let indexAnswer = findIndex(tu_luan.data[index].answers, function (o) { return o.id === id; });
            if (indexAnswer > -1) {
                tu_luan.data[index].reply_id = id;
                //Code cập nhật kết quả vào DB
                this.setState({
                    loading: true
                });
                _HomeService.chooseAnswer({ id: tu_luan.data[index].id, reply_id: id }).subscribe(() => {
                    this.setState({
                        loading: false,
                        tu_luan,
                    });
                },
                    (err) => {
                        this.setState({
                            loading: false
                        });
                        toast.error(err.error);
                    });
            }
        }
    }
    onSubmit() {
        this.setState({ isShow: true });

    }

    render() {
        let { tu_luan } = this.state;
        let soLuongDaTraLoi = tu_luan && tu_luan.data ? tu_luan.data.filter(x => (x.reply_id && parseInt(x.reply_id) > 0)) : 0;
        let countDaTraLoi = soLuongDaTraLoi != null ? soLuongDaTraLoi.length : 0;
        // console.log('TULUAN', tu_luan);
        return (
            (this.props.user && this.props.data) ? <div role="row" className="ant-row question-content row">
                <div role="cell" className="ant-col ant-col-18 col-md-9">
                    {
                        tu_luan && <div className="question-list">
                            <div id="wrap_title_ask_tu_luan"></div>
                            <p id="title_ask_tu_luan" dangerouslySetInnerHTML={{ __html: tu_luan.title }} style={{ fontWeight: 600 }}></p>                            
                            <div id="wrap_content_ask_tu_luan" ></div>
                            <div id="content_ask_tu_luan"  className='content-tu-luan' dangerouslySetInnerHTML={{ __html: tu_luan.content }}></div>
                            <div className='list-ask-tu-luan'>
                                {
                                    tu_luan.data && tu_luan.data.map((itemask, index) => {
                                        return (<div key={index}>
                                            <div className="question-info" id={1}><b style={{ fontSize: '20px', color: 'rgb(46, 102, 173)' }}>Câu {index + 1} ({`${itemask.diem_ask} điểm`})</b>:
                                                <span  id={`content_ask_${itemask.ask_id}`} className="title-exam" dangerouslySetInnerHTML={{ __html: itemask.content }}></span>
                                                <div id={`wrap_content_ask_${itemask.ask_id}`}></div>
                                            </div>

                                            {
                                                itemask.answers &&
                                                <div className="content-answer-question">
                                                    <ul>
                                                        {
                                                            itemask.answers && itemask.answers.map((item, index) => {
                                                                return (<li key={index} id={`answer_${item.id}`} onClick={() => this.ChooseAnswer(item.id, itemask.ask_id)} className={`item ${itemask.reply_id && item.id == itemask.reply_id ? 'active' : ''}`}>
                                                                    <div className="answer">
                                                                        <span className="answer-label">{item.title}</span>
                                                                        {/* <span className="answer-label">{(index + 1 + 9).toString(36).toUpperCase()}</span> */}
                                                                        <div  id={`content_answer_${item.id}`} className="answer-content" dangerouslySetInnerHTML={{ __html: item.content }}></div>
                                                                        <div id={`wrap_content_answer_${item.id}`}></div>
                                                                    </div>
                                                                </li>);
                                                            })
                                                        }
                                                    </ul>
                                                </div>
                                            }
                                        </div>);
                                    })
                                }
                            </div>


                        </div>
                    }

                </div>
                <div role="cell" className="ant-col ant-col-6 col-md-3 mt-4">

                    <p className="text-center mb-2">
                        <button style={{ width: '100%' }} className="btn-onclick submit-exam" onClick={() => this.onSubmit()}><b>Nộp bài</b>
                        </button>

                    </p>

                    <div className="exam-right-content" style={{ position: 'sticky', top: '0px' }}>
                        <div className="topbar-exam mt-2">
                            <p className="mg-0"><b style={{ fontSize: '16px' }}>Thời gian </b><b style={{ color: 'rgb(255, 255, 255)', fontSize: '20px' }}>
                                <Countdown date={Date.now() + parseInt(localStorage.getItem('test_' + this.props.user.uid + '_' + this.props.data.so_luot))}
                                    renderer={this.renderer}>
                                    <span>Hết giờ</span>
                                </Countdown>

                            </b>
                            </p>
                            <p className="mg-0"><b style={{ fontSize: '16px' }}>Số câu đã làm</b><b style={{ color: 'rgb(255, 255, 255)', fontSize: '24px' }}><span style={{ color: 'rgb(55, 54, 54)' }}>{countDaTraLoi}/{(tu_luan && tu_luan.data) ? tu_luan.data.length : 0}</span></b>
                            </p>
                        </div>
                        <div className="exam-right-info">
                            <p className="mg-0 color-blue text-center title-list-q"><b>Câu hỏi</b>
                            </p>
                            <ul>
                                {
                                    tu_luan && tu_luan.data && tu_luan.data.map((item, index) => {
                                        return (<li key={index} className={`item ${item.reply_id && parseInt(item.reply_id) > 0 ? 'active' : ''}`}><a onClick={() => this.LoadQuestion(item.ask_id)}>{index + 1}</a>
                                        </li>);
                                    })
                                }
                            </ul>
                        </div>

                    </div>
                </div>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <NopBaiComponent onClose={this.handleClose.bind(this)}
                    timer={parseInt(localStorage.getItem('test_' + this.props.user.uid + '_' + this.props.data.so_luot))}
                    lsName={'test_' + this.props.user.uid + '_' + this.props.data.so_luot}
                    data={this.props.data}
                    isShow={this.state.isShow} />
            </div> : ''
        );
    }
}

export { TuLuanComponent };