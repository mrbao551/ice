import { createContext } from 'react';
import { BaseService } from 'shared/services';
import { Subject } from 'rxjs';
import { http } from 'shared/utils';
import { map } from 'rxjs/operators';
class HomeService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/home' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }

    getDanhSachCauHoi() {
        return http.get('api/tests').pipe(map((res) => {
            return res;
        }));
    }
    chooseAnswer(obj) {
        return http.put(`api/tests/${obj.id}/${obj.reply_id}`);
    }
    getUserStep() {
        return http.get('api/user-step').pipe(map((res) => {
            return res;
        }));
    }
    getInfoUser() {
        return http.get('api/get-user').pipe(map((res) => {
            return res;
        }));
    }
    saveLuotTest(obj) {
        return http.put(`api/update-luot-thi`, obj);
    }
    finishTest(obj) {
        return http.put(`api/submit-luot-thi`, obj);
    }
    getConfig(){
        return http.get('api/getconfig').pipe(map((res) => {
            return res;
        }));
    }

}
const _HomeService = new HomeService();
const Context = createContext();
export { Context, _HomeService, HomeService };