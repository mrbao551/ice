import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, Row, Col, Breadcrumb, Button, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Countdown from "react-countdown";
import { _HomeService } from './HomeService';
import { findIndex } from 'lodash';
import { LoadingComponent } from 'shared/components';
import { NopBaiComponent } from './NopBaiComponent';
import * as htmlToImage from 'html-to-image';

class TracNghiemComponent extends Component {
    static propTypes = {
        user: PropTypes.object,
        data: PropTypes.object,
    }
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            dataQuestions: [],
            questionActive: null,
            indexActive: 0,
            isShow: false
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        if (this.props.data) {
            let questionActive = this.props.data.trac_nghiem[0];
            this.setState({
                dataQuestions: this.props.data.trac_nghiem,
                questionActive,
                indexActive: 0
            }
                , () => {

                    // if (questionActive) {
                    //     this.convertToImage("wrap_content_ask_" + questionActive.ask_id, "content_ask_" + questionActive.ask_id);
                    //     for (let i = 0; i < questionActive.answers.length; i++) {
                    //         this.convertToImage("wrap_content_answer_" + questionActive.answers[i].id, "content_answer_" + questionActive.answers[i].id);
                    //     }
                    // }
                }
            );
        }
    }
    convertToImage(wrapid, selector) {
        let note = document.getElementById(selector);
        note.style.display = 'block';
        const wrap = document.getElementById(wrapid);
        htmlToImage.toPng(note)
            .then(function (dataUrl) {
                const img = new Image();
                img.src = dataUrl;
                if (wrap) {
                    wrap.replaceChildren();
                }
                note.style.display = 'none';
                wrap.appendChild(img); // id của phần tử chứa hình ảnh
            })
            .catch(function (error) {
                console.error('Chuyển đổi sang hình ảnh thất bại: ', error);
            });
    }
    renderer = ({ hours, minutes, seconds, completed }) => {
        if (completed) {
            this.setState({ isShow: true });
            return "Hết giờ";
        } else {
            let timer = (hours * 60 * 60 + minutes * 60 + seconds) * 1000;
            localStorage.setItem('test_' + this.props.user.uid + '_' + this.props.data.so_luot, timer);
            //Viêt thêm Update vào db
            let time_save = 10 * 1000;//10 giây lưu lại 1 lần
            if (timer > 0 && timer % time_save == 0) {
                this.saveTimer(timer, 0);
            }
            const formatNumber = num => String(num).padStart(2, '0');
            return `${formatNumber(hours)}:${formatNumber(minutes)}:${formatNumber(seconds)}`;

        }

    };
    saveTimer(timer, status) {
        let { data } = this.props;
        if (data && data.id && data.id > 0) {
            _HomeService.saveLuotTest({ id: data.id, time_con_lai: timer, trang_thai: status }).subscribe(() => {
            },
                (err) => {
                    console.log(err);
                });
        }
    };
    handleClose(res) {
        this.setState({ isShow: false });
    }

    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    LoadQuestion(id) {
        let { dataQuestions, questionActive } = this.state;
        if (id == questionActive.ask_id)
            return false;
        let index = findIndex(dataQuestions, function (o) { return o.ask_id === id; });
        if (index > -1) {
            questionActive = dataQuestions[index];
            this.setState({ questionActive: questionActive, indexActive: index }, () => {
                // if (questionActive) {
                //     this.convertToImage("wrap_content_ask_" + questionActive.ask_id, "content_ask_" + questionActive.ask_id);
                //     for (let i = 0; i < questionActive.answers.length; i++) {
                //         this.convertToImage("wrap_content_answer_" + questionActive.answers[i].id, "content_answer_" + questionActive.answers[i].id);
                //     }
                // }
            });
        }
    }
    ChooseAnswer(id) {
        let { dataQuestions, questionActive } = this.state;

        let index = findIndex(dataQuestions, function (o) { return o.ask_id === questionActive.ask_id; });
        let indexAnswer = findIndex(questionActive.answers, function (o) { return o.id === id; });
        if (indexAnswer > -1) {
            questionActive.reply_id = questionActive.answers[indexAnswer].id;

        }
        if (index > -1) {
            dataQuestions[index] = questionActive;
        }
        //Code cập nhật kết quả vào DB
        this.setState({
            loading: true
        });
        _HomeService.chooseAnswer({ id: questionActive.id, reply_id: id }).subscribe(() => {
            this.setState({
                loading: false,
                questionActive,
                dataQuestions
            });
        },
            (err) => {
                this.setState({
                    loading: false
                });
                toast.error(err.error);
            });
    }
    next() {
        let { dataQuestions, indexActive, questionActive } = this.state;
        if (indexActive > -1) {
            indexActive = indexActive + 1;
            questionActive = dataQuestions[indexActive];
            this.setState({ questionActive: questionActive, indexActive: indexActive }, () => {
                // if (questionActive) {
                //     this.convertToImage("wrap_content_ask_" + questionActive.ask_id, "content_ask_" + questionActive.ask_id);
                //     for (let i = 0; i < questionActive.answers.length; i++) {
                //         this.convertToImage("wrap_content_answer_" + questionActive.answers[i].id, "content_answer_" + questionActive.answers[i].id);
                //     }
                // }
            });
        };
    }

    back() {
        let { dataQuestions, indexActive, questionActive } = this.state;
        if (indexActive > -1) {
            indexActive = indexActive - 1;
            questionActive = dataQuestions[indexActive];
            this.setState({ questionActive: questionActive, indexActive: indexActive }, () => {
                // if (questionActive) {
                //     this.convertToImage("wrap_content_ask_" + questionActive.ask_id, "content_ask_" + questionActive.ask_id);
                //     for (let i = 0; i < questionActive.answers.length; i++) {
                //         this.convertToImage("wrap_content_answer_" + questionActive.answers[i].id, "content_answer_" + questionActive.answers[i].id);
                //     }
                // }
            });
        }
    }
    onSubmit() {
        this.setState({ isShow: true });
    }


    renderQuestion(questionActive) {
        if (questionActive) {
            return (
                <div className="question-list">
                    <div className="question-info"><b style={{ fontSize: '22px', color: 'rgb(46, 102, 173)' }}>Câu {questionActive.stt} ({`${questionActive.diem_ask} điểm`})</b>
                        <ul className="action-links" />
                    </div>
                    <div id={`wrap_content_ask_${questionActive.ask_id}`}>
                    </div>
                    <div id={`content_ask_${questionActive.ask_id}`} className="title-exam content_ask" dangerouslySetInnerHTML={{ __html: questionActive.content }}>
                    </div>
                    {
                        questionActive.answers &&
                        <div className="content-answer-question">
                            <ul>
                                {
                                    questionActive.answers && questionActive.answers.map((item, index) => {
                                        return (<li key={index} id={`answer_${item.id}`} onClick={() => this.ChooseAnswer(item.id)} className={`item ${questionActive.reply_id && item.id == questionActive.reply_id ? 'active' : ''}`}>
                                            <div className="answer">
                                                <span className="answer-label">{item.title}</span>
                                                {/* <span className="answer-label">{(index + 1 + 9).toString(36).toUpperCase()}</span> */}
                                                <div id={`wrap_content_answer_${item.id}`}></div>
                                                <div id={`content_answer_${item.id}`} className="answer-content" dangerouslySetInnerHTML={{ __html: item.content }}></div>
                                            </div>
                                        </li>);
                                    })
                                }
                            </ul>
                        </div>
                    }

                </div>
            )
        }
        return null;

    }

    render() {
        let { dataQuestions, questionActive, indexActive } = this.state;
        let soLuongDaTraLoi = dataQuestions.filter(x => (x.reply_id && parseInt(x.reply_id) > 0));
        let countDaTraLoi = soLuongDaTraLoi != null ? soLuongDaTraLoi.length : 0;

        return (
            (this.props.user && this.props.data) ? <div role="row" className="ant-row question-content row">
                <div role="cell" className="ant-col ant-col-18 col-md-9">
                    {
                        this.renderQuestion(questionActive)
                    }
                    {/* {
                        this.convertQuestionToImage(questionActive)
                    } */}

                    <div className="d-flex">
                        {
                            indexActive != 0 && <Button onClick={() => { this.back() }} variant="success">Quay lại</Button>
                        }
                        {
                            indexActive != dataQuestions.length - 1 && <Button onClick={() => { this.next() }} className="ml-auto" variant="success">Tiếp theo</Button>
                        }

                    </div>

                </div>
                <div role="cell" className="ant-col ant-col-6 col-md-3 mt-4">
                    <div className="exam-right-content" style={{ position: 'sticky', top: '0px' }}>
                        <p className="text-center mb-2">
                            <button style={{ width: '100%' }} className="btn-onclick submit-exam" onClick={() => this.onSubmit()}><b>Nộp bài</b>
                            </button>

                        </p>

                        <div className="topbar-exam mt-2">
                            <p className="mg-0"><b style={{ fontSize: '16px' }}>Thời gian </b><b style={{ color: 'rgb(255, 255, 255)', fontSize: '20px' }}>
                                <Countdown date={Date.now() + parseInt(localStorage.getItem('test_' + this.props.user.uid + '_' + this.props.data.so_luot))}
                                    renderer={this.renderer}>
                                    <span>Hết giờ</span>
                                </Countdown>

                            </b>
                            </p>
                            <p className="mg-0"><b style={{ fontSize: '16px' }}>Số câu đã làm</b><b style={{ color: 'rgb(255, 255, 255)', fontSize: '24px' }}><span style={{ color: 'rgb(55, 54, 54)' }}>{countDaTraLoi}/{dataQuestions.length}</span></b>
                            </p>
                        </div>

                        <div className="exam-right-info">
                            <p className="mg-0 color-blue text-center title-list-q"><b>Câu hỏi</b>
                            </p>
                            <ul>
                                {
                                    dataQuestions && dataQuestions.map((item, index) => {
                                        return (<li key={index} className={`item ${item.reply_id && parseInt(item.reply_id) > 0 ? 'active' : ''}`}><a onClick={() => this.LoadQuestion(item.ask_id)}>{item.stt}</a>
                                        </li>);
                                    })
                                }

                            </ul>
                        </div>

                    </div>
                </div>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <NopBaiComponent onClose={this.handleClose.bind(this)}
                    timer={parseInt(localStorage.getItem('test_' + this.props.user.uid + '_' + this.props.data.so_luot))}
                    lsName={'test_' + this.props.user.uid + '_' + this.props.data.so_luot}
                    data={this.props.data}
                    isShow={this.state.isShow} />
            </div> : ''
        );
    }
}

export { TracNghiemComponent };