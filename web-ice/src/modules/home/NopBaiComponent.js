import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal } from 'react-bootstrap';
import { _HomeService } from './HomeService';
import { LoadingComponent } from 'shared/components';

class NopBaiComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func,
        isShow: PropTypes.bool,
        timer: PropTypes.number,
        lsName:PropTypes.string,
        data:PropTypes.object,
    };
    constructor(props) {
        super(props);
        this.state = {
            isShow: props.isShow || false,
            loading:false
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.setState({ isShow: this.props.isShow });

    }
    componentDidUpdate(prevProps, prevState) {
        if (prevProps.isShow !== this.props.isShow) {
            this.setState({ isShow: this.props.isShow });
        }
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }
    }
    onSubmit() {
        localStorage.removeItem(this.props.lsName);
        this.finishTest();
    }
    finishTest() {
        let { data } = this.props;
        if (data && data.id && data.id > 0) {
            this.setState({loading:true});
            _HomeService.finishTest({ id: data.id }).subscribe(() => {
                this.setState({loading:false});
                window.location.href="/result/"+data.id
            },
                (err) => {
                    this.setState({loading:false});
                    console.log(err);
                });
        }
    };
    render() {
        // console.log(this.props.timer);
        return (
            <Modal keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <Modal.Body >
                    <div className='text-center'><h3>Xác nhận nộp bài thi</h3></div>
                    <div className='text-center mt-5'>
                        {
                            this.props.timer && this.props.timer > 1000 && <Button variant="secondary" onClick={() => { this.handleClose(false); }}>
                                Tiếp tục làm bài
                            </Button>
                        }
                        <Button onClick={() => { this.onSubmit(); }} className='ml-2' variant="primary" >Nộp bài</Button>
                    </div>
                    <LoadingComponent loading={this.state.loading}></LoadingComponent>

                </Modal.Body>
            </Modal>
        );
    }
}
export { NopBaiComponent };