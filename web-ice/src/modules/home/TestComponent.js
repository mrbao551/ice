import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, Row, Col, Breadcrumb, Button, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Countdown from "react-countdown";
import { _HomeService } from './HomeService';
import { findIndex } from 'lodash';
import { LoadingComponent } from 'shared/components';
import { TracNghiemComponent } from './TracNghiemComponent';
import { TuLuanComponent } from './TuLuanComponent';

class TestComponent extends Component {
    static propTypes = {
        user: PropTypes.object
    }
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            type: 1,
            data: null,
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.setState({ loading: true });
        _HomeService.getDanhSachCauHoi().subscribe(res => {
            this.setState({ loading: false, data: res });
        },
        (err) => {
            this.setState({loading:false});
            console.log(err);
        });

    }

    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    changeType(type) {
        this.setState({ type });
    }

    render() {
        let { type, data } = this.state;
        return (
            <React.Fragment>
                <div className='text-center'>
                    <Button variant={type == 1 ? 'success' : 'outline-secondary'} size="lg" className='mr-3' onClick={() => this.changeType(1)}>
                        Trắc nghiệm
                    </Button>
                    <Button variant={type == 2 ? 'success' : 'outline-secondary'} size="lg" onClick={() => this.changeType(2)}>
                        Bài đọc
                    </Button>
                </div>
                {
                    (type == 1 && data && this.props.user) ? <TracNghiemComponent data={data} user={this.props.user} /> : ''
                }
                {

                    (type == 2 && data && this.props.user) ? <TuLuanComponent data={data} user={this.props.user} /> : ''
                }
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
            </React.Fragment>
        );
    }
}

export { TestComponent };