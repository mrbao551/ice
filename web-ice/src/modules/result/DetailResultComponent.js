import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button } from 'react-bootstrap';
import { _HomeService } from './HomeService';
import { LoadingComponent } from 'shared/components';
import { TracNghiemComponent } from './TracNghiemComponent';
import { TuLuanComponent } from './TuLuanComponent';
// import Scrollspy from 'react-scrollspy'

const timer = 60 * 60 * 1000;
class DetailResult extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        user: PropTypes.object
    }
    constructor(props) {
        super(props);
        this.state = {
            meta: {},
            loading: false,
            status: false,
            type: 1,
            data: null,
            luotthi: null,

        };
        this.subscriptions = {};
    }
    componentDidMount() {
        let id = this.props.match.params.id;
        if (id) {
            this.setState({ loading: true });
            _HomeService.getDetailResult(id).subscribe(res => {
                this.setState({ data: res, loading: false });
            },
                (err) => {
                    this.setState({ loading: false });
                    console.log(err);
                });
        }

    }


    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }

    changeType(type) {
        this.setState({ type });
    }

    render() {
        let { data, type } = this.state;
        return (
            <React.Fragment>
                {
                    data ? <React.Fragment>
                        <div role="cell" className="col-md-12 pt-5">
                            <div className='text-center' style={{ textTransform: 'uppercase', fontSize: 18, color: '#031e6d' }}>
                                Chúc mừng bạn đã hoàn thành đề thi tài năng anh ngữ năm 2023
                            </div>
                            <div className='mt-4 pt-2 pb-2 row' style={{ fontSize: 17, border: '1px solid orange', width: '400px', borderRadius: '8px', margin: 'auto', textAlign: 'center', justifyContent: 'space-evenly', alignItems: 'center' }}>
                                <div className='col-md-3'>
                                    <div>ĐIỂM SỐ</div>
                                    <div style={{ fontSize: 22, color: 'red', fontWeight: 'bold' }}>{`${data?.diem}/${data?.tong_diem}`}</div>
                                </div>
                                <div className='col-md-9 text-left'>
                                    <ul className='pl-3 m-0'>
                                        <li className='d-flex'><label>Thời gian làm:</label> <span className='ml-auto' style={{ fontWeight: 'bold' }}>{data?.con_lai}</span></li>
                                        <li className='d-flex'><label>Trắc nghiệm đúng:</label> <span className='ml-auto' style={{ fontWeight: 'bold' }}>{`${data?.count_trac_nghiem_true}/${data?.trac_nghiem.length}`}</span></li>
                                        <li className='d-flex'><label>Bài viết đúng:</label> <span className='ml-auto' style={{ fontWeight: 'bold' }}>{`${data?.count_tu_luan_true}/${data?.tu_luan.data.length}`}</span></li>
                                    </ul>
                                </div>                                
                            </div>
                        </div>

                        <div role="cell" className="col-md-12 mt-5">
                            <div className='text-center'>
                                <Button variant={type == 1 ? 'success' : 'outline-secondary'} size="lg" className='mr-3' onClick={() => this.changeType(1)}>
                                    Trắc nghiệm
                                </Button>
                                <Button variant={type == 2 ? 'success' : 'outline-secondary'} size="lg" onClick={() => this.changeType(2)}>
                                    Bài đọc
                                </Button>
                            </div>
                        </div>


                        {
                            (type == 1 && data) ? <TracNghiemComponent data={data} /> : ''
                        }
                        {

                            (type == 2 && data) ? <TuLuanComponent data={data} /> : ''
                        }
                    </React.Fragment> : ''
                }
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
            </React.Fragment>
        );
    }
}

DetailResult.propTypes = {
    user: PropTypes.object
};
const mapStateToProps = (state) => {
    return {
        user: state.oauth
    };
};
const DetailResultComponent = connect(mapStateToProps, {})(DetailResult);
export { DetailResultComponent };