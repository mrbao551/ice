import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, Row, Col, Breadcrumb, Button, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Countdown from "react-countdown";
import { _HomeService } from './HomeService';
import { findIndex } from 'lodash';
import { LoadingComponent } from 'shared/components';

class TracNghiemComponent extends Component {
    static propTypes = {
        data: PropTypes.object,
    }
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            dataQuestions: null,
            questionActive: null,
            indexActive: 0,
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        if (this.props.data) {
            this.setState({
                dataQuestions: this.props.data.trac_nghiem,
                questionActive: this.props.data.trac_nghiem[0],
                indexActive: 0
            });
        }

    }
    componentWillUnmount() {

    }
    next() {
        let { dataQuestions, indexActive } = this.state;
        if (indexActive > -1) {
            indexActive = indexActive + 1;
            this.setState({ questionActive: dataQuestions[indexActive], indexActive: indexActive });
        }
    }
    back() {
        let { dataQuestions, indexActive } = this.state;
        if (indexActive > -1) {
            indexActive = indexActive - 1;
            this.setState({ questionActive: dataQuestions[indexActive], indexActive: indexActive });
        }
    }
    LoadQuestion(id) {
        let { dataQuestions, questionActive } = this.state;
        if (id == questionActive.ask_id)
            return false;
        let index = findIndex(dataQuestions, function (o) { return o.ask_id === id; });
        if (index > -1) {
            this.setState({ questionActive: dataQuestions[index], indexActive: index });
        }
    }
    getTitleAnswerCorect(item) {
        if (item) {
            let anser_corect = item.answers.find(x => x.id == item.reply_id_true);
            if (anser_corect)
                return anser_corect.title;

        }
        return null;
    }
    render() {
        let { dataQuestions, questionActive, indexActive } = this.state;
        console.log('indexActive', indexActive);
        return (
            this.props.data &&
            <div role="row" className="ant-row question-content row">
                <div role="cell" className="ant-col ant-col-18 col-md-9 mb-5">
                    {
                        questionActive && <div className="question-list">
                            <div>
                                <div className="question-info pl-3" id={1}>
                                    <b style={{ fontSize: 22, color: "rgb(46, 102, 173)" }}>Câu {questionActive.stt} <span style={{ color: '#000', fontSize: '16px' }}>({questionActive.diem_ask} điểm)</span></b>
                                </div>
                                <div
                                    className="title-exam pt-2 pl-3 pr-3 pb-2"
                                    style={{ fontWeight: "bold", fontSize: 16 }}
                                    dangerouslySetInnerHTML={{ __html: questionActive.content }}
                                >
                                </div>

                                <div className="content-answer-question-detail mb-4">
                                    {
                                        questionActive.answers &&
                                        <div className="content-answer-question">
                                            <ul>
                                                {
                                                    questionActive.answers && questionActive.answers.map((item, index) => {
                                                        return (<li key={index} id={`answer_${item.id}`} className={`item 
                                                            ${questionActive.reply_id == 0 ? '' :
                                                                questionActive.reply_id == questionActive.reply_id_true ?
                                                                    questionActive.reply_id == item.id ? 'active-true' : '' :
                                                                    questionActive.reply_id == item.id ? 'active-false' : ''
                                                            }`}>
                                                            <div className="answer">
                                                                <span className="answer-label">{item.title}</span>

                                                                <div className="answer-content" dangerouslySetInnerHTML={{ __html: item.content }}></div>
                                                            </div>
                                                        </li>);
                                                    })
                                                }
                                            </ul><hr />
                                            <div className='ml-5'>Đáp án đúng: <b>{this.getTitleAnswerCorect(questionActive)}</b></div>
                                            {
                                                questionActive.giai_thich&&
                                                <div className='ml-5'>{questionActive.giai_thich}</div>
                                            }

                                            
                                        </div>
                                    }

                                </div>
                            </div>

                        </div>
                    }
                    <div className="d-flex">
                        {
                            !indexActive == 0 ? <Button onClick={() => { this.back() }} variant="success">Quay lại</Button> : ''
                        }
                        {
                            (dataQuestions && indexActive != dataQuestions.length - 1) ? <Button onClick={() => { this.next() }} className="ml-auto" variant="success">Tiếp theo</Button> : ''
                        }
                    </div>

                </div>
                <div role="cell" className="ant-col ant-col-6 col-md-3">
                    <div className="exam-right-content" style={{ position: "sticky", top: 0 }}>
                        <div className="topbar-exam-detail">
                            <ul className='pl-0'>
                                <li className="item active-true d-flex align-items-center"><a></a> <span>Câu trả lời đúng</span></li>
                                <li className="item active-false d-flex align-items-center"><a></a> <span>Câu trả lời sai</span></li>
                                <li className="item d-flex align-items-center"><a></a> <span>Chưa trả lời</span></li>
                            </ul>

                        </div>
                        <div className="exam-right-info">
                            <p className="mg-0 color-blue text-center title-list-q">
                                <b>Câu hỏi</b>
                            </p>
                            <ul>
                                {
                                    dataQuestions && dataQuestions.map((item, index) => {
                                        return (<li key={index} className={`item ${item.reply_id && parseInt(item.reply_id) > 0 ? item.reply_id == item.reply_id_true ? 'active-true' : 'active-false' : ''}`}><a onClick={() => this.LoadQuestion(item.ask_id)}>{item.stt}</a>
                                        </li>);
                                    })
                                }

                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export { TracNghiemComponent };