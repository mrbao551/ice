import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, Row, Col, Breadcrumb, Button, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Countdown from "react-countdown";
import { _HomeService } from './HomeService';
import { findIndex } from 'lodash';
import { LoadingComponent } from 'shared/components';

class TuLuanComponent extends Component {
    static propTypes = {
        data: PropTypes.object,
    }
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: null,
            tu_luan: null,
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        if (this.props.data && this.props.data.tu_luan) {
            this.setState({
                tu_luan: this.props.data.tu_luan,
            });
        }


    }

    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    getTitleAnswerCorect(item) {
        if (item) {
            let anser_corect = item.answers.find(x => x.id == item.reply_id_true);
            if (anser_corect)
                return anser_corect.title;

        }
        return null;
    }
    render() {
        let { tu_luan } = this.state;

        return (
            tu_luan &&
            <div role="row" className="ant-row question-content row">
                <div role="cell" className="ant-col ant-col-18 col-md-9 mb-5">

                    <div className="question-list">
                        <div className="title-tu-luan" dangerouslySetInnerHTML={{ __html: tu_luan.title }}>
                        </div>
                        <div className="content-tu-luan" dangerouslySetInnerHTML={{ __html: tu_luan.content }}>
                        </div>
                        <div className="list-ask-tu-luan">
                            {
                                tu_luan.data && tu_luan.data.map((itemask, index) => {
                                    return (<div key={index}>
                                        <div className="question-info" id={1}><b style={{ fontSize: '20px', color: 'rgb(46, 102, 173)' }}>Câu {index + 1} ({`${itemask.diem_ask} điểm`})</b>:
                                            <span className="title-exam" dangerouslySetInnerHTML={{ __html: itemask.content }}></span>
                                        </div>

                                        {
                                            itemask.answers &&
                                            <div className="content-answer-question mb-4">
                                                <ul>
                                                    {
                                                        itemask.answers && itemask.answers.map((item, index) => {
                                                            return (<li key={index} id={`answer_${item.id}`} className={`item 
                                                                ${itemask.reply_id == 0 ? '' :
                                                                    itemask.reply_id == itemask.reply_id_true ?
                                                                        item.id == itemask.reply_id ? 'active-true' : '' :
                                                                        item.id == itemask.reply_id ? 'active-false' : ''
                                                                }`}>
                                                                <div className="answer">
                                                                    <span className="answer-label">{item.title}</span>
                                                                    <div className="answer-content" dangerouslySetInnerHTML={{ __html: item.content }}></div>
                                                                </div>
                                                            </li>);
                                                        })
                                                    }
                                                </ul>
                                                <hr />
                                                <div className='ml-5'>Đáp án đúng: <b>{this.getTitleAnswerCorect(itemask)}</b></div>
                                                {
                                                    itemask.giai_thich &&
                                                    <div className='ml-5'>{itemask.giai_thich}</div>
                                                }
                                            </div>
                                        }
                                    </div>);
                                })
                            }
                        </div>
                    </div>
                </div>
                <div role="cell" className="ant-col ant-col-6 col-md-3">
                    <div className="exam-right-content" style={{ position: "sticky", top: 0 }}>
                        <div className="topbar-exam-detail">
                            <ul className='pl-0'>
                                <li className="item active-true d-flex align-items-center"><a></a> <span>Câu trả lời đúng</span></li>
                                <li className="item active-false d-flex align-items-center"><a></a> <span>Câu trả lời sai</span></li>
                                <li className="item d-flex align-items-center"><a></a> <span>Chưa trả lời</span></li>
                            </ul>

                        </div>
                        <div className="exam-right-info">
                            <p className="mg-0 color-blue text-center title-list-q">
                                <b>Câu hỏi</b>
                            </p>
                            <ul>
                                {
                                    tu_luan && tu_luan.data && tu_luan.data.map((item, index) => {
                                        return (<li key={index} className={`item ${item.reply_id && parseInt(item.reply_id) > 0 ? item.reply_id == item.reply_id_true ? 'active-true' : 'active-false' : ''}`}><a>{index + 1}</a>
                                        </li>);
                                    })
                                }
                            </ul>
                        </div>

                    </div>
                </div>
            </div >

        );
    }
}

export { TuLuanComponent };