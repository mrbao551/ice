import { createContext } from 'react';
import { BaseService } from 'shared/services';
import { Subject } from 'rxjs';
import { http } from 'shared/utils';
import { map } from 'rxjs/operators';
class HomeService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/home' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }

    getDanhSachCauHoi() {
        return http.get('api/tests').pipe(map((res) => {
            return res;
        }));
    }
    chooseAnswer(obj) {
        return http.put(`api/tests/${obj.id}/${obj.reply_id}`);
    }
    getUserStep() {
        return http.get('api/user-step').pipe(map((res) => {
            return res;
        }));
    }
    saveLuotTest(obj) {
        return http.put(`api/update-luot-thi`, obj);
    }

    getDetailResult(id) {
        return http.get('api/tests/result/'+id);
    }
    getListResult() {
        return http.get('api/tests/result/0');
    }

}
const _HomeService = new HomeService();
const Context = createContext();
export { Context, _HomeService, HomeService };