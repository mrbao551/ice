import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Card } from 'react-bootstrap';
import { _HomeService } from './HomeService';

const timer = 60 * 60 * 1000;
class ListResult extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
    }
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: null,

        };
        this.subscriptions = {};
    }
    componentDidMount() {

        _HomeService.getListResult().subscribe(res => {
            this.setState({ data: res });
        });
    }

    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }



    render() {
        let { data } = this.state;
        return (
            <div className='p-4'>
                <h5 className='text-center text-uppercase p-5' style={{ fontWeight: 700, color: 'rgb(3 30 109)' }}>Danh sách các lần thi của bạn</h5>

                <div className='row mt-2 text-center'>
                    {
                        data && data.map((item, index) => {
                            return (
                                <div key={index} className='col-md-4 mt-2'>
                                    <Card border="primary" bg="primary">
                                        <Card.Header className='text-white'>Lần thi thứ {item.so_luot}</Card.Header>
                                        <Card.Body className='bg-light text-left' style={{minHeight:145}}>
                                            <ul className='p-0'>
                                                <li>Thời điểm bắt đầu: <span style={{ fontWeight: 600 }}>{item.time_bat_dau_thi}</span></li>
                                                {
                                                    item.trang_thai == 1 && <li>Thời điểm kết thúc: <span style={{ fontWeight: 600 }}>{item.time_ket_thuc_thi}</span></li>
                                                }

                                                <li>Trạng thái: <span style={{ fontWeight: 600 }}>{item.trang_thai == 1 ? 'Hoàn thành' : 'Chưa hoàn thành'}</span></li>
                                            </ul>
                                            {
                                                 item.trang_thai == 1 ? <p className='text-center'><Card.Link style={{ fontSize: 14, fontWeight: 'bold'}} href={`/result/${item.id}`}>Xem kết quả</Card.Link></p> :                                                 
                                                 <p className='text-center pt-4'><Card.Link style={{ fontSize: 14, fontWeight: 'bold'}} href={`/`}>Tiếp tục thi</Card.Link></p>
                                            }

                                        </Card.Body>
                                    </Card></div>);
                        })
                    }


                </div>

            </div >
        );
    }
}

ListResult.propTypes = {
    user: PropTypes.object
};
const mapStateToProps = (state) => {
    return {
        user: state.oauth
    };
};
const ListResultComponent = connect(mapStateToProps, {})(ListResult);
export { ListResultComponent };