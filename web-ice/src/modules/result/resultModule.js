import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context,_HomeService } from './HomeService';
import { ListResultComponent } from './ListResultComponent';
import { DetailResultComponent } from './DetailResultComponent';
class ResultModule extends Component {
    static propTypes={
        match:PropTypes.object
    }
    render() {
        let {path}=this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({beh})=>(
                        <Context.Provider value={{
                            HomeService: _HomeService,
                            beh:beh
                        }} >
                            <Switch>
                                
                                <Route path={`${path}/:id`} render={(props) => <DetailResultComponent {...props} />} />
                                <Route path={path} render={(props) => <ListResultComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { ResultModule };