import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';
import PropTypes from 'prop-types';
import {BehaviorsContext} from 'shared/services';
import {Context, _TaiLieuService} from './TaiLieuService';
import {TaiLieuListComponent} from './list/TaiLieuListComponent';
class TaiLieuModule extends Component {
    static propTypes = {
        match: PropTypes.object
    }
    render() {
        let {path} = this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({beh}) => (
                        <Context.Provider value={{
                            TaiLieuService: _TaiLieuService,
                            beh: beh
                        }} >
                            <Switch>
                                <Route path={`${path}/view`} render={(props) => <TaiLieuListComponent {...props} ></TaiLieuListComponent>} ></Route>
                                <Route path={`${path}/nhaplieu`} render={(props) => <TaiLieuListComponent chucnang={1} {...props} ></TaiLieuListComponent>} ></Route>
         
                                <Route path={path} render={(props) => <TaiLieuListComponent {...props} />} />

                            </Switch>
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export {TaiLieuModule};