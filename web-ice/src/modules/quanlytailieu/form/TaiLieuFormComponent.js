import React from 'react';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal, ButtonGroup, Card, CardGroup, Tabs, Tab, InputGroup, Table } from 'react-bootstrap';
import { Formik, Field } from 'formik';
import { string, object, number } from 'yup';
import { LoadingComponent, FormSelect, DynamicFields } from 'shared/components';
import { Context } from '../TaiLieuService';
import { DOCUMENTS_TITLE } from 'redux/quantrihethong/documents/documentsConstant';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import { API_URL } from 'app-setting';
import { TCKFINDER_URL } from 'app-setting';
import CKEditor from 'ckeditor4-react';
import Split from 'react-split';
import DatePicker from 'react-datepicker';
import { VIEW_DOCUMENT_URL } from 'app-setting';

const schema = object({
    TieuDe: string().nullable().trim().required('Tên tài liệu không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
    LoaiTaiLieu_Id: number().nullable().required('Loại tài liệu không được để trống').integer('Bạn phải nhập kiểu số nguyên'),

});
const userDefault = {
    TieuDe: '',
    LoaiTaiLieu_Id: '',
    fileAttachs: [],
    LstAttachDeletes: [],
};

class TaiLieuFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
            isChangeDesign: false,
            loaiTaiLieuFields: [],
            tabactive: 'thongtinhoso',
            ChucNang: 1,
            fieldFolderstr: 1,
            dataFieldFolder: [],
            dataLoaiTaiLieuDM: [],
            dataLoaiTaiLieu: [],
            keyload: 1,
        schema : object({
               

        }),
            schemadefault: object({
                TieuDe: string().nullable().trim().required('Tên tài liệu không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
                LoaiTaiLieu_Id: number().nullable().required('Loại tài liệu không được để trống').integer('Bạn phải nhập kiểu số nguyên'),

            }),
            isHoanThanh: false,
            UrlFile: '',
            urlDoc: '',
        };
        this.subscriptions = {};
    }

    componentDidMount() {

        this.subscriptions['sendToForm'] = this.context.TaiLieuService.sendToForm.subscribe(res1 => {

            switch (res1.action) {
                case 'new':
                    this.setState({ editMode: true, isShow: res1.isShow, action: res1.action, id: res1.id, loaiTaiLieuFields:[],ChucNang: res1.ChucNang,UrlFile: '', data:userDefault ,tabactive: 'thongtinhoso',}, () => {

                        this.loadDataSelect();
                    });
                    break;
                case 'edit':
                    this.setState({editMode: true, isShow: res1.isShow, action: res1.action, id: res1.id, loading: true, ChucNang: res1.ChucNang, loaiTaiLieuFields:[], data:userDefault}, () => {
                        this.context.TaiLieuService.getById(res1.id).subscribe(res => {

                            this.setState({
                                data: res,
                                loading: false,
                            }, () => {
                                this.loadDataSelect();
                            });
                          
                          
                           
                        });
                    });

                    break;
            }
        });
    }

    loadDataSelect() {

        this.context.TaiLieuService.getLstLoaiTaiLieu().subscribe(res => { this.setState({ dataLoaiTaiLieu: res }); });

        this.context.TaiLieuService.getHoSoFields(false).subscribe(res => {
            this.setState({ dataFieldFolder: res });
            if (res.length > 0) {
                
                this.setState({ fieldFolderstr: res[0].tenTruong });
                this.state.schemadefault.fields[res[0].tenTruong] = string().trim().nullable().required(res[0].label + ' không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự');
                this.state.schemadefault._nodes.push(res[0].tenTruong);
                let lstFieldtext = res.filter(function (item) {
                    return item.kieuDuLieu==2;
                });
                for (let i = 0; i < lstFieldtext.length; i++) {
                    this.state.schemadefault.fields[lstFieldtext[i].tenTruong] = string().trim().nullable().max(250, 'Bạn nhập tối đa 250 ký tự');
                    this.state.schemadefault._nodes.push(lstFieldtext[i].tenTruong);
                }

                
            }

            console.log(this.state.schemadefault);
            var lstId = [];
            res.forEach((item) => {
                if (item.quanLyDanhMuc_Id > 0) {
                    lstId.push(item.quanLyDanhMuc_Id)
                }
            });

            this.context.TaiLieuService.getLstForCategory(lstId).subscribe(res => {
                this.setState({ dataLoaiTaiLieuDM: res });
            });
        });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }

    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }

    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    ChangeDesign() {
        this.setState({ isChangeDesign: !this.state.isChangeDesign });
    }
    ChangeTabs = (tabselect) => {
        this.setState({
            tabactive: tabselect
        });
    }
    getLoaiTaiLieu(loaiTaiLieu_Id) {

        loaiTaiLieu_Id = loaiTaiLieu_Id ? loaiTaiLieu_Id : 0;
        this.context.TaiLieuService.getLoaiTaiLieuFields(loaiTaiLieu_Id).subscribe(res => {
            this.setState
                ({
                    loaiTaiLieuFields: res
                }, () => {
                 //   this.state.schema = this.state.schema.concat(this.state.schemadefault);
                    this.context.TaiLieuService.getSchemaDynamic(res, this.state.data, this.state.schema);

                });

            let lstdanhmucId = res.filter(x => x.kieuDuLieu == 7)?.map(y => y.quanLyDanhMuc_Id);
            if (lstdanhmucId.length > 0)
                this.context.TaiLieuService.getDanhMucs(lstdanhmucId).subscribe(res => {
                    this.setState({ dataDanhMucs: res });

                });
        });
    }

    convertToJson(data) {
        var { loaiTaiLieuFields } = this.state;
        if (data) {
            for (var key in data) {
                // skip loop if the property is from prototype
                if (!data.hasOwnProperty(key)) continue;

                if (loaiTaiLieuFields?.filter(x => x.tenTruong === key).length == 0 && loaiTaiLieuFields?.filter(x => x.tenTruong === key.replace('Text', '')).length == 0) {
                    delete data[key]
                }
            }
        }
        return data;
    }
    convertToJsonHoSo(data) {
        var { dataFieldFolder, dataLoaiTaiLieuDM } = this.state;
        let newData = {};

        if (data) {
            for (var key in data) {
                // skip loop if the property is from prototype
                if (!data.hasOwnProperty(key)) continue;

                if (dataFieldFolder?.filter(x => x.tenTruong === key).length == 0 && dataFieldFolder?.filter(x => x.tenTruong === key.replace('Text', '')).length == 0) {
                    delete data[key]
                } else {
                    newData[key.replace('sh_', '')] = data[key];
                }
            }
        }
        //create url file
        if (data) {
            var strUrl = '';
            for (var i = 0; i < dataFieldFolder.length; i++) {
                if (data[dataFieldFolder[i].tenTruong]) {
                    if (dataFieldFolder[i].kieuDuLieu == 2) {
                        strUrl += '\\'+data[dataFieldFolder[i].tenTruong]  ;
                    } else if (dataFieldFolder[i].kieuDuLieu == 7) {
                        let strtemp = dataLoaiTaiLieuDM.find(x => x.id == data[dataFieldFolder[i].tenTruong])?.ma;
                        if (strtemp) {
                            strUrl += '\\'+strtemp ;
                        } else {
                            break;
                        }
                    }
                } else {
                    break;
                }
            }
            this.setState({ urlDoc: strUrl });
        }
        return newData;
    }
    onSubmit(data) {
        var ThongTinNhap = this.convertToJson(JSON.parse(JSON.stringify(data)))
        var ThongHoSo = this.convertToJsonHoSo(JSON.parse(JSON.stringify(data)))
        if (this.state.data?.fileAttachs.length ==0) {
            toast.error('Bạn cần upload file');

        } else if (this.state.urlDoc =='') {
            toast.error('Bạn cần nhập thông tin hồ sơ');
        }
        else {
            this.setState({
                loading: true
            });

            if (ThongTinNhap && ThongHoSo) {
                data.thongTinNhapObject = ThongTinNhap;
                data.thongTinHoSoObject = ThongHoSo;
            }
            data.UrlFile = this.state.urlDoc +'\\'+ this.state.data?.fileAttachs[0]?.name;
            data.TenFile = this.state.data?.fileAttachs[0]?.name;
            data.fileAttachs = this.state.data?.fileAttachs;
            this.subscriptions['create'] = this.context.TaiLieuService.createDoc(data, 'TaiLieuCreateDto').subscribe(() => {
                this.handleClose(true);
                this.setState({
                    loading: false
                });
                toast.success('Thêm mới thành công');
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });
        }

    }


    ChangeTabs = (tabselect) => {

        this.setState({
            tabactive: tabselect
        });

    }
    closeModal() {
        this.setState({ isShow: false });
        this.handleClose(true);
    }
   
    HoanThanh(setFieldValue, status) {
        switch (status) {
            case 1:
                setFieldValue('TrangThai', 1);
                break;
            case 2:
                setFieldValue('TrangThai', 3);
                break;
        }
    }

    upload() {
        document.getElementById('selectFileF').click()
    }
    onChangeFileHandlerF = event => {

        let { data, keyload } = this.state;
        if (event.target.files.length > 0 && event.target.files[0]?.type == 'application/pdf') {
            data.fileAttachs = event.target.files;
            if (data.fileAttachs) {
                this.getUrl(event.target.files[0]);
                this.setState({ data });
            }
        } else {
            toast.error('File không hợp lệ');
        }
    }
    async getUrl(data) {
        let temp = await this.context.TaiLieuService.uint8ToString(data);
        this.setState({ UrlFile: temp });
    }
    onChangeLoaiTaiLieu = (e, setFieldValue) => {
        let value = e.target.value;
        setFieldValue('LoaiTaiLieu_Id', value);
        
        setFieldValue('LoaiTaiLieu', this.state.dataLoaiTaiLieu.find(x => x.id == value)?.tieuDe);
        if (value) {
            this.getLoaiTaiLieu(value);
        }
    }
    
    computedTitle() {
        return 'Thêm mới tài liệu';
    }
    handleKeypressTT = e => {
         
        //it triggers by pressing the enter key
        if (e.keyCode === 13 || e. which === 13) {
            this.setState({ tabactive: 'thongtintailieu' });
        }
    };
    render() {

        let { isChangeDesign, dataLog, ChucNang, fieldFolderstr} = this.state;
        return (
            <Modal className="modal-full" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={this.state.isHoanThanh ? this.state.schema.concat(this.state.schemadefault) : this.state.schemadefault}
                    onSubmit={(values) => {
                        this.onSubmit(this.context.TaiLieuService.trimString(values));
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        setFieldValue,
                        values,
                        touched,
                        errors,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} onKeyPress={(ev) => { ev.stopPropagation(); }}>
                            <Modal.Header >
                                <Modal.Title>{this.computedTitle()}</Modal.Title>
                                <button type="button" onClick={() => { this.closeModal() }} className="close"><span aria-hidden="true">×</span><span className="sr-only">Close</span></button>
                            </Modal.Header>
                            <Modal.Body onKeyPress={this.onKeyPress.bind(this)}>

                                <CardGroup>

                                    <Split
                                        className={`split ${isChangeDesign ? 'vertical-bieumau' : 'horizontal-bieumau'}`}
                                        sizes={[60, 40]}
                                        direction={!isChangeDesign ? 'horizontal' : 'vertical'}

                                    >

                                        <Card className="bieumau">
                                            <Card.Header style={{ padding: '6px' }}>
                                                {/*{isChangeDesign ? <span style={{fontSize: "12px"}}>{values.UrlFile}</span> : ''}*/}
                                                <Form.Row style={{ float: 'right' }}>


                                                    <a href="#" onClick={() => { this.upload(); }} className="act-bieumau">
                                                        <span className="btn default btn-xs origan mr-1"><i className="fa fa-upload" aria-hidden="true"></i></span>Upload file
                                                    </a>
                                                    <input id={'selectFileF'} hidden type="file" accept=".pdf" onChange={(e)=>{this.onChangeFileHandlerF(e); 
                                                         if (e.target.files.length > 0 && e.target.files[0]?.type == 'application/pdf') {
                                                            setFieldValue('TieuDe', e.target.files[0]?.name);
                                                         }
                                                      
                                                    }} multiple={false} />

                                                    <a href="#" className="act-bieumau" onClick={() => { this.ChangeDesign(); }}>
                                                        <span className="btn default btn-xs blue mr-1"><i className="fa fa-laptop"></i></span>Đổi kiểu hiển thị
                                                    </a>
                                                </Form.Row>
                                            </Card.Header>
                                            <Card.Body style={{ paddingTop: '0' }} >
                                                {/*{!isChangeDesign ? <span style={{fontSize: "12px"}}>{values.UrlFile}</span> : ''}*/}
                                                <div className="pdf-viewer" width="100%" height="521px">
                                                    {this.state.data.fileAttachs && this.state.data.fileAttachs[0]?.size > 0 ?
                                                        <embed
                                                            key={this.state.keyload}
                                                            src={this.state.UrlFile}
                                                            type="application/pdf"
                                                            width="100%"
                                                            height="100%"
                                                        /> : ''
                                                    }
                                                </div>
                                            </Card.Body>

                                        </Card>
                                        <Card className="tabthongtintailieu">
                                            <Card.Body style={{ padding: '0' }}>

                                                <Tabs defaultActiveKey="thongtinhoso" activeKey={this.state.tabactive} style={{ marginBottom: '10px' }} id="uncontrolled-tab-example" onSelect={(ev) => { this.ChangeTabs(ev); }}>

                                                    <Tab eventKey="thongtinhoso" title="Thông tin hồ sơ">
                                                        <React.Fragment>
                                                            <DynamicFields
                                                                dataDanhMucs={this.state.dataLoaiTaiLieuDM}
                                                                values={values}
                                                                handleChange={handleChange}
                                                                errors={errors}
                                                                handleBlur={handleBlur}
                                                                touched={touched}
                                                                setFieldValue={setFieldValue}
                                                                loaiTaiLieuFields={this.state.dataFieldFolder}
                                                                service={this.context.TaiLieuService}
                                                                editMode={this.state.editMode}
                                                                setGetDMTen={true}

                                                            />

                                                            <Form.Row className="text-danger ml-1 mb-1">
                                                                <Button onClick={() => {
                                                                   
                                                                   
                                                                        this.setState({ tabactive: 'thongtintailieu' });

                                                                    
                                                                }
                                                                   
                                                                }
                                                                    onKeyPress={this.handleKeypressTT}

                                                                    style={{ marginBottom: '0px' }} className="act-bieumau blue" type="button" variant="primary" size="sm" >
                                                                    <i class="fa fa-share" aria-hidden="true"></i>&nbsp;
                                                                    Tiếp tục
                                                                </Button>


                                                            </Form.Row>
                                                        </React.Fragment>

                                                    </Tab>

                                                    <Tab eventKey="thongtintailieu" title="Thông tin tài liệu">
                                                        <Form.Row>
                                                            <Form.Group as={Col} md="12" controlId="TieuDe">
                                                                <Form.Label>Tên{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ''}</Form.Label>

                                                                {this.state.editMode ?
                                                                    <React.Fragment>
                                                                        <Form.Control
                                                                            type="text"
                                                                            name="TieuDe"
                                                                            value={values.TieuDe || ''}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            isInvalid={touched.TieuDe && !!errors.TieuDe}
                                                                        />
                                                                        <Form.Control.Feedback type="invalid">
                                                                            {errors.TieuDe}
                                                                        </Form.Control.Feedback>
                                                                    </React.Fragment> :
                                                                    <p className="form-control-static  " >{values.TieuDe}</p>
                                                                }
                                                            </Form.Group>

                                                        </Form.Row>
                                                        <Form.Row>

                                                            <Form.Group as={Col} md="12" controlId="LoaiTaiLieu_Id">
                                                                <Form.Label>Loại tài liệu{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ''}</Form.Label>
                                                                {this.state.editMode ?
                                                                    <React.Fragment>
                                                                        <FormSelect
                                                                            key={values.LoaiTaiLieu_Id}
                                                                            placeholder="Chọn loại tài liệu"
                                                                            name="LoaiTaiLieu_Id"
                                                                            data={this.state.dataLoaiTaiLieu}
                                                                            value={values.LoaiTaiLieu_Id}
                                                                            onChange={(e) => this.onChangeLoaiTaiLieu(e, setFieldValue)}
                                                                            onBlur={handleBlur}
                                                                            isInvalid={touched.LoaiTaiLieu_Id && !!errors.LoaiTaiLieu_Id}
                                                                            mode="simpleSelect"
                                                                        ></FormSelect>
                                                                        <Form.Control.Feedback type="invalid">
                                                                            {errors.LoaiTaiLieu_Id}
                                                                        </Form.Control.Feedback>
                                                                    </React.Fragment> :
                                                                    <p className="form-control-static">{values.LoaiTaiLieu}</p>
                                                                }
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <React.Fragment>
                                                            <DynamicFields
                                                                dataDanhMucs={this.state.dataDanhMucs}
                                                                values={values}
                                                                handleChange={handleChange}
                                                                errors={errors}
                                                                handleBlur={handleBlur}
                                                                touched={touched}
                                                                setFieldValue={setFieldValue}
                                                                loaiTaiLieuFields={this.state.loaiTaiLieuFields}
                                                                service={this.context.TaiLieuService}
                                                                editMode={this.state.editMode}
                                                            />
                                                        </React.Fragment>
                                                        {ChucNang == 3 || ChucNang == 2 || ChucNang == 1 ?
                                                            <Form.Row className="text-danger ml-1 mb-1" onKeyPress={(ev) => { ev.stopPropagation(); }}>
                                                                <Button onClick={() => {
                                                                    this.HoanThanh(setFieldValue, 1);
                                                                   
                                                                   
                                                                    this.setState({
                                                                        isHoanThanh: false
                                                                    }, () => {
                                                                        handleSubmit();
                                                                        if (errors[fieldFolderstr] && Object.keys(errors).length ==1) {
                                                                            this.setState({ tabactive: 'thongtinhoso' });
                                                                        }
                                                                    });
                                                                   
                                                                }} style={{ marginBottom: '0px' }} className="act-bieumau blue" type="button" variant="primary" size="sm" >
                                                                    <span className="iconify fa" data-icon="fa-solid:plus" data-inline="false"></span>&nbsp;
                                                                    Lưu
                                                                </Button>

                                                                {/*<Button onClick={() => {*/}
                                                                {/*    this.HoanThanh(setFieldValue, 2);*/}

                                                                {/*    this.setState({*/}
                                                                {/*        isHoanThanh: true*/}
                                                                {/*    }, () => {*/}
                                                                {/*        handleSubmit();*/}
                                                                {/*        if (errors[fieldFolderstr]) {*/}

                                                                {/*            this.setState({ tabactive: "thongtinhoso" });*/}
                                                                {/*        }*/}
                                                                {/*    });*/}
                                                                {/*}} style={{ marginBottom: '0px' }} className="act-bieumau green ml-2" type="button" variant="primary" size="sm" >*/}
                                                                {/*    <i className="fa fa-check-square" aria-hidden="true"></i>&nbsp;*/}
                                                                {/*    Hoàn thành*/}
                                                                {/*</Button>*/}
                                                            </Form.Row> : ''}
                                                    </Tab>


                                                </Tabs>
                                            </Card.Body>

                                        </Card>

                                    </Split>

                                </CardGroup>

                            </Modal.Body>

                        </Form>
                    )}
                </Formik>
            </Modal>

        );
    }
}
export { TaiLieuFormComponent };