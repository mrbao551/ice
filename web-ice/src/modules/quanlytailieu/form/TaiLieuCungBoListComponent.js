import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Breadcrumb, Card, Button, ButtonGroup, Table } from 'react-bootstrap';

import { Link } from 'react-router-dom';
import { Page, GridView } from 'shared/components';
import { Context } from '../TaiLieuService';

import { toast } from 'react-toastify';
class TaiLieuCungBoListComponent extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        setData: PropTypes.func,
        setMeta: PropTypes.func,
        data: PropTypes.array,
        meta: PropTypes.object,
        TaiLieu: PropTypes.object,
        selectDocument: PropTypes.func,
        lstTaiLieuCungBo:PropTypes.array,
    }
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            idchoosed: 0,
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        var { TaiLieu } = this.props;
        //this.fetchData(TaiLieu?.URLFolder);
        this.setState({ idchoosed: TaiLieu.Id })
    }
    componentDidUpdate(previousProps, previousState) {

        if (previousProps.TaiLieu?.Id != this.props.TaiLieu?.Id && previousProps.TaiLieu?.Id) {
            var { TaiLieu } = this.props;
            //this.fetchData(TaiLieu?.URLFolder);
            this.setState({ idchoosed: this.props.TaiLieu?.Id })
        }
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    fetchData(UrlTaiLieuCungBo) {
        this.setState({ loading: true });
        this.subscriptions['getmany'] = this.context.TaiLieuService.getLstTaiLieuCungBo(UrlTaiLieuCungBo).subscribe(res => {
            this.setState({ data: res });
        });
    }
    selectDocument(id) {
        this.setState({ idchoosed: id });
        this.props.selectDocument(id)
    }
    render() {
        var { data, idchoosed } = this.state;
        var {lstTaiLieuCungBo} =this.props;
        return (

            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                        <th style={{ "width": '70px' }}>STT</th>
                        <th>Tiêu đề</th>
                        <th style={{ "width": '120px' }}>Trạng thái</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        lstTaiLieuCungBo && lstTaiLieuCungBo.map((item, index) => {
                            return (
                                <tr key={index}>
                                    <td className="text-center">{index + 1}</td>
                                    <td><a href="javascript:;" className={idchoosed == item.id ? "text-choosed" : ""} onClick={() => { this.selectDocument(item.id) }}>{item.tieuDe}</a> </td>
                                    <td className="text-center"><span className={idchoosed == item.id ? "badge badge-secondary text-choosed" : "badge badge-secondary"}>{item.statusName}</span></td>
                                </tr>
                            )
                        })
                    }
               </tbody>
            </Table>

        );
    }
}

export { TaiLieuCungBoListComponent };