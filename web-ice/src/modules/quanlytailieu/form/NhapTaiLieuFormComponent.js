import React from 'react';
import PropTypes from 'prop-types';
import {Form, Col, Button, Modal, ButtonGroup, Card, CardGroup, Tabs, Tab, InputGroup, Table} from 'react-bootstrap';
import {Formik, Field} from 'formik';
import {string, object, number} from 'yup';
import {LoadingComponent, FormSelect, DynamicFields} from 'shared/components';

import {Context} from '../TaiLieuService';
import {DOCUMENTS_TITLE} from 'redux/quantrihethong/documents/documentsConstant';
import {toast} from 'react-toastify';
import {Link} from 'react-router-dom';
import {API_URL} from 'app-setting';
import {TCKFINDER_URL} from 'app-setting';
import CKEditor from 'ckeditor4-react';
import Split from 'react-split';
import DatePicker from 'react-datepicker';
import {VIEW_DOCUMENT_URL} from 'app-setting';
import {TaiLieuCungBoListComponent} from './TaiLieuCungBoListComponent';
const schema= object({
            TieuDe: string().nullable().trim().required('Tên tài liệu không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
    LoaiTaiLieu_Id: number().nullable().required('Loại tài liệu không được để trống').integer('Bạn phải nhập kiểu số nguyên'),
        });
const userDefault = {
    TieuDe: '',
    LoaiTaiLieu_Id: '',
    fileAttachs: [],
    LstAttachDeletes: [],
};
class NhapTaiLieuFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
            isChangeDesign: false,
            loaiTaiLieuFields: [],
            tabactive: 'thongtintailieu',
            ChucNang: 1,
            lstTaiLieuCungBo: [],
            dataKieuEnum: [],
            dataLoaiTaiLieu: [],
            keyload: 1,
            schema: object({}),
            isHoanThanh: false,

        };
        this.subscriptions = {};
    }
    componentDidMount() {
       
      
        this.subscriptions['sendToFormNhapTaiLieu'] = this.context.TaiLieuService.sendToFormNhapTaiLieu.subscribe(res1 => {
            switch (res1.action) {
                case 'edit':
                    this.setState({editMode: true, isShow: res1.isShow, action: res1.action, id: res1.id, loading: true, ChucNang: res1.ChucNang}, () => {
                        this.context.TaiLieuService.getById(res1.id).subscribe(res => {

                            this.setState({
                                data: res,
                                loading: false,
                            }, () => {
                                this.setState({})
                            });
                            this.getLoaiTaiLieu(res?.LoaiTaiLieu_Id);
                            this.context.TaiLieuService.getLstTaiLieuCungBo(res?.URLFolder, res1.ChucNang).subscribe(res => {
                                this.setState({lstTaiLieuCungBo: res});
                            });
                            this.loadDataSelect();
                        });
                    });

                    break;
                case 'read':
                    this.setState({editMode: false, isShow: res1.isShow, action: res1.action, id: res1.id, loading: true, ChucNang: res1.ChucNang}, () => {
                        this.context.TaiLieuService.getById(res1.id).subscribe(res => {

                            this.setState({
                                data: res,
                                loading: false,
                            }, () => {
                                this.setState({})
                            });
                            this.getLoaiTaiLieu(res?.LoaiTaiLieu_Id);
                            this.context.TaiLieuService.getLstTaiLieuCungBo(res?.URLFolder, res1.ChucNang).subscribe(res => {
                                this.setState({lstTaiLieuCungBo: res});
                            });
                            this.loadDataSelect();
                        });
                    });
            }
        });
    }
    loadDataSelect() {
        this.context.TaiLieuService.getDataEnum('TaiLieu_TrangThai').subscribe(res => {this.setState({dataKieuEnum: res});});
        this.context.TaiLieuService.getLstLoaiTaiLieu().subscribe(res => {this.setState({dataLoaiTaiLieu: res});});
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }

    handleClose(isRefesh) {
        this.setState({isShow: false});
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }

    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    ChangeDesign() {
        this.setState({isChangeDesign: !this.state.isChangeDesign});
    }
    ChangeTabs = (tabselect) => {
        this.setState({
            tabactive: tabselect
        });
    }
    getLoaiTaiLieu(loaiTaiLieu_Id) {

        loaiTaiLieu_Id = loaiTaiLieu_Id ? loaiTaiLieu_Id : 0;
        this.context.TaiLieuService.getLoaiTaiLieuFields(loaiTaiLieu_Id).subscribe(res => {
            this.setState
                ({
                    loaiTaiLieuFields: res
                }, () => {
                    this.state.schema = object({
                        TieuDe: string().nullable().trim().required('Tên tài liệu không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
                        LoaiTaiLieu_Id: number().nullable().required('Loại tài liệu không được để trống').integer('Bạn phải nhập kiểu số nguyên'),
                    });
                    this.context.TaiLieuService.getSchemaDynamic(res, this.state.data, this.state.schema);

                });

            let lstdanhmucId = res.filter(x => x.kieuDuLieu == 7)?.map(y => y.quanLyDanhMuc_Id);
            if (lstdanhmucId.length > 0)
                this.context.TaiLieuService.getDanhMucs(lstdanhmucId).subscribe(res => {
                    this.setState({dataDanhMucs: res});

                });


        });
    }
   

    convertToJson(data) {
        var {loaiTaiLieuFields} = this.state;
        if (data) {
            for (var key in data) {
                // skip loop if the property is from prototype
                if (!data.hasOwnProperty(key)) continue;

                if (loaiTaiLieuFields?.filter(x => x.tenTruong === key).length == 0 && loaiTaiLieuFields?.filter(x => x.tenTruong === key.replace('Text','')).length==0) {
                    delete data[key]
                }
            }
        }
        return data;
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        var ThongTinNhap = this.convertToJson(JSON.parse(JSON.stringify(data)))
        if (ThongTinNhap) {
            //var noidung = JSON.stringify(ThongTinNhap);
            //data.thongTinNhap = JSON.stringify(noidung);
            data.thongTinNhapObject = ThongTinNhap;
        }
        if (this.state.id) {
            this.subscriptions['update'] = this.context.TaiLieuService.update(data, this.state.id).subscribe(() => {
                //this.handleClose(true);
                this.setState({
                    loading: false
                });
                toast.success('Cập nhật thành công');
                this.xuLynextDocument(data);
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });
        }
    }
    xuLynextDocument(data) {
        var {lstTaiLieuCungBo, id, dataKieuEnum} = this.state;
        var lstitem = lstTaiLieuCungBo.filter(x => x.id < id);
        let idold = id;
        if (lstitem.length > 0) {
            let idnew = lstitem[0].id;
            this.setState({id: idnew});
            this.context.TaiLieuService.getById(idnew).subscribe(res => {
                this.setState({
                    data: res,
                    loading: false,
                }, () => {
                    this.setState({})
                });
                this.getLoaiTaiLieu(res?.LoaiTaiLieu_Id);
            });
        } else if(lstTaiLieuCungBo.length >1) {
            let idnew = lstTaiLieuCungBo[0].id;
            this.setState({id: idnew});
            this.context.TaiLieuService.getById(idnew).subscribe(res => {
                this.setState({
                    data: res,
                    loading: false,
                }, () => {
                    this.setState({})
                });
                this.getLoaiTaiLieu(res?.LoaiTaiLieu_Id);
            });
        } else{
            this.closeModal();
        }
        //xu ly ban ghi cung bo
        lstTaiLieuCungBo.find(x => x.id == idold).statusName = dataKieuEnum.find(x => x.value == data.TrangThai)?.label;
        // neu trang thai 3 va trong cn nhap lieu thi bo khoi ds ho so cung bo
        if (data.TrangThai == 3 && this.state.ChucNang == 1) {
            lstTaiLieuCungBo = lstTaiLieuCungBo.filter(x => x.id != idold);
        }
        this.setState({lstTaiLieuCungBo});
    }

    ChangeTabs = (tabselect) => {
        this.setState({
            tabactive: tabselect
        });
        if (tabselect == 'canhanxuly') {
            this.context.TaiLieuService.getLstlogTaiLieu(this.state.id).subscribe(res => {
                this.setState({dataLog: res});
            });
        }
    }
    closeModal() {
        this.setState({isShow: false});
        this.handleClose(true);
    }
    XuLySau(setFieldValue) {
        switch (this.state.ChucNang) {
            case 1:
                setFieldValue('TrangThai', 2);
                break;
            case 2:
                setFieldValue('TrangThai', 4);
                break;
            case 3:
                setFieldValue('TrangThai', 6);
                break;

        }
      
    }
    async TraLai(Id) {
        let {data} = this.state;
        if (await this.context.beh.confirm('Bạn có muốn trả lại bản ghi này?')) {
            this.setState({
                loading: true
            });
            this.context.TaiLieuService.TraLai(Id).subscribe(() => {

                this.setState({
                    loading: false
                });
                data.TrangThai = 8;
                this.xuLynextDocument(data);
                toast.success('Trả lại thành công');
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });          
        }
    }
    HoanThanh(setFieldValue) {

        switch (this.state.ChucNang) {
            case 1:
                setFieldValue('TrangThai', 3);
                break;
            case 2:
                setFieldValue('TrangThai', 5);
                break;
            case 3:
                setFieldValue('TrangThai', 7);
                break;

        }
    }
    selectDocument(id) {
        if (id) {
            this.setState({id: id, loading: false});
            this.context.TaiLieuService.getById(id).subscribe(res => {
                this.setState({
                    data: res,
                    loading: false,
                }, () => {
                    this.setState({})
                });
                this.getLoaiTaiLieu(res?.LoaiTaiLieu_Id);
            });
        }
    }
    uploadChange() {
        document.getElementById('selectFileChange').click();
    }
    onChangeFileTLHandler = event => {
        let {data, keyload} = this.state;
        if (event.target.files.length > 0 && event.target.files[0]?.type == 'application/pdf') {
            data.fileAttachs = event.target.files;
            this.setState({loading: true});
            if (this.state.id && data.fileAttachs) {
                this.subscriptions['update'] = this.context.TaiLieuService.updatefiletailieu(data, 'oTaiLieuUpdateDto').subscribe(() => {
                    //this.handleClose(true);

                    keyload += 1;
                    this.setState({
                        loading: false,
                        keyload: keyload
                    });
                    toast.success('Cập nhật thành công');
                },
                    (err) => {
                        this.setState({
                            loading: false
                        });
                        toast.error(err.error);
                    });
            }
        } else {
            toast.error('File không hợp lệ');
        }
    }
    onChangeLoaiTaiLieu = (e, setFieldValue) => {
        let value = e.target.value;
        setFieldValue('LoaiTaiLieu_Id', value);
        setFieldValue('LoaiTaiLieu', this.state.dataLoaiTaiLieu.find(x => x.id == value)?.tieuDe);
        if (value) {
            this.getLoaiTaiLieu(value);
        }

    }
    computedTitle() {
        if (this.state.ChucNang == 2) {
            return 'Kiểm tra lần 1';
        }
        else if (this.state.ChucNang == 3) {
            return 'Kiểm tra lần 2';
        }
        return 'Nhập thông tin tài liệu';
    }
    render() {
      
        let {isChangeDesign, dataLog, ChucNang} = this.state;
        return (
            <Modal className="modal-full" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={this.state.isHoanThanh ? this.state.schema:schema}
                    onSubmit={(values) => {
                        this.onSubmit(this.context.TaiLieuService.trimString(values));
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        setFieldValue,
                        values,
                        touched,
                        errors,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} onKeyPress={(ev) => { ev.stopPropagation(); }}>
                            <Modal.Header >
                                <Modal.Title>{this.computedTitle()}</Modal.Title>
                                <button type="button" onClick={() => {this.closeModal()}} className="close"><span aria-hidden="true">×</span><span className="sr-only">Close</span></button>
                            </Modal.Header>
                            <Modal.Body onKeyPress={this.onKeyPress.bind(this)}>

                                <CardGroup>

                                    <Split
                                        className={`split ${isChangeDesign ? 'vertical-bieumau' : 'horizontal-bieumau'}`}
                                        sizes={[60, 40]}
                                        direction={!isChangeDesign ? 'horizontal' : 'vertical'}

                                    >

                                        <Card className="bieumau">
                                            <Card.Header style={{ padding: '6px' }} onKeyPress={(ev) => { ev.stopPropagation(); }}>
                                                {/*{isChangeDesign ? <span style={{fontSize: "12px"}}>{values.UrlFile}</span> : ''}*/}
                                                <Form.Row style={{float: 'right'}}>
                                                   
                                                
                                                    <a href="#" onClick={() => {this.uploadChange();}} className="act-bieumau">
                                                        <span className="btn default btn-xs origan mr-1"><i class="fa fa-upload" aria-hidden="true"></i></span>Đổi file
                                                    </a>
                                                    <input id={'selectFileChange'} hidden type="file" accept=".pdf" onChange={this.onChangeFileTLHandler} multiple={false} />
                                                    {ChucNang == 3 || ChucNang == 2 || ChucNang==1? <a href="javascript:void(0);" onClick={() => {this.TraLai(values.Id);}} className="act-bieumau">
                                                        <span className="btn default btn-xs origan mr-1"><i className="fa fa-undo" aria-hidden="true"></i></span>Trả lại
                                                    </a> : ''}
                                                    {ChucNang == 3 || ChucNang == 2 || ChucNang == 1 ?
                                                        <a href="#" onClick={() => {
                                                            this.XuLySau(setFieldValue); this.setState({
                                                                isHoanThanh: false
                                                            }, () => {
                                                                handleSubmit();
                                                            });
                                                        }} className="act-bieumau">
                                                            <span className="btn default btn-xs origan mr-1"><i className="fa fa-share"></i></span>Xử lý sau
                                                    </a>
                                                        : ''}
                                                    <a href={VIEW_DOCUMENT_URL + values?.UrlFile} target="_blank" className="act-bieumau">
                                                        <span className="btn default btn-xs blue mr-1"><i className="fa fa-download"></i></span>Tải về
                                                    </a>
                                                    <a href="javascript:void(0);" className="act-bieumau" onClick={() => {this.ChangeDesign();}}>
                                                        <span className="btn default btn-xs blue mr-1"><i className="fa fa-laptop"></i></span>Đổi kiểu hiển thị
                                                    </a>
                                                 
                                                </Form.Row>

                                            </Card.Header>
                                            <Card.Body style={{paddingTop: '0'}} >
                                                {/*{!isChangeDesign ? <span style={{fontSize: "12px"}}>{values.UrlFile}</span> : ''}*/}
                                                <div className="pdf-viewer" width="100%" height="521px">
                                                    <embed

                                                        key={this.state.keyload}
                                                        src={VIEW_DOCUMENT_URL + values?.UrlFile}
                                                        type="application/pdf"
                                                        width="100%"
                                                        height="100%"
                                                    />

                                                </div>
                                            </Card.Body>

                                        </Card>
                                        <Card className="tabthongtintailieu">
                                            <Card.Body style={{padding: '0'}}>
                                                <Tabs defaultActiveKey="thongtintailieu" style={{marginBottom: '10px'}} id="uncontrolled-tab-example" onSelect={(ev) => {this.ChangeTabs(ev);}}>
                                                    <Tab eventKey="thongtintailieu" title="Thông tin tài liệu">
                                                        <Form.Row>
                                                            <Form.Group as={Col} md="12" controlId="TieuDe">
                                                                <Form.Label>Tên{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ''}</Form.Label>

                                                                {this.state.editMode ?
                                                                    <React.Fragment>
                                                                        <Form.Control
                                                                            type="text"
                                                                            name="TieuDe"
                                                                            value={values.TieuDe || ''}
                                                                            onChange={handleChange}
                                                                            onBlur={handleBlur}
                                                                            isInvalid={ !!errors.TieuDe}
                                                                        />
                                                                        <Form.Control.Feedback type="invalid">
                                                                            {errors.TieuDe}
                                                                        </Form.Control.Feedback>
                                                                    </React.Fragment> :
                                                                    <p className="form-control-static  " >{values.TieuDe}</p>
                                                                }
                                                            </Form.Group>

                                                        </Form.Row>
                                                        <Form.Row>

                                                            <Form.Group as={Col} md="12" controlId="LoaiTaiLieu_Id">
                                                                <Form.Label>Loại tài liệu{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ''}</Form.Label>
                                                                {this.state.editMode ?
                                                                    <React.Fragment>
                                                                        <FormSelect
                                                                            key={values.LoaiTaiLieu_Id}
                                                                            placeholder="Chọn loại tài liệu"
                                                                            name="LoaiTaiLieu_Id"
                                                                            data={this.state.dataLoaiTaiLieu}
                                                                            value={values.LoaiTaiLieu_Id}
                                                                            onChange={(e) => this.onChangeLoaiTaiLieu(e, setFieldValue)}
                                                                            onBlur={handleBlur}
                                                                            isInvalid={touched.LoaiTaiLieu_Id && !!errors.LoaiTaiLieu_Id}
                                                                            mode="simpleSelect"
                                                                        ></FormSelect>
                                                                        <Form.Control.Feedback type="invalid">
                                                                            {errors.LoaiTaiLieu_Id}
                                                                        </Form.Control.Feedback>
                                                                    </React.Fragment> :
                                                                    <p className="form-control-static">{values.LoaiTaiLieu}</p>
                                                                }
                                                            </Form.Group>
                                                        </Form.Row>
                                                        <React.Fragment>
                                                            <DynamicFields
                                                                dataDanhMucs={this.state.dataDanhMucs}
                                                                values={values}
                                                                handleChange={handleChange}
                                                                errors={errors}
                                                                handleBlur={handleBlur}
                                                                touched={touched}
                                                                setFieldValue={setFieldValue}
                                                                loaiTaiLieuFields={this.state.loaiTaiLieuFields}
                                                                service={this.context.TaiLieuService}
                                                                editMode={this.state.editMode}
                                                            />
                                                        </React.Fragment>
                                                        {ChucNang == 3 || ChucNang == 2 || ChucNang == 1 ?
                                                            <Form.Row className="text-danger ml-1 mb-1" onKeyPress={(ev) => { ev.stopPropagation(); }}>
                                                                <Button onClick={() => {
                                                                    this.HoanThanh(setFieldValue);
                                                                    this.setState({
                                                                        isHoanThanh: true
                                                                    }, () => {
                                                                        handleSubmit();
                                                                    });
                                                                }} style={{marginBottom: '0px'}} className="act-bieumau blue" type="button" variant="primary" size="sm" >
                                                                    <i className="fa fa-check-square" aria-hidden="true"></i>&nbsp;
                                                                Hoàn thành
                                                            </Button>
                                                            </Form.Row> : ''}
                                                    </Tab>
                                                    <Tab eventKey="tailieucungbo" title="Tài liệu cùng bộ">
                                                        {this.state.tabactive == 'tailieucungbo' && values?.Id ? <TaiLieuCungBoListComponent TaiLieu={values} selectDocument={this.selectDocument.bind(this)} lstTaiLieuCungBo={this.state.lstTaiLieuCungBo} /> : ''}

                                                    </Tab>
                                                    <Tab eventKey="canhanxuly" title="Cá nhân xử lý">
                                                        {
                                                            (this.state.tabactive == 'canhanxuly' && this.state.id) ?
                                                                (
                                                                    <Table striped bordered hover size="sm">
                                                                        <thead>
                                                                            <tr>
                                                                                <th style={{'width': '70px'}}>STT</th>
                                                                                <th>Chức năng</th>
                                                                                <th>Thời gian</th>
                                                                                <th>Người xử lý</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            {
                                                                                dataLog && dataLog.map((item, index) => {
                                                                                    return (
                                                                                        <tr key={index}>
                                                                                            <td className="text-center">{index + 1}</td>
                                                                                            <td>{item.actionName} </td>
                                                                                            <td className="text-center">{this.context.TaiLieuService.formatTime(item.created)}</td>
                                                                                            <td className="">{item.createdBy}</td>
                                                                                        </tr>
                                                                                    )
                                                                                })
                                                                            }
                                                                        </tbody>
                                                                    </Table>
                                                                ) : ''
                                                        }



                                                    </Tab>
                                                </Tabs>
                                            </Card.Body>

                                        </Card>

                                    </Split>

                                </CardGroup>

                            </Modal.Body>

                        </Form>
                    )}
                </Formik>
            </Modal>

        );
    }
}
export {NhapTaiLieuFormComponent};