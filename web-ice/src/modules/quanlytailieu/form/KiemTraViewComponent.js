import React from 'react';
import PropTypes from 'prop-types';
import {Form, Col, Button, Modal, ButtonGroup, Card, CardGroup, Tabs, Tab, InputGroup, Table, TabContent} from 'react-bootstrap';
import {Formik, Field} from 'formik';
import {string, object} from 'yup';
import {LoadingComponent, FormSelect, MultiDynamicFields} from 'shared/components';
import {Context} from '../TaiLieuService';
import {DOCUMENTS_TITLE} from 'redux/quantrihethong/documents/documentsConstant';
import {toast} from 'react-toastify';
import {Link} from 'react-router-dom';
import {API_URL} from 'app-setting';
import {TCKFINDER_URL} from 'app-setting';
import CKEditor from 'ckeditor4-react';
import Split from 'react-split';
import DatePicker from 'react-datepicker';
import {VIEW_DOCUMENT_URL} from 'app-setting';
import {TaiLieuCungBoListComponent} from './TaiLieuCungBoListComponent';
const schema = object({
    //title: string().trim().required('Tiêu đề không được để trống'),
});
const userDefault = {
    title: '',
    content: '',
    fileAttachs: [],
    LstAttachDeletes: [],
};
class KiemTraViewComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
            isChangeDesign: true,
            loaiTaiLieuFields: [],
            tabactive: 'thongtintailieu',
            ChucNang: 1,
            lstTaiLieuCungBo: [],
            dataKieuEnum: [],
            lstData: [],
            percentage: 40
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.subscriptions['sendToViewKiemTra'] = this.context.TaiLieuService.sendToViewKiemTra.subscribe(res => {
            switch (res.action) {
                case 'read':
                    this.setState({editMode: true, isShow: res.isShow, action: res.action, id: res.id, loading: true, ChucNang: res.ChucNang}, () => {
                        this.context.TaiLieuService.getByUrlPath("getversions/" + res.id).subscribe(res => {
                            this.setState({
                                lstData: res,
                                data: res[0],
                                loading: false,
                            }, () => {
                                this.setState({})
                            });
                            this.getLoaiTaiLieu(res[0].LoaiTaiLieu_Id);
                            this.context.TaiLieuService.getLstTaiLieuCungBo(res[0].URLFolder, res[0].ChucNang).subscribe(res => {
                                this.setState({lstTaiLieuCungBo: res});
                            });
                            this.context.TaiLieuService.getDataEnum("TaiLieu_TrangThai").subscribe(res => {
                                this.setState({dataKieuEnum: res});
                            });
                        });
                    });
                    break;
            }
        });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    handleClose(isRefesh) {
        this.setState({isShow: false});
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }
    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    ChangeDesign() {
        this.setState({isChangeDesign: !this.state.isChangeDesign});
    }
    ChangeTabs = (tabselect) => {
        this.setState({
            tabactive: tabselect
        });
    }
    getLoaiTaiLieu(loaiTaiLieu_Id) {
        loaiTaiLieu_Id = loaiTaiLieu_Id ? loaiTaiLieu_Id : 0;
        this.context.TaiLieuService.getLoaiTaiLieuFields(loaiTaiLieu_Id).subscribe(res => {
            this.setState({loaiTaiLieuFields: res});
            let lstdanhmucId = res.filter(x => x.kieuDuLieu == 7)?.map(y => y.quanLyDanhMuc_Id);
            if (lstdanhmucId.length > 0)
                this.context.TaiLieuService.getDanhMucs(lstdanhmucId).subscribe(res => {
                    this.setState({dataDanhMucs: res});
                });
        });
    }
    convertToJson(data) {
        var {loaiTaiLieuFields} = this.state;
        if (data) {
            for (var key in data) {
                // skip loop if the property is from prototype
                if (!data.hasOwnProperty(key)) continue;
                if (loaiTaiLieuFields?.filter(x => x.tenTruong === key).length == 0) {
                    delete data[key]
                }
            }
        }
        return data;
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        var ThongTinNhap = this.convertToJson(JSON.parse(JSON.stringify(data)))
        if (ThongTinNhap) {
            //var noidung = JSON.stringify(ThongTinNhap);
            //data.thongTinNhap = JSON.stringify(noidung);
            data.thongTinNhapObject = ThongTinNhap;
        }
        if (this.state.id) {
            this.subscriptions['update'] = this.context.TaiLieuService.update(data, this.state.id).subscribe(() => {
                //this.handleClose(true);
                this.setState({
                    loading: false
                });
                toast.success('Cập nhật thành công');
                this.xuLynextDocument(data);
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });
        }
    }
    xuLynextDocument(data) {
        var {lstTaiLieuCungBo, id, dataKieuEnum, data} = this.state;
        var lstitem = lstTaiLieuCungBo.filter(x => x.id > id);
        let idold = id;
        if (lstitem.length > 0) {
            let idnew = lstitem[0].id;
            this.setState({id: idnew});
            this.context.TaiLieuService.getById(idnew).subscribe(res => {
                this.setState({
                    data: res,
                    loading: false,
                }, () => {
                    this.setState({})
                });
                this.getLoaiTaiLieu(res?.LoaiTaiLieu_Id);
            });
        } else {
            this.closeModal();
        }
        //xu ly ban ghi cung bo
        lstTaiLieuCungBo.find(x => x.id == idold).statusName = dataKieuEnum.find(x => x.value == data.TrangThai)?.label;
        // neu trang thai 3 va trong cn nhap lieu thi bo khoi ds ho so cung bo
        if (data.trangThai == 3 && this.state.ChucNang == 1) {
            lstTaiLieuCungBo = lstTaiLieuCungBo.filter(x => x.id != idold);
        }
        this.setState({lstTaiLieuCungBo});
    }
    ChangeTabs = (tabselect) => {
        this.setState({
            tabactive: tabselect
        });
        if (tabselect == "canhanxuly") {
            this.context.TaiLieuService.getLstlogTaiLieu(this.state.id).subscribe(res => {
                this.setState({dataLog: res});
            });
        }
    }
    closeModal() {
        this.setState({isShow: false});
        this.handleClose(true);
    }
    changeSize(sizes) {
        // console.log(sizes);
        // this.setState({percentage: sizes[1] });
        //// console.log(document.getElementsByClassName("vertical-bieumau").clientHeight);  ;
        // document.getElementsByClassName("tab-content")[0].style.maxHeight = sizes[1] -10+ '%';
    }
    XuLySau(setFieldValue) {
        switch (this.state.ChucNang) {
            case 1:
                setFieldValue('TrangThai', 2);
                break;
            case 2:
                setFieldValue('TrangThai', 4);
                break;
            case 3:
                setFieldValue('TrangThai', 6);
                break;
        }
    }
    HoanThanh(setFieldValue) {
        switch (this.state.ChucNang) {
            case 1:
                setFieldValue('TrangThai', 3);
                break;
            case 2:
                setFieldValue('TrangThai', 5);
                break;
            case 3:
                setFieldValue('TrangThai', 7);
                break;
        }
    }
    selectDocument(id) {
        if (id) {
            this.setState({id: id});
            this.context.TaiLieuService.getById(id).subscribe(res => {
                this.setState({
                    data: res,
                    loading: false,
                }, () => {
                    this.setState({})
                });
                this.getLoaiTaiLieu(res?.LoaiTaiLieu_Id);
            });
        }
    }
    render() {
        let {isChangeDesign, dataLog, lstData} = this.state;
        let colwidth = (lstData.length == 3 ? 3 : (lstData.length==2?4:9))
        if (lstData.length > 0)
            return (
                <Modal className="modal-full" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                    <LoadingComponent loading={this.state.loading}></LoadingComponent>
                    <Formik
                        validationSchema={schema}
                        onSubmit={(values) => {
                            console.log(values);
                            this.onSubmit(values);
                        }}
                        enableReinitialize={true}
                        initialValues={this.state.lstData}
                    >
                        {({
                            handleSubmit,
                            handleChange,
                            handleBlur,
                            setFieldValue,
                            values,
                            touched,
                            errors,
                        }) => (
                            <Form noValidate onSubmit={handleSubmit} onKeyPress={(ev) => { ev.stopPropagation(); }}>
                                <Modal.Header >
                                    <Modal.Title>Nhập thông tin tài liệu</Modal.Title>
                                    <button type="button" onClick={() => {this.closeModal()}} className="close"><span aria-hidden="true">×</span><span className="sr-only">Close</span></button>
                                </Modal.Header>
                                <Modal.Body>
                                    <CardGroup>
                                        <Split
                                            className={`split ${isChangeDesign ? "vertical-bieumau" : "horizontal-bieumau"}`}
                                            sizes={[60, 40]}
                                            direction={!isChangeDesign ? "horizontal" : "vertical"}
                                            onDrag={(sizes) => {this.changeSize(sizes)}}
                                        >
                                            <Card className="bieumau">
                                                <Card.Header style={{padding: "6px"}}>
                                                   {/* <span style={{fontSize: "12px"}}>{values[0]?.UrlFile}</span>*/}
                                                    <Form.Row style={{float: "right"}}>

                                                            <a href={VIEW_DOCUMENT_URL + values[0]?.UrlFile} target="_blank" className="act-bieumau">
                                                                <span className="btn default btn-xs blue mr-1"><i className="fa fa-download"></i></span>Tải về
                                                    </a>

                                                    </Form.Row>
                                                </Card.Header>
                                          
                                                    <Card.Body style={{paddingTop: "0"}} >
                                                   
                                                    <div className="pdf-viewer" width="100%" height="521px">
                                                        <embed
                                                            src={VIEW_DOCUMENT_URL + values[0]?.UrlFile}
                                                            type="application/pdf"
                                                            width="100%"
                                                            height="100%"
                                                        />
                                                    </div>
                                                </Card.Body>
                                            </Card>
                                            <Card className="tabthongtintailieu">
                                                <Card.Header style={{padding: "0"}} >
                                                    <Tabs defaultActiveKey="thongtintailieu" style={{marginBottom: "10px"}} id="uncontrolled-tab-example" onSelect={(ev) => {this.ChangeTabs(ev);}}>
                                                        <Tab eventKey="thongtintailieu" title="Thông tin tài liệu" >
                                                            <Form.Row>
                                                                <Form.Group as={Col} md={colwidth==4?4:3} >
                                                                    <Form.Label>Tiêu đề</Form.Label>
                                                                </Form.Group>
                                                                <Form.Group as={Col} md={colwidth} >
                                                                    <Form.Label>Nhập liệu</Form.Label>
                                                                </Form.Group>
                                                                {values.length > 1 ?
                                                                    <Form.Group as={Col} md={colwidth} >
                                                                        <Form.Label>Kiểm tra lần 1</Form.Label>
                                                                    </Form.Group> : ''}
                                                                {values.length > 2 ?
                                                                    <Form.Group as={Col} md={colwidth} >
                                                                        <Form.Label>Kiểm tra lần 2</Form.Label>
                                                                    </Form.Group> : ''}
                                                            </Form.Row>
                                                            <div className="ViewVer">
                                                                <Form.Row>
                                                                    <Form.Group as={Col} md={colwidth == 4 ? 4 : 3} >
                                                                        <Form.Label>Tên</Form.Label>
                                                                    </Form.Group>
                                                                    <Form.Group as={Col} md={colwidth} >
                                                                        {this.state.editMode ?
                                                                            <React.Fragment>
                                                                                <Form.Control
                                                                                    type="text"
                                                                                    name="TieuDe"
                                                                                    disabled="true"
                                                                                    value={values[0].TieuDe || ''}
                                                                                    onChange={(e) => {
                                                                                        let value = e.target.value;
                                                                                    }}
                                                                                    onBlur={handleBlur}
                                                                                    isInvalid={touched.TieuDe && !!errors.TieuDe}
                                                                                />
                                                                                <Form.Control.Feedback type="invalid">
                                                                                    {errors.TieuDe}
                                                                                </Form.Control.Feedback>
                                                                            </React.Fragment> :
                                                                            <p className="form-control-static  " >{values[0].TieuDe}</p>
                                                                        }
                                                                    </Form.Group>
                                                                    {values.length > 1 ?
                                                                        <Form.Group as={Col} md={colwidth} >
                                                                            {this.state.editMode ?
                                                                                <React.Fragment>
                                                                                    <Form.Control
                                                                                        type="text"
                                                                                        name="TieuDe"
                                                                                        disabled="true"
                                                                                        value={values[1].TieuDe || ''}
                                                                                        onChange={(e) => {
                                                                                            let value = e.target.value;
                                                                                        }}
                                                                                        onBlur={handleBlur}
                                                                                        isInvalid={touched.TieuDe && !!errors.TieuDe}
                                                                                    />
                                                                                    <Form.Control.Feedback type="invalid">
                                                                                        {errors.TieuDe}
                                                                                    </Form.Control.Feedback>
                                                                                </React.Fragment> :
                                                                                <p className="form-control-static  " >{values.TieuDe}</p>
                                                                            }
                                                                        </Form.Group>
                                                                        : ''
                                                                    }
                                                                    {values.length > 2 ?
                                                                        <Form.Group as={Col} md={colwidth} >
                                                                            {this.state.editMode ?
                                                                                <React.Fragment>
                                                                                    <Form.Control
                                                                                        type="text"
                                                                                        name="TieuDe"
                                                                                        disabled="true"
                                                                                        value={values[2].TieuDe || ''}
                                                                                        onChange={(e) => {
                                                                                            let value = e.target.value;
                                                                                        }}
                                                                                        onBlur={handleBlur}
                                                                                        isInvalid={touched.TieuDe && !!errors.TieuDe}
                                                                                    />
                                                                                    <Form.Control.Feedback type="invalid">
                                                                                        {errors.TieuDe}
                                                                                    </Form.Control.Feedback>
                                                                                </React.Fragment> :
                                                                                <p className="form-control-static  " >{values.TieuDe}</p>
                                                                            }
                                                                        </Form.Group>
                                                                        : ''
                                                                    }
                                                                </Form.Row>
                                                                <Form.Row>
                                                                    <Form.Group as={Col} md={colwidth == 4 ? 4 : 3} >
                                                                        <Form.Label>Loại văn bản</Form.Label>
                                                                    </Form.Group>
                                                                    <Form.Group as={Col} md={colwidth} >
                                                                        {this.state.editMode ?
                                                                            <React.Fragment>
                                                                                <Form.Control
                                                                                    type="text"
                                                                                    name="LoaiTaiLieu"
                                                                                    disabled="true"
                                                                                    value={values[0].LoaiTaiLieu || ''}
                                                                                    onChange={(e) => {
                                                                                        let value = e.target.value;
                                                                                    }}
                                                                                    onBlur={handleBlur}
                                                                                    isInvalid={touched.LoaiTaiLieu && !!errors.LoaiTaiLieu}
                                                                                />
                                                                                <Form.Control.Feedback type="invalid">
                                                                                    {errors.LoaiTaiLieu}
                                                                                </Form.Control.Feedback>
                                                                            </React.Fragment> :
                                                                            <p className="form-control-static  " >{values[0].LoaiTaiLieu}</p>
                                                                        }
                                                                    </Form.Group>
                                                                    {values.length > 1 ?
                                                                        <Form.Group as={Col} md={colwidth} >
                                                                            {this.state.editMode ?
                                                                                <React.Fragment>
                                                                                    <Form.Control
                                                                                        type="text"
                                                                                        name="LoaiTaiLieu"
                                                                                        disabled="true"
                                                                                        value={values[1].LoaiTaiLieu || ''}
                                                                                        onChange={(e) => {
                                                                                            let value = e.target.value;
                                                                                        }}
                                                                                        onBlur={handleBlur}
                                                                                        isInvalid={touched.LoaiTaiLieu && !!errors.LoaiTaiLieu}
                                                                                    />
                                                                                    <Form.Control.Feedback type="invalid">
                                                                                        {errors.LoaiTaiLieu}
                                                                                    </Form.Control.Feedback>
                                                                                </React.Fragment> :
                                                                                <p className="form-control-static  " >{values.LoaiTaiLieu}</p>
                                                                            }
                                                                        </Form.Group>
                                                                        : ''
                                                                    } {values.length > 2 ?
                                                                        <Form.Group as={Col} md={colwidth} >
                                                                            {this.state.editMode ?
                                                                                <React.Fragment>
                                                                                    <Form.Control
                                                                                        type="text"
                                                                                        name="LoaiTaiLieu"
                                                                                        disabled="true"
                                                                                        value={values[2].LoaiTaiLieu || ''}
                                                                                        onChange={(e) => {
                                                                                            let value = e.target.value;
                                                                                        }}
                                                                                        onBlur={handleBlur}
                                                                                        isInvalid={touched.LoaiTaiLieu && !!errors.LoaiTaiLieu}
                                                                                    />
                                                                                    <Form.Control.Feedback type="invalid">
                                                                                        {errors.LoaiTaiLieu}
                                                                                    </Form.Control.Feedback>
                                                                                </React.Fragment> :
                                                                                <p className="form-control-static  " >{values.LoaiTaiLieu}</p>
                                                                            }
                                                                        </Form.Group>
                                                                        : ''
                                                                    }
                                                                </Form.Row>
                                                                <React.Fragment>
                                                                    <MultiDynamicFields
                                                                        dataDanhMucs={this.state.dataDanhMucs}
                                                                        lstvalues={values}
                                                                        handleChange={handleChange}
                                                                        errors={errors}
                                                                        handleBlur={handleBlur}
                                                                        touched={touched}
                                                                        colwidth={colwidth}
                                                                        setFieldValue={setFieldValue}
                                                                        loaiTaiLieuFields={this.state.loaiTaiLieuFields}
                                                                        service={this.context.TaiLieuService}
                                                                        editMode={this.state.editMode}
                                                                    />
                                                                </React.Fragment>
                                                            </div>
                                                        </Tab>
                                                        <Tab eventKey="canhanxuly" title="Cá nhân xử lý">
                                                            {
                                                                (this.state.tabactive == 'canhanxuly' && this.state.id) ?
                                                                    (
                                                                        <Table striped bordered hover size="sm">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th style={{"width": '70px'}}>STT</th>
                                                                                    <th>Chức năng</th>
                                                                                    <th>Thời gian</th>
                                                                                    <th>Người xử lý</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                {
                                                                                    dataLog && dataLog.map((item, index) => {
                                                                                        return (
                                                                                            <tr key={index}>
                                                                                                <td className="text-center">{index + 1}</td>
                                                                                                <td>{item.actionName} </td>
                                                                                                <td className="text-center">{this.context.TaiLieuService.formatDateTime(item.created)}</td>
                                                                                                <td className="">{item.createdBy}</td>
                                                                                            </tr>
                                                                                        )
                                                                                    })
                                                                                }
                                                                            </tbody>
                                                                        </Table>
                                                                    ) : ''
                                                            }
                                                        </Tab>
                                                    </Tabs>
                                                </Card.Header>
                                            </Card>
                                        </Split>
                                    </CardGroup>
                                </Modal.Body>
                            </Form>
                        )}
                    </Formik>
                </Modal>
            );
        else
            return '';
    }
}
export {KiemTraViewComponent};