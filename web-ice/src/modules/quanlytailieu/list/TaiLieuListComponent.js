import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Breadcrumb, Card, Button, ButtonGroup, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Page, GridView, FormSelect } from 'shared/components';
import { Formik } from 'formik';
import { Context } from '../TaiLieuService';
import { TaiLieuAction } from 'redux/actions';
import { NhapTaiLieuFormComponent } from '../form/NhapTaiLieuFormComponent';
import { TAILIEU_TITLE } from 'redux/tailieu/TaiLieuConstant';
import { KiemTraViewComponent } from '../form/KiemTraViewComponent';
import { TaiLieuFormComponent } from '../form/TaiLieuFormComponent';
import { toast } from 'react-toastify';
import DatePicker from 'react-datepicker';
import reactSplit from 'react-split/dist/react-split';
import { http } from 'shared/utils';
class TaiLieuList extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        setData: PropTypes.func,
        setMeta: PropTypes.func,
        data: PropTypes.array,
        meta: PropTypes.object
    }
    static contextType = Context;
    constructor(props) {
        super(props);
        
        this.state = {
            loading: false, datafolders: [],
            dataphongs: [],
            datathoiGianBaoQuans: [],
            datamucLucSos: [],
            datahopSos: [],
            datahoSoSos: [],
            dataTrangthai: [],
            dataFolderFieldHienThi: [],
            datachildFolder: [],
            metaDefault:{}
        };
        this.subscriptions = {};
    }

    componentDidMount() {
        
        let newMeta = Object.assign({}, this.props.meta, {
            filter: {},search:''
        });
        this.props.setMeta({filter: {}, search: ''});
        this.fetchData(newMeta);

        this.context.TaiLieuService.getDataEnum("TaiLieu_TrangThai").subscribe(res => { this.setState({ dataTrangThai: res }); });

        this.context.TaiLieuService.getFieldFolderHienThi(true).subscribe(res => {
            this.setState({
                dataFolderFieldHienThi: res

            });
            let { datachildFolder } = this.state;



        });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    fetchData(meta) {
        this.setState({ loading: true });
        meta.filter.ChucNangXem = 14;
        this.subscriptions['getmany'] = this.context.TaiLieuService.getMany(meta).subscribe(res => {
            this.props.setData([]);
            this.props.setData(res.data || []);
            this.props.setMeta({
                page: res.currentPage,
                page_size: res.pageSize,
                total: res.totalRows
            });
            this.setState({ loading: false });
        }, err => {
            console.log(err);
            this.setState({ loading: false });
        });
    }
    handleChange(res) {

        this.context.TaiLieuService.handleChange(res, this);
    }
    handleClose(res) {
        if (res) {
            this.fetchData(this.props.meta);
        }
    }
    addNew() {
        this.context.TaiLieuService.sendToForm.next({
            ChucNang: 1,
            id: null,
            action: 'new',
            isShow: true
        });
    }


    view(item) {
        this.context.TaiLieuService.sendToViewKiemTra.next({
            ChucNang: 3,
            id: item.id,
            action: 'read',
            isShow: true
        });
    }
    viewEdit(item) {
        this.context.TaiLieuService.sendToForm.next({
            ChucNang: 1,
            id: item.id,
            action: 'edit',
            isShow: true
        });
    }
    async delete(item) {
        await this.context.TaiLieuService.DeleteAction(item, this);
    }

    getLookUpTitle(item) {
        if (item && item.includes(";#"))
            return item.split(";#")[1];
        return "";
    }

    convertToJson(data) {
        var { dataFolderFieldHienThi } = this.state;
        var objectreturn = [];
        if (data) {
            dataFolderFieldHienThi.forEach(function (item) {
                if (data["sf_" + item.maThuMuc] && item?.quanLyDanhMuc_Id < 0) {
                    objectreturn.push({ FieldDM: "sh_" + item.maThuMuc, Field: "sf_" + item.maThuMuc, Value: data["sf_" + item.maThuMuc] || '' })
                } else {
                    objectreturn.push({ Field: "sf_" + item.maThuMuc, Value: data["sf_" + item.maThuMuc] || '' })
                }
            })

        }

        return JSON.stringify(objectreturn);
    }
    viewStatus(item) {
        switch (item?.trangThai) {
            case 1:
                return <span style={{ fontSize: '12px' }} className="badge badge-secondary">{item.statusName}</span>;
                break;
            case 2:
                return <span style={{ fontSize: '12px' }} className="badge badge-warning">{item.statusName}</span>;
                break;
            case 3 || 4 || 5:
                return <span style={{ fontSize: '12px' }} className="badge badge-primary">{item.statusName}</span>;
                break;
            case 6:
                return <span style={{ fontSize: '12px' }} className="badge badge-success">{item.statusName}</span>;
                break;
            case 8:
                return <span style={{ fontSize: '12px' }} className="badge badge-danger">{item.statusName}</span>;
                break;
            default:
                return <span style={{ fontSize: '12px' }} className="badge badge-info">{item.statusName}</span>;

        }
    }
    Import() {
        document.getElementById("selectFile").click();
    }
    onChangeHandler = event => {
        let files = event.target.files;
        this.setState({
            loading: true
        });
        const headers = {
            'Content-Type': 'multipart/form-data',
        }
        const formData = new FormData()
        for (let i = 0; i < files.length; i++) {
            formData.append(`file`, files[i])
        }
        http.post(`api/configexcel/import/document`, formData, {
            headers: headers
        }).subscribe(
            res => { // then print response status
                toast.success('Import thành công');

                this.setState({
                    loading: false
                });
                document.getElementById("selectFile").value = '';
                this.fetchData(this.props.meta);
            },
            error => {
                this.setState({
                    loading: false
                });
                toast.error('Đã có lỗi xảy ra');
                if (document.getElementById("selectFile"))
                    document.getElementById("selectFile").value = '';
            }
        )
    }
    render() {
        return (
            <Page>
                <Page.Header>
                    <Row className="mb-2">
                        <Col sm={6}>
                            <h5>Danh sách {TAILIEU_TITLE}</h5>
                        </Col>
                        <Col sm={6}>
                            <Breadcrumb className="float-sm-right">
                                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/' }}>Tổng quan</Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    {TAILIEU_TITLE}
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Page.Header>
                <Page.Content>
                    <Card>
                        <Card.Body>
                            <GridView
                                loading={this.state.loading}
                                handleChange={this.handleChange.bind(this)}>
                                <GridView.Header
                                    enableReinitialize={true}
                                    keySearch={this.state.metaDefault.search}
                                    ActionBar={
                                        <React.Fragment>
                                            <Button size="sm" className="ml-2" type="button" variant="success" onClick={() => { this.Import(); }}>
                                                <span className="iconify" data-icon="fa-solid:file-import" data-inline="false" />
                                                Import
                                            </Button>
                                            <Button className="ml-2" variant="primary" size="sm" onClick={this.addNew.bind(this)} >
                                                <span className="iconify fa" data-icon="fa-solid:plus" data-inline="false"></span>
                                                Thêm mới
                                            </Button>
                                            <input id='selectFile' hidden type="file" accept=".xlsx" onChange={this.onChangeHandler} multiple={this.props.multiple} />
                                        </React.Fragment>
                                    }
                                    AdvanceFilter={
                                        < Formik onSubmit={(values) => {
                                            console.log(values);

                                            const result = {};
                                            Object.keys(values)
                                                .forEach(key => result[key] = values[key]);


                                            var ThongTinNhap = this.convertToJson(values);
                                            result["SearchByFieldFoldersStr"] = ThongTinNhap;
                                            this.handleChange({ event: 'changeFilter', data: { filter: result } });
                                        }}
                                            enableReinitialize={true}
                                            initialValues={this.props.meta.filter}
                                        >
                                            {({ handleSubmit, handleChange, handleBlur, values, setFieldValue }) => (
                                                <Form noValidate onSubmit={handleSubmit}>
                                                    <Row className="form-group">
                                                        <Col sm={2} className="text-left">Ngày tạo</Col>
                                                        <Col sm={2}>
                                                            <DatePicker
                                                                className="form-control custom-datepicker"
                                                                todayButton="Hôm nay"
                                                                isClearable
                                                                showMonthDropdown
                                                                showYearDropdown
                                                                dropdownMode="select"
                                                                placeholderText="Từ ngày"
                                                                maxDate={values["enddate"] ? new Date(values["enddate"]) : null}
                                                                onChange={(date) => {
                                                                    setFieldValue("startdate", this.context.TaiLieuService.changeFormatDateTime(date));
                                                                }}

                                                                selected={values["startdate"] ? new Date(values["startdate"]) : null}
                                                                dateFormat="dd/MM/yyyy"
                                                            />
                                                        </Col>
                                                        <Col sm={2}>
                                                            <DatePicker
                                                                className="form-control custom-datepicker"
                                                                todayButton="Hôm nay"
                                                                isClearable
                                                                showMonthDropdown
                                                                showYearDropdown
                                                                dropdownMode="select"
                                                                placeholderText="Đến ngày"

                                                                minDate={values["startdate"] ? new Date(values["startdate"]) : null}
                                                                onChange={(date) => {
                                                                    setFieldValue("enddate", this.context.TaiLieuService.changeFormatDateTime(date));
                                                                }}

                                                                selected={values["enddate"] ? new Date(values["enddate"]) : null}
                                                                dateFormat="dd/MM/yyyy"
                                                            />
                                                        </Col>
                                                        <Col sm={2} className="text-left">Trạng thái</Col>
                                                        <Col sm={4}>
                                                            <FormSelect className="form-control-sm"
                                                                placeholder="Chọn trạng thái"
                                                                name="trangthai"
                                                                data={this.state.dataTrangThai}
                                                                mode="simpleSelect"
                                                                value={values.trangthai}
                                                                onChange={handleChange}
                                                            ></FormSelect>
                                                        </Col>
                                                    </Row>

                                                    <Row className="form-group fielddong">
                                                        {
                                                            this.state.dataFolderFieldHienThi && this.state.dataFolderFieldHienThi.map((itemfield, index) => {

                                                                return (<React.Fragment key={index}>


                                                                    <Col sm={2} className="text-left">{itemfield.tieuDe}</Col>
                                                                    <Col sm={4}>

                                                                        <React.Fragment>
                                                                            <Form.Control
                                                                                placeholder={itemfield.tieuDe}
                                                                                type="text"
                                                                                name={"sf_" + itemfield.maThuMuc}
                                                                                //value={values.TieuDe || ''}
                                                                                onChange={handleChange}
                                                                                onBlur={handleBlur}

                                                                            />
                                                                        </React.Fragment>
                                                                    </Col>
                                                                </React.Fragment>)
                                                            })
                                                        }
                                                    </Row>

                                                    <Row className="form-group">
                                                        <Col sm={12} className="text-center">
                                                            <Button size="sm" type="submit" variant="primary" >
                                                                <span className="iconify" data-icon="fa-solid:search" data-inline="false" />
                                                                Tìm kiếm
                                                            </Button>
                                                        </Col>
                                                    </Row>
                                                </Form>
                                            )}
                                        </Formik >
                                    }
                                >
                                </GridView.Header>
                                <GridView.Table
                                    className="col-12"
                                    noSelected={true}
                                    data={this.props.data}
                                    keyExtractor={({ item }) => {
                                        return item.id;
                                    }}
                                    sort={this.props.meta.sort}
                                    page={this.props.meta.page}
                                    page_size={this.props.meta.page_size}
                                    total={this.props.meta.total}
                                >

                                    <GridView.Table.Column style={{ width: '20px' }}
                                        title="STT"
                                        className="text-center"
                                        body={({ index }) => (
                                            <span>{index + 1 + (this.props.meta.page - 1) * this.props.meta.page_size}</span>
                                        )} />
                                    <GridView.Table.Column style={{ width: '100px' }}
                                        //className="text-center"

                                        title="Trạng thái"
                                        //sortKey="trangthai"
                                        body={({ item }) => (
                                            this.viewStatus(item)
                                            // <span className="badge badge-success">{item.statusName}</span>
                                        )} />

                                    <GridView.Table.Column style={{}} title="Tiêu đề" body={({ item }) => (<span>{item.tieuDe}</span>)} />
                                    {/* sortKey="TieuDe" */}
                                    {
                                        this.state.dataFolderFieldHienThi && this.state.dataFolderFieldHienThi.map((itemfield, index) => {

                                            if (!itemfield.quanLyDanhMuc_Id)
                                                return <GridView.Table.Column key={index} style={{}} title={itemfield.tieuDe} body={({ item }) => (<span>{this.getLookUpTitle(item.fieldFolder[itemfield.maThuMuc])}</span>)} />
                                            // sortKey={"sf_" + itemfield.maThuMuc}
                                            else
                                                return <GridView.Table.Column key={index} style={{}} title={itemfield.tieuDe} body={({ item }) => (<span>{this.getLookUpTitle(item.fieldTTHoSo[itemfield.maThuMuc])}</span>)} />
                                            // sortKey={"sh_" + itemfield.maThuMuc}
                                        })
                                    }

                                    <GridView.Table.Column className="text-center" style={{ width: '100px' }} title="Ngày tạo" sortKey="NgayTao" body={({ item }) => (<span>{this.context.TaiLieuService.formatDateTime(item.ngayTao)}</span>)} />


                                    <GridView.Table.Column style={{ width: '100px' }} className="view-action"
                                        title="Tác vụ"
                                        body={({ item }) => (
                                            <ButtonGroup size="sm">

                                                <React.Fragment>

                                                    <Button variant="outline-info" title="Xem chi tiết tài liệu" onClick={() => { this.view(item); }}>
                                                        <span className="iconify" data-icon="fa-solid:eye" data-inline="false"></span>
                                                    </Button>
                                                    {/*<Button variant="outline-info" title="Nhập liệu" onClick={() => { this.viewEdit(item); }}>*/}
                                                    {/*    <span className="iconify" data-icon="fa-solid:edit" data-inline="false"></span>*/}
                                                    {/*</Button>*/}
                                                    <Button variant="outline-danger" onClick={() => { this.delete(item); }}>
                                                        <span className="iconify" data-icon="fa-solid:trash-alt" data-inline="false"></span>
                                                    </Button>
                                                </React.Fragment>

                                            </ButtonGroup>
                                        )} />
                                </GridView.Table>
                            </GridView>
                        </Card.Body>
                    </Card>

                    <TaiLieuFormComponent onClose={this.handleClose.bind(this)} />
                    <NhapTaiLieuFormComponent onClose={this.handleClose.bind(this)} />
                    <KiemTraViewComponent onClose={this.handleClose.bind(this)} />
                </Page.Content >
            </Page >
        );
    }
}
const mapStateToProps = (state) => {
    return {
        data: state.TaiLieu.TaiLieuList,
        meta: state.TaiLieu.meta
    };
};
const TaiLieuListComponent = connect(mapStateToProps, TaiLieuAction)(TaiLieuList);
export { TaiLieuListComponent };