import { createContext } from 'react';
import { BaseService } from 'shared/services';
import { Subject } from 'rxjs';
import { http } from 'shared/utils';
import { map } from 'rxjs/operators';

class TaiLieuService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/tailieu' }, props);
        super(_props);
        this.sendToForm = new Subject();
        this.sendToFormNhapTaiLieu = new Subject();
        this.sendToViewKiemTra = new Subject();
    }
   
    getFieldFolderHienThi(HienThiDanhSach) {
        return http.get(`api/folderconfigs?page=1&itemsPerPage=-1&HienThiDanhSach=${HienThiDanhSach}&sortDesc=false&SortBy=ViTri`).pipe(map((res) => {
            return res.data;
        }));
    }

    getLoaiTaiLieu() {
        return http.get('api/loaitailieu?page=1&itemsPerPage=-1&sortDesc=false').pipe(map((res) => {
            if (res.data) {
                return res.data.map(x => {
                    x.value = x.id;
                    x.label = x.tieuDe;
                    return x;
                });
            }
        }));
    }
    getLoaiTaiLieuFields(id) {
        id = id ? id : 0;
        return http.get(`api/cauhinhtailieu/getFields/${id}?page=1&itemsPerPage=-1`).pipe(map((res) => {
            if (res.data) {
                return res.data;
            }
        }));
    }
    getHoSoFields() {
        return http.get(`api/folderconfigs/getfieldbyconfig?page=1&itemsPerPage=-1`).pipe(map((res) => {
            if (res) {
                return res;
            }
        }));
    }
    getLstForCategory(lstId) {
        var temp =lstId?.length>0 ?lstId.join() : "";
        var filter ={};
        const params=Object.assign({},{
            page: 1,
            itemsPerPage: -1,
            FilterText: "",
            lstId:temp
        }
        , filter);
        return http.get(`api/loaitailieu/getlstforcategory`,{ params: params }).pipe(map((res) => {
            if (res) {
                return res.map(x => {
                    x.value = x.id;
                    x.label = x.tieuDe;
                    return x;
                });
            }
        }));
    }

    getDanhMucs(ids) {
        return http.get('api/danhmuc?page=1&itemsPerPage=-1&sortDesc=false&quanLyDanhMucStr=' + ids).pipe(map((res) => {
            if (res.data) {
                return res.data.map(x => {
                    x.value = x.id;
                    x.label = x.tieuDe;
                    return x;
                });
            }

        }));
    }





    getLstTaiLieuCungBo(UrlTaiLieuCungBo, ChucNangXem) {
        ChucNangXem = ChucNangXem ? ChucNangXem : 1;
        return http.get('api/tailieu?page=1&itemsPerPage=-1&sortBy=T_ID&sortDesc=true'  + '&UrlTaiLieuCungBo=' + UrlTaiLieuCungBo).pipe(map((res) => {
            if (res.data) {
                return res.data;
            }
        }));
    }
    getLstlogTaiLieu(id) {
        // idfolder = idfolder?idfolder:0;
        return http.get('api/logs/logtailieu?page=1&itemsPerPage=-1&sortBy=Created&sortDesc=false&ObjectID=' + id).pipe(map((res) => {
            if (res.data) {
                return res.data;
            }
        }));
    }
    TraLai(id) {
        return http.put(`api/tailieu/TraLai/${id}`, { id: id });
    }
    updatefiletailieu(data, obj) {
        const headers = {
            'Content-Type': 'multipart/form-data',
        };
        const formData = new FormData();
        if (data && data.fileAttachs) {
            for(let i=0;i<data.fileAttachs.length;i++) {
                formData.append(`files`,data.fileAttachs[i]);
            }
        }
        formData.append(obj, JSON.stringify(data));
    
        return http.put(`fileupload/changefile/${data.Id}`, formData, {
            headers: headers
        });
    }

    getLstLoaiTaiLieu() {
        return http.get('api/loaitailieu?page=1&itemsPerPage=-1&sortDesc=false').pipe(map((res) => {
            if (res.data) {
                return res.data.map(x => {
                    x.value = x.id;
                    x.label = x.tieuDe;
                    return x;
                });
            }
        }));
    }
    createDoc(data,obj) {

        const headers={
            'Content-Type': 'multipart/form-data',
        };
        const formData=new FormData();
        if(data&&data.fileAttachs) {
            for(let i=0;i<data.fileAttachs.length;i++) {
                formData.append(`files`,data.fileAttachs[i]);
            }
        }
        formData.append(obj,JSON.stringify(data));
        return http.post(`api/tailieu/create`,formData,{
            headers: headers
        });
    }

}
const _TaiLieuService = new TaiLieuService();
const Context = createContext();
export { Context, _TaiLieuService, TaiLieuService };