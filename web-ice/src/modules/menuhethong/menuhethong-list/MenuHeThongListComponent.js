import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Row, Col, Breadcrumb, Card, Button, ButtonGroup, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {Page, GridView, FormSelect} from 'shared/components';
import {Formik} from 'formik';
import {Context} from '../MenuHeThongService';
import {MenuHeThongAction} from '../../../redux/actions';
import {MenuHeThongFormComponent} from '../menuhethong-form/MenuHeThongFormComponent';
class MenuHeThongList extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        setData: PropTypes.func,
        setMeta: PropTypes.func,
        data: PropTypes.array,
        meta: PropTypes.object,

    }
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            dataMenu: [],
            loading: false,
            meta: {
                filter: {
                    idMenuCha: 0,
                },
            }
        };
        this.subscriptions = {};

    }
    componentDidMount() {

        /* super(this.props);*/
        this.fetchData(this.props.meta);
        this.context.menuhethongService.getSelectMenu().subscribe(res => {
            this.setState({dataMenu: res});
        });
    }

    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    fetchData(meta) {
        this.setState({loading: true});
        this.subscriptions['getmany'] = this.context.menuhethongService.getMany(meta).subscribe(res => {
            this.props.setData(res.data || []);
            this.props.setMeta({
                page: res.currentPage,
                page_size: res.pageSize,
                total: res.totalRows
            });
            this.setState({loading: false});
        }, err => {
            console.log(err);
            this.setState({loading: false});
        });
    }

    handleChange(res) {
        this.context.menuhethongService.handleChange(res, this);
    }
    handleClose(res) {
        if (res) {
         
            if (this.state.newMeta1)

                this.fetchData(this.state.newMeta1);
            else
            this.fetchData(this.props.meta);
        }
    }
    addNew() {
        this.context.menuhethongService.sendToForm.next({
            id: null,
            action: 'new',
            isShow: true
        });
    }
    viewDetail(item) {
        this.context.menuhethongService.sendToForm.next({
            id: item.id,
            action: 'read',
            isShow: true
        });

    }
    viewEdit(item) {
        this.context.menuhethongService.sendToForm.next({
            id: item.id,
            action: 'edit',
            isShow: true
        });
    }
    async delete(item) {
        await this.context.menuhethongService.DeleteAction(item, this);
    }
    async lock(item) {
        let confirm = "Bạn có muốn khóa quyền này";
        if (item.lock === 1)
            confirm = "Bạn có muốn mở khóa quyền này";
        if (await this.context.beh.confirm(confirm)) {
            this.context.menuhethongService.lock(item.id).subscribe(() => {
                this.fetchData(this.props.meta);
            });
        }
    }
    render() {
        return (
            <Page>
                <Page.Header>
                    <Row className="mb-0">
                        <Col sm={6}>
                            <h5 className="mb-0">Danh sách menu</h5>
                        </Col>
                        <Col sm={6}>
                            <Breadcrumb className="float-sm-right">
                                <Breadcrumb.Item linkAs={Link} linkProps={{to: '/'}}>Tổng quan</Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    Quản trị menu
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Page.Header>
                <Page.Content>
                    <Card>
                        <Card.Body className="p-2">
                            <GridView
                                loading={this.state.loading}
                                handleChange={this.handleChange.bind(this)}>
                                <GridView.Header
                                    keySearch={this.props.meta.search}
                                    ActionBar={
                                        <Button variant="primary" size="sm" onClick={this.addNew.bind(this)} >
                                            <span className="iconify fa" data-icon="fa-solid:plus" data-inline="false"></span>
                                        Thêm mới
                                        </Button>
                                    }
                                    AdvanceFilter={
                                        <Formik onSubmit={(values) => {
                                            this.handleChange({event: 'changeFilter', data: {filter: values}});
                                        }}
                                            enableReinitialize={true}
                                            initialValues={this.state.meta.filter}
                                        >
                                            {({handleSubmit, handleChange, values}) => (
                                                <Form noValidate onSubmit={handleSubmit}>
                                                    <Row>
                                                        <Col sm={4}>
                                                            <FormSelect className="form-control-sm"
                                                                name="ParentId"
                                                                placeholder="Chọn menu cha"
                                                                data={this.state.dataMenu}
                                                                mode="simpleSelect"
                                                                value={values.ParentId}
                                                                onChange={handleChange}
                                                            ></FormSelect>
                                                        </Col>
                                                        <Col sm={2} className="">
                                                            <Button size="sm" type="submit" variant="primary">
                                                                <span className="iconify" data-icon="fa-solid:search" data-inline="false" />
                                                                Tìm kiếm
                                                            </Button>
                                                        </Col>
                                                    </Row>
                                                </Form>
                                            )}
                                        </Formik>
                                    }
                                >
                                </GridView.Header>
                                <GridView.Table
                                    className="col-12"
                                    noSelected={true}
                                    data={this.props.data}
                                    keyExtractor={({item}) => {
                                        return item.id;
                                    }}
                                    sort={this.props.meta.sort}
                                    page={this.props.meta.page}
                                    page_size={this.props.meta.page_size}
                                    total={this.props.meta.total}
                                >
                                    <GridView.Table.Column style={{width: '6%'}}
                                        title="STT"
                                        className="text-center"
                                        body={({index}) => (
                                            <span>{index + 1 + (this.props.meta.page - 1) * this.props.meta.page_size}</span>
                                        )} />
                                    <GridView.Table.Column style={{}}
                                        title="Tên menu"
                                        sortKey="title"
                                        body={({item}) => (
                                            <span>{item.title}</span>
                                        )} />
                                    <GridView.Table.Column style={{}}
                                        title="Đường dẫn"
                                        sortKey="url"
                                        body={({item}) => (
                                            <span>{item.url}</span>
                                        )} />
                                    <GridView.Table.Column style={{}}
                                        title="Menu cha"
                                        sortKey="parentId"
                                        body={({item}) => (
                                            <span>{item.parent}</span>
                                        )} />

                                    <GridView.Table.Column
                                        style={{width: '100px'}}
                                        className="view-action"
                                        title="Tác vụ"
                                        body={({item}) => (
                                            <ButtonGroup size="sm">
                                                <Button variant="outline-info" onClick={() => {this.viewDetail(item);}}>
                                                    <span className="iconify" data-icon="fa-solid:eye" data-inline="false"></span>
                                                </Button>
                                                <Button variant="outline-info" onClick={() => {this.viewEdit(item);}}>
                                                    <span className="iconify" data-icon="fa-solid:edit" data-inline="false"></span>
                                                </Button>
                                                <Button variant="outline-danger" onClick={() => {this.delete(item);}}>
                                                    <span className="iconify" data-icon="fa-solid:trash-alt" data-inline="false"></span>
                                                </Button>
                                            </ButtonGroup>
                                        )} />
                                </GridView.Table>
                            </GridView>
                        </Card.Body>
                    </Card>
                    <MenuHeThongFormComponent onClose={this.handleClose.bind(this)} />
                </Page.Content >
            </Page >
        );
    }
}
const mapStateToProps = (state) => {
    return {
        data: state.menuhethong.menuhethongList,
        meta: state.menuhethong.meta
    };
};
const MenuHeThongListComponent = connect(mapStateToProps, MenuHeThongAction)(MenuHeThongList);
export {MenuHeThongListComponent};