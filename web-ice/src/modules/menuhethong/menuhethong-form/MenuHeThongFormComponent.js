import React from 'react';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal } from 'react-bootstrap';
import { Formik } from 'formik';
import { string, object } from 'yup';
import { LoadingComponent, FormSelect } from 'shared/components';
import { Context } from '../MenuHeThongService';
import { toast } from 'react-toastify';
const schema = object({
    title: string().nullable().trim().required('Tên menu không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
    url: string().nullable().trim().required('Đường dẫn không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
    stt: string().nullable().trim().required('Không đúng định dạng'),
    icon: string().nullable().trim().max(250, 'Bạn nhập tối đa 250 ký tự')
});
const userDefault = {
    title: '',
    url: '',
    stt: 1,
    parentId: 0,
    groups: '',
    isShow: true,
    isBlank: false,
    groupIds: [],
    groupTitles: [],
};
class MenuHeThongFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: 0,
            dataMenuCha: [],
            dataGroupMenu: [{ "value": 1, "label": "Quản trị hệ thống" }, { "value": 2, "label": "Kho lưu trữ" }, { "value": 3, "label": "Nhập liệu" }, { "value": 4, "label": "Phân công nhập liệu" }
                , { "value": 5, "label": "Kiểm tra nhập liệu lần 1" }, { "value": 6, "label": "Kiểm tra nhập liệu lần 2" }, { "value": 7, "label": "Thống kê" }],
            dataNhomND: [],
            value: 'fipicon-angle-left',
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.subscriptions['sendToForm'] = this.context.menuhethongService.sendToForm.subscribe(res => {
            switch (res.action) {
                case 'new':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: userDefault, loading: false });
                    // this.context.menuhethongService.getSTT(0).subscribe(res => {
                    //     this.setState({stt: res.stt+1 });                        
                    // });
                    break;
                case 'edit':
                    this.setState({
                        data: userDefault,
                        editMode: true,
                        isShow: res.isShow,
                        action: res.action,
                        id: res.id,
                        loading: true
                    });
                    this.context.menuhethongService.getById(res.id).subscribe(res => {
                        var groupids = [];
                        if (res && res.lstGroups) {
                            for (var i = 0; i < res.lstGroups.length; i++) {
                                groupids.push(res.lstGroups[i].id);
                            }
                        }
                        res.groupIds = groupids;
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                case 'read':
                    this.setState({ editMode: false, isShow: res.isShow, action: res.action, id: res.id, loading: true,data: userDefault, });
                    this.context.menuhethongService.getById(res.id).subscribe(res => {
                        var groupTitles = '';
                        if (res && res.lstGroups) {
                            for (let i = 0; i < res.lstGroups.length; i++) {
                                
                                groupTitles += i === 0 ? `${res.lstGroups[i].title}` : `, ${res.lstGroups[i].title}`
                            }
                        }
                        res.groupTitles=groupTitles;
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                default:
                    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: userDefault });
                    break;
            }
        });
        this.context.menuhethongService.getSelectMenu().subscribe(res => {
            this.setState({ dataMenuCha: res });
        });
        this.context.menuhethongService.getAllGroups().subscribe(res => {
            this.setState({ dataNhomND: res });
        });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    onSubmit(data) {
        let groups = '';
        if (data.groupIds) {
            groups+=",";
            for (let i = 0; i < data.groupIds.length; i++) {
                groups += i === 0 ? `${data.groupIds[i]}` : `,${data.groupIds[i]}`
            }
            groups+=",";
        }
        data.groups = groups;
        this.setState({
            loading: true
        });
        
        if (this.state.id) {
            this.subscriptions['update'] = this.context.menuhethongService.update(data, this.state.id).subscribe(() => {
                toast.success('Cập nhật thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        } else {
            this.subscriptions['create'] = this.context.menuhethongService.create(data).subscribe(() => {
                toast.success('Thêm mới thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        }

    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }

    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    computedTitle() {
        if (this.state.id) {
            if (this.state.editMode) {
                return 'Chỉnh sửa thông tin menu';
            }
            return 'Chi tiết menu';
        }
        return 'Tạo mới menu';
    }
    groupMenuTitle(value) {
        switch (value) {
            case 1:
                return "Quản trị hệ thống";
            case 2:
                return "Kho lưu trữ";
            case 3:
                return "Nhập liệu";
            case 4:
                return "Phân công nhập liệu";
            case 5:
                return "Kiểm tra nhập liệu lần 1";
            case 6:
                return "Kiểm tra nhập liệu lần 2";
            case 7:
                return "Thống kê";
        }
    }
    render() {
        return (
            <Modal size="lg" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                        this.onSubmit(values);
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        setFieldValue,
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        values,
                        touched,
                        errors,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} onKeyPress={(ev) => { ev.stopPropagation(); }}>
                            <Modal.Header closeButton>
                                <Modal.Title>{this.computedTitle()}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body onKeyPress={this.onKeyPress.bind(this)}>
                                <Form.Row>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>Tên menu</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                    </Form.Group>
                                    <Form.Group as={Col} md="10" controlId="title">
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <Form.Control
                                                    type="text"
                                                    name="title"
                                                    value={values.title || ''}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.title && !!errors.title}
                                                />
                                                <Form.Control.Feedback type="invalid">
                                                    {errors.title}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.title}</p>
                                        }

                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>Đường dẫn</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                    </Form.Group>
                                    <Form.Group as={Col} md="5" controlId="url">
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <Form.Control
                                                    type="text"
                                                    name="url"
                                                    value={values.url || ''}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.url && !!errors.url}
                                                />
                                                <Form.Control.Feedback type="invalid">
                                                    {errors.url}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.url}</p>
                                        }
                                    </Form.Group>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>STT</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} md="3" controlId="stt">
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <Form.Control
                                                    type="number"
                                                    name="stt"
                                                    value={values.stt || ''}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.stt && !!errors.stt}
                                                />
                                                <Form.Control.Feedback type="invalid">
                                                    {errors.stt}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.stt}</p>
                                        }
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>Menu cha</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} md="5" controlId="parentId">
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <FormSelect
                                                    name="parentId"
                                                    data={this.state.dataMenuCha}
                                                    value={values.parentId}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.parentId && !!errors.parentId}
                                                    mode="simpleSelect"
                                                ></FormSelect>

                                                <Form.Control.Feedback type="invalid">
                                                    {errors.parentId}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.parent}</p>
                                        }
                                    </Form.Group>
                                    <Form.Group as={Col} md="2" >
                                        <Form.Label>Icon</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} md="3" controlId="icon">
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <Form.Control
                                                    type="text"
                                                    name="icon"
                                                    value={values.icon || ''}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.icon && !!errors.icon}
                                                />
                                                <Form.Control.Feedback type="invalid">
                                                    {errors.icon}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.icon}</p>
                                        }
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="2" >
                                        <Form.Label>Nhóm quyền</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} md="10" controlId="groupIds">
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <FormSelect
                                                    placeholder="Chọn nhóm quyền"
                                                    name="groupIds"
                                                    data={this.state.dataNhomND}
                                                    value={values.groupIds}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.groupIds && !!errors.groupIds}
                                                    mode="multiSelect"
                                                ></FormSelect>
                                                <Form.Control.Feedback type="invalid">
                                                    {errors.groupIds}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.groupTitles}</p>
                                        }
                                    </Form.Group>
                                </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col} md="2">
                                            <Form.Label>Nhóm menu</Form.Label>
                                        </Form.Group>
                                        <Form.Group as={Col} md="4" controlId="groupMenu">
                                            {this.state.editMode ?
                                                <React.Fragment>
                                                    <FormSelect
                                                        name="groupMenu"
                                                        data={this.state.dataGroupMenu}
                                                        value={values.groupMenu}
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        isInvalid={touched.groupMenu && !!errors.groupMenu}
                                                        mode="simpleSelect"
                                                    ></FormSelect>
                                                    <Form.Control.Feedback type="invalid">
                                                        {errors.groupMenu}
                                                    </Form.Control.Feedback>
                                                </React.Fragment> :
                                                <p className="form-control-static">{this.groupMenuTitle(values.groupMenu)}</p>
                                            }
                                        </Form.Group>
                                    <Form.Group as={Col} md="3" controlId="isShow">
                                        {
                                            this.state.editMode ? <Form.Check type="checkbox" onKeyPress={event => {
                                                if (event.key === 'Enter') {
                                                    event.preventDefault();
                                                    setFieldValue("isShow", !values.isShow);
                                                }
                                            }} name="isShow" checked={values.isShow}
                                                onChange={handleChange}
                                                onBlur={handleBlur} label="Hiển thị" /> :
                                                <Form.Check type="checkbox" disabled checked={values.isShow}
                                                   
                                                    label="Hiển thị" />
                                        }

                                    </Form.Group>
                                    <Form.Group as={Col} md="3" controlId="isBlank">
                                        {
                                            this.state.editMode ? <Form.Check type="checkbox" onKeyPress={event => {
                                                if (event.key === 'Enter') {
                                                    event.preventDefault();
                                                    setFieldValue("isBlank", !values.isBlank);
                                                }
                                            }} name="isBlank" checked={values.isBlank}
                                                onChange={handleChange}
                                                onBlur={handleBlur} label="Cửa sổ mới" /> :
                                                <Form.Check type="checkbox" disabled checked={values.isBlank}
                                                    label="Cửa sổ mới" />
                                        }
                                        </Form.Group>
                                       
                                </Form.Row>
                            </Modal.Body>
                            <Modal.Footer>

                                {this.state.editMode ?
                                    <Button type="submit">Lưu lại</Button>
                                    :
                                    ''
                                }
                                <Button variant="default" onClick={() => { this.handleClose(false); }}>
                                    Đóng
                                </Button>
                            </Modal.Footer>
                        </Form>
                    )}
                </Formik>
            </Modal>

        );
    }
}


export { MenuHeThongFormComponent };