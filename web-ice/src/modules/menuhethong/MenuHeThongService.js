import { createContext } from 'react';
import { BaseService } from 'shared/services';
import { Subject } from 'rxjs';
import { http } from 'shared/utils';
import { map } from 'rxjs/operators';
class MenuHeThongService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/menumanager' }, props);
        super(_props);
        this.sendToForm = new Subject();  
    }      
    getMenuCha() {
        return http.get('api/menumanager').pipe(map((res) => {
            if (res.data) {
                return res.data.map(x => {
                    x.value = x.id;
                    x.label = x.title;
                    return x;
                });
            }
        }));
    }
    getSTT(id) {
        return http.get('api/menumanager/getstt/'+id);
    }
    getSelectMenu(){
        return http.get('api/menumanager/selectmenu').pipe(map((res) => {
            if (res) {
                return res.map(x => {
                    x.value = x.id;
                    x.label = x.title;
                    return x;
                });
            }
        }));
    }
    getTree() {
        return http.get('api/menumanager/treemenu');
    }
    getAllGroups() {
        return http.get('api/groups').pipe(map((res) => {
            if (res.data) {
                return res.data.map(x => {
                    x.value = x.id;
                    x.label = x.title;
                    return x;
                });
            }
        }));
    }
}
const Context = createContext();
const _menuhethongService = new MenuHeThongService();
export { Context, _menuhethongService, MenuHeThongService };