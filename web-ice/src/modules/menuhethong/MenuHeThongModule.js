import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context, _menuhethongService } from 'modules/menuhethong/MenuHeThongService';
import { MenuHeThongListComponent } from './menuhethong-list/MenuHeThongListComponent';
import { MenuHeThongFormComponent } from './menuhethong-form/MenuHeThongFormComponent';



class MenuHeThongModule extends Component {
    static propTypes={
        match:PropTypes.object
    }
    render() {
        let {path}=this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({beh})=>(
                        <Context.Provider value={{
                            menuhethongService: _menuhethongService,
                            beh:beh
                        }} >
                         
                            <Switch>
                                <Route path={`${path}/view`} render={(props) => <MenuHeThongListComponent {...props} ></MenuHeThongListComponent>} ></Route>
                                <Route path={`${this.props.match.path}/form/:id`} render={(props) => <MenuHeThongFormComponent {...props} />} />
                                <Route path={this.props.match.path} render={(props) => <MenuHeThongListComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { MenuHeThongModule };