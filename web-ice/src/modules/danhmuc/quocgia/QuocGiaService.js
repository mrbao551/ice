import { createContext } from 'react';
import { BaseService } from 'shared/services';
import { Subject } from 'rxjs';
import { http } from 'shared/utils';
import { map } from 'rxjs/operators';
class QuocGiaService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/quocgia' }, props);
        super(_props);
        this.sendToForm = new Subject();        
    }   
}
const _QuocGiaService= new QuocGiaService();
const Context = createContext();
export { Context, _QuocGiaService, QuocGiaService };