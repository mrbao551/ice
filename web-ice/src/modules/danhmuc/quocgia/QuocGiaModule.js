import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context,_QuocGiaService } from './QuocGiaService';
import { QuocGiaListComponent } from './list/QuocGiaListComponent';
import { QuocGiaFormComponent } from './form/QuocGiaFormComponent';
class QuocGiaModule extends Component {
    static propTypes={
        match:PropTypes.object
    }
    render() {
        let {path}=this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({beh})=>(
                        <Context.Provider value={{
                            QuocGiaService: _QuocGiaService,
                            beh:beh
                        }} >
                            <Switch>
                                <Route path={`${path}/view`} render={(props)=><QuocGiaListComponent {...props} ></QuocGiaListComponent>} ></Route>
                                <Route path={`${path}/form/:id`} render={(props) => <QuocGiaFormComponent {...props} />} />
                                <Route path={path} render={(props) => <QuocGiaListComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { QuocGiaModule };