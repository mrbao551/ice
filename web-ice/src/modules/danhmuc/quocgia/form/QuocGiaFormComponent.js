import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal, Card } from 'react-bootstrap';
import { Formik } from 'formik';
import { string, object } from 'yup';
import { LoadingComponent } from 'shared/components';
import { Context } from '../QuocGiaService';
import { QUOCGIA_TITLE } from 'redux/danhmuc//quocgia/QuocGiaConstant';
import { toast } from 'react-toastify';
const schema = object({
    ma: string().trim().nullable().required('Mã quốc gia không được để trống').max(100, 'Bạn nhập tối đa 100 ký tự'),
    ten: string().trim().nullable().required('Tên quốc gia không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
    moTa: string().nullable().trim().max(1000, 'Bạn nhập tối đa 1000 ký tự'),
});
const userDefault = {
    ma: '',
    ten: '',
    moTa: '',
};
class QuocGiaFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.setInputFocus = this.setInputFocus.bind(this);
        this.state = {
            data: userDefault,
            listGroup: [],
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.subscriptions['sendToForm'] = this.context.QuocGiaService.sendToForm.subscribe(res => {
            switch (res.action) {
                case 'new':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: userDefault });
                    this.loadDataSelect();
                    break;
                case 'edit':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.QuocGiaService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false,
                        });
                    });
                    this.loadDataSelect();
                    break;
                case 'read':
                    this.setState({ editMode: false, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.QuocGiaService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                //default:
                //    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: userDefault });
                //    this.loadDataSelect();
                //    break;
            }
        });
    }
    loadDataSelect() {
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    componentDidUpdate(prevProps, prevState) {
        this.setInputFocus();
    }
    setInputFocus() {
        if (this.myInputRef)
            // eslint-disable-next-line react/no-find-dom-node
            ReactDOM.findDOMNode(this.myInputRef).focus();
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        if (this.state.id) {
            this.subscriptions['update'] = this.context.QuocGiaService.update(data, this.state.id).subscribe(() => {
                this.handleClose(true);
                this.setState({
                    loading: false
                });
                toast.success('Cập nhật thành công');
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });
        } else {
            this.subscriptions['create'] = this.context.QuocGiaService.create(data, this.state.id).subscribe(() => {
                this.handleClose(true);
                this.setState({
                    loading: false
                });
                toast.success('Thêm mới thành công');
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });
        }
    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }
    }
    onKeyPress(ev) {
        this.context.QuocGiaService.onKeyPress(ev);
    }
    computedTitle() {
        if (this.state.id) {
            if (this.state.editMode) {
                return 'Chỉnh sửa thông tin ' + this.context.QuocGiaService.lowerCaseStartChar(QUOCGIA_TITLE);
            }
            return 'Chi tiết ' + this.context.QuocGiaService.lowerCaseStartChar(QUOCGIA_TITLE);
        }
        return 'Thêm mới ' + this.context.QuocGiaService.lowerCaseStartChar(QUOCGIA_TITLE);
    }
    render() {
        return (
            <Modal size="lg" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                        this.onSubmit(this.context.QuocGiaService.trimString(values));
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        setFieldValue,
                        values,
                        touched,
                        errors,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} onKeyPress={(ev) => { ev.stopPropagation(); }}>
                            <Modal.Header closeButton>
                                <Modal.Title>{this.computedTitle()}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body >
                                <Card>
                                    <Card.Body>
                                        <Form.Row>
                                            <Form.Group as={Col} md="12" controlId="ma">
                                                <Form.Label>Mã quốc gia{this.state.editMode ? <span className="text-danger">(*)</span> : ''}</Form.Label>
                                                {this.state.editMode ?
                                                    <React.Fragment>
                                                        <Form.Control
                                                            type="text"
                                                            ref={c => (this.myInputRef = c)}
                                                            name="ma"
                                                            value={values.ma || ''}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            isInvalid={touched.ma && !!errors.ma}
                                                        />
                                                        <Form.Control.Feedback type="invalid">
                                                            {errors.ma}
                                                        </Form.Control.Feedback>
                                                    </React.Fragment> :
                                                    <p className="form-control-static  " >{values.ma}</p>
                                                }
                                            </Form.Group>

                                        </Form.Row>
                                        <Form.Row>

                                            <Form.Group as={Col} md="12" controlId="ten">
                                                <Form.Label>Tên quốc gia{this.state.editMode ? <span className="text-danger">(*)</span> : ''}</Form.Label>
                                                {this.state.editMode ?
                                                    <React.Fragment>
                                                        <Form.Control
                                                            type="text"
                                                            name="ten"
                                                            value={values.ten || ''}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            isInvalid={touched.ten && !!errors.ten}
                                                        />
                                                        <Form.Control.Feedback type="invalid">
                                                            {errors.ten}
                                                        </Form.Control.Feedback>
                                                    </React.Fragment> :
                                                    <p className="form-control-static  " >{values.ten}</p>
                                                }
                                            </Form.Group>

                                        </Form.Row>
                                        <Form.Row>
                                            <Form.Group as={Col} md="12" controlId="moTa">
                                                <Form.Label>Mô tả</Form.Label>
                                                {this.state.editMode ?
                                                    <React.Fragment>
                                                        <Form.Control
                                                            as="textarea"
                                                            name="moTa"
                                                            value={values.moTa || ''}
                                                            onChange={handleChange}
                                                            onBlur={handleBlur}
                                                            isInvalid={touched.moTa && !!errors.moTa}
                                                        />
                                                        <Form.Control.Feedback type="invalid">
                                                            {errors.moTa}
                                                        </Form.Control.Feedback>
                                                    </React.Fragment> :
                                                    <p className="form-control-static text-pre-wrap " >{values.moTa}</p>
                                                }
                                            </Form.Group>

                                        </Form.Row>
                                    </Card.Body>
                                </Card>
                            </Modal.Body>
                            <Modal.Footer>
                                {this.state.editMode ?
                                    <Button type="submit">Lưu lại</Button>
                                    :
                                    ''
                                }
                                <Button variant="default" onClick={() => { this.handleClose(false); }}>
                                    Đóng
                                </Button>
                            </Modal.Footer>
                        </Form>
                    )}
                </Formik>
            </Modal>
        );
    }
}
export { QuocGiaFormComponent };