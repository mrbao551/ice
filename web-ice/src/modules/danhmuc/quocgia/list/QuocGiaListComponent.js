import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Breadcrumb, Card, Button, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Page, GridView } from 'shared/components';
import { Context } from '../QuocGiaService';
import { QuocGiaAction } from 'redux/actions';
import { QuocGiaFormComponent } from '../form/QuocGiaFormComponent';
import { QUOCGIA_TITLE } from 'redux/danhmuc//quocgia/QuocGiaConstant';
const metaDefault = {
    page: 1,
    page_size: 10,
    sort: { id: 'desc' },
    search: '',
    filter: {
    },
};
class QuocGiaList extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        setData: PropTypes.func,
        setMeta: PropTypes.func,
        data: PropTypes.array,
        meta: PropTypes.object
    }
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            check: false,
            meta: {},
            loading: false
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        let meta=metaDefault;
        this.setState({meta});
        this.fetchData(meta);
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    fetchData(meta) {
        this.setState({ loading: true });
        this.subscriptions['getmany'] = this.context.QuocGiaService.getMany(meta).subscribe(res => {
            this.props.setData(res.data || []);
            var newMeta = Object.assign({}, this.state.meta, {
                page: res.currentPage,
                page_size: res.pageSize,
                total: res.totalRows
            });
            this.setState({ meta: newMeta, loading: false });
        }, err => {
            console.log(err);
            this.setState({ loading: false });
        });
    }
    handleChange(res) {
        this.context.QuocGiaService.handleChange(res, this);
    }
    handleClose(res) {
        if (res) {
            this.fetchData(this.state.meta);
        }
    }
    addNew() {
        this.context.QuocGiaService.sendToForm.next({
            id: null,
            action: 'new',
            isShow: true
        });
    }
    viewDetail(item) {
        this.context.QuocGiaService.sendToForm.next({
            id: item.id,
            action: 'read',
            isShow: true
        });
    }
    viewEdit(item) {
        this.context.QuocGiaService.sendToForm.next({
            id: item.id,
            action: 'edit',
            isShow: true
        });
    }
    async delete(item) {
        this.context.QuocGiaService.DeleteAction(item, this);
    }
    render() {        
        return (
            <Page>
                <Page.Header>
                    <Row className="mb-2">
                        <Col sm={6}>
                            <h5>Danh sách {this.context.QuocGiaService.lowerCaseStartChar(QUOCGIA_TITLE)}</h5>
                        </Col>
                        <Col sm={6}>
                            <Breadcrumb className="float-sm-right">
                                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/home' }}>Trang chủ</Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    {QUOCGIA_TITLE}
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Page.Header>
                <Page.Content>
                    <Card>
                        <Card.Body>
                            <GridView
                                loading={this.state.loading}
                                handleChange={this.handleChange.bind(this)}>
                                <GridView.Header
                                    keySearch={this.state.meta.search}
                                    ActionBar={
                                        <Button variant="primary" size="sm" onClick={this.addNew.bind(this)} >
                                            <span className="iconify fa" data-icon="fa-solid:plus" data-inline="false"></span>
                                            Thêm mới
                                        </Button>
                                    }
                                >
                                </GridView.Header>
                                <GridView.Table
                                    className="col-12"
                                    noSelected={true}
                                    data={this.props.data}
                                    keyExtractor={({ item }) => {
                                        return item.id;
                                    }}
                                    sort={this.state.meta.sort}
                                    page={this.state.meta.page}
                                    page_size={this.state.meta.page_size}
                                    total={this.state.meta.total}
                                >
                                    <GridView.Table.Column style={{ width: '20px' }}
                                        title="STT"
                                        className="text-center"
                                        body={({ index }) => (
                                            <span>{index + 1 + (this.state.meta.page - 1) * this.state.meta.page_size}</span>
                                        )} />
                                    <GridView.Table.Column style={{width:'150px'}} title="Mã quốc gia" sortKey="ma" body={({ item }) => (<span>{item.ma}</span>)} />
                                    <GridView.Table.Column style={{}} title="Tên quốc gia" sortKey="ten" body={({ item }) => (<span>{item.ten}</span>)} />
                                    <GridView.Table.Column style={{ width: '100px' }} className="view-action"
                                        title="Tác vụ"
                                        body={({ item }) => (
                                            <ButtonGroup size="sm">
                                                <Button variant="outline-info" onClick={() => { this.viewDetail(item); }}>
                                                    <span className="iconify" data-icon="fa-solid:eye" data-inline="false"></span>
                                                </Button>
                                                <Button variant="outline-info" onClick={() => { this.viewEdit(item); }}>
                                                    <span className="iconify" data-icon="fa-solid:edit" data-inline="false"></span>
                                                </Button>
                                                <Button variant="outline-danger" onClick={() => { this.delete(item); }}>
                                                    <span className="iconify" data-icon="fa-solid:trash-alt" data-inline="false"></span>
                                                </Button>
                                            </ButtonGroup>
                                        )} />
                                </GridView.Table>
                            </GridView>
                        </Card.Body>
                    </Card>
                    <QuocGiaFormComponent onClose={this.handleClose.bind(this)} />
                </Page.Content >
            </Page >
        );
    }
}
const mapStateToProps = (state) => {
    return {
        data: state.QuocGia.QuocGiaList,
        meta: state.QuocGia.meta
    };
};
const QuocGiaListComponent = connect(mapStateToProps, QuocGiaAction)(QuocGiaList);
export { QuocGiaListComponent };