import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Breadcrumb, Card, Button, ButtonGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Page, GridView } from 'shared/components';
import { Context } from '../DanhSachDanhMucService';
import { DanhSachDanhMucAction } from 'redux/actions';
import { DanhSachDanhMucFormComponent } from '../form/DanhSachDanhMucFormComponent';
import { DANHSACHDANHMUC_TITLE } from 'redux/danhsachdanhmuc/DanhSachDanhMucConstant';
import { toast } from 'react-toastify';
class DanhSachDanhMucList extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        setData: PropTypes.func,
        setMeta: PropTypes.func,
        data: PropTypes.array,
        meta: PropTypes.object
    }
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.fetchData(this.props.meta);
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    fetchData(meta) {
        this.setState({ loading: true });
        this.subscriptions['getmany'] = this.context.DanhSachDanhMucService.getMany(meta).subscribe(res => {
            this.props.setData(res.data || []);
            this.props.setMeta({
                page: res.currentPage,
                page_size: res.pageSize,
                total: res.totalRows
            });
            this.setState({ loading: false });
        }, err => {
            console.log(err);
            this.setState({ loading: false });
        });
    }
    handleChange(res) {
        let newMeta = {};
        switch (res.event) {
        case 'changeSelected':
            break;
        case 'changePageSize':
            newMeta = Object.assign({}, this.props.meta, res.data);
            if (this.props.meta.page_size !== newMeta.page_size) {
                // this.props.setMeta({ page_size: newMeta.page_size });
                this.fetchData(newMeta);
            }
            break;
        case 'changePage':
            newMeta = Object.assign({}, this.props.meta, res.data);
            if (this.props.meta.page !== newMeta.page) {
                // this.props.setMeta({ page: newMeta.page });
                this.fetchData(newMeta);
            }
            break;
        case 'changeSort':
            newMeta = Object.assign({}, this.props.meta, res.data);
            if (this.props.meta.sort !== newMeta.sort) {
                this.props.setMeta({ sort: newMeta.sort });
                this.fetchData(newMeta);
            }
            break;
        case 'changeKeySearch':
            newMeta = Object.assign({}, this.props.meta, res.data);
            this.props.setMeta({ search: newMeta.search });
            this.fetchData(newMeta);
            break;
        case 'changeFilter':
            newMeta = Object.assign({}, this.props.meta, res.data);
            this.props.setMeta({ filter: newMeta.filter });
            this.fetchData(newMeta);
            break;
        default:
            break;
        }
    }
    handleClose(res) {
        if (res) {
            this.fetchData(this.props.meta);
        }
    }
    addNew() {
        this.context.DanhSachDanhMucService.sendToForm.next({
            id: null,
            action: 'new',
            isShow: true
        });
    }
    viewDetail(item) {
        this.context.DanhSachDanhMucService.sendToForm.next({
            id: item.id,
            action: 'read',
            isShow: true
        });
    }
    viewEdit(item) {
        this.context.DanhSachDanhMucService.sendToForm.next({
            id: item.id,
            action: 'edit',
            isShow: true
        });
    }
    async delete(item) {
        if (await this.context.beh.confirm('Bạn có muốn xóa bản ghi này')) {
            this.setState({
                loading: true
            });
            this.context.DanhSachDanhMucService.del(item.id).subscribe(() => {
                toast.success('Xóa thành công');
                this.fetchData(this.props.meta);
            });
            this.setState({
                loading: false
            });
        }
    }
    render() {
        return (
            <Page>
                <Page.Header>
                    <Row className="mb-2">
                        <Col sm={6}>
                            <h5>Danh sách {DANHSACHDANHMUC_TITLE}</h5>
                        </Col>
                        <Col sm={6}>
                            <Breadcrumb className="float-sm-right">
                                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/' }}>Tổng quan</Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    {DANHSACHDANHMUC_TITLE}
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Page.Header>
                <Page.Content>
                    <Card>
                        <Card.Body>
                            <GridView
                                loading={this.state.loading}
                                handleChange={this.handleChange.bind(this)}>
                                <GridView.Header
                                    keySearch={this.props.meta.search}
                                    ActionBar={
                                        <Button variant="primary" size="sm" onClick={this.addNew.bind(this)} >
                                            <span className="iconify fa" data-icon="fa-solid:plus" data-inline="false"></span>
                                            Thêm mới
                                        </Button>
                                    }
                                >
                                </GridView.Header>
                                <GridView.Table
                                    className="col-12"
                                    noSelected={true}
                                    data={this.props.data}
                                    keyExtractor={({ item }) => {
                                        return item.id;
                                    }}
                                    sort={this.props.meta.sort}
                                    page={this.props.meta.page}
                                    page_size={this.props.meta.page_size}
                                    total={this.props.meta.total}
                                >
                                    <GridView.Table.Column style={{ width: '20px' }}
                                        title="STT"
                                        className="text-center"
                                        body={({ index }) => (
                                            <span>{index + 1 + (this.props.meta.page - 1) * this.props.meta.page_size}</span>
                                        )} />
                                    <GridView.Table.Column style={{}} title="Tên Danh Mục" sortKey="tieuDe" body={({ item }) => (<span>{item.tieuDe}</span>)} />
                                    <GridView.Table.Column style={{}} title="Mã danh mục" sortKey="maDanhMuc" body={({ item }) => (<span>{item.maDanhMuc}</span>)} />
                                    <GridView.Table.Column style={{ width: '100px' }} className="view-action"
                                        title="Tác vụ"
                                        body={({ item }) => (
                                            <ButtonGroup size="sm">
                                                <Button variant="outline-info" onClick={() => { this.viewDetail(item); }}>
                                                    <span className="iconify" data-icon="fa-solid:eye" data-inline="false"></span>
                                                </Button>
                                                <Button variant="outline-info" onClick={() => { this.viewEdit(item); }}>
                                                    <span className="iconify" data-icon="fa-solid:edit" data-inline="false"></span>
                                                </Button>
                                                <Button variant="outline-danger" onClick={() => { this.delete(item); }}>
                                                    <span className="iconify" data-icon="fa-solid:trash-alt" data-inline="false"></span>
                                                </Button>
                                            </ButtonGroup>
                                        )} />
                                </GridView.Table>
                            </GridView>
                        </Card.Body>
                    </Card>
                    <DanhSachDanhMucFormComponent onClose={this.handleClose.bind(this)} />
                </Page.Content >
            </Page >
        );
    }
}
const mapStateToProps = (state) => {
    return {
        data: state.DanhSachDanhMuc.DanhSachDanhMucList,
        meta: state.DanhSachDanhMuc.meta
    };
};
const DanhSachDanhMucListComponent = connect(mapStateToProps, DanhSachDanhMucAction)(DanhSachDanhMucList);
export { DanhSachDanhMucListComponent };