import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context,_DanhSachDanhMucService } from './DanhSachDanhMucService';
import { DanhSachDanhMucListComponent } from './list/DanhSachDanhMucListComponent';
import { DanhSachDanhMucFormComponent } from './form/DanhSachDanhMucFormComponent';
class DanhSachDanhMucModule extends Component {
    static propTypes={
        match:PropTypes.object
    }
    render() {
        let {path}=this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({beh})=>(
                        <Context.Provider value={{
                            DanhSachDanhMucService: _DanhSachDanhMucService,
                            beh:beh
                        }} >
                            <Switch>
                                <Route path={`${path}/view`} render={(props)=><DanhSachDanhMucListComponent {...props} ></DanhSachDanhMucListComponent>} ></Route>
                                <Route path={`${path}/form/:id`} render={(props) => <DanhSachDanhMucFormComponent {...props} />} />
                                <Route path={path} render={(props) => <DanhSachDanhMucListComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { DanhSachDanhMucModule };