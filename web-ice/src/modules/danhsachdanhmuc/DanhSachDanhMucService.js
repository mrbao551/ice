import { createContext } from 'react';
import { BaseService } from 'shared/services';
import { Subject } from 'rxjs';
import { http } from 'shared/utils';
import { map } from 'rxjs/operators';
class DanhSachDanhMucService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/danhsachdanhmuc' }, props);
        super(_props);
        this.sendToForm = new Subject();        
    }   
}
const _DanhSachDanhMucService= new DanhSachDanhMucService();
const Context = createContext();
export { Context, _DanhSachDanhMucService, DanhSachDanhMucService };