import React from 'react';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal, InputGroup ,Card} from 'react-bootstrap';
import { Formik } from 'formik';
import { string, object, number } from 'yup';
import { LoadingComponent, FormSelect, FileAttach, FileUploadView } from 'shared/components';
import { Context } from '../DanhSachDanhMucService';
import {DANHSACHDANHMUC_TITLE} from 'redux/danhsachdanhmuc/DanhSachDanhMucConstant';
import { toast } from 'react-toastify';
import DatePicker from 'react-datepicker';
const schema = object({
        stT: number().nullable().integer('Bạn phải nhập kiểu số nguyên'),
    tieuDe: string().nullable().trim().required('Tên Danh Mục không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
    maDanhMuc: string().nullable().trim().required('Mã danh mục không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
});
const userDefault = {
        stT: '',
    tieuDe: '',
    maDanhMuc: '',
};
class DanhSachDanhMucFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            listGroup: [],
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.subscriptions['sendToForm'] = this.context.DanhSachDanhMucService.sendToForm.subscribe(res => {
            switch (res.action) {
                case 'new':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: userDefault  });
                     this.loadDataSelect();
                    break;
                case 'edit':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.DanhSachDanhMucService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false,
                        });
                    });
                     this.loadDataSelect();
                    break;
                case 'read':
                    this.setState({ editMode: false, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.DanhSachDanhMucService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                //default:
                //    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: userDefault });
                //    this.loadDataSelect();
                //    break;
            }
        });
    }
    loadDataSelect() {
    }   
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        if (this.state.id) {
            this.subscriptions['update'] = this.context.DanhSachDanhMucService.update(data, this.state.id).subscribe(() => {
                    this.handleClose(true);
                    this.setState({
                        loading: false
                    });
                    toast.success('Cập nhật thành công');
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                     toast.error(err.error);
                });
        } else {
            this.subscriptions['create'] = this.context.DanhSachDanhMucService.create(data, this.state.id).subscribe(() => {
                    this.handleClose(true);
                    this.setState({
                        loading: false
                    });
                    toast.success('Thêm mới thành công');
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                      toast.error(err.error);
                });
        }
    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }
    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    computedTitle() {
        if (this.state.id) {
            if (this.state.editMode) {
                return 'Chỉnh sửa thông tin '+DANHSACHDANHMUC_TITLE;
            }
            return 'Chi tiết '+DANHSACHDANHMUC_TITLE;
        }
        return 'Thêm mới '+DANHSACHDANHMUC_TITLE;
    }
    render() {
        return (
            <Modal size="lg" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                        this.onSubmit(values);
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                          setFieldValue,
                        values,
                        touched,
                        errors,
                    }) => (                            
                            <Form noValidate onSubmit={handleSubmit} onKeyPress={(ev) => { ev.stopPropagation(); }}>
                                <Modal.Header closeButton>
                                    <Modal.Title>{this.computedTitle()}</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                 <Card>
                                        {/*<Card.Header><b>Thông tin chung</b></Card.Header>*/}
                                        <Card.Body>
                                      <Form.Row> 
<Form.Group as={Col} md="2" >
<Form.Label>STT</Form.Label>
</Form.Group>
<Form.Group as={Col} md="4" controlId="stT">
{this.state.editMode ?
<React.Fragment>
<Form.Control
type="number"
name="stT"
value={values.stT || ''}
onChange={handleChange}
onBlur={handleBlur}
isInvalid={touched.stT && !!errors.stT}
/>
<Form.Control.Feedback type="invalid">
{errors.stT}
</Form.Control.Feedback>
</React.Fragment> :
<p className="form-control-static text-pre-wrap " >{values.stT}</p>
}
</Form.Group>
<Form.Group as={Col} md="2" >
<Form.Label>Tên Danh Mục{this.state.editMode ?<Form.Label className="text-danger">(*)</Form.Label>:''}</Form.Label>
</Form.Group>
<Form.Group as={Col} md="4" controlId="tieuDe">
{this.state.editMode ?
<React.Fragment>
<Form.Control
type="text"
name="tieuDe"
value={values.tieuDe || ''}
onChange={handleChange}
onBlur={handleBlur}
isInvalid={touched.tieuDe && !!errors.tieuDe}
/>
<Form.Control.Feedback type="invalid">
{errors.tieuDe}
</Form.Control.Feedback>
</React.Fragment> :
<p className="form-control-static  " >{values.tieuDe}</p>
}
</Form.Group>

</Form.Row>
  <Form.Row> 
<Form.Group as={Col} md="2" >
<Form.Label>Mã danh mục{this.state.editMode ?<Form.Label className="text-danger">(*)</Form.Label>:''}</Form.Label>
</Form.Group>
<Form.Group as={Col} md="4" controlId="maDanhMuc">
{this.state.editMode ?
<React.Fragment>
<Form.Control
type="text"
name="maDanhMuc"
value={values.maDanhMuc || ''}
onChange={handleChange}
onBlur={handleBlur}
isInvalid={touched.maDanhMuc && !!errors.maDanhMuc}
/>
<Form.Control.Feedback type="invalid">
{errors.maDanhMuc}
</Form.Control.Feedback>
</React.Fragment> :
<p className="form-control-static  " >{values.maDanhMuc}</p>
}
</Form.Group>

</Form.Row>
                                     </Card.Body>
                                     </Card>
                                </Modal.Body>
                                <Modal.Footer>                                    
                                    {this.state.editMode ?
                                        <Button type="submit">Lưu lại</Button>
                                        :
                                       ''
                                    }
									<Button variant="default" onClick={() => { this.handleClose(false); }}>
                                        Đóng
                                </Button>
                                </Modal.Footer>
                            </Form>
                        )}
                </Formik>
            </Modal>
        );
    }
}
export { DanhSachDanhMucFormComponent };