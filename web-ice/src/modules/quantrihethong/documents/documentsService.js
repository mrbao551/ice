import { createContext } from 'react';
import { BaseService } from 'shared/services';
import { Subject } from 'rxjs';
import { http } from 'shared/utils';
import { map } from 'rxjs/operators';
import { API_URL } from 'app-setting';
class documentsService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/documents' }, props);
        super(_props);
        this.sendToForm = new Subject();
        this.sendToFormPermission = new Subject();
        this.sendToFormBieuMau = new Subject();
    }
    addDocuments(data, obj) {
        const headers = {
            'Content-Type': 'multipart/form-data',
        };
        const formData = new FormData();
        if(data&&data.fileAttachs){
            for (let i = 0; i < data.fileAttachs.length; i++) {
                formData.append(`files`, data.fileAttachs[i]);
            }
        }        
        formData.append(obj, JSON.stringify(data));
        return http.post(`${API_URL}api/documents`, formData, {
            headers: headers
        });
    }
    updateDocuments(data, obj) {
        const headers = {
            'Content-Type': 'multipart/form-data',
        };
        const formData = new FormData();
        if(data&&data.fileAttachs){
            for (let i = 0; i < data.fileAttachs.length; i++) {
                formData.append(`files`, data.fileAttachs[i]);
            }
        }        
        formData.append(obj, JSON.stringify(data));
        return http.put(`${API_URL}api/documents/${data.id}`, formData, {
            headers: headers
        });
    }
    
    
    
}

const Context = createContext();
const _documentsService = new documentsService();
export { Context, _documentsService, documentsService };

