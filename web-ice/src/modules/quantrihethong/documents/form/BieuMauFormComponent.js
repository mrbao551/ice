import React from 'react';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal, ButtonGroup, Card, CardGroup, Tabs, Tab } from 'react-bootstrap';
import { Formik, Field } from 'formik';
import { string, object } from 'yup';
import { LoadingComponent } from 'shared/components';
import { Context } from '../documentsService';
import { DOCUMENTS_TITLE } from 'redux/quantrihethong/documents/documentsConstant';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import { API_URL } from 'app-setting';
import Split from 'react-split';

import { ThongTinTaiLieuFormComponent } from './ThongTinTaiLieuFormComponent';
const schema = object({
    title: string().trim().required('Tiêu đề không được để trống'),
});
const userDefault = {
    title: '',
    content: '',
    fileAttachs: [],
    LstAttachDeletes: [],
};

// Style cho Split chuyển màn hình

// End style

class BieuMauFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
            isLoadCK: false,
            isChangeDesign: false,
            tabactive: 'thongtintailieu',
        };
        this.subscriptions = {};
    }
    ChangeTabs = (tabselect) => {
        this.setState({
            tabactive: tabselect
        });
    }
    componentDidMount() {


        this.subscriptions['sendToFormBieuMau'] = this.context.documentsService.sendToFormBieuMau.subscribe(res => {
            switch (res.action) {
                case 'new':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: userDefault, isLoadCK: true });
                    break;
                case 'edit':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: res.id, loading: true, isLoadCK: false });
                    this.context.documentsService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false,
                        }, () => {
                            this.setState({ isLoadCK: true })
                        });
                    });
                    break;
                case 'read':
                    this.setState({ editMode: false, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.documentsService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                default:
                    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: userDefault });
                    break;
            }
        });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        if (this.state.id) {
            this.context.documentsService.updateDocuments(data, "DocumentUpdateDto").subscribe(() => {
                toast.success('Cập nhật thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        } else {
            this.context.documentsService.addDocuments(data, "DocumentCreateDto").subscribe(() => {
                toast.success('Thêm mới thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });
        }

    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }

    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    computedTitle() {
        if (this.state.id) {
            if (this.state.editMode) {
                return 'Chỉnh sửa thông tin ' + DOCUMENTS_TITLE;
            }
            return 'Chi tiết ' + DOCUMENTS_TITLE;
        }
        return 'Thêm mới ' + DOCUMENTS_TITLE;
    }
    ChangeDesign() {
        this.setState({ isChangeDesign: !this.state.isChangeDesign });

    }
    renderFileAttach(files) {
        if (files) {
            return (
                <ul>
                    {
                        files.map((file, index) => {
                            return (
                                <li className="li_FileAttach" key={index}>
                                    <Link to={`${API_URL}${file.url}`}>{file.title}</Link>
                                </li>
                            )
                        })
                    }
                </ul>
            )
        }
        else {
            return '';
        }
    }

    render() {
        let { isChangeDesign } = this.state;
        console.log(isChangeDesign);
        return (
            <Modal className="modal-full" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>



                <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                        console.log(values);
                        this.onSubmit(values);
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        setFieldValue,
                        values,
                        touched,
                        errors,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} onKeyPress={this.onKeyPress.bind(this)}>
                            <Modal.Header closeButton>
                                <Modal.Title>{this.computedTitle()}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>

                                <CardGroup>
                                    <Split
                                        className={`split ${isChangeDesign ? "vertical-bieumau" : "horizontal-bieumau"}`}
                                        sizes={[60, 40]}
                                        direction={!isChangeDesign ? "horizontal" : "vertical"}
                                    >


                                        <Card className="bieumau">
                                            <Card.Header style={{ padding: "6px" }}>
                                                <Form.Row style={{ float: "right" }}>
                                                    <a href="javascript:;" className="act-bieumau">
                                                        <span className="btn default btn-xs origan mr-1" href="javascript:;"><i className="fa fa-share"></i></span>Xử lý sau
                                                    </a>

                                                    <a href="javascript:;" className="act-bieumau">
                                                        <span className="btn default btn-xs blue mr-1" href="javascript:;"><i className="fa fa-download"></i></span>Tải về
                                                    </a>
                                                    <a href="javascript:;" className="act-bieumau" onClick={() => { this.ChangeDesign(); }}>
                                                        <span className="btn default btn-xs blue mr-1" href="javascript:;"><i className="fa fa-laptop"></i></span>Đổi kiểu hiển thị
                                                    </a>
                                                </Form.Row>
                                            </Card.Header>
                                            <Card.Body>
                                                <div className="pdf-viewer">
                                                    <embed
                                                        src={`http://sohoadulieu.vn/fileuploadfolder/view/CSDLSOHOA1\\BTC1\\VP1\\50A\\HP111115.pdf`}
                                                        type="application/pdf"
                                                        width="100%"
                                                        height="100%"
                                                    />
                                                   
                                                </div>
                                            </Card.Body>

                                        </Card>
                                        <Card style={{ paddingLeft: "9px", width: "100%" }}>
                                            <Card.Header style={{ padding: "0" }}>
                                                <Tabs defaultActiveKey="thongtintailieu" style={{ marginBottom: "10px" }} id="uncontrolled-tab-example" onSelect={(ev) => { this.ChangeTabs(ev); }}>
                                                    <Tab eventKey="thongtintailieu" title="Thông tin tài liệu">
                                                        {
                                                            (this.state.tabactive == 'thongtintailieu') ?
                                                                <ThongTinTaiLieuFormComponent></ThongTinTaiLieuFormComponent> : ''
                                                        }
                                                    </Tab>
                                                    <Tab eventKey="tailieucungbo" title="Tài liệu cùng bộ">

                                                    </Tab>
                                                    <Tab eventKey="canhanxuly" title="Cá nhân xử lý">

                                                    </Tab>
                                                </Tabs>
                                            </Card.Header>

                                        </Card>

                                    </Split>
                                </CardGroup>

                            </Modal.Body>

                        </Form>
                    )}
                </Formik>
            </Modal>

        );
    }
}
export { BieuMauFormComponent };