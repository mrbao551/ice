import React from 'react';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal, InputGroup } from 'react-bootstrap';
import { Formik, Field } from 'formik';
import { string, object } from 'yup';
import { LoadingComponent, FileAttach, FileUploadView } from 'shared/components';
import { Context } from '../documentsService';
import { DOCUMENTS_TITLE } from 'redux/quantrihethong/documents/documentsConstant';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import { API_URL } from 'app-setting';
import { TCKFINDER_URL } from 'app-setting';
import CKEditor from 'ckeditor4-react';

const schema = object({
    title: string().trim().required('Tiêu đề không được để trống'),
});
const userDefault = {
    title: '',
    content: '',
    fileAttachs: [],
    LstAttachDeletes: [],
};
class DocumentsFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
            isLoadCK: false
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.subscriptions['sendToForm'] = this.context.documentsService.sendToForm.subscribe(res => {
            switch (res.action) {
                case 'new':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: userDefault, isLoadCK: true });
                    break;
                case 'edit':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: res.id, loading: true, isLoadCK: false });
                    this.context.documentsService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false,
                        }, () => {
                            this.setState({ isLoadCK: true })
                        });
                    });
                    break;
                case 'read':
                    this.setState({ editMode: false, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.documentsService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                default:
                    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: userDefault });
                    break;
            }
        });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        if (this.state.id) {
            this.context.documentsService.updateDocuments(data, "DocumentUpdateDto").subscribe(() => {
                toast.success('Cập nhật thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        } else {
            this.context.documentsService.addDocuments(data, "DocumentCreateDto").subscribe(() => {
                toast.success('Thêm mới thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });
        }

    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }

    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    computedTitle() {
        if (this.state.id) {
            if (this.state.editMode) {
                return 'Chỉnh sửa thông tin ' + DOCUMENTS_TITLE;
            }
            return 'Chi tiết ' + DOCUMENTS_TITLE;
        }
        return 'Thêm mới ' + DOCUMENTS_TITLE;
    }

    renderFileAttach(files) {
        if (files) {
            return (
                <ul>
                    {
                        files.map((file, index) => {
                            return (
                                <li className="li_FileAttach" key={index}>
                                    <Link to={`${API_URL}${file.url}`}>{file.title}</Link>
                                </li>
                            )
                        })
                    }
                </ul>
            )
        }
        else {
            return '';
        }
    }

    render() {
        let { isLoadCK } = this.state;
        return (
            <Modal size="lg" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                        console.log(values);
                        this.onSubmit(values);
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        setFieldValue,
                        values,
                        touched,
                        errors,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} onKeyPress={this.onKeyPress.bind(this)}>
                            <Modal.Header closeButton>
                                <Modal.Title>{this.computedTitle()}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Form.Row>
                                    <Form.Group as={Col} md="12" controlId="title">
                                        <Form.Label>Tiêu đề</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <Form.Control
                                                    type="text"
                                                    name="title"
                                                    value={values.title || ''}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.title && !!errors.title}
                                                />
                                                <Form.Control.Feedback type="invalid">
                                                    {errors.title}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.title}</p>
                                        }
                                    </Form.Group>
                                </Form.Row>
                                <Form.Group as={Col} md="12" controlId="content">
                                    <Form.Label>Nội dung</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                    {this.state.editMode ?
                                        <React.Fragment>
                                            <InputGroup>
                                                {
                                                    isLoadCK ?
                                                        <Field name="content"
                                                            className={'form-control' + (errors.noidung && touched.noidung ? ' is-invalid' : '')}
                                                            onChange={(evt) =>
                                                                setFieldValue('content', evt.editor.getData())
                                                            }
                                                            data={values.content}
                                                            config={{
                                                                extraPlugins: 'image2',
                                                                filebrowserImageBrowseUrl: `${TCKFINDER_URL}` //& CKEditor= editor2 & CKEditorFuncNum=2
                                                            }}
                                                            component={CKEditor}>
                                                        </Field> : ''
                                                }

                                                <Form.Control.Feedback type="invalid">
                                                    {errors.content}
                                                </Form.Control.Feedback>
                                            </InputGroup>
                                        </React.Fragment> :
                                        <p className="content-tienich pt-3" dangerouslySetInnerHTML={{ __html: values.content }}>
                                        </p>
                                    }
                                </Form.Group>
                                <Form.Row>
                                    <Form.Group as={Col} md="6" controlId="lstAttachment">
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <FileAttach
                                                    name="lstAttachment"
                                                    value={values.lstAttachments}
                                                    multiple={true}
                                                    action={this.state.action}
                                                    btnName=" Thêm tệp"
                                                    fileAllow="fileAll"
                                                    action={values.id ? 'edit' : 'new'}
                                                    size={50}
                                                    onFileDelete={(lstId) => {
                                                        setFieldValue('LstAttachDeletes', lstId)
                                                    }
                                                    }
                                                    onFileChange={(files) => {
                                                        setFieldValue('fileAttachs', files)
                                                    }
                                                    }
                                                ></FileAttach>
                                            </React.Fragment> :
                                            <React.Fragment>
                                                <FileUploadView value={values.lstAttachments} label="Đính kèm" />                                               
                                            </React.Fragment>
                                        }

                                    </Form.Group>
                                    <Form.Group as={Col} md="6">

                                    </Form.Group>
                                </Form.Row>
                            </Modal.Body>
                            <Modal.Footer>
                                {this.state.editMode ?
                                    <Button type="submit">Lưu lại</Button>
                                    :
                                    <React.Fragment>
                                        <Button variant="primary" className="ml-2" type="button" onClick={this.onEdit.bind(this)}>Sửa</Button>
                                    </React.Fragment>
                                }
                                <Button variant="default" onClick={() => { this.handleClose(false); }}>
                                    Đóng
                                </Button>
                            </Modal.Footer>
                        </Form>
                    )}
                </Formik>
            </Modal>

        );
    }
}
export { DocumentsFormComponent };