import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context, _documentsService } from './documentsService';
import { DocumentsListComponent } from './list/DocumentsListComponent';
import { DocumentsFormComponent } from './form/DocumentsFormComponent';
import { BieuMauFormComponent } from './form/BieuMauFormComponent';
class documentsModule extends Component {
    static propTypes = {
        match: PropTypes.object
    }
    render() {
        let { path } = this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({ beh }) => (
                        <Context.Provider value={{
                            documentsService: _documentsService,
                            beh:beh
                        }} >
                         
                            <Switch>
                                <Route path={`${path}/view`} render={(props) => <DocumentsListComponent {...props} ></DocumentsListComponent>} ></Route>
                                <Route path={`${this.props.match.path}/form/:id`} render={(props) => <DocumentsFormComponent {...props} />} />
                                <Route path={`${this.props.match.path}/form/:id`} render={(props) => <BieuMauFormComponent {...props} />} />
                                <Route path={this.props.match.path} render={(props) => <DocumentsListComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { documentsModule };