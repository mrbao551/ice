import { createContext } from 'react';
import { BaseService } from 'shared/services';
import { Subject } from 'rxjs';
class permissionsService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/permissions' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }
}

const Context = createContext();
const _permissionsService = new permissionsService();
export { Context, _permissionsService, permissionsService };

