import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context, _permissionsService } from './permissionsService';
import { PermissionsListComponent } from './list/PermissionsListComponent';
import { PermissionsFormComponent } from './form/PermissionsFormComponent';
class permissionsModule extends Component {
    static propTypes = {
        match: PropTypes.object
    }
    render() {
        let { path } = this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({ beh }) => (
                        <Context.Provider value={{
                            permissionsService: _permissionsService,
                            beh:beh
                        }} >
                         
                            <Switch>
                                <Route path={`${path}/view`} render={(props) => <PermissionsListComponent {...props} ></PermissionsListComponent>} ></Route>
                                <Route path={`${this.props.match.path}/form/:id`} render={(props) => <PermissionsFormComponent {...props} />} />
                                <Route path={this.props.match.path} render={(props) => <PermissionsListComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { permissionsModule };