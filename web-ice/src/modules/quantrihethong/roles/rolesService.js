import { createContext } from 'react';
import { BaseService } from 'shared/services';
import { Subject } from 'rxjs';
class rolesService extends BaseService {
    constructor(props) {
        const _props = Object.assign({}, { url: 'api/roles' }, props);
        super(_props);
        this.sendToForm = new Subject();
    }
    
}

const Context = createContext();
const _rolesService = new rolesService();
export { Context, _rolesService, rolesService };

