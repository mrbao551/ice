import React from 'react';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal } from 'react-bootstrap';
import { Formik } from 'formik';
import { string, object } from 'yup';
import { LoadingComponent, } from 'shared/components';
import { Context } from '../rolesService';
import {ROLES_TITLE} from 'redux/quantrihethong/roles/rolesConstant';
import {toast} from 'react-toastify';


const schema = object({
    name: string().trim().required('Tên đối tượng không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự')
});
const userDefault = {
    name: '',
    description: '',
};
class RolesFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            listGroup: [],
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
            // dataTinhThanh:[]
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        
        this.subscriptions['sendToForm'] = this.context.rolesService.sendToForm.subscribe(res => {
            switch (res.action) {
                case 'new':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: userDefault });
                    break;
                case 'edit':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.rolesService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                case 'read':
                    this.setState({ editMode: false, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.rolesService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                default:
                    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: userDefault });
                    break;
            }
        }); 
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        if (this.state.id) {
            this.subscriptions['update'] = this.context.rolesService.update(data, this.state.id).subscribe(() => {
               
                toast.success('Cập nhật thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        } else {
            this.subscriptions['create'] = this.context.rolesService.create(data).subscribe(() => {
                toast.success('Thêm mới thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        }

    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }

    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    computedTitle() {
        if (this.state.id) {
            if (this.state.editMode) {
                return 'Chỉnh sửa thông tin '+ROLES_TITLE;
            }
            return 'Chi tiết '+ROLES_TITLE;
        }
        return 'Thêm mới '+ROLES_TITLE;
    }
    render() {
        return (
            <Modal size="lg" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                        this.onSubmit(values);
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        values,
                        touched,
                        errors,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} onKeyPress={(ev) => {ev.stopPropagation();}}>
                                <Modal.Header closeButton>
                                    <Modal.Title>{this.computedTitle()}</Modal.Title>
                                </Modal.Header>
                                <Modal.Body >
                                    
                                <Form.Row onKeyPress={this.onKeyPress.bind(this)}>
                                        <Form.Group as={Col} md="12" controlId="name">
                                            <Form.Label>Tên đối tượng</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                            {this.state.editMode ?
                                                <React.Fragment>
                                                    <Form.Control
                                                        type="text"
                                                        name="name"
                                                        value={values.name || ''}
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        isInvalid={touched.name && !!errors.name}
                                                    />
                                                    <Form.Control.Feedback type="invalid">
                                                        {errors.name}
                                                    </Form.Control.Feedback>
                                                </React.Fragment> :
                                                <p className="form-control-static">{values.name}</p>
                                            }

                                        </Form.Group> 
                                    </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col} md="12" controlId="description">
                                            <Form.Label>Mô tả</Form.Label>
                                            {this.state.editMode ?
                                                 <React.Fragment>
                                                 <Form.Control
                                                    rows={4}
                                                     as="textarea"
                                                     name="description"
                                                     value={values.description || ''}
                                                     onChange={handleChange}
                                                     onBlur={handleBlur}
                                                     isInvalid={touched.description && !!errors.description}
                                                 />
                                                 <Form.Control.Feedback type="invalid">
                                                     {errors.description}
                                                 </Form.Control.Feedback>
                                             </React.Fragment> :
                                                <p className="form-control-static">{values.description}</p>
                                            }
                                        </Form.Group>                                    
                                    </Form.Row>
                                </Modal.Body>
                                <Modal.Footer>                                    
                                    {this.state.editMode ?
                                        <Button type="submit">Lưu lại</Button>
                                        :
                                       ''
                                    }
                                    <Button variant="default" onClick={() => { this.handleClose(false); }}>
                                        Đóng
                                </Button>
                                </Modal.Footer>
                            </Form>
                        )}
                </Formik>
            </Modal>

        );
    }
}
export { RolesFormComponent };