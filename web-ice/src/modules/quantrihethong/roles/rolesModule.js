import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context, _rolesService } from './rolesService';
import { RolesListComponent } from './list/RolesListComponent';
import { RolesFormComponent } from './form/RolesFormComponent';
class rolesModule extends Component {
    static propTypes = {
        match: PropTypes.object
    }
    render() {
        let { path } = this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({ beh }) => (
                        <Context.Provider value={{
                            rolesService: _rolesService,
                            beh:beh
                        }} >
                         
                            <Switch>
                                <Route path={`${path}/view`} render={(props) => <RolesListComponent {...props} ></RolesListComponent>} ></Route>
                                <Route path={`${this.props.match.path}/form/:id`} render={(props) => <RolesFormComponent {...props} />} />
                                <Route path={this.props.match.path} render={(props) => <RolesListComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { rolesModule };