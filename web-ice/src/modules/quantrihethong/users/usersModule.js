import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context, _usersService } from './usersService';
import { UsersListComponent } from './list/UsersListComponent';
import { UsersFormComponent } from './form/UsersFormComponent';
class usersModule extends Component {
    static propTypes = {
        match: PropTypes.object
    }
    render() {
        let { path } = this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({ beh }) => (
                        <Context.Provider value={{
                            usersService: _usersService,
                            beh:beh
                        }} >
                         
                            <Switch>
                                <Route path={`${path}/view`} render={(props) => <UsersListComponent {...props} ></UsersListComponent>} ></Route>
                                <Route path={`${this.props.match.path}/form/:id`} render={(props) => <UsersFormComponent {...props} />} />
                                <Route path={this.props.match.path} render={(props) => <UsersListComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { usersModule };