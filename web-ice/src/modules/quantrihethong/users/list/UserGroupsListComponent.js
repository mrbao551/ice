import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Modal, Button, ButtonGroup } from 'react-bootstrap';
import { GridView } from 'shared/components';
import { Context, _usersService } from '../usersService';
import { usersAction } from 'redux/actions';
import { toast } from 'react-toastify';
class UserGroupsList extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        setData: PropTypes.func,
        setMeta: PropTypes.func,
        data: PropTypes.array,
        meta: PropTypes.object
    }
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: {}
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        _usersService.sendToForm.subscribe(res => {
            switch (res.action) {
                case 'updateUser':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: res.data });
                    let meta = this.props.meta;
                    meta.filter.groupid = res.data.id;
                    this.fetchData(meta);
                    break;
                default:
                    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null });
                    break;
            }
        });

    }

    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    fetchData(meta) {
        this.setState({ loading: true });
        _usersService.getMany(meta).subscribe(res => {
            this.props.setData(res.data || []);
            this.props.setMeta({
                page: res.currentPage,
                page_size: res.pageSize,
                total: res.totalRows
            });
            this.setState({ loading: false });
        }, err => {
            console.log(err);
            this.setState({ loading: false });
        });
    }

    handleChange(res) {
        let newMeta = {};
        switch (res.event) {
            case 'changeSelected':
                break;
            case 'changePageSize':
                newMeta = Object.assign({}, this.props.meta, res.data);
                if (this.props.meta.page_size !== newMeta.page_size) {
                    // this.props.setMeta({ page_size: newMeta.page_size });
                    this.fetchData(newMeta);
                }
                break;
            case 'changePage':
                newMeta = Object.assign({}, this.props.meta, res.data);
                if (this.props.meta.page !== newMeta.page) {
                    // this.props.setMeta({ page: newMeta.page });
                    this.fetchData(newMeta);
                }
                break;
            case 'changeSort':
                newMeta = Object.assign({}, this.props.meta, res.data);
                if (this.props.meta.sort !== newMeta.sort) {
                    this.props.setMeta({ sort: newMeta.sort });
                    this.fetchData(newMeta);
                }
                break;
            case 'changeKeySearch':
                res.data.page = 1;
                newMeta = Object.assign({}, this.props.meta, res.data);
                this.props.setMeta({
                    search: newMeta.search,
                });
                this.fetchData(newMeta);
                break;
            case 'changeFilter':
                res.data.page = 1;
                newMeta = Object.assign({}, this.props.meta, res.data);
                this.props.setMeta({
                    filter: newMeta.filter
                });
                this.fetchData(newMeta);

                break;
            default:
                break;
        }
    }

    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }

    }

    async add(item) {
        let {data}=this.state;
        let {meta}=this.props;
        meta.filter.groupid=data.id;
        if(data.id)
        {
            if (await this.context.beh.confirm('Bạn có chắc chắn muốn thêm tài khoản này vào group')) {
                this.setState({ loading: true });
                _usersService.addUserToGroup(item.id, data.id).subscribe(() => {                    
                    this.fetchData(meta);
                    this.setState({ loading: false });
                    toast.success('Thêm tài khoản vào group thành công');
                },err => {
                    toast.error(err.error);
                    this.setState({ loading: false });
                });
            }
        }      
        else
        {
            toast.error('Group không tồn tại');
        }  
    }

    async remove(item) {
        let {data}=this.state;
        let {meta}=this.props;
        meta.filter.groupid=data.id;
        if(data.id)
        {            
            if (await this.context.beh.confirm('Bạn có chắc chắn muốn xóa tài khoản này khỏi group')) {
                this.setState({ loading: true });
                _usersService.removeUserToGroup(item.id, data.id).subscribe(() => {           
                    this.setState({ loading: false });         
                    this.fetchData(meta);
                    toast.success('Xóa tài khoản khỏi group thành công');
                },err => {
                    toast.error(err.error);
                    this.setState({ loading: false });
                });
            }
        }      
        else
        {
            toast.error('Group không tồn tại');
        }  
    }
    render() {
        let { data } = this.state;
        return (
            <Modal size="xl" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <Modal.Header closeButton>
                    <Modal.Title>Cập nhật người dùng cho group {data.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <GridView
                        loading={this.state.loading}
                        handleChange={this.handleChange.bind(this)}>
                        <GridView.Header
                            keySearch={this.props.meta.search}
                             >
                        </GridView.Header>
                        <GridView.Table
                            className="col-12"
                            noSelected={true}
                            data={this.props.data}
                            keyExtractor={({ item }) => {
                                return item.id;
                            }}
                            sort={this.props.meta.sort}
                            page={this.props.meta.page}
                            page_size={this.props.meta.page_size}
                            total={this.props.meta.total}
                        >
                            <GridView.Table.Column style={{ width: '6%' }}
                                title="STT"
                                className="text-center"
                                body={({ index }) => (
                                    <span>{index + 1 + (this.props.meta.page - 1) * this.props.meta.page_size}</span>
                                )} />
                            <GridView.Table.Column style={{ width: '20%' }}
                                title="Tên đăng nhập"
                                sortKey="username"
                                body={({ item }) => (
                                    <span>{item.username}</span>
                                )} />
                            <GridView.Table.Column
                                title="Họ và tên"
                                sortKey="fullName"
                                body={({ item }) => (
                                    <span>{item.fullName}</span>
                                )} />
                            <GridView.Table.Column style={{ width: '12%' }}
                                title="Ngày tạo"
                                sortKey="createdDate"
                                body={({ item }) => (
                                    <span>{_usersService.formatDateTime(item.createdDate)}</span>
                                )} />
                            <GridView.Table.Column style={{ width: '12%' }}
                                title="Trạng thái"
                                sortKey="status"
                                className="text-center"
                                body={({ item }) => (
                                    <span>{item.status === 1 ? <span className="badge badge-danger">Không hoạt động</span> : item.status === 2 ? <span className="badge badge-success">Đang hoạt động</span> : ''}</span>
                                )} />
                            <GridView.Table.Column style={{ width: '10%' }} className="view-action text-center"
                                title="Tác vụ"
                                body={({ item }) => (
                                    <ButtonGroup size="sm">
                                        {item.isCheck ? <Button title="Xóa người dùng khỏi group" variant="danger" onClick={() => { this.remove(item); }}>
                                            Gỡ
                                        </Button> : <Button title="Thêm người dùng vào group" variant="info" onClick={() => { this.add(item); }}>Thêm
                                            </Button>}



                                    </ButtonGroup>
                                )} />
                        </GridView.Table>
                    </GridView>
                </Modal.Body>
                <Modal.Footer >
                    <Button variant="default" onClick={() => { this.handleClose(false); }}>
                        Đóng
                                </Button>
                </Modal.Footer>
            </Modal>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        data: state.users.usersList,
        meta: state.users.meta
    };
};
const UserGroupsListComponent = connect(mapStateToProps, usersAction)(UserGroupsList);
export { UserGroupsListComponent };