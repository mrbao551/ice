import React from 'react';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal } from 'react-bootstrap';
import { Formik } from 'formik';
import { string, object, ref } from 'yup';
import { LoadingComponent } from 'shared/components';
import { Context, _usersService } from '../usersService';
import { toast } from 'react-toastify';
const schema = object({
    oldPassword: string().trim().required('Bạn phải nhập mật khẩu cũ').nullable(),
    newPassword: string().trim().required('Bạn phải nhập mật khẩu mới').min(6, 'Bạn nhập tối thiểu 6 ký tự').max(30, 'Bạn nhập tối đa 30 ký tự').matches(/(?=^.{6,30}$)(?=.*\d)(?=.*[!@#$%^&*]+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/, 'Mật khẩu phải có ít nhất một chữ số, một chữ cái hoa, một chữ cái thường, một ký tự đặc biệt'),
    newPasswordConfirm: string().trim().required('Bạn chưa nhập lại mật khẩu').oneOf([ref('newPassword'), null], 'Mật khẩu nhập lại không đúng'),
});
const userDefault = {
    oldPassword: '',
    newPassword: '',
    newPasswordConfirm: '',
};
class ChangePassFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new'
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        _usersService.sendToFormChangePassword.subscribe(res => {
            switch (res.action) {
                case 'changePass':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: userDefault });
                    break;
                default:
                    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: userDefault });
                    break;
            }
        });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        _usersService.changePassword(data).subscribe(() => {
            toast.success('Đổi mật khẩu thành công');
            this.handleClose(true);
            this.setState({
                loading: false
            });
        },
            (err) => {
                this.setState({
                    loading: false
                });
                toast.error(err.error);
            });
    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }
    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    computedTitle() {
        return 'Đổi mật khẩu';
    }
    render() {
        return (
            <Modal keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                        console.log(values);
                        this.onSubmit(values);
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        setFieldValue,
                        values,
                        touched,
                        errors,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} onKeyPress={this.onKeyPress.bind(this)}>
                            <Modal.Header closeButton>
                                <Modal.Title>{this.computedTitle()}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Form.Row>
                                    <Form.Group as={Col} md="12" controlId="oldPassword">
                                        <Form.Label>Mật khẩu cũ</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <Form.Control
                                                    type="password"
                                                    name="oldPassword"
                                                    value={values.oldPassword || ''}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.oldPassword && !!errors.oldPassword}
                                                />
                                                <Form.Control.Feedback type="invalid">
                                                    {errors.oldPassword}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.oldPassword}</p>
                                        }
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="12" controlId="newPassword">
                                        <Form.Label>Mật khẩu</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <Form.Control
                                                    type="password"
                                                    name="newPassword"
                                                    value={values.newPassword || ''}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.newPassword && !!errors.newPassword}
                                                />
                                                <Form.Control.Feedback type="invalid">
                                                    {errors.newPassword}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.newPassword}</p>
                                        }
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="12" controlId="newPasswordConfirm">
                                        <Form.Label>Nhập lại mật khẩu</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                        {this.state.editMode ?
                                            <React.Fragment>
                                                <Form.Control
                                                    type="password"
                                                    name="newPasswordConfirm"
                                                    value={values.newPasswordConfirm || ''}
                                                    onChange={handleChange}
                                                    onBlur={handleBlur}
                                                    isInvalid={touched.newPasswordConfirm && !!errors.newPasswordConfirm}
                                                />
                                                <Form.Control.Feedback type="invalid">
                                                    {errors.newPasswordConfirm}
                                                </Form.Control.Feedback>
                                            </React.Fragment> :
                                            <p className="form-control-static">{values.newPasswordConfirm}</p>
                                        }
                                    </Form.Group>
                                </Form.Row>

                            </Modal.Body>
                            <Modal.Footer>
                                {this.state.editMode ?
                                    <Button type="submit">Lưu lại</Button>
                                    :
                                    <React.Fragment>
                                        <Button variant="primary" className="ml-2" type="button" onClick={this.onEdit.bind(this)}>Sửa</Button>
                                    </React.Fragment>
                                }
                                <Button variant="default" onClick={() => { this.handleClose(false); }}>
                                    Đóng
                                </Button>
                            </Modal.Footer>
                        </Form>
                    )}
                </Formik>
            </Modal>
        );
    }
}
export { ChangePassFormComponent };