import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Row, Col, Breadcrumb, Card, Button, ButtonGroup, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Page, GridView, } from 'shared/components';
import { Context } from '../logsService';
import { logsAction } from 'redux/actions';
import { LogsFormComponent } from '../form/LogsFormComponent';
import { LOGS_TITLE } from 'redux/quantrihethong/logs/logsConstant';
import { Formik } from 'formik';
import DatePicker from 'react-datepicker';


class LogsAccess extends Component {
    static propTypes = {
        history: PropTypes.object,
        location: PropTypes.object,
        setData: PropTypes.func,
        setMeta: PropTypes.func,
        data: PropTypes.array,
        meta: PropTypes.object
    }
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        let newMeta = Object.assign({}, this.props.meta, {
            filter: { type: 2 }
        });
        this.fetchData(newMeta);

    }

    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    fetchData(meta) {
        this.setState({ loading: true });
        this.subscriptions['getmany'] = this.context.logsService.getMany(meta).subscribe(res => {
            this.props.setData(res.data || []);
            this.props.setMeta({
                page: res.currentPage,
                page_size: res.pageSize,
                total: res.totalRows,
                filter: meta.filter
            });
            this.setState({ loading: false });
        }, err => {
            console.log(err);
            this.setState({ loading: false });
        });
    }

    handleChange(res) {
        this.context.logsService.handleChange(res, this);
    }

    handleClose(res) {
        if (res) {
            //let newMeta = Object.assign({}, this.props.meta, { page: 1 });
            let newMeta = Object.assign({}, this.props.meta);
            this.props.setMeta({
                page: newMeta.page
            });
            this.fetchData(newMeta);
        }
    }
    addNew() {
        this.context.logsService.sendToForm.next({
            id: null,
            action: 'new',
            isShow: true
        });
    }
    viewDetail(item) {
        this.context.logsService.sendToForm.next({
            id: item.id,
            action: 'read',
            isShow: true
        });

    }
    viewEdit(item) {
        this.context.logsService.sendToForm.next({
            id: item.id,
            action: 'edit',
            isShow: true
        });
    }

    async delete(item) {
        if (await this.context.beh.confirm('Bạn có muốn xóa bản ghi này')) {
            this.context.logsService.del(item.id).subscribe(() => {
                if (this.props.data.length === 1 && this.props.meta.page !== 1) {
                    this.props.setMeta({ page: this.props.meta.page - 1 });
                }
                this.fetchData(this.props.meta);
            });
        }
    }
    render() {
        return (
            <Page>
                <Page.Header>
                    <Row className="mb-2">
                        <Col sm={6}>
                            <h5>Danh sách {LOGS_TITLE}</h5>
                        </Col>
                        <Col sm={6}>
                            <Breadcrumb className="float-sm-right">
                                <Breadcrumb.Item linkAs={Link} linkProps={{ to: '/' }}>Tổng quan</Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    {LOGS_TITLE}
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Page.Header>
                <Page.Content>
                    <Card>
                        <Card.Body>
                            <GridView
                                loading={this.state.loading}
                                handleChange={this.handleChange.bind(this)}>

                                <GridView.Header
                                    keySearch={this.props.meta.search}
                                    // ActionBar={
                                    //     <Button variant="primary" size="sm" onClick={this.addNew.bind(this)} >
                                    //         <span className="iconify fa" data-icon="fa-solid:plus" data-inline="false"></span>
                                    //     Thêm mới
                                    //     </Button>
                                    // }
                                    AdvanceFilter={
                                        <Formik onSubmit={(values) => {
                                            const result = {};
                                            Object.keys(values)
                                                .forEach(key => result[key] = values[key]);
                                        
                                            this.handleChange({ event: 'changeFilter', data: { filter: result } });
                                        }}
                                            enableReinitialize={true}
                                            initialValues={this.props.meta.filter}
                                        >
                                            {({handleSubmit, handleChange, values, setFieldValue}) => (
                                                <Form noValidate onSubmit={handleSubmit}>
                                                    <Row className="form-group">                                                       
                                                        <Col sm={'auto'} className="text-right">Ngày xử lý</Col>
                                                        <Col sm={'auto'}>
                                                            <DatePicker
                                                                className="form-control custom-datepicker"
                                                                todayButton="Hôm nay"
                                                                isClearable
                                                                showMonthDropdown
                                                                showYearDropdown
                                                                dropdownMode="select"
                                                                placeholderText="Từ ngày"
                                                                maxDate={values["enddate"] ? new Date(values["enddate"]) : null}
                                                                onChange={(date) => {
                                                                    setFieldValue("startdate", this.context.logsService.changeFormatDateTime(date));
                                                                }}

                                                                selected={values["startdate"] ? new Date(values["startdate"]) : null}
                                                                dateFormat="dd/MM/yyyy"
                                                            />
                                                        </Col>
                                                        <Col sm={'auto'}>
                                                            <DatePicker
                                                                className="form-control custom-datepicker"
                                                                todayButton="Hôm nay"
                                                                isClearable
                                                                showMonthDropdown
                                                                showYearDropdown
                                                                dropdownMode="select"
                                                                placeholderText="Đến ngày"
                                                                minDate={values["startdate"] ? new Date(values["startdate"]) : null}
                                                                onChange={(date) => {
                                                                    setFieldValue("enddate", this.context.logsService.changeFormatDateTime(date));
                                                                }}

                                                                selected={values["enddate"] ? new Date(values["enddate"]) : null}
                                                                dateFormat="dd/MM/yyyy"
                                                            />
                                                        </Col>
                                                        <Col sm={'auto'} className="">
                                                            <Button size="sm" type="submit" variant="primary">
                                                                <span className="iconify" data-icon="fa-solid:search" data-inline="false" />
                                                                Tìm kiếm
                                                            </Button>
                                                        </Col>
                                                    </Row>
                                                </Form>
                                            )}
                                        </Formik>
                                    }
                                >
                                </GridView.Header>
                                <GridView.Table
                                    className="col-12"
                                    noSelected={true}
                                    data={this.props.data}
                                    keyExtractor={({ item }) => {
                                        return item.id;
                                    }}
                                    sort={this.props.meta.sort}
                                    page={this.props.meta.page}
                                    page_size={this.props.meta.page_size}
                                    total={this.props.meta.total}
                                >
                                    <GridView.Table.Column style={{ width: '6%' }}
                                        title="STT"
                                        className="text-center"
                                        body={({ index }) => (
                                            <span>{index + 1 + (this.props.meta.page - 1) * this.props.meta.page_size}</span>
                                        )} />
                                    <GridView.Table.Column style={{ width: '12%' }}
                                        title="Thời gian"
                                        sortKey="Created"
                                        body={({ item }) => (
                                            <span>{this.context.logsService.formatFullDateTime(item.created)}</span>
                                        )} />
                                    <GridView.Table.Column style={{ width: '20%' }}
                                        title="Người thao tác"
                                        sortKey="CreatedBy"
                                        body={({ item }) => (
                                            <span>{item.createdBy}</span>
                                        )} />
                                    <GridView.Table.Column style={{ width: '12%' }} className="view-action"
                                        title="Địa chỉ truy cập"
                                        body={({ item }) => (
                                            <span>{item.remoteIpAddress}</span>
                                        )} />
                                    <GridView.Table.Column
                                        title="Đường dẫn trang"
                                        sortKey="Content"
                                        body={({ item }) => (
                                            <span>{item.content}</span>
                                        )} />
                                    <GridView.Table.Column style={{ width: '12%' }}
                                        title="Trạng thái"
                                        sortKey="status"
                                        body={({ item }) => (
                                            <span>{item.status === 1?"Thành công":"Không thành công"}</span>
                                        )} />
                                    
                                </GridView.Table>
                            </GridView>
                        </Card.Body>
                    </Card>
                    <LogsFormComponent onClose={this.handleClose.bind(this)} />
                </Page.Content >
            </Page >
        );
    }
}
const mapStateToProps = (state) => {
    return {
        data: state.logs.logsList,
        meta: state.logs.meta
    };
};
const LogsAccessComponent = connect(mapStateToProps, logsAction)(LogsAccess);
export { LogsAccessComponent };