import React from 'react';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal } from 'react-bootstrap';
import { Formik } from 'formik';
import { string, object } from 'yup';
import { LoadingComponent, } from 'shared/components';
import { Context } from '../logsService';
import { LOGS_TITLE } from 'redux/quantrihethong/logs/logsConstant';
import { toast } from 'react-toastify';


const schema = object({
    title: string().trim().required('Tên nhóm không được để trống'),
    code: string().trim().required('Mã nhóm không được để trống')
});
const userDefault = {
    title: '',
    code: '',
};
class LogsFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            listGroup: [],
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
            // dataTinhThanh:[]
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.subscriptions['sendToForm'] = this.context.logsService.sendToForm.subscribe(res => {
            switch (res.action) {
                case 'new':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: userDefault });
                    break;
                case 'edit':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.logsService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                case 'read':
                    this.setState({ editMode: false, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.logsService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                default:
                    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: userDefault });
                    break;
            }
        });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        if (this.state.id) {
            this.subscriptions['update'] = this.context.logsService.update(data, this.state.id).subscribe(() => {

                toast.success('Cập nhật thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        } else {
            this.subscriptions['create'] = this.context.logsService.create(data).subscribe(() => {
                toast.success('Thêm mới thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        }

    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }

    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    computedTitle() {
        if (this.state.id) {
            if (this.state.editMode) {
                return 'Chỉnh sửa thông tin ' + LOGS_TITLE;
            }
            return 'Chi tiết ' + LOGS_TITLE;
        }
        return 'Thêm mới ' + LOGS_TITLE;
    }
    render() {
        return (
            <Modal size="lg" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                        console.log(values);
                        this.onSubmit(values);
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        values,
                        touched,
                        errors,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} onKeyPress={this.onKeyPress.bind(this)}>
                            <Modal.Header closeButton>
                                <Modal.Title>{this.computedTitle()}</Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <Form.Row>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>Thời gian</Form.Label>

                                    </Form.Group>
                                    <Form.Group as={Col} md="10">
                                        {this.context.logsService.formatFullDateTime(values.created)}
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>Người xử lý</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} md="10">
                                        {values.createdBy}
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>Hành động</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} md="10">
                                        {values.actionName}
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>Đối tượng</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} md="10">
                                        {values.title}
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>Nội dung</Form.Label>
                                    </Form.Group>
                                    <Form.Group as={Col} md="10">
                                        {values.content}
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md="2">
                                        <Form.Label>Trạng thái</Form.Label>

                                    </Form.Group>
                                    <Form.Group as={Col} md="10">
                                        {values.statusName}
                                    </Form.Group>
                                </Form.Row>                                
                            </Modal.Body>
                            <Modal.Footer>                                
                                <Button variant="default" onClick={() => { this.handleClose(false); }}>
                                    Đóng
                                </Button>
                            </Modal.Footer>
                        </Form>
                    )}
                </Formik>
            </Modal>

        );
    }
}
export { LogsFormComponent };