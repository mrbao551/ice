import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context, _logsService } from './logsService';
import { LogsListComponent } from './list/LogsListComponent';
import { LogsLoginComponent } from './list/LogsLoginComponent';
import { LogsAccessComponent } from './list/LogsAccessComponent';
import { LogsFormComponent } from './form/LogsFormComponent';
class logsModule extends Component {
    static propTypes = {
        match: PropTypes.object
    }
    render() {
        let { path } = this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({ beh }) => (
                        <Context.Provider value={{
                            logsService: _logsService,
                            beh:beh
                        }} >
                         
                            <Switch>
                                <Route path={`${path}/view`} render={(props) => <LogsListComponent {...props} ></LogsListComponent>} ></Route>
                                <Route path={`${path}/form/:id`} render={(props) => <LogsFormComponent {...props} />} />
                                <Route path={`${path}/login`} render={(props) => <LogsLoginComponent {...props} />} />
                                <Route path={`${path}/access`} render={(props) => <LogsAccessComponent {...props} />} />
                                <Route path={`${path}/change`} render={(props) => <LogsListComponent {...props} />} />
                                <Route path={this.props.match.path} render={(props) => <LogsListComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { logsModule };