import React from 'react';
import PropTypes from 'prop-types';
import { Form, Col, Button, Modal } from 'react-bootstrap';
import { Formik } from 'formik';
import { string, object } from 'yup';
import { LoadingComponent, } from 'shared/components';
import { Context } from '../groupsService';
import {GROUPS_TITLE} from 'redux/quantrihethong/groups/groupsConstant';
import {toast} from 'react-toastify';


const schema = object({
    title: string().trim().required('Tên nhóm không được để trống').max(250, 'Bạn nhập tối đa 250 ký tự'),
    code: string().trim().required('Mã nhóm không được để trống').max(20, 'Bạn nhập tối đa 20 ký tự')
});
const userDefault = {
    title: '',
    code: '',
};
class GroupsFormComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: userDefault,
            listGroup: [],
            editMode: false,
            loading: false,
            isShow: false,
            action: 'new',
            id: null,
            // dataTinhThanh:[]
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.subscriptions['sendToForm'] = this.context.groupsService.sendToForm.subscribe(res => {
            switch (res.action) {
                case 'new':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: null, data: userDefault });
                    break;
                case 'edit':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.groupsService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                case 'read':
                    this.setState({ editMode: false, isShow: res.isShow, action: res.action, id: res.id, loading: true });
                    this.context.groupsService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    break;
                default:
                    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: userDefault });
                    break;
            }
        }); 
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        if (this.state.id) {
            this.subscriptions['update'] = this.context.groupsService.update(data, this.state.id).subscribe(() => {
               
                toast.success('Cập nhật thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        } else {
            this.subscriptions['create'] = this.context.groupsService.create(data).subscribe(() => {
                toast.success('Thêm mới thành công');
                this.handleClose(true);
                this.setState({
                    loading: false
                });
            },
                (err) => {
                    this.setState({
                        loading: false
                    });
                    toast.error(err.error);
                });

        }

    }
    onEdit() {
        this.setState({
            editMode: true
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }

    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    computedTitle() {
        if (this.state.id) {
            if (this.state.editMode) {
                return 'Chỉnh sửa thông tin '+GROUPS_TITLE;
            }
            return 'Chi tiết '+GROUPS_TITLE;
        }
        return 'Thêm mới '+GROUPS_TITLE;
    }
    render() {
        return (
            <Modal size="lg" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Formik
                    validationSchema={schema}
                    onSubmit={(values) => {
                        console.log(values);
                        this.onSubmit(values);
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        values,
                        touched,
                        errors,
                    }) => (
                            <Form noValidate onSubmit={handleSubmit} onKeyPress={(ev) => {ev.stopPropagation();}}>
                                <Modal.Header closeButton>
                                    <Modal.Title>{this.computedTitle()}</Modal.Title>
                                </Modal.Header>
                            <Modal.Body onKeyPress={this.onKeyPress.bind(this)}>
                                    
                                    <Form.Row >
                                        <Form.Group as={Col} md="12" controlId="mahom">
                                            <Form.Label>Mã nhóm</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                            {this.state.editMode ?
                                                <React.Fragment>
                                                    <Form.Control
                                                        type="text"
                                                        name="code"
                                                        value={values.code || ''}
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        isInvalid={touched.code && !!errors.code}
                                                    />
                                                    <Form.Control.Feedback type="invalid">
                                                        {errors.code}
                                                    </Form.Control.Feedback>
                                                </React.Fragment> :
                                                <p className="form-control-static">{values.code}</p>
                                            }

                                        </Form.Group> 
                                    </Form.Row>
                                    <Form.Row>
                                        <Form.Group as={Col} md="12" controlId="title">
                                            <Form.Label>Tên nhóm</Form.Label>{this.state.editMode ? <Form.Label className="text-danger">(*)</Form.Label> : ""}
                                            {this.state.editMode ?
                                                <React.Fragment>
                                                    <Form.Control
                                                        type="text"
                                                        name="title"
                                                        value={values.title || ''}
                                                        onChange={handleChange}
                                                        onBlur={handleBlur}
                                                        isInvalid={touched.title && !!errors.title}
                                                    />
                                                    <Form.Control.Feedback type="invalid">
                                                        {errors.title}
                                                    </Form.Control.Feedback>
                                                </React.Fragment> :
                                                <p className="form-control-static">{values.title}</p>
                                            }
                                        </Form.Group>                                    
                                    </Form.Row>
                                </Modal.Body>
                                <Modal.Footer>                                    
                                    {this.state.editMode ?
                                        <Button type="submit">Lưu lại</Button>
                                        :
                                        ''
                                    }
                                    <Button variant="default" onClick={() => { this.handleClose(false); }}>
                                        Đóng
                                </Button>
                                </Modal.Footer>
                            </Form>
                        )}
                </Formik>
            </Modal>

        );
    }
}
export { GroupsFormComponent };