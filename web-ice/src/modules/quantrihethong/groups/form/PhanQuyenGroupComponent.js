import React from 'react';
import PropTypes from 'prop-types';
import { Button, Modal, Col, Row } from 'react-bootstrap';
import { LoadingComponent, } from 'shared/components';
import { Context } from '../groupsService';
import { GROUPS_TITLE } from 'redux/quantrihethong/groups/groupsConstant';
import { RoleItemComponent } from './RoleItemComponent';

class PhanQuyenGroupComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            editMode: false,
            loading: false,
            isShow: false,
            dataPermissions: [],
            dataRoles: []
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        this.subscriptions['sendToFormPermission'] = this.context.groupsService.sendToFormPermission.subscribe(res => {
            switch (res.action) {
                case 'actpermission':
                    this.setState({ editMode: true, isShow: res.isShow, action: res.action, id: res.id, loading: true, data: {} });
                    this.context.groupsService.getById(res.id).subscribe(res => {
                        this.setState({
                            data: res,
                            loading: false
                        });
                    });
                    this.context.groupsService.getAllPermissions().subscribe(res => {
                        this.setState({
                            dataPermissions: res
                        });
                    });
                    this.context.groupsService.getAllRoles().subscribe(res => {
                        this.setState({
                            dataRoles: res
                        });
                    });
                    break;
                default:
                    this.setState({ editMode: true, isShow: res.isShow, action: 'new', id: null, data: {} });
                    break;
            }
        });
    }
    componentWillUnmount() {
        Object.keys(this.subscriptions).forEach((key) => {
            this.subscriptions[key].unsubscribe();
        });
    }
    handleClose(isRefesh) {
        this.setState({ isShow: false });
        if (this.props.onClose) {
            this.props.onClose(isRefesh);
        }
    }
    onKeyPress(ev) {
        const keyCode = ev.keyCode || ev.which;
        if (keyCode === 13) {
            ev.preventDefault();
            return false;
        }
    }
    computedTitle() {
        return 'Phân quyền ' + GROUPS_TITLE;
    }
    render() {
        const { dataPermissions, data, dataRoles } = this.state;
        return (
            <Modal size="xl" keyboard={false} backdrop="static" show={this.state.isShow} onHide={this.handleClose.bind(this)}>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>
                <Modal.Header closeButton>
                    <Modal.Title>{this.computedTitle()}</Modal.Title>
                </Modal.Header>
                <Modal.Body className="grid-permission">
                    <Row className="header">
                        <Col className="td" md={1}>STT</Col>
                        <Col className="td" md={4}>Tên đối tượng</Col>
                        <Col className="td">Thêm</Col>
                        <Col className="td">Sửa</Col>
                        <Col className="td">Xóa</Col>
                        <Col className="td">Phê duyệt</Col>
                        <Col className="td">Quyền chức năng</Col>
                        <Col className="td">Xử lý</Col>
                    </Row>
                    {
                        dataRoles && dataRoles.map((item, index) => (
                            <RoleItemComponent key={index} stt={(index + 1)} groupId={data.id} role={item} dataPermissions={dataPermissions} />
                        ))
                    }                    
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="default" onClick={() => { this.handleClose(false); }}>
                        Đóng
                                </Button>
                </Modal.Footer>
            </Modal>

        );
    }
}
export { PhanQuyenGroupComponent };