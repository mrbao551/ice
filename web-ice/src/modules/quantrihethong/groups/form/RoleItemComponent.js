import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Row, Col } from 'react-bootstrap';
import { Formik } from 'formik';
import { Context } from '../groupsService';
import { string, object } from 'yup';
import { toast } from 'react-toastify';
import { LoadingComponent, } from 'shared/components';

const schema = object({
    title: string().trim().required('Tên nhóm không được để trống'),
    code: string().trim().required('Mã nhóm không được để trống')
});
const dataDefault = {
    act01: false,
    act02: false,
    act03: false,
    act04: false,
    act05: false,
    roleId: 0,
    groupId: 0,
};
class RoleItemComponent extends React.Component {
    static propTypes = {
        onClose: PropTypes.func,
        groupId: PropTypes.number,
        role: PropTypes.object,
        dataPermissions: PropTypes.array,
        stt: PropTypes.number,
    };
    static contextType = Context;
    constructor(props) {
        super(props);
        this.state = {
            data: dataDefault,
            loading: false,
        };
        this.subscriptions = {};
    }
    componentDidMount() {
        const { groupId, role } = this.props;
        this.setState({
            roleId: role.id,
            groupId: groupId
        });
        if (groupId && role && role.id) {
            this.feachData(groupId, role.id);
        }
    }
    feachData(groupId, roleId) {
        if (groupId && roleId) {
            this.context.groupsService.getPermissionsRoles(groupId, roleId).subscribe(res => {
                this.setState({
                    data: res
                });
            });
        }

    }
    componentDidUpdate(prevProps, prevState) {

        if (prevProps.groupId !== this.props.groupId) {
            this.feachData(this.props.groupId, this.state.roleId)
        }
        if (prevProps.role.id !== this.props.role.id) {
            this.feachData(this.state.groupId, this.props.role.id)
        }
    }
    onSubmit(data) {
        this.setState({
            loading: true
        });
        this.context.groupsService.updatePermission(data).subscribe(res => {
            toast.success('Cập nhật thành công');
            this.setState({
                loading: false
            });
        }, err => {
            this.setState({
                loading: false
            });
            toast.error(err);
        });
    }
    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.checked ? true : false;
        this.setState({
            [name]: value
        });
    }
    render() {
        const { role, stt } = this.props;
        return (
            <React.Fragment>
                <LoadingComponent loading={this.state.loading}></LoadingComponent>

                <Formik
                    onSubmit={(values) => {
                        console.log(values);
                        this.onSubmit(values);
                    }}
                    enableReinitialize={true}
                    initialValues={this.state.data}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        values,
                    }) => (
                        <Form noValidate onSubmit={handleSubmit} >
                            <Row className="body">
                                <Col className="td text-center" md={1}>{stt}</Col>
                                <Col className="td" md={4}>{role.description}</Col>
                                <Col className="td text-center"><Form.Check type="checkbox" name="act01" checked={values.act01} onChange={handleChange}
                                    onBlur={handleBlur} /></Col>
                                <Col className="td text-center"><Form.Check type="checkbox" name="act02" checked={values.act02} onChange={handleChange}
                                    onBlur={handleBlur} /></Col>
                                <Col className="td text-center"><Form.Check type="checkbox" name="act03" checked={values.act03} onChange={handleChange}
                                    onBlur={handleBlur} /></Col>
                                <Col className="td text-center"><Form.Check type="checkbox" name="act04" checked={values.act04} onChange={handleChange}
                                    onBlur={handleBlur} /></Col>
                                <Col className="td text-center"><Form.Check type="checkbox" name="act05" checked={values.act05} onChange={handleChange}
                                    onBlur={handleBlur} /></Col>
                                <Col className="td text-center"><Button size="sm" variant="primary" type="submit">Cập nhật</Button></Col>
                            </Row>
                        </Form>
                    )}
                </Formik>
            </React.Fragment>
        );
    }
}
export { RoleItemComponent };