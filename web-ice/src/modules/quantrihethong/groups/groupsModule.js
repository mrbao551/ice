import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { BehaviorsContext } from 'shared/services';
import { Context, _groupsService } from './groupsService';
import { GroupsListComponent } from './list/GroupsListComponent';
import { GroupsFormComponent } from './form/GroupsFormComponent';
class groupsModule extends Component {
    static propTypes = {
        match: PropTypes.object
    }
    render() {
        let { path } = this.props.match;
        return (
            <BehaviorsContext.Consumer>
                {
                    ({ beh }) => (
                        <Context.Provider value={{
                            groupsService: _groupsService,
                            beh:beh
                        }} >
                         
                            <Switch>
                                <Route path={`${path}/view`} render={(props) => <GroupsListComponent {...props} ></GroupsListComponent>} ></Route>
                                <Route path={`${this.props.match.path}/form/:id`} render={(props) => <GroupsFormComponent {...props} />} />
                                <Route path={this.props.match.path} render={(props) => <GroupsListComponent {...props} />} />
                            </Switch>   
                        </Context.Provider>
                    )
                }
            </BehaviorsContext.Consumer>
        );
    }
}
export { groupsModule };