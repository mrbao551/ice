<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* API */
//$route['api/product'] = 'api/Product';
//$route['api/product/(:any)'] = 'api/Product/$1';
//$route['api/product/(:num)']['PUT'] = 'api/Product/$1';
//$route['api/product/(:num)']['DELETE'] = 'api/Product/$1';
$route['api/register'] = 'api/User/register';
$route['api/re-register'] = 'api/User/re_register';
$route['api/login'] = 'api/User/login';
$route['api/logout'] = 'api/User/logout';
$route['api/user-step'] = 'api/User/step';
$route['api/reGenToken'] = 'api/Token/reGenToken';
$route['api/create'] = 'api/Tests';
$route['api/tests'] = 'api/Tests';
$route['api/tests/(:num)/(:num)']['PUT'] = 'api/Tests/$1/$2';
$route['api/update-luot-thi'] = 'api/User/stepUpdate';
$route['api/submit-luot-thi'] = 'api/User/stepSubmit';
$route['api/get-user'] = 'api/User/getDetailUser';
$route['api/tests/result/(:num)'] = 'api/Tests/result/$1';
$route['api/getconfig'] = 'api/Config';
$route['api/get-total-register'] = 'api/User/totalRegister';
$route['api/get-total-exam'] = 'api/User/totalExam';
$route['api/edit-member']['PUT'] = 'api/User/editMember';

$route['upload-trac-nghiem'] = 'Upload/index';
$route['upload-bai-doc'] = 'Upload/baidoc';

$route['bang-a'] = 'Upload/banga';
$route['bang-b'] = 'Upload/bangb';
$route['tu-luan-bang-a'] = 'Upload/tuluanbanga';
$route['tu-luan-bang-b'] = 'Upload/tuluanbangb';

//$route['cap-nhat-diem-thi'] = 'api/User/updateAllDiemThi';

$route['danh-sach-diem-thi-bang-a'] = 'api/User/danhSachDiemThiBangA';
$route['export-diem-bang-a'] = 'api/User/exportExcelBangA';
$route['danh-sach-diem-thi-bang-b'] = 'api/User/danhSachDiemThiBangB';
$route['export-diem-bang-b'] = 'api/User/exportExcelBangB';

$route['get-diem-thi-user'] = 'api/User/getDiemThiByUser';


$route['danh-sach-bang-a'] = 'Home/thongKeHightShools';
$route['danh-sach-bang-b'] = 'Home/thongKeShools';

/** Backend router */
$route['calendar'] = 'backend/Hoso/calendar';


$route['backend-admin'] = 'backend/Home/index';
$route['admin-login'] = 'backend/Home/login';
$route['logout'] = 'backend/Home/logout';
$route['admin-register'] = 'backend/Home/register';
$route['update-status'] = 'backend/Home/updateStatus';
$route['delete-row'] = 'backend/Home/deleteRow';
$route['delete-rows'] = 'backend/Home/deleteRows';
$route['update-stt'] = 'backend/Home/updateStt';

$route['list-ask'] = 'backend/Ask/index';
$route['list-ask/edit/(:num)'] = 'backend/Ask/formExam/$1';
$route['list-ask/add'] = 'backend/Ask/formExam';
$route['action-form-ask'] = 'backend/Ask/actionForm';

$route['list-essay'] = 'backend/Essay/index';
$route['list-essay/edit/(:num)'] = 'backend/Essay/formExam/$1';
$route['list-essay/add'] = 'backend/Essay/formExam';
$route['action-form-essay'] = 'backend/Essay/actionForm';

$route['list-language'] = 'backend/Language/index';
$route['all-language/(:num)'] = 'backend/Language/loadRecord/$1';
$route['list-language/edit/(:num)'] = 'backend/Language/formExam/$1';
$route['list-language/add'] = 'backend/Language/formExam';
$route['action-form-language'] = 'backend/Language/actionForm';

$route['list-level'] = 'backend/Level/index';
$route['all-level/(:num)'] = 'backend/Level/loadRecord/$1';
$route['list-level/edit/(:num)'] = 'backend/Level/formExam/$1';
$route['list-level/add'] = 'backend/Level/formExam';
$route['action-form-level'] = 'backend/Level/actionForm';

$route['list-ho-so'] = 'backend/Hoso/index';
$route['all-ho-so/(:num)'] = 'backend/Hoso/loadRecord/$1';
$route['list-ho-so/edit/(:num)'] = 'backend/Hoso/formExam/$1';
$route['list-ho-so/add'] = 'backend/Hoso/formExam';
$route['action-form-ho-so'] = 'backend/Hoso/actionForm';

$route['list-fee'] = 'backend/Fee/index';
$route['action-form-fee'] = 'backend/Fee/actionForm';

$route['list-register'] = 'backend/Fee/register';

$route['list-province'] = 'backend/Province/index';
$route['all-province/(:num)'] = 'backend/Province/loadRecord/$1';
$route['list-province/edit/(:num)'] = 'backend/Province/formExam/$1';
$route['list-province/add'] = 'backend/Province/formExam';
$route['action-form-province'] = 'backend/Province/actionForm';

$route['list-schools'] = 'backend/Schools/index';
$route['all-schools/(:num)'] = 'backend/Schools/loadRecord/$1';
$route['list-schools/edit/(:num)'] = 'backend/Schools/formExam/$1';
$route['list-schools/add'] = 'backend/Schools/formExam';
$route['action-form-schools'] = 'backend/Schools/actionForm';

$route['list-hightschool'] = 'backend/Hightschool/index';
$route['all-hightschool/(:num)'] = 'backend/Hightschool/loadRecord/$1';
$route['list-hightschool/edit/(:num)'] = 'backend/Hightschool/formExam/$1';
$route['list-hightschool/add'] = 'backend/Hightschool/formExam';
$route['action-form-hightschool'] = 'backend/Hightschool/actionForm';

$route['list-users'] = 'backend/Users/index';
//$route['action-form-users'] = 'backend/Users/actionForm';
$route['all-users/(:num)'] = 'backend/Users/loadRecord/$1';
$route['list-users/edit/(:num)'] = 'backend/Users/formExam/$1';
$route['list-users/add'] = 'backend/Users/formExam';
$route['action-form-users'] = 'backend/Users/actionForm';
