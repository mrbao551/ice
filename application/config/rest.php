<?php defined('BASEPATH') OR exit('No direct script access allowed');

//Change this to TRUE
$config['check_cors'] = TRUE;

//No change here
$config['allowed_cors_headers'] = [
    'Origin',
    'X-Requested-With',
    'Content-Type',
    'Accept',
    'Access-Control-Request-Method',
    'Authorization',
];

//No change here
$config['allowed_cors_methods'] = [
    'GET',
    'POST',
    'OPTIONS',
    'PUT',
    'PATCH',
    'DELETE'
];

//Set to TRUE to enable Cross-Origin Resource Sharing (CORS) from any source domain
$config['allow_any_cors_domain'] = TRUE;


//Used if $config['check_cors'] is set to TRUE and $config['allow_any_cors_domain'] is set to FALSE.
//Set all the allowable domains within the array
//e.g. $config['allowed_origins'] =['http://www.example.com','https://spa.example.com']

$config['allowed_cors_origins'] = [];


/*
|--------------------------------------------------------------------------
| CORS Forced Headers
|--------------------------------------------------------------------------
|
| If using CORS checks, always include the headers and values specified here
| in the OPTIONS client preflight.
| Example:
| $config['forced_cors_headers'] = [
|   'Access-Control-Allow-Credentials' => 'true'
| ];
|
| Added because of how Sencha Ext JS framework requires the header
| Access-Control-Allow-Credentials to be set to true to allow the use of
| credentials in the REST Proxy.
| See documentation here:
| http://docs.sencha.com/extjs/6.5.2/classic/Ext.data.proxy.Rest.html#cfg-withCredentials
|
*/
$config['forced_cors_headers'] = [];

/*
|--------------------------------------------------------------------------
| REST Enable Logging
|--------------------------------------------------------------------------
|
| When set to TRUE, the REST API will log actions based on the column names 'key', 'date',
| 'time' and 'ip_address'. This is a general rule that can be overridden in the
| $this->method array for each controller
|
| Default table schema:
|   CREATE TABLE `logs` (
|       `id` INT(11) NOT NULL AUTO_INCREMENT,
|       `uri` VARCHAR(255) NOT NULL,
|       `method` VARCHAR(6) NOT NULL,
|       `params` TEXT DEFAULT NULL,
|       `api_key` VARCHAR(40) NOT NULL,
|       `ip_address` VARCHAR(45) NOT NULL,
|       `time` INT(11) NOT NULL,
|       `rtime` FLOAT DEFAULT NULL,
|       `authorized` VARCHAR(1) NOT NULL,
|       `response_code` smallint(3) DEFAULT '0',
|       PRIMARY KEY (`id`)
|   ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
|
*/
$config['rest_enable_logging'] = false;


/*
|--------------------------------------------------------------------------
| REST IP White-list
|--------------------------------------------------------------------------
|
| Limit connections to your REST server with a comma separated
| list of IP addresses
|
| e.g: '123.456.789.0, 987.654.32.1'
|
| 127.0.0.1 and 0.0.0.0 are allowed by default
|
*/
$config['rest_ip_whitelist'] = '';

/*
|--------------------------------------------------------------------------
| Global IP Blacklisting
|--------------------------------------------------------------------------
|
| Prevent connections to the REST server from blacklisted IP addresses
|
| Usage:
| 1. Set to TRUE and add any IP address to 'rest_ip_blacklist'
|
*/
$config['rest_ip_blacklist_enabled'] = false;

/*
|--------------------------------------------------------------------------
| REST IP Blacklist
|--------------------------------------------------------------------------
|
| Prevent connections from the following IP addresses
|
| e.g: '123.456.789.0, 987.654.32.1'
|
*/
$config['rest_ip_blacklist'] = '';

/*
|--------------------------------------------------------------------------
| REST Database Group
|--------------------------------------------------------------------------
|
| Connect to a database group for keys, logging, etc. It will only connect
| if you have any of these features enabled
|
*/
$config['rest_database_group'] = 'default';








//$config['force_https'] = FALSE;
//$config['rest_default_format'] = 'json';
//$config['rest_supported_formats'] = [
//    'json',
//    'array',
//    'csv',
//    'html',
//    'jsonp',
//    'php',
//    'serialized',
//    'xml',
//];
//$config['rest_status_field_name'] = 'status';
//$config['rest_message_field_name'] = 'error';
//$config['enable_emulate_request'] = TRUE;
//$config['rest_realm'] = 'REST API';
//$config['rest_auth'] = 'basic';
//$config['auth_source'] = '';
//$config['allow_auth_and_keys'] = TRUE;
//$config['auth_library_class'] = '';
//$config['auth_library_function'] = '';
//$config['auth_override_class_method_http']['user']['register']['post'] = 'none';
//$config['auth_override_class_method_http']['user']['login']['post'] = 'none';
//$config['rest_valid_logins'] = ['user1' => '12345'];
//$config['rest_ip_whitelist_enabled'] = FALSE;
//$config['rest_ip_whitelist'] = '';
//$config['rest_ip_blacklist_enabled'] = FALSE;
//$config['rest_ip_blacklist'] = '';
//$config['rest_database_group'] = 'default';
//$config['rest_keys_table'] = 'my_users';
//$config['rest_enable_keys'] = TRUE;
//$config['rest_key_column'] = 'apikey';
//$config['rest_limits_method'] = 'ROUTED_URL';
//$config['rest_key_length'] = 40;
//$config['rest_key_name'] = 'X-API-KEY';
//$config['rest_enable_logging'] = TRUE;
//$config['rest_logs_table'] = 'app_logs';
//$config['rest_enable_access'] = FALSE;
//$config['rest_access_table'] = 'access';
//$config['rest_logs_json_params'] = FALSE;
//$config['rest_enable_limits'] = TRUE;
//$config['rest_limits_table'] = 'app_limits';
//$config['rest_ignore_http_accept'] = FALSE;
//$config['rest_ajax_only'] = FALSE;
//$config['rest_language'] = 'english';
//$config['check_cors'] = FALSE;
//$config['allowed_cors_headers'] = [
//    'Origin',
//    'X-Requested-With',
//    'Content-Type',
//    'Accept',
//    'Access-Control-Request-Method'
//];
//$config['allowed_cors_methods'] = [
//    'GET',
//    'POST',
//    'OPTIONS',
//    'PUT',
//    'PATCH',
//    'DELETE'
//];
//$config['allow_any_cors_domain'] = TRUE;
//$config['allowed_cors_origins'] = [];