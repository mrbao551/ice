<?php defined('BASEPATH') OR exit('No direct script access allowed');
class General {
    private $CI;
    public function __construct()
    {
        $this->CI =& get_instance();
        // Do something with $params
    }
    public function random($leng = 10, $char= FALSE){
        if($char == FALSE) $s = 'ABCDEFGHIJKLMNOPQTUVWXYZabcdefghijklmnopqtuvxyz0123456789!@#$)(&%^%';
        else $s = 'ABCDEFGHIJKLMNOPQTUVWXYZabcdefghijklmnopqtuvxyz0123456789';
        mt_srand((double)microtime() * 1000000);
        $salt = '';
        for($i=0;$i<$leng;$i++){
            $salt = $salt . substr($s, (mt_rand()%(strlen($s))),1);
        }
        return $salt;
    }
    
    public function allow_post($param, $allow){
        $_temp = NULL;
        if(isset($param) && count($param) && isset($allow) && count($allow)){
            foreach($param as $key=>$value){
                if(in_array($key,$allow) == TRUE){
                    $_temp[$key] = trim($value);
                }
            }
            return $_temp;
        }
        return $param;
    }
    public function php_redirect($url){
        header('Location: '.$url); die;
    }
    function fullurl()
    {
        return $actual_link = $_SERVER['DOMAIN'].$_SERVER['PHP_SELF'];
    }
    public function js_redirect($alert, $url){
        die('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><script type="text/javascript">alert(\''.$alert.'\'); location.href = \''.$url.'\';</script>');
    }
    public function js_reload_page($alert){
        die('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><script type="text/javascript">alert(\''.$alert.'\'); location.href = \''. $_SERVER['REQUEST_URI'].'\'; </script>');
    }
    public function js_reload($alert){
        die('<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><script type="text/javascript">alert(\''.$alert.'\'); location.href = \''.BASE_URL.substr($_SERVER['REQUEST_URI'],1).'\'; </script>');
    }
    public function active_field($id,$tbl,$field,$status) {
        $this->CI->db->where(array('id'=>$id))->update($tbl, array($field=>$status));
        return true;
    }
    public function str_substr($str = NULL, $n = 0){
        if(strlen($str) < $n) return $str;
        $html = substr($str, 0, $n);
        $html = substr($html, 0, strrpos($html,' '));
        return $html;
    }
    public function seo_name_url($str){
        $tmp = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
        $tmp = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $tmp);
        $tmp = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $tmp);
        $tmp = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $tmp);
        $tmp = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $tmp);
        $tmp = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $tmp);
        $tmp = preg_replace("/(đ)/", 'd', $tmp);
        $tmp = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $tmp);
        $tmp = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $tmp);
        $tmp = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $tmp);
        $tmp = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $tmp);
        $tmp = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $tmp);
        $tmp = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $tmp);
        $tmp = preg_replace("/(Đ)/", 'D', $tmp);
        $tmp = strtolower(trim($tmp));
        //$tmp = str_replace('-','',$tmp);
        $tmp = str_replace(' ', '', $tmp);
        $tmp = str_replace('(', '', $tmp);
        $tmp = str_replace(')', '', $tmp);
        $tmp = str_replace('_', '', $tmp);
        $tmp = str_replace('.', '', $tmp);
        $tmp = str_replace("'", '', $tmp);
        $tmp = str_replace('"', '', $tmp);
        $tmp = str_replace('"', '', $tmp);
        $tmp = str_replace('"', '', $tmp);
        $tmp = str_replace("'", '', $tmp);
        $tmp = str_replace('̀', '', $tmp);
        $tmp = str_replace('&', '', $tmp);
        $tmp = str_replace('@', '', $tmp);
        $tmp = str_replace('^', '', $tmp);
        $tmp = str_replace('=', '', $tmp);
        $tmp = str_replace('+', '', $tmp);
        $tmp = str_replace(':', '', $tmp);
        $tmp = str_replace(',', '', $tmp);
        $tmp = str_replace('{', '', $tmp);
        $tmp = str_replace('}', '', $tmp);
        $tmp = str_replace('?', '', $tmp);
        $tmp = str_replace('\\', '', $tmp);
        $tmp = str_replace('/', '', $tmp);
        $tmp = str_replace('quot;', '', $tmp);
        return $tmp;
    }
}