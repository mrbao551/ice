<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BackendHoso_model extends CI_Model
{

    /**
     * CONSTRUCTOR | LOAD DB
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'users';
    }

    public function show($id = 0)
    {
        if (!empty($id)) {
            $query = $this->db->get_where($this->table, ['id' => $id])->row_array();
        } else {
            $query = $this->db->get($this->table)->result_array();
        }
        return $query;
    }

    public function showWhereIn($id)
    {
        $query = $this->db->where_in('id', explode(',', $id))->where('active', 1)->get($this->table)->result_array();
        return $query;
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($data, $id)
    {
        $this->db->update($this->table, $data, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function delete($id)
    {
        $this->db->delete($this->table, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function addMaxStt()
    {
        $ret = $this->db->select("MAX(stt) AS stt")->from($this->table)->get()->row_array();
        return $ret['stt'] + 1;
    }

    public function countAllHoSoDHNN($fee = 0)
    {
        if (empty($fee)) {
            $query = $this->db->from($this->table)->where(['active' => 1, 'university_id' => 210, 'university_agency' => 1])->count_all_results();
        } else {
            $this->db->from($this->table);
            $this->db->group_start(); //this will start grouping
            $this->db->where(['active' => 1, 'university_agency' => 1, 'university_id' => 210, 'active_fee' => 1]);
            $this->db->or_where('active_fee', 2);
            $this->db->group_end(); //this will end grouping
            $query = $this->db->count_all_results();
        }
        return $query;
    }

    public function countAllHoSoDHQG($fee = 0)
    {
        if (empty($fee)) {
            $this->db->from($this->table);
            $this->db->where(['active' => 1]);
            $this->db->where("                
                (university_agency = 1 AND university_id != 210 
             OR (university_agency = 3 AND university_id != 210)
             OR (university_agency = 3 AND university_id = 210))
            ", NULL, FALSE);
            $query = $this->db->count_all_results();
        } else {
            $this->db->from($this->table);
            $this->db->where(['active' => 1]);
            $this->db->where(" 
             (university_agency = 1 AND university_id != 210 
             OR (university_agency = 3 AND university_id != 210)
             OR (university_agency = 3 AND university_id = 210))
             AND (active_fee = 1 OR active_fee = 2)
            ", NULL, FALSE);
            $query = $this->db->count_all_results();
        }
        return $query;
    }


    public function countAllHoSoOther($fee = 0)
    {
        if (empty($fee)) {
            $query = $this->db->from($this->table)->where(['active' => 1, 'university_agency' => 2])->count_all_results();
        } else {
            $this->db->from($this->table);
            $this->db->group_start(); //this will start grouping
            $this->db->where(['active' => 1, 'university_agency' => 2, 'active_fee' => 1]);
            $this->db->group_end(); //this will end grouping
            $query = $this->db->count_all_results();
        }
        return $query;
    }


}
