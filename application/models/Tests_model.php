<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tests_model extends CI_Model
{

    /**
     * CONSTRUCTOR | LOAD DB
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * SHOW | GET method.
     *
     * @return Response
     */
    public function creatRandomAskAnswer($class_id, $litmit)
    {
        $this->db->select('id, title,class_id,stt, diem'); //class_id Bảng cuộc thi
        $this->db->order_by('id', 'RANDOM');
        $this->db->where(array('active' => 1, 'class_id' => $class_id, 'tu_luan_id' => 0));
        $this->db->limit($litmit, 0); // 70
        $query = $this->db->get('ask')->result();
        return $query;
    }
    public function creatRandomTuLuan($class_id)
    {
        $this->db->select('id, content'); //class_id Bảng cuộc thi
        $this->db->order_by('id', 'RANDOM');
        $this->db->where(array('active' => 1, 'class_id' => $class_id));
        $this->db->limit(1, 0); // Lấy 1 bài tự luận
        $query = $this->db->get('tu_luan')->row_array();
        return $query;
    }
    public function getListAskTuLuan($tu_luan_id)
    {
        $this->db->select('id, title,class_id,stt,tu_luan_id, diem'); 
        $this->db->order_by('stt', 'ASC');
        $this->db->where(array('active' => 1, 'tu_luan_id' => $tu_luan_id));        
        $query = $this->db->get('ask')->result();
        return $query;
    }
    public function showReplyMember($user_id = 0, $luot_thi, $type)
    {
        $this->db->select('id, ask_id, reply_id, tu_luan_id, diem_ask'); //class_id Bảng cuộc thi
        $this->db->where(array('user_id' => $user_id, 'user_luot_thi_id' => $luot_thi, 'tu_luan_id'=>$type));        
        $query = $this->db->get('reply_member')->result();
        // echo $this->db->last_query();
        return $query;
    }
    public function showReplyMemberResult($luot_thi, $bai_viet_id)
    {
        $this->db->select('id, ask_id, reply_id, reply_id_true, diem_ask'); //class_id Bảng cuộc thi
        $this->db->where(array('user_luot_thi_id' => $luot_thi, 'tu_luan_id'=>$bai_viet_id));        
        $query = $this->db->get('reply_member')->result();
        // echo $this->db->last_query();
        return $query;
    }

    public function getDiemLuotThi($luot_thi_id, $bai_viet_id)
    {
        $this->db->select_sum('diem_ask'); //class_id Bảng cuộc thi
        $this->db->where("reply_id <=> reply_id_true AND user_luot_thi_id = $luot_thi_id AND tu_luan_id = $bai_viet_id", NULL, FALSE);
        $query = $this->db->get('reply_member')->result_array();
//         echo $this->db->last_query();
        return $query[0]['diem_ask'];
    }

    public function getAnswers($ask_id)
    {
        if ($ask_id) {
            $this->db->select('id, title,content'); //class_id Bảng cuộc thi
            $this->db->where(array('ask_id' => $ask_id, 'active' => 1));
            $query = $this->db->get('reply')->result();
        } else {
            return null;
        }
        return $query;
    }

    public function getAnswersTrue($ask_id)
    {
        if ($ask_id) {
            $this->db->select('id'); //class_id Bảng cuộc thi
            $this->db->where(array('ask_id' => $ask_id, 'reply_true' => 1, 'active' => 1));
            $query = $this->db->get('reply')->row('id');
        } else {
            return null;
        }
        return $query;
    }

    public function getAnswersByAskId($ask_id)
    {
        if ($ask_id) {
            $this->db->select('id, title,content,stt'); //class_id Bảng cuộc thi
            $this->db->where(array('ask_id' => $ask_id, 'active' => 1));
            $query = $this->db->get('reply')->result();
        } else {
            return null;
        }
        return $query;
    }

    public function getUserReplyById($id)
    {
        if ($id) {
            $this->db->select('user_id'); //class_id Bảng cuộc thi
            $this->db->where(array('id' => $id));
            $query = $this->db->get('reply_member')->row_array();
        } else {
            return null;
        }
        return $query;
    }

    public function getTitleByAskId($ask_id)
    {
        if ($ask_id) {
            $this->db->select('title'); //class_id Bảng cuộc thi
            $this->db->where(array('id' => $ask_id, 'active' => 1));
            $query = $this->db->get('ask')->row_array();
        } else {
            return null;
        }
        return $query['title'];
    }

    public function getCauTraLoiDung($ask_id)
    {
        if ($ask_id) {
            $this->db->select('id'); //class_id Bảng cuộc thi
            $this->db->where(array('ask_id'=>$ask_id, 'reply_true' => 1, 'active' => 1));
            $query = $this->db->get('reply')->row_array();
        } else {
            return null;
        }
        return $query['id'];
    }


    public function getTuLuanByTuLuanId($id)
    {
        if ($id) {
            //$this->db->select('content'); //class_id Bảng cuộc thi
            $this->db->where(array('id' => $id, 'active' => 1));
            $query = $this->db->get('tu_luan')->row_array();
            return $query;
        } else {
            return null;
        }        
    }

    public function getLuotThiCurrent($userId, $soLuot)
    {
        if ($userId) {
            //$this->db->select('user_id'); //class_id Bảng cuộc thi
            $this->db->where(array('user_id' => $userId, 'so_luot'=>$soLuot));
            $this->db->limit(1, 0);
            $query = $this->db->get('user_luot_thi')->row_array();
            //echo $this->db->last_query();
        } else {
            return null;
        }
        return $query;
    }
    

    /**
     * INSERT | POST method.
     *
     * @return Response
     */
    public function insert($data)
    {
        $this->db->insert('reply_member', $data);
        return $this->db->insert_id();
    }

    /**
     * UPDATE | PUT method.
     *
     * @return Response
     */
    public function update($data, $id)
    {
        $data = $this->db->update('reply_member', $data, array('id' => $id));
//        echo $this->db->last_query();
        return $this->db->affected_rows();
    }


    /**
     * UPDATE | PUT method.
     *
     * @return Response
     */
    public function updateReplyMember($data, $id)
    {
        $data = $this->db->update('reply_member', $data, array('id' => $id));
//        echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    /**
     * DELETE method.
     *
     * @return Response
     */
    public function delete($id)
    {
        $this->db->delete('products', array('id' => $id));
        return $this->db->affected_rows();
    }
}
