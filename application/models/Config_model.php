<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Config_model extends CI_Model
{

    /**
     * CONSTRUCTOR | LOAD DB
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * SHOW | GET method.
     *
     * @return Response
     */
    
    
    public function getConfig()
    {        
        $this->db->order_by('id', 'DESC');
        $this->db->limit(1, 0); // Lấy 1 bài tự luận
        $query = $this->db->get('config')->row_array();
        return $query;
    }
    
    

    /**
     * INSERT | POST method.
     *
     * @return Response
     */
    public function insert($data)
    {
        $this->db->insert('config', $data);
        return $this->db->insert_id();
    }

    /**
     * UPDATE | PUT method.
     *
     * @return Response
     */
    public function update($data, $id)
    {
        $data = $this->db->update('config', $data, array('id' => $id));
//        echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    /**
     * DELETE method.
     *
     * @return Response
     */
    public function delete($id)
    {
        $this->db->delete('config', array('id' => $id));
        return $this->db->affected_rows();
    }
}
