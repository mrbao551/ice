<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class User_model extends CI_Model
{

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {

        parent::__construct();
        $this->load->database();

    }

    /**
     * create_user function.
     *
     * @access public
     * @param mixed $username
     * @param mixed $email
     * @param mixed $password
     * @return bool true on success, false on failure
     */
    public function create_user($username, $email, $password, $fullname, $class_id, $phone, $ngay_sinh, $gioi_tinh, $user_info, $truong_hoc_id, $truong_hoc_ten)
    {

        $data = array(
            'username' => $username,
            'email' => $email,
            'fullname' => $fullname,
            'class_id' => $class_id,
            'phone' => $phone,
            'ngay_sinh' => $ngay_sinh,
            'gioi_tinh' => $gioi_tinh,
            'user_info' => $user_info,
            'truong_hoc_id' => $truong_hoc_id,
            'truong_hoc_ten' => $truong_hoc_ten,
            'password' => $this->hash_password($password),
            'created_at' => date('Y-m-j H:i:s'),
        );

        $this->db->insert('users', $data);
        return $this->db->insert_id();

    }

    public function create_user_admin($username, $email, $fullname, $password)
    {
        $data = array(
            'username' => $username,
            'email' => $email,
            'fullname' => $fullname,
            'password' => $this->hash_password($password)
        );
        print_r($data);
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    public function update($data, $id)
    {
        $data = $this->db->update('users', $data, array('id'=>$id));
        return $this->db->affected_rows();
    }

    public function update_user($userId, $email, $fullname, $password)
    {
        if($password == ''){
            $data = array(
                'email' => $email,
                'fullname' => $fullname,
            );
        } else {
            $data = array(
                'email' => $email,
                'fullname' => $fullname,
                'password' => $this->hash_password($password)
            );
        }

        $this->db->where('id', $userId);
        $this->db->update('users', $data);

        return $this->db->affected_rows();
    }

    public function matchOldPassword($id, $password)
    {
        $this->db->select('password');
        $this->db->from('users');
        $this->db->where('id', $id);
        $hash = $this->db->get()->row('password');

        return $this->verify_password_hash($password, $hash);
    }
    
    /**
     * resolve_user_login function.
     *
     * @access public
     * @param mixed $username
     * @param mixed $password
     * @return bool true on success, false on failure
     */
    public function resolve_user_login($email, $password)
    {

        $this->db->select('password');
        $this->db->from('users');
        $this->db->where('email', $email);
        $hash = $this->db->get()->row('password');

        return $this->verify_password_hash($password, $hash);

    }

    /**
     * get_user_id_from_username function.
     *
     * @access public
     * @param mixed $username
     * @return int the user id
     */
    public function get_user_id_from_username($email)
    {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $email);

        return $this->db->get()->row('id');

    }


    public function checkUserName($param)
    {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('username', $param);

        return $this->db->get()->row('id');

    }

    public function checkEmail($param)
    {

        $this->db->select('id');
        $this->db->from('users');
        $this->db->where('email', $param);

        return $this->db->get()->row('id');

    }

    /**
     * get_user function.
     *
     * @access public
     * @param mixed $user_id
     * @return object the user object
     */
    public function get_user($user_id)
    {

        $this->db->from('users');
        $this->db->where('id', $user_id);
        return $this->db->get()->row();

    }
    
    public function totalRegisterMember()
    {
        $this->db->select('id');
        $this->db->from('users');
        $count = $this->db->count_all_results();
        return $count;
    }


    public function totalExam()
    {
        $this->db->select('id');
        $this->db->from('reply_member');
        $count = $this->db->count_all_results();
        return $count;
    }

    
    /**
     * hash_password function.
     *
     * @access private
     * @param mixed $password
     * @return string|bool could be a string on success, or bool false on failure
     */
    private function hash_password($password)
    {

        return password_hash($password, PASSWORD_BCRYPT);

    }

    /**
     * verify_password_hash function.
     *
     * @access private
     * @param mixed $password
     * @param mixed $hash
     * @return bool
     */
    private function verify_password_hash($password, $hash)
    {

        return password_verify($password, $hash);

    }

    public function getAllUserLimit($limit){
        $this->db->select('id');
        $this->db->where(array('tong_diem' => -1));
        $this->db->order_by('id ASC');
        $this->db->limit($limit);
        $result = $this->db->get('users')->result_array();
        return $result;
    }

    public function getAllUsersByClass($class_id){
        $this->db->select('id, fullname, phone, ngay_sinh, email, user_info, diem_trac_nghiem, diem_tu_luan, tong_diem');
        $this->db->where(array('tong_diem >=' => 0, 'class_id' => $class_id));
        $this->db->order_by('tong_diem desc');
//        $this->db->limit(100);
        $result = $this->db->get('users')->result_array();
        return $result;
    }
    public function getAllUsersByClass_page($class_id, $page_size, $page){
        $this->db->select('id, fullname, phone, ngay_sinh, email, user_info, diem_trac_nghiem, diem_tu_luan, tong_diem');
        $this->db->where(array('tong_diem >=' => 0, 'class_id' => $class_id));
        $this->db->order_by('tong_diem desc');
        $result = $this->db->limit($page_size, ($page - 1) * $page_size)->get('users')->result_array();
        return $result;
    }
    public function getAllUsersByClassCouunt($class_id){
        $result = $this->db->from('users')->where(array('tong_diem >=' => 0, 'class_id' => $class_id))->count_all_results();
        return $result;
    }

    public function getAllUserLuotThi(){
        $this->db->select('user_id');
        $this->db->where(array('tong_diem >=' => 0));
        $this->db->group_by('user_idc');
        $result = $this->db->get('user_luot_thi')->result_array();
        return $result;
    }

}
