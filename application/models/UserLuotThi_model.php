<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 *
 * @extends CI_Model
 */
class UserLuotThi_model extends CI_Model {

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct() {

        parent::__construct();
        $this->load->database();

    }

    public function getLastByUser($user_id)
    {
        $query = $this->db->where(['user_id' => $user_id])->order_by('so_luot DESC')->limit(1)->get('user_luot_thi')->row_array();
        return $query;
    }

    /**
     * get_user function.
     *
     * @access public
     * @param mixed $user_id
     * @return object the user object
     */
    public function getDetail($user_id) {

        $this->db->from('user_luot_thi');
        $this->db->where(array('user_id' => $user_id));
        $this->db->order_by('so_luot DESC');
        return $this->db->get()->result();

    }

    /**
     * INSERT | POST method.
     *
     * @return Response
     */
    public function insert($data)
    {
        $this->db->insert('user_luot_thi',$data);
        return $this->db->insert_id();
    }

    /**
     * UPDATE | PUT method.
     *
     * @return Response
     */
    public function update($data, $id)
    {
        $data = $this->db->update('user_luot_thi', $data, array('id'=>$id));
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    /**
     * DELETE method.
     *
     * @return Response
     */
    public function delete($id)
    {
        $this->db->delete('user_luot_thi', array('id'=>$id));
        return $this->db->affected_rows();
    }

    public function getUserLuotThiById($id)
    {
        if ($id) {            
            $this->db->where(array('id' => $id));
            $query = $this->db->get('user_luot_thi')->row_array();
        } else {
            return null;
        }
        return $query;
    }

    public function getListLuotThiByUserId($id)
    {
        if ($id) {            
            $this->db->select('id,so_luot,trang_thai,time_bat_dau_thi,time_ket_thuc_thi,time_con_lai'); //class_id Bảng cuộc thi
            $this->db->where(array('user_id' => $id));
            $query = $this->db->get('user_luot_thi')->result();
        } else {
            return null;
        }
        return $query;
    }
    public function getListLuotThi_ErorrByUserId($id)
    {
        if ($id) {
            $this->db->where(array('user_id' => $id, 'diem_bi_loi' => 1));
            $query = $this->db->get('user_luot_thi')->result();
        } else {
            return null;
        }
        return $query;
    }
    public function getListLuotThi_ByUserId($id)
    {
        if ($id) {
            $this->db->select('id, tu_luan_id');
            $this->db->where(array('user_id' => $id));
            $query = $this->db->get('user_luot_thi')->result();
        } else {
            return null;
        }
        return $query;
    }
    public function getDetailLuotThiByUserId($id)
    {
        if ($id) {            
            //$this->db->select('id,user_id,so_luot,trang_thai,time_bat_dau_thi,time_ket_thuc_thi,time_con_lai'); //class_id Bảng cuộc thi
            $this->db->where(array('id' => $id));
            $query = $this->db->get('user_luot_thi')->row_array();
        } else {
            return null;
        }
        return $query;
    }



}
