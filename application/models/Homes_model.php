<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Homes_model extends CI_Model
{

    /**
     * CONSTRUCTOR | LOAD DB
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    /**
     * SHOW | GET method.
     *
     * @return Response
     */

    public function show($id = 0)
    {
        if (!empty($id)) {
            $query = $this->db->get_where('exams', ['id' => $id, 'active' => 1])->row_array();
        } else {
            $query = $this->db->where(['active' => 1])->order_by('stt ASC')->get('exams')->result_array();
        }
        return $query;
    }

    public function getUniversityDhqg($number)
    {

        if ($number == 1) {
            $query = $this->db->select('id, title')->where(array("view" => 1000))->get('schools')->result_array();
        } elseif ($number == 3) {
            $query = $this->db->select('id, title')->where(array("view !=" => 1000))->get('schools')->result_array();
        }
        return $query;
    }

    public function getAddressByProvinceExam()
    {
        $query = $this->db->select('id, title')->where(array("provinceId" => 1, "active" => 1))->get('aptis_address')->result_array();
        return $query;
    }

    public function getProvinceExam()
    {
        $query = $this->db->select('*')->where(['active' => 1])->order_by('id ASC')->get('province')->result_array();
        return $query;
    }

    public function deadline()
    {
        $_background_image = $this->db->select('val_vi')->where(array('keyword' => 'deadline'))->get('config')->row_array();
        return $_background_image['val_vi'];
    }

    public function getAptisDate($addressId, $dateReg)
    {
        if (!empty($addressId) || !empty($dateReg)) {
            $query = $this->db->select('id, title')->where(array("addressId" => $addressId, "provinceId" => 1, 'time_title >=' => $dateReg, 'active' => 1))->order_by('time_title ASC')->get('aptis_date')->result_array();
            return $query;
        }
        return null;
    }

    public function shiftsList($dateId)
    {
        if (!empty($dateId)) {
            $query = $this->db->where(array("id" => $dateId, 'active' => 1))->get('date_exam')->row_array();
            return $query;
        }
        return null;
    }

    public function countShiftsShiningHoSo($date_exam_id, $shifts_exam)
    {
        if (!empty($date_exam_id) || !empty($shifts_exam)) {
            $count = $this->db->where(array("date_exam_id" => $date_exam_id, 'shifts_exam' => $shifts_exam, 'active_fee !=' => 0))->from('hoso')->count_all_results();
            return $count;
        }
    }

    public function insert($data)
    {
        $this->db->insert('hoso', $data);
        return $this->db->insert_id();
    }

    public function update($table, $data, $id)
    {
        $this->db->update($table, $data, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function delete($table, $id)
    {
        $this->db->delete($table, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function deleteMultiple($table, $data)
    {
        if (!empty($data)) {
            $this->db->where_in('id', explode(",", $data));
            $this->db->delete($table);
        }
        return $this->db->affected_rows();
    }


}
