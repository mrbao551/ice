<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BackendEssay_model extends CI_Model
{

    /**
     * CONSTRUCTOR | LOAD DB
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'tu_luan';
    }

    public function show($id = 0)
    {
        if(!empty($id)){
            $query = $this->db->get_where($this->table, ['id' => $id])->row_array();
        }else{
            $query = $this->db->get($this->table)->result_array();
        }
        return $query;
    }


    public function showAsk($id = 0, $tu_luan_id)
    {
        if(!empty($id)){
            $query = $this->db->get_where('ask', ['id' => $id, 'tu_luan_id' => $tu_luan_id])->row_array();
        }else{
            $query = $this->db->get($this->table)->result_array();
        }
        return $query;
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($data, $id)
    {
        $this->db->update($this->table, $data, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function delete($id)
    {
        $this->db->delete($this->table, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function addMaxStt() {
        $ret = $this->db->select("MAX(stt) AS stt")->from($this->table)->get()->row_array();
        return $ret['stt']+1;
    }


}
