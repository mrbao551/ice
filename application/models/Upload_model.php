<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_model extends CI_Model {

    /**
     * CONSTRUCTOR | LOAD DB
     */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    /**
     * SHOW | GET method.
     *
     * @return Response
     */
    public function show($id = 0)
    {
        if(!empty($id)){
            $query = $this->db->get_where("ask", ['id' => $id])->row_array();
        }else{
            $query = $this->db->get("ask")->result();
        }
        return $query;
    }
   public function getBangAsk($id)
    {
        if(!empty($id)){
            $query = $this->db->get_where("ask", ['class_id' => $id, 'tu_luan_id' => 0])->result_array();
        }else{
            $query = $this->db->get("ask")->result();
        }
        return $query;
    }

   public function getBangReply($id)
    {
        $query = $this->db->get_where("reply", ['ask_id' => $id])->result_array();
        return $query;
    }

    public function getTuLuanAsk($id)
    {
        if(!empty($id)){
            $query = $this->db->get_where("ask", ['class_id' => $id, 'tu_luan_id !=' => 0, 'tu_luan_id <' => 5])->result_array();
        }else{
            $query = $this->db->get("ask")->result();
        }
        return $query;
    }

    public function getTuLuanAskb($id)
    {
        if(!empty($id)){
            $query = $this->db->get_where("ask", ['class_id' => $id, 'tu_luan_id !=' => 0, 'tu_luan_id >' => 4])->result_array();
        }else{
            $query = $this->db->get("ask")->result();
        }
        return $query;
    }

    public function getTuLuanReply($id)
    {
        $query = $this->db->get_where("reply", ['ask_id' => $id])->result_array();
        return $query;
    }


    /**
     * INSERT | POST method.
     *
     * @return Response
     */
    public function insert($data)
    {
        $this->db->insert('ask',$data);
        return $this->db->insert_id();
    }

    public function insertReply($data)
    {
        $this->db->insert('reply',$data);
        return $this->db->insert_id();
    }
    
    /**
     * UPDATE | PUT method.
     *
     * @return Response
     */
    public function update($data, $id)
    {
        $data = $this->db->update('ask', $data, array('id'=>$id));
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    /**
     * DELETE method.
     *
     * @return Response
     */
    public function delete($id)
    {
        $this->db->delete('ask', array('id'=>$id));
        return $this->db->affected_rows();
    }


}
