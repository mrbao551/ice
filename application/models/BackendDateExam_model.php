<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BackendDateExam_model extends CI_Model
{

    /**
     * CONSTRUCTOR | LOAD DB
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'date_exam';
    }

    public function show($id = 0)
    {
        if(!empty($id)){
            $query = $this->db->get_where($this->table, ['id' => $id, 'active' => 1])->row_array();
        }else{
            $query = $this->db->get($this->table)->result_array();
        }
        return $query;
    }


    public function showByExamsId($id = 0)
    {
        $query = $this->db->get_where($this->table, ['exams_id' => $id])->result_array();
        return $query;
    }
    
    public function showWhereIn($id)
    {
        $query = $this->db->where_in('id', explode(',', $id))->where('active', 1)->get($this->table)->result_array();
        return $query;
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function insert_batch($data)
    {
        $this->db->insert_batch($this->table, $data);
        return true;
    }
    
    public function update($data, $id)
    {
        $this->db->update($this->table, $data, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function update_batch($data, $id)
    {
        $this->db->update_batch($this->table, $data, $id);
        return true;
    }
    
    public function delete($id)
    {
        $this->db->delete($this->table, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function addMaxStt() {
        $ret = $this->db->select("MAX(stt) AS stt")->from($this->table)->get()->row_array();
        return $ret['stt']+1;
    }


}
