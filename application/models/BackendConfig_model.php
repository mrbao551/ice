<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BackendConfig_model extends CI_Model
{

    /**
     * CONSTRUCTOR | LOAD DB
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'config';
    }


    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($data, $keyword)
    {
        $this->db->update($this->table, $data, array('keyword' => $keyword));
        return $this->db->affected_rows();
    }
    public function updateBatch($data)
    {
        $this->db->update_batch($this->table, $data, 'keyword');
        return $this->db->affected_rows();
    }

    public function delete($keyword)
    {
        $this->db->delete($this->table, array('keyword' => $keyword));
        return $this->db->affected_rows();
    }



}
