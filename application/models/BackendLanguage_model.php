<?php
defined('BASEPATH') or exit('No direct script access allowed');

class BackendLanguage_model extends CI_Model
{

    /**
     * CONSTRUCTOR | LOAD DB
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->table = 'language';
    }

    public function show($id = 0)
    {
        if(!empty($id)){
            $query = $this->db->get_where($this->table, ['id' => $id, 'active' => 1])->row_array();
        }else{
            $query = $this->db->get($this->table)->result_array();
        }
        return $query;
    }

    public function showWhereIn($id)
    {
        $query = $this->db->where_in('id', explode(',', $id))->where('active', 1)->get($this->table)->result_array();
        return $query;
    }

    public function insert($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function update($data, $id)
    {
        $this->db->update($this->table, $data, array('id' => $id));
        return $this->db->affected_rows();
    }

    public function delete($id)
    {
        $this->db->delete($this->table, array('id' => $id));
        return $this->db->affected_rows();
    }


    public function addMaxStt() {
        $ret = $this->db->select("MAX(stt) AS stt")->from($this->table)->get()->row_array();
        return $ret['stt']+1;
    }



}
