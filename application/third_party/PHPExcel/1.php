<?php  
error_reporting(E_ALL);  
date_default_timezone_set('Europe/London');   
require_once '/Classes/PHPExcel.php'; 
$objPHPExcel = new PHPExcel();  
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")  
                             ->setLastModifiedBy("Maarten Balliauw")  
                             ->setTitle("Office 2007 XLSX Document")  
                             ->setSubject("Office 2007 XLSX Document")  
                             ->setDescription("Document for Office 2007 XLSX")  
                             ->setKeywords("Office 2007 openxml")  
                             ->setCategory("Result file");
//Cau hinh kich thuoc, mau, size...
$objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');							 
$objPHPExcel->getActiveSheet()->mergeCells('A1:H1');
//$objPHPExcel->getActiveSheet()->getStyle('A2:H2')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('f3f5f6');
$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);  
$objPHPExcel->getActiveSheet()->getStyle('A3:A1000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('D3:D1000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('C3:C1000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getStyle('E3:E1000')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);  
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(100);
$objPHPExcel->getActiveSheet()->getStyle('A2:Z2')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(16);
//Tieu de danh sach
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', 'DANH SÁCH HỒ SƠ');  
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A2', 'ID');  
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('B2', 'Họ tên');  
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C2', 'Ngày sinh');  
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('D2', 'Giới tính');  
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E2', 'Điện thoại');  
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F2', 'Email');
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G2', 'Quê quán');			
$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H2', 'Địa chị thường trú');
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'csdltainan_db');
$connection = mysql_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD);
$selected = mysql_select_db(DB_DATABASE,$connection); 
mysql_query("set names 'utf8'",$connection);
$sqlgroups="select * from tbl_hoso where 1=1 order by ten ASC limit 15";  
$resultgroups=mysql_query($sqlgroups);  
    $numrows=mysql_num_rows($resultgroups);  
    if ($numrows>0){  
        $count=2;  
        while($data=mysql_fetch_array($resultgroups)){  
            $count+=1;  
            $l1="A"."$count";  
            $l2="B"."$count";  
            $l3="C"."$count";  
            $l4="D"."$count";  
            $l5="E"."$count";  
            $l6="F"."$count";  
            $l7="G"."$count";
			$l8="H"."$count";			
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l1, $data['id']); 
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l2, $data['fullname']);  
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l3, $data['ngaysinh']);  
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l4, $data['gioitinh']);  
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l5, $data['sodienthoai']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($l6, $data['email']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($l7, $data['quequan']);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($l8, $data['diachithuongtru']);  
        }  
    }          
$objPHPExcel->getActiveSheet()->setTitle('Danh sach ho so');  
$objPHPExcel->setActiveSheetIndex(0); 
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="Danh-sach-hoso.xlsx"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
header ('Cache-Control: cache, must-revalidate');
header ('Pragma: public');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');  
exit;  
?>