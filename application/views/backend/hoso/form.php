<div class="layout-page pt-3">
    <div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light"></span><?php echo $action == 'edit' ? 'Sửa thông tin' : 'Thêm mới' ?></h4>

        <!-- Basic Layout & Basic with Icons -->
        <div class="row">
            <!-- Basic Layout -->
            <div class="col-xxl">
                <div class="card mb-4">
                    <div class="card-body">
                        <?php echo form_open('', 'id="actionForm"'); ?>
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="nav-align-top mb-4">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <button
                                                    type="button"
                                                    class="nav-link active"
                                                    role="tab"
                                                    data-bs-toggle="tab"
                                                    data-bs-target="#navs-top-home"
                                                    aria-controls="navs-top-home"
                                                    aria-selected="true">
                                                Thông tin cá nhân
                                            </button>
                                        </li>


                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="navs-top-home" role="tabpanel">
                                            <div class="col-sm-12">

                                                <div class="form-group">
                                                    <label for="usr"><strong>Họ và tên thí sinh (tiếng Việt có dấu) <span style="color: red">*</span></strong>
                                                    </label>
                                                    <input type="text" class="form-control" id="FullName" required="" name="fullname" value="<?php echo isset($detail['fullname']) ? $detail['fullname'] : '' ?>">
                                                </div>
                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="usr"><strong>Số điện thoại<span style="color: red">*</span></strong>
                                                            </label>
                                                            <input type="text" value="<?php echo isset($detail['phone']) ? $detail['phone'] : '' ?>" class="form-control" required="" name="phone">
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="usr"><strong>Email của thí sinh<span style="color: red">*</span></strong>
                                                            </label>
                                                            <input type="email" value="<?php echo isset($detail['email']) ? $detail['email'] : '' ?>" class="form-control" required="" name="email">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-6 col-sm-4  mt-1">
                                                        <div class=" form-group">
                                                            <label for="usr"><strong>Ngày sinh <span style="color: red">*</span></strong>
                                                            </label>
                                                            <input type="text" class="form-control" name="ngay_sinh" value="<?php echo isset($detail['ngay_sinh']) ? $detail['ngay_sinh'] : '' ?>" required="" placeholder="DD-MM-YYYY">
                                                        </div>
                                                    </div>

                                                    <div class="col-6 col-sm-4  mt-1">
                                                        <div class=" form-group">
                                                            <label for="usr"><strong>Giới tính <span style="color: red">*</span></strong>
                                                            </label>
                                                            <div class="form-control" style="border: none;padding-left: 0;">

                                                                <label class="form-check-label">
                                                                    <input type="radio" value="Nam" name="gioi_tinh" <?php echo (isset($detail['gioi_tinh']) && $detail['gioi_tinh'] == 'Name') ? 'checked=""' : '' ?>> <strong>Nam </strong> &nbsp;&nbsp;&nbsp;
                                                                </label>

                                                                <label class="form-check-label">
                                                                    <input type="radio" value="Nữ" name="gioi_tinh" <?php echo (isset($detail['gioi_tinh']) && $detail['gioi_tinh'] == 'Nữ') ? 'checked=""' : '' ?>> <strong>Nữ </strong>
                                                                </label>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-12">
                                <div class="nav-align-top mb-4">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <button
                                                    type="button"
                                                    class="nav-link active"
                                                    role="tab"
                                                    data-bs-toggle="tab"
                                                    data-bs-target="#navs-top-home"
                                                    aria-controls="navs-top-home"
                                                    aria-selected="true">
                                                Thông tin đăng ký
                                            </button>
                                        </li>


                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane fade show active" id="navs-top-home" role="tabpanel">
                                            <div class="col-sm-12">
                                                <?php if($detail['user_info']){
                                                    $user_info = json_decode($detail['user_info'], true);
                                                } ?>

                                                <div class="row">
                                                    <div class="col-12 col-sm-6">
                                                        <label for="usr"><strong>Bảng đăng ký </strong>
                                                        </label>
                                                        <input type="text" class="form-control" id="FullName" required="" name="class_id" value="<?php echo isset($detail['class_id']) ? ($detail['class_id'] == 2 ? 'Bảng A - THPT' : 'Bảng B - Đại học') : '' ?>">
                                                    </div>
                                                    <div class="col-12 col-sm-6">
                                                        <label for="usr"><strong>Tổng điểm đạt được </strong>
                                                        </label>
                                                        <input type="text" class="form-control" id="FullName" required="" name="tong_diem" value="<?php echo isset($detail['tong_diem']) ? $detail['tong_diem'] : '' ?>">
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-12 col-sm-4">
                                                        <label for="usr"><strong>Thành phố </strong>
                                                        </label>
                                                        <input type="text" class="form-control" id="FullName" required="" name="fullname" value="<?php echo isset($user_info['thanh_pho']) ? $user_info['thanh_pho'] : '' ?>">
                                                    </div>
                                                    <div class="col-12 col-sm-4">
                                                        <div class="form-group">
                                                            <label for="usr"><strong>Quận/huyện<span style="color: red">*</span></strong>
                                                            </label>
                                                            <input type="text" value="<?php echo isset($user_info['quan_huyen']) ? $user_info['quan_huyen'] : '' ?>" class="form-control" required="" name="phone">
                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-sm-4">
                                                        <div class="form-group">
                                                            <label for="usr"><strong>Phường/xã<span style="color: red">*</span></strong>
                                                            </label>
                                                            <input type="email" value="<?php echo isset($user_info['phuong_xa']) ?$user_info['phuong_xa'] : '' ?>" class="form-control" required="" name="email">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-6 col-sm-4  mt-1">
                                                        <div class=" form-group">
                                                            <label for="usr"><strong>Tên trường</strong>
                                                            </label>
                                                            <input type="text" class="form-control" name="ten_truong" value="<?php echo isset($user_info['ten_truong']) ? $user_info['ten_truong'] : '' ?>" required="" placeholder="DD-MM-YYYY">
                                                        </div>
                                                    </div>

                                                    <div class="col-6 col-sm-4  mt-1">
                                                        <div class=" form-group">
                                                            <label for="usr"><strong>Khoa/lớp</strong>
                                                            </label>
                                                            <input type="text" class="form-control" name="lop" value="<?php echo isset($user_info['lop']) ? $user_info['lop'] : '' ?>" required="" placeholder="DD-MM-YYYY">
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!-- Tabs -->
                            <div class="row justify-content-start">
                                <div class="col-sm-10">
                                    <a href="javascript:;" onclick="history.back();" name="submit" class="btn btn-primary"><?php echo $action == 'edit' ? 'Quay lại' : 'Thêm mới' ?></a>
<!--                                    <button type="submit" name="submit" class="btn btn-primary">--><?php //echo $action == 'edit' ? 'Thay đổi' : 'Thêm mới' ?><!--</button>-->
                                    <input type="hidden" name="action" value="<?php echo $action ?>">
                                    <input type="hidden" name="id" value="<?php echo !empty($detail['id']) ? $detail['id'] : ''  ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Basic with Icons -->
        </div>
    </div>
    <!-- / Content -->

<!--    <div class="content-backdrop fade"></div>-->
</div>
</div>
<script>
    $(document).on('click', 'input[name="exam_level"]', function() {
        $('input[name="exam_level"]').not(this).prop('checked', false);
    });

    let university_agency = $("input[name='university_agency']:checked").val();
    if(university_agency !== "undefined"){
        showUniversity(university_agency);
    }

    $('input[type=radio][name=university_agency]').change(function() {
        let value = this.value;
        showUniversity(value);
    });

    function showUniversity(value){
        if(value == 3){
            $(document).find('#university-show').show();
            $(document).find('#infoNotTuDo').show();
            $(document).find('#hight-schools-show').hide();
            $(document).find('#priceFeeDhqg').show();
        } else if(value == 2){
            $(document).find('#university-show').hide();
            $(document).find('#hight-schools-show').show();
            $(document).find('#priceFeeDhqg').hide();
            $(document).find('#infoNotTuDo').hide();
        }
        else if(value == 1){
            $(document).find('#university-show').show();
            $(document).find('#infoNotTuDo').show();
            $(document).find('#hight-schools-show').hide();
            $(document).find('#priceFeeDhqg').show();
        }
    }

    $("#actionForm").submit(function(event){
        event.preventDefault();
            $.ajax({
                url:'action-form-ho-so',
                type:'POST',
                dataType: 'json',
                data:$(this).serialize(),
                success:function(result){
                    if(result.status == true){
                        loadToastAjax(result);
                        setTimeout(function(){
                            window.location.href = 'list-ho-so';
                        }, 1000);
                    } else {
                        loadToastAjax(result);
                    }
                }

            });
    });

    $('body').on('click', '.showModalUrl', function () {
        var href = $(this).attr('data-img');
        // console.log('href', href);
        $('body').find('#modalShowImg').modal('show');
        if(href !== ''){
            $('body').find('.modal-body .imgShow').show();
            $('body').find('.imgShow').attr('src', '<?php echo base_url() ?>' + href);
        } else {
            $('body').find('.modal-body .imgShow').hide();
            $('body').find('.modal-body .showTextNoImg').html('<p>Không có hình ảnh</p>');
        }
        $('body').find('#modalCenterTitle').text($(this).text());
    })
</script>

<!-- Modal -->
<div class="modal fade" id="modalShowImg" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCenterTitle">Modal title</h5>
                <button
                        type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                ></button>
            </div>
            <div class="modal-body">
                <img class="imgShow" style="max-width: 100%" src="">
                <div class="showTextNoImg"></div>
            </div>

        </div>
    </div>
</div>