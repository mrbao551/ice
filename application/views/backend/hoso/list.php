<div class="layout-page pt-3">
    <div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Danh sách hồ sơ</span> </h4>

        <!-- Responsive Table -->
        <div class="card p-2 pt-3">
<!--            <div class="row mb-4">-->
<!--                <div class="col-md-2">-->
<!--                    <a href="list-ho-so/add" class="btn btn-primary float-start ">Thêm mới</a>-->
<!--                </div>-->
<!--            </div>-->
            <div class="row">
                <?php $this->load->view('backend/common/sort_page_size') ?>
                <div class="col-md-4">
                    <select class="form-select class_id" name="class_id" aria-label="Default select example">
                        <option selected value="all">Bảng dự thi</option>
                        <option value="2">Bảng A (THPT)</option>
                        <option value="4">Bảng B (Đại học)</option>
                    </select>
                </div>
                <div class="col-md-2 ">
                    <div class="input-group">

                        <a class="btn btn-outline-primary btn-export" href="javascript:void(0);">Export Excel</a>

<!--                        <button class="btn btn-outline-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">-->
<!--                            Export Excel-->
<!--                        </button>-->
<!--                        <ul class="dropdown-menu" style="">-->
<!--                            <li><a class="dropdown-item btn-export" href="javascript:void(0);">Trung tâm khảo thí</a></li>-->
<!--                            <li><a class="dropdown-item btn-export-two" href="javascript:void(0);">Kế hoạch tài chính</a></li>-->
<!--                        </ul>-->
                    </div>
                    
                </div>
                <div class="col-md-2 ">
                    <button type="button" class="showActionDelete btn btn-danger float-end" data-table="users">Xóa lựa chọn</button>
                </div>
                
                <div class="row  mt-3">
                    <div class="col-md-3">
                        <select class="select2 form-select" id="shools" name="truong_hoc_id" aria-label="Default select example">
                            <option selected value="all">Chọn trường Đại học</option>
                            <?php foreach ($schools as $key => $vl){ ?>
                            <option style="padding: 10px 0; margin: 10px 0" value="<?php echo $vl['id'] ?>"><?php echo $vl['title'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select class="select2 form-select" id="hightShools" name="truong_hoc_id" aria-label="Default select example">
                            <option value="all" selected>Chọn trường THPT</option>
                            <?php foreach ($hightSchools as $key => $vl){ ?>
                                <option style="padding: 10px 0; margin: 10px 0" value="<?php echo $vl['id'] ?>"><?php echo $vl['title'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4 ">
                        <input type="text" name="keyword" class="form-control" id="defaultFormControlInput" placeholder="Họ tên/email/SĐT/trường học" aria-describedby="defaultFormControlHelp">
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-primary float-start btn-search">Tìm kiếm</button>
                    </div>
                </div>
            </div>
            
            <div class="table-responsive text-nowrap">
                <table class="table table-borderd" id='postsList'>
                    <thead>
                    <tr class="text-nowrap">
                        <th style="width: 7%;">
                            <div class="form-check mt-3">
                                <input style="width: 18px;height: 18px;" class="form-check-input" type="checkbox" value="" id="selectAll">
                            </div>
                           
                        </th>
                        <th>Họ tên</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th style="width: 10%;">Action</th>
                        <th style="width: 10%;">Status</th>
<!--                        <th class="text-center" style="width: 10%;">STT</th>-->
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <!-- Paginate -->
                <div class="mt-3" id='pagination'></div>
            </div>
        </div>
        <!--/ Responsive Table -->
    </div>
    <!-- / Content -->


    <div class="content-backdrop fade"></div>
</div>
</div>
<script type='text/javascript'>

    //https://www.javatpoint.com/php-codeigniter-3-ajax-pagination-using-jquery
    $(document).ready(function () {
        $('#pagination').on('click', 'a', function (e) {
            e.preventDefault();
            var pageno = $(this).attr('data-ci-pagination-page');
            loadPagination(pageno, '', '', '');
        });
        
        loadPagination(0, '', '', '');
        function loadPagination(page, page_size, keyword, sortBy) {
            var page_size = $('.page-size-show').find(":selected").val();
            var keyword = $('body').find('input[name="keyword"]').val();
            var sortBy = $('.sortByShow').find(":selected").val();
            var class_id = $('.class_id').find(":selected").val();
            var shools = $('#shools').find(":selected").val();
            var hightShools = $('#hightShools').find(":selected").val();
            var csrf_test_name = $("input[name='csrf_test_name']").val();
            $.ajax({
                url: 'all-ho-so/' + page,
                type: 'POST',
                dataType: 'json',
                data: {shools: shools, hightShools: hightShools, class_id: class_id, page_size: page_size, keyword: keyword, sortBy: sortBy, csrf_test_name: csrf_test_name},
                // data: 'page_size=' + page_size + '&keyword=' + keyword + '&sortBy=' + sortBy  + '&csrf_test_name=' + csrf_test_name,
                beforeSend: function() {
                    // setting a timeout
                    $('body').find('.loading').show();
                },
                success: function (response) {
                    $('body').find('.loading').hide();
                    $('#pagination').html(response.pagination);
                    createTable(response.result, response.row);
                    $("#csrfName").val(response.regen_token).trigger("change");
                }
            });
        }

        function loadExportExcel(page, page_size, keyword, sortBy) {
            var page_size = $('.page-size-show').find(":selected").val();
            var keyword = $('body').find('input[name="keyword"]').val();
            var sortBy = $('.sortByShow').find(":selected").val();
            var class_id = $('.class_id').find(":selected").val();
            var shools = $('#shools').find(":selected").val();
            var hightShools = $('#hightShools').find(":selected").val();
            var csrf_test_name = $("input[name='csrf_test_name']").val();
            window.location.href = "backend/Hoso/exportHoso/" + page + '?hightShools='+hightShools +
            '&class_id='+class_id +
            '&shools='+shools +
            '&page_size='+page_size +
            '&keyword='+keyword +
            '&sortBy='+sortBy +
            '&csrf_test_name='+csrf_test_name;
        }
        function loadExportExcelTwo(page, page_size, keyword, sortBy) {
            var page_size = $('.page-size-show').find(":selected").val();
            var keyword = $('body').find('input[name="keyword"]').val();
            var sortBy = $('.sortByShow').find(":selected").val();
            var class_id = $('.class_id').find(":selected").val();
            var exam_id = $('#exam_id').find(":selected").val();
            var date_exam_id = $('#shiftsexam').find(":selected").val();
            var csrf_test_name = $("input[name='csrf_test_name']").val();
            window.location.href = "backend/Hoso/exportHosoTwo/" + page + '?date_exam_id='+date_exam_id +
            '&exam_id='+exam_id +
            '&class_id='+class_id +
            '&page_size='+page_size +
            '&keyword='+keyword +
            '&sortBy='+sortBy +
            '&csrf_test_name='+csrf_test_name;
        }
        
        function createTable(result, sno) {
            sno = Number(sno);
            $('#postsList tbody').empty();
            for (index in result) {
                var id = result[index].id;
                var title = result[index].fullname;
                var phone = result[index].phone;
                var email = result[index].email;
                var stt = result[index].stt;
                var active = result[index].active;
                var active_fee = result[index].active_fee;
                sno += 1;
                var tr = "<tr>";
                // tr += "<td>" + sno + "</td>";
                var checked = '';
                if(active == 1){
                    checked = 'checked=\"\"';
                } else {
                    checked = '';
                }
                var checked_fee = '';
                if(active_fee == 1){
                    checked_fee = 'checked=\"\" disabled';
                } else {
                    checked_fee = 'disabled';
                }
                tr += "<td><div class=\"form-check \">\n" +
                    "  <input name=\"item\" class=\"form-check-input check-item\" type=\"checkbox\" value=\""+id+"\" id=\"itemCheck\">\n" +
                    "  </div></td>";
                tr += "<td><a href='list-ho-so/edit/" + id + "'>" + title + "</a></td>";
                tr += "<td>" + phone + "</td>";
                tr += "<td>" + email + "</td>";

                tr += '<td><a href="list-ho-so/edit/' + id + '" class="badge bg-info"><i class="bx bx-edit"></i></a> <a href="javascript:void(0);" data-id="' + id + '" data-table="users" class="badge bg-danger deleteRow"><i class="bx bx-trash"></i></a></td>';
                tr += "<td class=\"text-center\"><div class=\"d-flex justify-content-center form-check form-switch\">\n" +
                    "  <input class=\"form-check-input switchCheckStatus\" type=\"checkbox\" data-table=\"users\" value=\"" + id + "\" "+checked+">\n" +
                    "  </div></td>";
                // tr += '<td class="d-flex justify-content-center"><input type="text" name="stt" data-id="'+ id +'" data-table="users" value="' + stt + '" class="form-control sttInputText" ></td>';
                tr += "</tr>";
                $('#postsList tbody').append(tr);
            }
        }

        $('.page-size-show').on('change', function() {
            loadPagination(0, this.value, '', '');
        });
        
        $('.sortByShow').on('change', function() {
            loadPagination(0, '', this.value, '');
        });

        $('.class_id').on('change', function() {
            loadPagination(0, '', this.value, '');
        });

        $('#hightShools').on('change', function() {
            loadPagination(0, '', this.value, '');
        });

        $('#shools').on('change', function() {
            loadPagination(0, '', this.value, '');
        });

        $('.btn-search').on('click', function() {
            var keyword = $('body').find('input[name="keyword"]').val();
            loadPagination(0, '', '', keyword);
        });

        $('.btn-export').on('click', function() {
            var keyword = $('body').find('input[name="keyword"]').val();
            loadExportExcel(0, '', '', keyword);
        });

        $('.btn-export-two').on('click', function() {
            var keyword = $('body').find('input[name="keyword"]').val();
            loadExportExcelTwo(0, '', '', keyword);
        });

        $('body').on("change", "#exam_id",function(e) {
            var exam_id = this.value;
            $.ajax ({
                type: 'get',
                dataType: 'json',
                url: "backend/Hoso/loadDateExam",
                data: {"exam_id": exam_id, csrf_test_name: csrf_test_name},
                success: function (json) {
                    let html = '<option value="all" selected>Chọn ngày thi</option>';
                    let data = json.data;
                    for (var i = 0; i < data.length; i++) {
                        let dataRow = data[i];
                        html += '<option value="' + dataRow.id + '">' + dataRow.date_exam + '</option>'
                    }
                    $('#shiftsexam').html(html);

                }
            })
        });

    });
</script>  