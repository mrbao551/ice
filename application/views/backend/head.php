<meta charset="utf-8" />
<meta
    name="viewport"
    content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0"
/>

<title>Quản lý hệ thống - HCmedia.com.vn</title>
<base href="<?php echo base_url() ?>">
<meta name="description" content="" />

<!-- Favicon -->
<link rel="icon" type="image/x-icon" href="public/backend/assets/img/favicon/favicon.ico" />

<!-- Fonts -->
<link rel="preconnect" href="https://fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<link
    href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap"
    rel="stylesheet"
/>

<!-- Icons. Uncomment required icon fonts -->
<link rel="stylesheet" href="public/backend/assets/vendor/fonts/boxicons.css" />

<!-- Core CSS -->
<link rel="stylesheet" href="public/backend/assets/vendor/css/core.css" class="template-customizer-core-css" />
<link rel="stylesheet" href="public/backend/assets/vendor/css/theme-default.css" class="template-customizer-theme-css" />
<link rel="stylesheet" href="public/backend/assets/css/demo.css" />
<link rel="stylesheet" href="public/backend/assets/css/custom.css" />
<link rel="stylesheet" href="public/backend/assets/vendor/css/rtl/core.css" class="template-customizer-core-css" />
<link rel="stylesheet" href="public/backend/assets/vendor/css/rtl/theme-default.css" class="template-customizer-theme-css" />
<!-- Font Awesome -->
<link href="public/backend/assets/vendor/libs/font-awesome/font-awesome.min.css" rel="stylesheet">
<!-- Vendors CSS -->
<link rel="stylesheet" href="public/backend/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css" />
<link rel="stylesheet" href="public/backend/assets/vendor/libs/flatpickr/flatpickr.css" />

<link rel="stylesheet" href="public/backend/assets/vendor/libs/quill/typography.css" />
<link rel="stylesheet" href="public/backend/assets/vendor/libs/quill/katex.css" />
<link rel="stylesheet" href="public/backend/assets/vendor/libs/quill/editor.css" />
<link rel="stylesheet" href="public/backend/assets/vendor/libs/select2/select2.css" />
<link rel="stylesheet" href="public/backend/assets/vendor/libs/fullcalendar/fullcalendar.css" />

<!-- Page CSS -->
<!-- Page -->
<link rel="stylesheet" href="public/backend/assets/vendor/css/pages/page-auth.css" />
<link rel="stylesheet" href="public/backend/assets/vendor/css/pages/app-calendar.css" />
<!-- Helpers -->
<script src="public/backend/assets/vendor/js/helpers.js"></script>

<!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
<!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
<script src="public/backend/assets/js/config.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<input type="hidden" id="csrfName" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
<script>
    var csrf_test_name = $("input[name='csrf_test_name']").val();
</script>
