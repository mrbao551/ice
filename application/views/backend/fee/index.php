<div class="layout-page pt-3">
    <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light"></span>Sửa thông tin</h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <div class="card-body">
                            <?php echo form_open('', 'id="actionForm"'); ?>
                            <?php if(isset($config) && count($config)){
                            foreach($config as $keyConfig=>$valConfig){ ?>
                            <div class="row mb-3">
                                <label class="col-sm-3 col-form-label" for="basic-default-name"><?php echo $valConfig['label']; ?></label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="<?php echo $valConfig['keyword']; ?>" value="<?php echo $valConfig['val_vi']; ?>" id="<?php echo $valConfig['keyword']; ?>" />
                                </div>
                            </div>
                            <?php } } ?>
                            <div class="row justify-content-end">
                                <div class="col-sm-10">
                                    <button type="submit" name="submit" class="btn btn-primary">Thay đổi</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Basic with Icons -->
            </div>
        </div>
        <!-- / Content -->

        <div class="content-backdrop fade"></div>
    </div>
</div>
<script>
    $("#actionForm").submit(function(event){
        tinyMCE.triggerSave(true, true);
        event.preventDefault();
        $.ajax({
            url:'action-form-fee',
            type:'POST',
            dataType: 'json',
            data:$(this).serialize(),
            beforeSend: function() {
                // setting a timeout
                $('body').find('.loading').show();
            },
            success:function(result){
                $('body').find('.loading').hide();
                if(result.status == true){
                    loadToastAjax(result);
                } else {
                    loadToastAjax(result);
                }
            }

        });
    });
</script>