<!DOCTYPE html>
<html
        lang="en"
        class="light-style layout-menu-fixed"
        dir="ltr"
        data-theme="theme-default"
        data-assets-path="../assets/"
        data-template="vertical-menu-template-free"
>
<head>
    <?php $this->load->view('backend/head') ?>
</head>
</head>

<body>
<!-- Layout wrapper -->
<div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">
        <?php $this->load->view('backend/nav-left') ?>
        <!-- Layout container -->
        <?php $this->load->view($template, isset($data) ? $data : NULL); ?>
        <!-- / Layout page -->
    </div>

    <!-- Overlay -->
    <div class="layout-overlay layout-menu-toggle"></div>
</div>
<!-- / Layout wrapper -->

<?php $this->load->view('backend/footer') ?>

</body>
</html>
