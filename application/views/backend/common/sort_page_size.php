<div class="col-md-2">
    <select class="form-select page-size-show" name="page_size" aria-label="Default select example">
        <option hidden>Số bản ghi hiển thị</option>
        <option value="10" selected>10 dòng</option>
        <option value="50">50 dòng</option>
        <option value="100">100 dòng</option>
        <option value="500">500 dòng</option>
        <option value="1000">1,000 dòng</option>
        <option value="10000">10,000 dòng</option>
    </select>
</div>
<div class="col-md-2">
    <select class="form-select sortByShow" name="sortBy" aria-label="Default select example">
        <option hidden>Sắp xếp theo</option>
        <option value="desc" selected>Theo Z-A</option>
        <option value="asc" >Theo A-Z</option>
    </select>
</div>