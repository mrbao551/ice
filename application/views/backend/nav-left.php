<?php
$currentURL = $this->uri->segment('1');
?>
<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo mb-2">
        <a href="#" class="app-brand-link">
              <img  src="public/backend/hcmedia.png">
            <span class="app-brand-text demo menu-text fw-bolder ms-2">CMS</span>
        </a>

<!--        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto d-block d-xl-none">-->
<!--            <i class="bx bx-chevron-left bx-sm align-middle"></i>-->
<!--        </a>-->
        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-inner-shadow"></div>

    <ul class="menu-inner py-1">
        <li class="menu-item <?php echo $currentURL == 'backend-admin' ? ' active open' : ''; ?> ">
            <a href="backend-admin" class="menu-link">
                <i class="menu-icon tf-icons bx bx-home-circle"></i>
                <div data-i18n="Analytics">Bảng điều khiển</div>
            </a>
        </li>
        <li class="menu-header small text-uppercase"><span class="menu-header-text">Danh sách chức năng</span></li>
        <li class="menu-item <?php echo $currentURL == 'list-ho-so' ? ' active open' : ''; ?>">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-layout"></i>
                <div data-i18n="Misc">Danh sách hồ sơ</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item <?php  echo $currentURL == 'list-ho-so' ? ' active' : ''; ?>">
                    <a href="list-ho-so" class="menu-link">
                        <div data-i18n="Error">Hồ sơ thí sinh</div>
                    </a>
                </li>
            </ul>
        </li>
<!--        <li class="menu-header small text-uppercase"><span class="menu-header-text">Danh sách kì thi</span></li>-->
        <li class="menu-item
        <?php
        $arrayLanguage = array(
            'list-ask','list-essay',
        );
        if(in_array($currentURL, $arrayLanguage)){
            echo ' active open';
        } else {
            echo '';
        }
        ?>">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-layout"></i>
                <div data-i18n="Misc">Danh sách học liệu</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item
                <?php  echo $currentURL == 'list-ask' ? ' active' : ''; ?>
                ">
                    <a href="list-ask" class="menu-link">
                        <div data-i18n="Danh sách kì thi">Câu hỏi/trả lời trắc nghiệm</div>
                    </a>
                </li>
                <li class="menu-item
                <?php  echo $currentURL == 'list-essay' ? ' active' : ''; ?>
                ">
                    <a href="list-essay" class="menu-link">
                        <div data-i18n="Danh sách kì thi">Câu hỏi/trả lời bài đọc</div>
                    </a>
                </li>
            </ul>
        </li>

        <li class="menu-header small text-uppercase"><span class="menu-header-text">Cấu hình chung</span></li>
        <li class="menu-item <?php 
        $arrayLanguage = array(
            'list-language', 'list-level', 'list-schools', 'list-province', 'list-fee', 'list-register'
        );
        if(in_array($currentURL, $arrayLanguage)){
            echo ' active open';
        } else {
            echo '';
        }
        ?>">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="menu-icon tf-icons bx bx-layout"></i>
                <div data-i18n="Misc">Cấu hình chung</div>
            </a>
            <ul class="menu-sub">
                

                <li class="menu-item <?php echo $currentURL == 'list-schools' ? ' active' : ''; ?>">
                    <a href="list-schools" class="menu-link">
                        <div data-i18n="Error">Danh sách trường ĐH/CĐ </div>
                    </a>
                </li>
                <li class="menu-item <?php echo $currentURL == 'list-hightschool' ? ' active' : ''; ?>">
                    <a href="list-hightschool" class="menu-link">
                        <div data-i18n="Error">Danh sách trường THPT </div>
                    </a>
                </li>
            </ul>
        </li>

<!--        <li class="menu-header small text-uppercase"><span class="menu-header-text">Thông tin tài khoản</span></li>-->
        <li class="menu-item <?php
        $arrayLanguage = array(
            'list-users'
        );
        if(in_array($currentURL, $arrayLanguage)){
            echo ' active open';
        } else {
            echo '';
        }
        ?>">
            <a href="javascript:void(0);" class="menu-link menu-toggle">
                <i class="bx bx-cog me-2"></i>
                <div data-i18n="Misc">Thông tin tài khoản</div>
            </a>
            <ul class="menu-sub">
                <li class="menu-item <?php echo $currentURL == 'list-users' ? ' active' : ''; ?>">
                    <a href="list-users" class="menu-link">
                        <div data-i18n="Error">Danh sách tài khoản</div>
                    </a>
                </li>
                <li class="menu-item">
                    <a href="logout" class="menu-link">
                        <i class="bx bx-power-off me-2"></i>
                        <div data-i18n="Analytics">Đăng xuất</div>
                    </a>
                </li>
            </ul>
        </li>

    </ul>
</aside>
