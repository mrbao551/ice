<!DOCTYPE html>
<html lang="en" class="light-style customizer-hide">
<head>
    <?php $this->load->view('backend/head') ?>
</head>

<body>
<!-- Content -->
<div class="container-xxl">
    <div class="authentication-wrapper authentication-basic container-p-y">
        <div class="authentication-inner">
            <!-- Register -->
            <div class="card">
                <div class="card-body">
                    <!-- /Logo -->
                    <h4 class="mb-4 text-center">Đăng nhập hệ thống</h4>
                    <div class="container">
                        <?php echo form_open('admin-login', 'id="formAuthentication" class="mb-3"'); ?>
                        <div class="mb-3">
                            <label for="username">Tài khoản</label>
                            <input type="text" class="form-control" value="<?php echo set_value('username'); ?>" id="username" name="username" aria-describedby="emailHelp" placeholder="Nhập tài khoản">
                            <span class="error"><?php echo form_error('username'); ?></span>
                        </div>
                        <div class="mb-3 form-password-toggle">
                            <label for="password">Mật khẩu</label>
                            <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Mật khẩu">
                            <span class="error"><?php echo form_error('password'); ?></span>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-primary d-grid w-100" type="submit">Sign in</button>
                        </div>
                        <span class="error"><?php echo $this->session->flashdata('login_error'); ?></span>
                        <?php form_close(); ?>
                    </div>
                </div>
            </div>
            <!-- /Register -->
        </div>
    </div>
</div>

<!-- / Content -->

<div class="buy-now">
    <a href="http://hcmedia.com.vn" target="_blank" class="btn btn-danger btn-buy-now">Phát triển bởi HCmedia.com.vn</a>
</div>
<?php $this->load->view('backend/footer') ?>
</body>
</html>



