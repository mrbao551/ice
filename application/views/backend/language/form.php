<div class="layout-page pt-3">
    <div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light"></span><?php echo $action == 'edit' ? 'Sửa thông tin' : 'Thêm mới' ?></h4>

        <!-- Basic Layout & Basic with Icons -->
        <div class="row">
            <!-- Basic Layout -->
            <div class="col-xxl">
                <div class="card mb-4">
                    <div class="card-body">
                        <?php echo form_open('', 'id="actionForm"'); ?>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label" for="basic-default-name">Tiêu đề</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="title" value="<?php echo !empty($detail['title']) ? $detail['title'] : ''  ?>" />
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label" for="basic-default-company">Mô tả</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="des" value="<?php echo !empty($detail['des']) ? $detail['des'] : ''  ?>"/>
                                </div>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-sm-10">
                                    <button type="submit" name="submit" class="btn btn-primary"><?php echo $action == 'edit' ? 'Thay đổi' : 'Thêm mới' ?></button>
                                    <input type="hidden" name="action" value="<?php echo $action ?>">
                                    <input type="hidden" name="id" value="<?php echo !empty($detail['id']) ? $detail['id'] : ''  ?>">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Basic with Icons -->
        </div>
    </div>
    <!-- / Content -->

    <div class="content-backdrop fade"></div>
</div>
</div>
<script>
    $("#actionForm").submit(function(event){
        event.preventDefault();
            $.ajax({
                url:'action-form-language',
                type:'POST',
                dataType: 'json',
                data:$(this).serialize(),
                success:function(result){
                    if(result.status == true){
                        loadToastAjax(result);
                        setTimeout(function(){
                            window.location.href = 'list-language';
                        }, 1000);
                    } else {
                        loadToastAjax(result);
                    }
                }

            });
    });
</script>