<div class="layout-page pt-3">
    <div class="content-wrapper">
    <!-- Content -->

    <div class="container-xxl flex-grow-1 container-p-y">
        <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Danh sách kì thi</span> </h4>

        <!-- Responsive Table -->
        <div class="card p-2 pt-3">
            <div class="row mb-4">
                <div class="col-md-2">
                    <a href="list-essay/add" class="btn btn-primary float-start ">Thêm mới</a>
                </div>
            </div>
            <div class="row">
                <?php $this->load->view('backend/common/sort_page_size') ?>
                <div class="col-md-2">
                    <select class="select2 form-select class_id" name="class_id" aria-label="Default select example">
                        <option selected value="all">Bảng</option>
                        <option value="2">Bảng A</option>
                        <option value="4">Bang B</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <input type="text" name="keyword" class="form-control" id="defaultFormControlInput" placeholder="Nhập tiêu đề/mô tả" aria-describedby="defaultFormControlHelp">
                </div>
                <div class="col-md-2">
                    <button type="button" class="btn btn-primary float-start btn-search">Tìm kiếm</button>
                </div>
                <div class="col-md-2 ">
                    <button type="button" class="showActionDelete btn btn-danger float-end" data-table="tu_luan">Xóa lựa chọn</button>
                </div>
            </div>
            
            <div class="table-responsive text-nowrap">
                <table class="table table-borderd" id='postsList'>
                    <thead>
                    <tr class="text-nowrap">
                        <th style="width: 7%;">
                            <div class="form-check mt-3">
                                <input style="width: 18px;height: 18px;" class="form-check-input" type="checkbox" value="" id="selectAll">
                            </div>
                           
                        </th>
                        <th>Tiêu đề</th>
                        <th>Bảng cuộc thi</th>
                        <th style="width: 10%;">Action</th>
                        <th style="width: 10%;">Status</th>
<!--                        <th class="text-center" style="width: 10%;">STT</th>-->
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <!-- Paginate -->
                <div class="mt-3" id='pagination'></div>
            </div>
        </div>
        <!--/ Responsive Table -->
    </div>
    <!-- / Content -->


    <div class="content-backdrop fade"></div>
</div>
</div>
<script type='text/javascript'>

    //https://www.javatpoint.com/php-codeigniter-3-ajax-pagination-using-jquery
    $(document).ready(function () {
        $('#pagination').on('click', 'a', function (e) {
            e.preventDefault();
            var pageno = $(this).attr('data-ci-pagination-page');
            loadPagination(pageno, '', '', '');
        });
        
        loadPagination(0, '', '', '');
        function loadPagination(page, page_size, keyword, sortBy) {
            var page_size = $('.page-size-show').find(":selected").val();
            var keyword = $('body').find('input[name="keyword"]').val();
            var sortBy = $('.sortByShow').find(":selected").val();
            var csrf_test_name = $("input[name='csrf_test_name']").val();
            var class_id = $('.class_id').find(":selected").val();
            $.ajax({
                url: 'backend/Essay/loadRecord/' + page,
                type: 'POST',
                dataType: 'json',
                data: {class_id: class_id, page_size: page_size, keyword: keyword, sortBy: sortBy, csrf_test_name: csrf_test_name},
                // data: 'page_size=' + page_size + '&keyword=' + keyword + '&sortBy=' + sortBy  + '&csrf_test_name=' + csrf_test_name,
                beforeSend: function() {
                    // setting a timeout
                    $('body').find('.loading').show();
                },
                success: function (response) {
                    $('body').find('.loading').hide();
                    $('#pagination').html(response.pagination);
                    createTable(response.result, response.row);
                    $("#csrfName").val(response.regen_token).trigger("change");
                }
            });
        }
        function createTable(result, sno) {
            sno = Number(sno);
            $('#postsList tbody').empty();
            for (index in result) {
                var id = result[index].id;
                var title = result[index].des;
                var class_id = (result[index].class_id == 2 ? 'Bảng A' : 'Bảng B');
                var stt = result[index].stt;
                var active = result[index].active;
                sno += 1;
                var tr = "<tr>";
                // tr += "<td>" + sno + "</td>";
                var checked = '';
                if(active == 1){
                    checked = 'checked=\"\"';
                } else {
                    checked = '';
                }

                if(title.length > 100){
                    title = title.substring(0, 100) + '...';
                }

                tr += "<td><div class=\"form-check \">\n" +
                    "  <input name=\"item\" class=\"form-check-input check-item\" type=\"checkbox\" value=\""+id+"\" id=\"itemCheck\">\n" +
                    "  </div></td>";
                tr += "<td><a href='list-essay/edit/" + id + "'>" + title + "</a></td>";
                tr += "<td><a href='list-essay/edit/" + id + "'>" + class_id + "</a></td>";

                tr += '<td><a href="list-essay/edit/' + id + '" class="badge bg-info"><i class="bx bx-edit"></i></a> <a href="javascript:void(0);" data-id="' + id + '" data-table="tu_luan" class="badge bg-danger deleteRow"><i class="bx bx-trash"></i></a></td>';
                tr += "<td class=\"text-center\"><div class=\"d-flex justify-content-center form-check form-switch\">\n" +
                    "  <input class=\"form-check-input switchCheckStatus\" type=\"checkbox\" data-table=\"tu_luan\" value=\"" + id + "\" "+checked+">\n" +
                    "  </div></td>";
                // tr += '<td class="d-flex justify-content-center"><input type="text" name="stt" data-id="'+ id +'" data-table="tu_luan" value="' + stt + '" class="form-control sttInputText"></td>';
                tr += "</tr>";
                $('#postsList tbody').append(tr);
            }
        }

        $('.page-size-show').on('change', function() {
            loadPagination(0, this.value, '', '');
        });
        $('.sortByShow').on('change', function() {
            loadPagination(0, '', this.value, '');
        });
        $('.class_id').on('change', function() {
            loadPagination(0, '', this.value, '');
        });
        $('.btn-search').on('click', function() {
            var keyword = $('body').find('input[name="keyword"]').val();
            loadPagination(0, '', '', keyword);
        });

    });
</script>  