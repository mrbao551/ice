<div class="layout-page pt-3">
    <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light"></span><?php echo $action == 'edit' ? 'Sửa thông tin' : 'Thêm mới' ?></h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <?php if($action == 'edit'){ ?>
                        <div class="card-body nav-align-top">

                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-top-home" aria-controls="navs-top-home" aria-selected="true">
                                        <h5 class="mb-0">Tiêu đề</h5>
                                    </button>
                                </li>
                                <li class="nav-item">
                                    <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-top-ask" aria-controls="navs-top-home" aria-selected="true">
                                        <h5 class="mb-0">Câu hỏi</h5>
                                    </button>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="navs-top-home" role="tabpanel">
                                    <div class="" id="tab_address_place ">
                                        <?php echo form_open('action-form-essay', 'id="actionForm"  enctype="multipart/form-data"'); ?>
                                        <div class="row mb-3">
                                            <label class="col-sm-2 col-form-label" for="basic-default-name">Tiêu đề</label>
                                            <div class="col-sm-10">
                                                <textarea class="editCustomTextarea" name="title"><?php echo $detail['title']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <label class="col-sm-2 col-form-label" for="basic-default-name">Nội dung</label>
                                            <div class="col-sm-10">
                                                <textarea class="editCustomTextarea" name="content"><?php echo $detail['content']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="row justify-content-start mt-3">
                                            <div class="col-sm-10">
                                                <button type="submit" name="submit" class="btn btn-primary"><?php echo $action == 'edit' ? 'Thay đổi' : 'Thêm mới' ?></button>
                                                <input type="hidden" name="action" value="<?php echo $action ?>">
                                                <input type="hidden" name="id" value="<?php echo !empty($detail['id']) ? $detail['id'] : ''  ?>">
                                            </div>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane fade show" id="navs-top-ask" role="tabpanel">
                                    <div class="" id=" ">
                                        <?php if($askList){ ?>
                                        <?php foreach ($askList as $key => $vl){ ?>
                                        <div class="row mb-3">
                                            <label class="col-sm-2 col-form-label" for="basic-default-name">Câu hỏi <?php echo ($key + 1) ?></label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control askByEssay" data-id = "<?php echo !empty($vl['id']) ? $vl['id'] : ''  ?>" name="title" value="<?php echo !empty($vl['title']) ? $vl['title'] : ''  ?>" />
                                            </div>
                                            <div class="col-sm-2 col-form-label"><a class="showReplyByAsk" data-title="Câu hỏi <?php echo ($key + 1) ?>" data-value="<?php echo !empty($vl['title']) ? $vl['title'] : ''  ?>" data-bs-toggle="modal" data-bs-target="#backDropModal" ask-id="<?php echo !empty($vl['id']) ? $vl['id'] : ''  ?>" href="javascript:;">Xem/sửa Đáp án</a></div>
                                        </div>

                                        <?php } } ?>

                                    </div>
                                </div>

                            </div>


                        </div>
                        <?php } else { ?>
                            <?php echo form_open('', 'id="actionFormAdd"  enctype="multipart/form-data"'); ?>
                            <div class="card-body nav-align-top">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-top-essay" aria-controls="navs-top-essay" aria-selected="true">
                                            <h5 class="mb-0">Nội dung bài tự luận</h5>
                                        </button>
                                    </li>
                                    <li class="nav-item">
                                        <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-top-ask" aria-controls="navs-top-ask" aria-selected="true">
                                            <h5 class="mb-0">Import câu hỏi/trả lời trắc nghiệm</h5>
                                        </button>
                                    </li>
                                </ul>

                                <div class="tab-content">

                                    <div class="tab-pane fade show active" id="navs-top-essay" role="tabpanel">
                                        <div class="" id="tab_address_place ">
                                            <div class="row mb-3">
                                                <div class="row mb-3">
                                                    <label class="col-sm-2 col-form-label" for="basic-default-name">Tiêu đề</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" name="des" value="<?php echo $detail['des']; ?>" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <label class="col-sm-2 col-form-label" for="basic-default-name">Mô tả</label>
                                                    <div class="col-sm-10">
                                                        <textarea class="editCustomTextarea" name="title"><?php echo $detail['title']; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <label class="col-sm-2 col-form-label" for="basic-default-name">Nội dung</label>
                                                    <div class="col-sm-10">
                                                        <textarea class="editCustomTextarea" name="content"><?php echo $detail['content']; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade " id="navs-top-ask" role="tabpanel">
                                        <div class="" id="tab_address_place ">
                                            <div class="row mb-3">
                                                <div class="col-sm-3">
                                                    <select name="class_id" class="form-control select2">
                                                        <option value="0">Chọn bảng</option>
                                                        <option value="2">Bảng A</option>
                                                        <option value="4">Bảng B</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="file" class="form-control" name="uploadFile">
                                                </div>
                                                <label class="col-sm-3 col-form-label" for="basic-default-name"><a href="">Tải file mẫu tại đây</a></label>
                                            </div>
                                        </div>
                                        <div class="row justify-content-start mt-3">
                                            <div class="col-sm-10">
                                                <button type="submit" name="submit" class="btn btn-primary btn-add"><?php echo $action == 'edit' ? 'Thay đổi' : 'Thêm mới' ?></button>
                                                <input type="hidden" name="action" value="<?php echo $action ?>">
                                                <input type="hidden" name="id" value="<?php echo !empty($detail['id']) ? $detail['id'] : ''  ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- Basic with Icons -->
            </div>
        </div>
        <!-- / Content -->

        <div class="content-backdrop fade"></div>
    </div>
</div>


<!-- Modal Backdrop -->
<div class="modal fade" id="backDropModal" data-bs-backdrop="static" tabindex="-1">
    <div class="modal-dialog">
        <?php echo form_open('action-form-essay', 'class="modal-content" id="actionFormReplyTrue"  enctype="multipart/form-data"'); ?>
            <div class="modal-header">
                <h5 class="modal-title" id="backDropModalTitle">Câu hỏi</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

                <div class="modal-body">
                    <p class="showContentAsk"></p>
                    <div id="showReplyByAsk"></div>
                </div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-primary ">Save</button>
                <input type="hidden" value="" name="askId" class="askId">
            </div>
        </form>
    </div>
</div>

<script>
    $('body').on('click', 'input[name="reply_true"]', function() {
        $('input[name="reply_true"]').not(this).prop('checked', false);
    });

    var csrf_test_name = $('body').find('#csrfName').val();

    $("#actionForm").submit(function(event){
        event.preventDefault();
        $.ajax({
            url:'action-form-essay',
            type:'POST',
            dataType: 'json',
            data:$(this).serialize(),
            success:function(result){
                if(result.status == true){
                    loadToastAjax(result);
                    // setTimeout(function(){
                    //     window.location.href = 'list-ask';
                    // }, 1000);
                } else {
                    loadToastAjax(result);
                }
            }

        });
    });

    $("#actionFormReplyTrue").submit(function(event){
        event.preventDefault();
        $.ajax({
            url:'backend/Essay/updateReplyTrue',
            type:'POST',
            dataType: 'json',
            data:$(this).serialize(),
            success:function(result){
                if(result.status == true){
                    loadToastAjax(result);
                    // setTimeout(function(){
                    //     window.location.href = 'list-ask';
                    // }, 1000);
                } else {
                    loadToastAjax(result);
                }
            }

        });
    });

    $('body').on('click', '.btn-add', function (e) {
        e.preventDefault();
        tinyMCE.triggerSave(true, true);
        var form_data = new FormData($('form')[0]);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'action-form-essay',
            data: form_data,
            processData: false,
            contentType: false,
            success: function(result) {
                if(result.status == true){
                    loadToastAjax(result);
                    setTimeout(function(){
                        window.location.href = 'list-essay';
                    }, 1000);
                } else {
                    loadToastAjax(result);
                }
            }
        });
    })

    $('body').on('click', '.showReplyByAsk', function (e) {
        e.preventDefault();
        let askId = $(this).attr("ask-id");
        let askTitle = $(this).attr("data-title");
        let askValue = $(this).attr("data-value");
        let showRepply = '';
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'backend/Essay/showEssayByAsk',
            data: {"askId": askId, "csrf_test_name": csrf_test_name},
            success: function(result) {
                if(result.status == true){
                    let data = result.data;
                    for(var i = 0; i < data.length; i++) {
                        let result = data[i];
                        showRepply += '' +
                            '<div class="form-check mb-3">\n' +
                            '      <input style="margin-top: 10px" class="form-check-input" value="' + result.id + '" ' + result.reply_true + ' type="checkbox" id="reply_true-' + result.id + '" name="reply_true">\n' +
                            '      <label class="form-check-label" for="reply_true-' + result.id + '">\n' +
                            '          ' + result.title + ' -\n' +
                            '          <input type="text" style="width: 400px;display: unset;" data-id="' + result.id + '" value="' + result.content + '" class="form-control replyExam">\n' +
                            '      </label>\n' +
                            '</div>';
                    }
                    $('body').find("#backDropModal #showReplyByAsk").html(showRepply);
                    $('body').find(".askId").val(askId);
                    $('body').find("#backDropModalTitle").text(askTitle);
                    $('body').find(".showContentAsk").html(askValue);
                }
            }
        });
    })

    $('body').on('change', '.askByEssay', function () {
        let value = $(this).val();
        let dataId = $(this).attr('data-id');
        $.ajax({
            url:'backend/Essay/updateReply',
            type:'POST',
            dataType: 'json',
            data:{"value": value, "dataId":dataId, "csrf_test_name": csrf_test_name},
            success:function(result){
                if(result.status == true){
                    loadToastAjax(result);
                } else {
                    loadToastAjax(result);
                }
            }
        });
    })

    $('body').on('change', '.replyExam', function () {
        let value = $(this).val();
        let dataId = $(this).attr('data-id');
        $.ajax({
            url:'backend/Ask/updateReply',
            type:'POST',
            dataType: 'json',
            data:{"value": value, "dataId":dataId, "csrf_test_name": csrf_test_name},
            success:function(result){
                if(result.status == true){
                    loadToastAjax(result);
                } else {
                    loadToastAjax(result);
                }
            }
        });
    })

</script>