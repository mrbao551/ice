<div class="layout-page pt-3">
    <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light"></span>Sửa thông tin</h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <div class="card-body">
                            <?php echo form_open('', 'id="actionForm"'); ?>

                            <div class="row mb-3">
                                <label class="col-sm-3 col-form-label" for="basic-default-name">Tài khoản</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" readonly name="username" value="<?php echo $user->username ?>" id="" />
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-3 col-form-label" for="basic-default-name">Họ tên</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="fullname" value="<?php echo $user->fullname ?>" id="" />
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-sm-3 col-form-label" for="basic-default-name">Mật khẩu cũ</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password" value="" id="" />
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-3 col-form-label" for="basic-default-name">Mật khẩu mới</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="new_password" value="" id="" />
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label class="col-sm-3 col-form-label" for="basic-default-name">Nhập lại mật khẩu</label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="re_password" value="" id="" />
                                </div>
                            </div>

                            <div class="row justify-content-end">
                                <div class="col-sm-10">
                                    <button type="submit" name="submit" class="btn btn-primary">Thay đổi</button>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- Basic with Icons -->
            </div>
        </div>
        <!-- / Content -->

        <div class="content-backdrop fade"></div>
    </div>
</div>
<script>
    $("#actionForm").submit(function(event){
        event.preventDefault();
        $.ajax({
            url:'action-form-users',
            type:'POST',
            dataType: 'json',
            data:$(this).serialize(),
            success:function(result){
                if(result.status == true){
                    loadToastAjax(result);
                    window.location.href = "logout";
                } else {
                    loadToastAjax(result);
                }
            }

        });
    });
</script>