<div class="loading"></div>

<!-- Core JS -->
<!-- build:js assets/vendor/js/core.js -->
<script src="public/backend/assets/vendor/libs/jquery/jquery.js"></script>
<script src="public/backend/assets/vendor/libs/popper/popper.js"></script>
<script src="public/backend/assets/vendor/js/bootstrap.js"></script>
<script src="public/backend/assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js"></script>

<script src="public/backend/assets/vendor/js/menu.js"></script>
<!-- endbuild -->

<!-- Vendors JS -->
<script src="public/backend/assets/vendor/libs/apex-charts/apexcharts.js"></script>
<script src="public/backend/assets/vendor/libs/flatpickr/flatpickr.js"></script>

<script src="public/backend/assets/vendor/libs/quill/katex.js"></script>
<script src="public/backend/assets/vendor/libs/quill/quill.js"></script>

<script src="public/backend/assets/vendor/libs/select2/select2.js"></script>
<script src="public/backend/assets/vendor/libs/fullcalendar/fullcalendar.js"></script>
<script src="public/backend/assets/vendor/libs/formvalidation/dist/js/FormValidation.min.js"></script>
<script src="public/backend/assets/vendor/libs/formvalidation/dist/js/plugins/Bootstrap5.min.js"></script>
<script src="public/backend/assets/vendor/libs/formvalidation/dist/js/plugins/AutoFocus.min.js"></script>

<!-- datepicker -->
<script src="public/backend/assets/vendor/libs/datepicker/moment.min.js"></script>
<script src="public/backend/assets/vendor/libs/datepicker/daterangepicker.js"></script>

<!-- Main JS -->
<script src="public/backend/assets/js/main.js"></script>

<!-- Page JS -->
<script src="public/backend/assets/js/dashboards-analytics.js"></script>
<script src="public/backend/assets/js/ui-toasts.js"></script>
<script src="public/backend/assets/js/app-invoice-add.js"></script>
<script src="public/backend/assets/js/forms-editors.js"></script>
<script src="public/backend/assets/js/forms-selects.js"></script>
<script src="public/backend/assets/js/app-calendar-events.js"></script>
<script src="public/backend/assets/js/app-calendar.js"></script>

<!-- tinymce -->
<script src="https://cdn.tiny.cloud/1/ncw8rw2nyg332fgc634ha100w5lnsvmf18dpperrih40bt32/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<!--<script src="https://cdn.tiny.cloud/1/ncw8rw2nyg332fgc634ha100w5lnsvmf18dpperrih40bt32/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>-->

<script>
    var csrf_test_name = $('body').find('#csrfName').val();

    //var imageFilePicker = function (callback, value, meta) {
    //    tinymce.activeEditor.windowManager.open({
    //        title: 'Image Picker',
    //        url: '<?php //echo base_url() ?>//source/backend/images',
    //        width: 650,
    //        height: 550,
    //        buttons: [{
    //            text: 'Insert',
    //            onclick: function () {
    //                //.. do some work
    //                tinymce.activeEditor.windowManager.close();
    //            }
    //        }, {
    //            text: 'Close',
    //            onclick: 'close'
    //        }],
    //    }, {
    //        oninsert: function (url) {
    //            callback(url);
    //            console.log("derp");
    //        },
    //    });
    //};


    tinymce.init({
        selector: '.editCustomTextarea',
        a11y_advanced_options: true,
        mode: "specific_textareas",
        editor_selector : "mceEditor",
        image_description: true,
        height: 300,
        plugins: [
            "image",
            // "autoresize", // tự động auto
            "advlist autolink lists link charmap print preview anchor textcolor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime table contextmenu paste textcolor"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
        image_advtab: true,
        paste_data_images: true,
        automatic_uploads: true,
        // file_picker_callback: function(callback, value, meta) {
        //     imageFilePicker(callback, value, meta);
        // },
        images_upload_handler: function (blobInfo, success, failure) {
            var xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', 'backend/Fee/uploadFileBackend');
            xhr.onload = function() {
                var json;

                if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
                json = JSON.parse(xhr.responseText);

                if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success(json.location);
            };
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            formData.append('csrf_test_name', csrf_test_name);
            xhr.send(formData);
        }
    });



</script>

<!-- Place this tag in your head or just before your close body tag. -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Toast with Placements -->
<div
        class="bs-toast toast toast-placement-ex m-2"
        role="alert"
        aria-live="assertive"
        aria-atomic="true"
        data-delay="2000">
    <div class="toast-header">
        <i class="bx bx-bell me-2"></i>
        <div class="me-auto fw-semibold">Thông báo</div>
        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
    </div>
    <div class="toast-body"></div>
</div>
<!-- Toast with Placements -->

<script>
    var csrf_test_name = $('body').find('#csrfName').val();
    /** * UI Toasts */
    const toastPlacementExample = document.querySelector('.toast-placement-ex');
    let selectedType, selectedPlacement, toastPlacement;
    // Dispose toast when open another
    function toastDispose(toast) {
        if (toast && toast._element !== null) {
            if (toastPlacementExample) {
                toastPlacementExample.classList.remove(selectedType);
                DOMTokenList.prototype.remove.apply(toastPlacementExample.classList, selectedPlacement);
            }
            toast.dispose();
        }
    }
    function loadToastAjax(response){
        if(response.status == true){
            selectedType = 'bg-info';
        } else  {
            selectedType = 'bg-danger';
        }
        if (toastPlacement) {
            toastDispose(toastPlacement);
        }
        selectedPlacement = 'top-0 end-0'.split(' ');
        toastPlacementExample.classList.add(selectedType);
        DOMTokenList.prototype.add.apply(toastPlacementExample.classList, selectedPlacement);
        toastPlacement = new bootstrap.Toast(toastPlacementExample);
        toastPlacement.show();
        $('body').find('.toast-body').html(response.message);
    }
    /** * End Toasts */


    $(document).on('focus', '.time_datepicker',function(){
        // alert('okoko');
        $(this).daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            drops: "auto",
            format: "DD/MM/YYYY",
        });
    });

    $(document).ready(function() {
        $('body').on('change', '#selectAll', function () {
            $('.check-item').prop('checked', this.checked);

        })

        $('body').on('click', '.showActionDelete', function () {
            var selecteds = [];
            var confirmation = confirm("Bạn có chắc chắn muốn xóa dữ liệu?");
            if (confirmation) {
                let table = $(this).attr('data-table');
                $('#postsList input[name=item]:checked').each(function () {
                    selecteds.push(this.value);
                });
                $.ajax({
                    url: 'delete-rows',
                    type: 'POST',
                    dataType: 'json',
                    data: 'selecteds=' + selecteds + '&table=' + table + '&csrf_test_name=' + csrf_test_name,
                    beforeSend: function() {
                        // setting a timeout
                        $('body').find('.loading').show();
                    },
                    success: function (response) {
                        $('body').find('.loading').hide();
                        loadToastAjax(response);
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }
                });
            }
        })


        $('body').on('change', '.switchCheckStatus', function () {
            let checked = $(this).prop('checked');
            let table = $(this).attr('data-table');
            let that = this.value;
            $.ajax({
                url: 'update-status',
                type: 'POST',
                dataType: 'json',
                data: 'checked=' + checked + '&id=' + that + '&table=' + table + '&csrf_test_name=' + csrf_test_name,
                beforeSend: function() {
                    // setting a timeout
                    $('body').find('.loading').show();
                },
                success: function (response) {
                    $('body').find('.loading').hide();
                    loadToastAjax(response)
                }
            });
        })

        $('body').on('click', '.deleteRow', function () {
            var confirmation = confirm("Bạn có chắc chắn muốn xóa dữ liệu?");
            if (confirmation) {
                let table = $(this).attr('data-table');
                let id = $(this).attr('data-id');
                $.ajax({
                    url: 'delete-row',
                    type: 'POST',
                    dataType: 'json',
                    data: 'id=' + id + '&table=' + table + '&csrf_test_name=' + csrf_test_name,
                    beforeSend: function() {
                        // setting a timeout
                        $('body').find('.loading').show();
                    },
                    success: function (response) {
                        $('body').find('.loading').hide();
                        loadToastAjax(response);
                        setTimeout(function() {
                            location.reload();
                        }, 2000);
                    }
                });
            }
        })
        $('body').on('change', '.sttInputText', function () {
                let table = $(this).attr('data-table');
                let id = $(this).attr('data-id');
                let value = $(this).val();
                $.ajax({
                    url: 'update-stt',
                    type: 'POST',
                    dataType: 'json',
                    data: 'id=' + id + '&table=' + table + '&value=' + value + '&csrf_test_name=' + csrf_test_name,
                    beforeSend: function() {
                        // setting a timeout
                        $('body').find('.loading').show();
                    },
                    success: function (response) {
                        $('body').find('.loading').hide();
                        loadToastAjax(response);
                    }
                });
        })

    });
</script>

<!--https://demos.themeselection.com/sneat-bootstrap-html-admin-template/html/vertical-menu-template/app-calendar.html-->
