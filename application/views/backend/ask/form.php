<div class="layout-page pt-3">
    <div class="content-wrapper">
        <!-- Content -->

        <div class="container-xxl flex-grow-1 container-p-y">
            <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light"></span><?php echo $action == 'edit' ? 'Sửa thông tin' : 'Thêm mới' ?></h4>

            <!-- Basic Layout & Basic with Icons -->
            <div class="row">
                <!-- Basic Layout -->
                <div class="col-xxl">
                    <div class="card mb-4">
                        <?php if($action == 'edit'){ ?>
                        <?php echo form_open('', 'id="actionForm"  enctype="multipart/form-data"'); ?>
                        <div class="card-body nav-align-top">

                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-top-home" aria-controls="navs-top-home" aria-selected="true">
                                        <h5 class="mb-0">Câu hỏi</h5>
                                    </button>
                                </li>
                                <li class="nav-item">
                                    <button type="button" class="nav-link" role="tab" data-bs-toggle="tab" data-bs-target="#navs-top-profile" aria-controls="navs-top-profile" aria-selected="false">
                                        <h5 class="mb-0">Câu trả lời</h5>
                                    </button>
                                </li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fade show active" id="navs-top-home" role="tabpanel">
                                    <div class="" id="tab_address_place ">

                                        <div class="row mb-3">
                                            <label class="col-sm-2 col-form-label" for="basic-default-name">Tiêu đề</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" name="title" value="<?php echo !empty($detail['title']) ? $detail['title'] : ''  ?>" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="navs-top-profile" role="tabpanel">
                                    <?php
                                    if(!empty($replyList)){
                                        foreach ($replyList as $key => $vl){
                                            $checked = $vl['reply_true'] == 1 ? 'checked' : '';
                                    ?>
                                        <div class="form-check mb-3">
                                            <input style="margin-top: 10px" class="form-check-input" <?php echo $checked ?> value="<?php echo $vl['id'] ?>" type="checkbox" id="reply_true-<?php echo $vl['id'] ?>" name="reply_true">
                                            <label class="form-check-label" for="reply_true-<?php echo $vl['id'] ?>">
                                                <?php echo $vl['title'] ?> - <input type="text" style="width: auto;display: unset;"  data-id="<?php echo $vl['id'] ?>" value="<?php echo $vl['content'] ?>" class="form-control replyExam">
                                            </label>
                                        </div>
                                    <?php
                                        }
                                    }
                                    ?>

                                </div>
                            </div>

                            <div class="row justify-content-start mt-3">
                                <div class="col-sm-10">
                                    <button type="submit" name="submit" class="btn btn-primary"><?php echo $action == 'edit' ? 'Thay đổi' : 'Thêm mới' ?></button>
                                    <input type="hidden" name="action" value="<?php echo $action ?>">
                                    <input type="hidden" name="id" value="<?php echo !empty($detail['id']) ? $detail['id'] : ''  ?>">
                                </div>
                            </div>
                            </form>

                        </div>
                        <?php } else { ?>
                            <?php echo form_open('backend/Ask/updateReply', 'id="actionFormAdd"  enctype="multipart/form-data"'); ?>
                            <div class="card-body nav-align-top">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab" data-bs-target="#navs-top-home" aria-controls="navs-top-home" aria-selected="true">
                                            <h5 class="mb-0">Import File</h5>
                                        </button>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane fade show active" id="navs-top-home" role="tabpanel">
                                        <div class="" id="tab_address_place ">
                                            <div class="row mb-3">
                                                <div class="col-sm-3">
                                                    <select name="class_id" class="form-control select2">
                                                        <option value="0">Chọn bảng</option>
                                                        <option value="2">Bảng A</option>
                                                        <option value="4">Bảng B</option>
                                                    </select>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="file" class="form-control" name="uploadFile">
                                                </div>
                                                <label class="col-sm-3 col-form-label" for="basic-default-name"><a href="">Tải file mẫu tại đây</a></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row justify-content-start mt-3">
                                    <div class="col-sm-10">
                                        <button type="submit" name="submit" class="btn btn-primary btn-add"><?php echo $action == 'edit' ? 'Thay đổi' : 'Thêm mới' ?></button>
                                        <input type="hidden" name="action" value="<?php echo $action ?>">
                                        <input type="hidden" name="id" value="<?php echo !empty($detail['id']) ? $detail['id'] : ''  ?>">
                                    </div>
                                </div>
                                </form>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <!-- Basic with Icons -->
            </div>
        </div>
        <!-- / Content -->

        <div class="content-backdrop fade"></div>
    </div>
</div>
<script>
    $('body').on('click', 'input[name="reply_true"]', function() {
        $('input[name="reply_true"]').not(this).prop('checked', false);
    });
    $("#actionForm").submit(function(event){
        event.preventDefault();
        $.ajax({
            url:'action-form-ask',
            type:'POST',
            dataType: 'json',
            data:$(this).serialize(),
            success:function(result){
                if(result.status == true){
                    loadToastAjax(result);
                    setTimeout(function(){
                        window.location.href = 'list-ask';
                    }, 1000);
                } else {
                    loadToastAjax(result);
                }
            }

        });
    });
    var csrf_test_name = $('body').find('#csrfName').val();

    $('body').on('click', '.btn-add', function (e) {
        e.preventDefault();
        var form_data = new FormData($('form')[0]);
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: 'action-form-ask',
            data: form_data,
            processData: false,
            contentType: false,
            success: function(result) {
                if(result.status == true){
                    loadToastAjax(result);
                } else {
                    loadToastAjax(result);
                }
            }
        });
    })

    $('body').on('change', '.replyExam', function () {
        let value = $(this).val();
        let dataId = $(this).attr('data-id');
        $.ajax({
            url:'backend/Ask/updateReply',
            type:'POST',
            dataType: 'json',
            data:{"value": value, "dataId":dataId, "csrf_test_name": csrf_test_name},
            success:function(result){
                if(result.status == true){
                    loadToastAjax(result);
                    setTimeout(function(){
                        window.location.href = 'list-ask';
                    }, 1000);
                } else {
                    loadToastAjax(result);
                }
            }

        });
    })
</script>