<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container">
    <h2 class="m-3">Danh sách trường và số lượng đăng ký theo bảng A</h2>
    <div>
        <table class="table">
            <thead>
            <th>STT</th>
            <th>Quận/huyện</th>
            <th>Trường</th>
            <th>Số lượng đăng ký</th>
            </thead>
            <tbody>
            <?php
            foreach ($district as $key => $vl){ ?>
                <tr>
                    <td><?php echo ($key+1) ?></td>
                    <td><?php echo $vl['name'] ?></td>
                    <td>
                        <?php
                        $list = layDanhSachTruongHoc($vl['id']);
                        foreach ($list as $k => $val){
                            ?>
                                   <p> <a href="danh-sach-bang-a?id=<?php echo $val['id'] ?>"><?php echo $val['title'] ?></a></p>
                        <?php } ?>
                    </td> 
                    <td>
                        <?php
                        $list = layDanhSachTruongHoc($vl['id']);
                        foreach ($list as $k => $val){
                            ?>
                            <p><?php echo $val['view'] ?></p>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
