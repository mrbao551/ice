<?php
$actual_link = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$page_size = $this->input->get('page_size', true);
$page = $this->input->get('page', true);
$page_size = $page_size ? $page_size : 100;
$page = $page ? $page : 1;
$total_page = ceil($countAll / $page_size);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bảng A</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        $(document).ready(function() {
            $(".page_size").on('change', function() {
                location.href = "?page_size=" + $(this).val() + '&page=<?php echo $page ?>';
            });
        });
        $(document).ready(function() {
            $(".page").on('change', function() {
                location.href = "?page=" + $(this).val() + '&page_size=<?php echo $page_size ?>';
            });
        });
    </script>
</head>
<body>

<div class="container">
    <h2 class="text-center">Danh sách điểm thi bảng A</h2>
    <p style="display: flex;justify-content: end;">
        Số dòng hiển thị: <select name="page_size" class="form-select page_size">
            <option>Chọn số dòng</option>
            <option <?php echo $page_size == 100 ? 'selected' : ''; ?> value="100">100</option>
            <option <?php echo $page_size == 500 ? 'selected'  : ''; ?> value="500">500</option>
            <option <?php echo $page_size == 1000 ? 'selected'  : '' ; ?> value="1000">1000</option>
            <option <?php echo $page_size == 2000 ? 'selected' : ''; ?> value="2000">2000</option>
            <option <?php echo $page_size == 5000 ? 'selected' : ''; ?> value="5000">5000</option>
            <option <?php echo $page_size == 10000 ? 'selected' : ''; ?> value="10000">10000</option>
        </select>
        &nbsp; Số trang: <select name="page" class="form-select page">
            <option>Chọn trang</option>
            <?php for ($i=1; $i<=$total_page;$i++){ ?>
            <option <?php echo $i == $page ? 'selected'  : '' ; ?> value="<?php echo $i ?>"><?php echo $i ?></option>
            <?php } ?>
        </select>
    </p>
    <p style="display: flex;justify-content: end;"><a class="btn btn-primary" href="export-diem-bang-a?page=<?php echo $page ?>&page_size=<?php echo $page_size ?>">Xuất Excel</a></p>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <th>STT</th>
            <th>Họ tên</th>
            <th>Ngày sinh</th>
            <th>SĐT</th>
            <th>Email</th>
            <th>Trường</th>
            <th>Khoa</th>
            <th>Tổng điểm</th>
            </thead>
            <tbody>
            <?php
            foreach ($list as $key => $vl){
                $userInfo = json_decode($vl['user_info'], true);
                ?>
                <tr>
                    <td><?php echo ($key+1) ?></td>
<!--                    <td>--><?php //echo $vl['id'] ?><!--</td>-->
                    <td><?php echo $vl['fullname'] ?></td>
                    <td><?php echo $vl['ngay_sinh'] ?></td>
                    <td><?php echo $vl['phone'] ?></td>
                    <td><?php echo $vl['email'] ?></td>
                    <td><?php echo $userInfo['ten_truong'] ?></td>
                    <td><?php echo $userInfo['lop'] ?></td>
                    <td><?php echo $vl['tong_diem'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
