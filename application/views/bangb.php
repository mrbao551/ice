<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Danh sách trắc nghiệm bảng B</h2>
    <div>
        <table class="table">
            <thead>
            <th>STT</th>
            <th>Câu hỏi</th>
            <th>Trả lời</th>
            </thead>
            <tbody>
            <?php
            foreach ($list as $key => $vl){ ?>
                <tr>
                    <td><?php echo $vl['id'] ?></td>
                    <td><?php echo $vl['title'] ?></td>
                    <td>
                        <table class="table">
                            <?php $replys = getReplyByAsk($vl['id']);
                            foreach ($replys as $k => $val){
                                ?>
                                <tr <?php echo $val['reply_true'] == 1 ? 'style="color: red"' : '' ?> ><td><?php echo $val['title'] ?>. <?php echo $val['content'] ?></td></tr>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
