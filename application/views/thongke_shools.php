<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container">
    <h2 class="m-3">Danh sách trường và số lượng đăng ký theo bảng B</h2>
    <div>
        <table class="table">
            <thead>
            <th>STT</th>
            <th>Trường</th>
            <th>Số lượng đăng ký</th>
            </thead>
            <tbody>
            <?php
            foreach ($school as $key => $vl){ ?>
                <tr>
                    <td><?php echo ($key+1) ?></td>
                    <td><a href="danh-sach-bang-b?id=<?php echo $vl['id'] ?>"><?php echo $vl['title'] ?></a></td>
                    <td><?php echo $vl['view'] ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>
