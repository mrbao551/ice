<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.4/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Tải bài thi trắc nghiệm</h2>
    <form action="<?php echo base_url();?>Upload/importFile" method="post" enctype="multipart/form-data">
        <div class="form-group">
            <label for="email">File bài thi trắc nghiệm:</label>
            <input type="file" class="form-control" id="file" name="uploadFile">
        </div>
        <input type="submit" name="submit" value="Upload" class="btn btn-primary">
    </form>
</div>

</body>
</html>
