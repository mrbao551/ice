<?php

function getReplyByAsk($ask_id){
    $CI =& get_instance();
    $user = $CI->db->select('*')->from('reply')->where(array('ask_id' => $ask_id))->get()->result_array();
    return $user;
}

if (!function_exists('str_substr')) {
    function str_substr($str = NULL, $n = 0)
    {
        if (strlen($str) < $n) return $str;
        $html = substr($str, 0, $n);
        $html = substr($html, 0, strrpos($html, ' '));
        return $html . '...';
    }
}