<?php

require_once APPPATH . "/third_party/PHPExcel.php";

class Upload extends CI_Controller
{

    /**
     * CONSTRUCTOR | LOAD MODEL
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Upload_model', 'import');
        $this->load->helper(array('url', 'html', 'form'));
    }

    /**
     * SHOW | GET method.
     *
     * @return Response
     */
    public function index()
    {
        $this->load->view('UploadTracNghiem');
    }


    public function baidoc(){
        $this->load->view('UploadBaiDoc');
    }

    public function importFile()
    {
        if ($this->input->post('submit')) {
            $path = 'uploads/';
            require_once APPPATH . "/third_party/PHPExcel.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls|csv';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('uploadFile')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if (empty($error)) {
                if (!empty($data['upload_data']['file_name'])) {
                    $import_xls_file = $data['upload_data']['file_name'];
                } else {
                    $import_xls_file = 0;
                }
                $inputFileName = $path . $import_xls_file;
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                    $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $flag = true;
                    foreach ($allDataInSheet as $value) {
                        if ($flag) {
                            $flag = false;
                            continue;
                        }
                        $inserdataAsk['stt'] = $value['B'];
                        $inserdataAsk['diem'] = $value['I'];
                        $inserdataAsk['class_id'] = $value['A'];
                        $inserdataAsk['title'] = $value['C'];
                        $inserdataAsk['active'] = 1;

                        $resultAsk = $this->import->insert($inserdataAsk);

                        $reply_a = trim($value['D']);
                        $reply_b = trim($value['E']);
                        $reply_c = trim($value['F']);
                        $reply_d = trim($value['G']);
                        $reply_true = trim($value['H']);
                        $class_id = (int)trim($value['A']);
                        $ask_id = $resultAsk;
                        $this->InsertReply('A', $class_id, $ask_id, $reply_a, $reply_true);
                        $this->InsertReply('B', $class_id, $ask_id, $reply_b, $reply_true);
                        $this->InsertReply('C', $class_id, $ask_id, $reply_c, $reply_true);
                        $this->InsertReply('D', $class_id, $ask_id, $reply_d, $reply_true);

                    }
                    echo "Imported successfully";
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
                }
            } else {
                echo $error['error'];
            }
        }
        $this->load->view('UploadTracNghiem');
    }

    private function InsertReply($title, $class_id, $ask_id, $content, $reply_true)
    {
        $reply_true_active = 0;
        if ($reply_true === $title) {
            $reply_true_active = 1;
        }
        $data['title'] = $title;
        $data['class_id'] = $class_id;
        $data['ask_id'] = $ask_id;
        $data['content'] = $content;
        $data['reply_true'] = $reply_true_active;
        $data['active'] = 1;
        $resultReply = $this->import->insertReply($data);
    }

    public function importFileBaiDoc()
    {
        if ($this->input->post('submit')) {
            $path = 'uploads/';
            require_once APPPATH . "/third_party/PHPExcel.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls|csv';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('uploadFile')) {
                $error = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if (empty($error)) {
                if (!empty($data['upload_data']['file_name'])) {
                    $import_xls_file = $data['upload_data']['file_name'];
                } else {
                    $import_xls_file = 0;
                }
                $inputFileName = $path . $import_xls_file;
                try {
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                    $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                    $flag = true;
                    foreach ($allDataInSheet as $value) {
                        if ($flag) {
                            $flag = false;
                            continue;
                        }
                        $inserdataAsk['stt'] = $value['B'];
                        $inserdataAsk['diem'] = $value['I'];
                        $inserdataAsk['class_id'] = $value['A'];
                        $inserdataAsk['title'] = $value['C'];
                        $inserdataAsk['active'] = 1;
                        $inserdataAsk['tu_luan_id'] = $this->input->post('tu_luan_id');

                        $resultAsk = $this->import->insert($inserdataAsk);

                        $reply_a = trim($value['D']);
                        $reply_b = trim($value['E']);
                        $reply_c = trim($value['F']);
                        $reply_d = trim($value['G']);
                        $reply_true = trim($value['H']);
                        $class_id = (int)trim($value['A']);
                        $ask_id = $resultAsk;
                        $this->InsertReply('A', $class_id, $ask_id, $reply_a, $reply_true);
                        $this->InsertReply('B', $class_id, $ask_id, $reply_b, $reply_true);
                        $this->InsertReply('C', $class_id, $ask_id, $reply_c, $reply_true);
                        $this->InsertReply('D', $class_id, $ask_id, $reply_d, $reply_true);

                    }
                    echo "Imported successfully";
                } catch (Exception $e) {
                    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
                        . '": ' . $e->getMessage());
                }
            } else {
                echo $error['error'];
            }
        }
        $this->load->view('UploadTracNghiem');
    }

    private function InsertReplyBaiDoc($title, $class_id, $ask_id, $content, $reply_true)
    {
        $reply_true_active = 0;
        if ($reply_true === $title) {
            $reply_true_active = 1;
        }
        $data['title'] = $title;
        $data['class_id'] = $class_id;
        $data['ask_id'] = $ask_id;
        $data['content'] = $content;
        $data['reply_true'] = $reply_true_active;
        $data['active'] = 1;
        $resultReply = $this->import->insertReply($data);
    }
    
}