<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        // User login status
        $this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
        if (!$this->isUserLoggedIn) {
            redirect('admin-login');
        }
    }

    public function index()
    {
        $data = array();
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($userId);

        $data['template'] = 'backend/users/list';
        $this->load->view('backend/index', $data);
    }

    public function formExam($id = '')
    {
        $id = (int)$id;
        $data = array();
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($id);
        if ($id > 0) {
            $data['action'] = 'edit';
        } else {
            $data['action'] = 'add';
        }
        $data['template'] = 'backend/users/form';
        $this->load->view('backend/index', $data);
    }

    public function actionForm()
    {
        $userId = $this->session->userdata('user_id');
        $_post = $this->input->post(null, true);

        if ($_post['action'] == 'edit') {
            $password = $_post['password'];
            $new_password = $_post['new_password'];
            $re_password = $_post['re_password'];
            $email = $_post['email'];
            $fullname = $_post['fullname'];

            if ($password != '' && $new_password != '' && $re_password != '') {
                $this->form_validation->set_rules('password', 'Mật khẩu cũ', 'required');
                $this->form_validation->set_rules('new_password', 'Mật khẩu mới', 'required|min_length[6]');
                $this->form_validation->set_rules('re_password', 'Xác nhận mật khẩu', 'required|matches[new_password]|min_length[6]');
            } else {
                $this->form_validation->set_rules('fullname', 'Họ tên', 'required');
                $this->form_validation->set_rules('email', 'Email', 'required');
            }
            if ($this->form_validation->run() == false) {
                echo json_encode(['status' => false, 'message' => validation_errors()]);
                exit();
            } else {
                if ($password == '' && $new_password == '' && $re_password == '') {
                    $this->user_model->update_user($userId, $email, $fullname, '');
                } else {
                    if ($this->user_model->matchOldPassword($userId, $_post['password'])) {
                        $this->user_model->update_user($userId, $email, $fullname, $new_password);
                    } else {
                        echo json_encode(['status' => false, 'message' => 'Mật khẩu không đúng']);
                        exit();
                    }
                }
            }
        } else {
            $this->form_validation->set_rules('username', 'Tên tài khoản', 'required');
            $this->form_validation->set_rules('fullname', 'Họ tên', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('new_password', 'Mật khẩu', 'required|min_length[6]');
            $this->form_validation->set_rules('re_password', 'Xác nhận mật khẩu', 'required|matches[new_password]|min_length[6]');

            if ($this->form_validation->run() == false) {
                echo json_encode(['status' => false, 'message' => validation_errors()]);
                exit();
            } else {
                $username = $_post['username'];
                $password = $_post['new_password'];
                $email = $_post['email'];
                $fullname = $_post['fullname'];
                $this->user_model->create_user_admin(
                    $username, $email, $fullname, $password
                );
            }
        }


        echo json_encode(['status' => true, 'message' => 'Thành công !.']);
        exit();
    }

    public function loadRecord($page, $rowperpage = 0)
    {
        $rowperpage = $this->input->post('page_size', true);
        $sortBy = $this->input->post('sortBy', true);
        $keyword = $this->input->post('keyword', true);

        $rowperpage = !empty($rowperpage) ? $rowperpage : 1;
        if ($page != 0) {
            $page = ($page - 1) * $rowperpage;
        }

        if (!empty($keyword) && $keyword) {
            $this->db->group_start();
            $this->db->like('username', $keyword);
            $this->db->or_like('email', $keyword);
            $this->db->group_end();
        }
        $this->db->where('is_admin', 1);
        $allcount = $this->db->count_all_results('users');

        if (!empty($keyword) && $keyword) {
            $this->db->group_start();
            $this->db->like('username', $keyword);
            $this->db->or_like('email', $keyword);
            $this->db->group_end();
        }

        $this->db->limit($rowperpage, $page);
        if (!empty($sortBy) && $sortBy) {
            $this->db->order_by('id', $sortBy);
        } else {
            $this->db->order_by('id desc');
        }

        $this->db->where('is_admin', 1);

        $users_record = $this->db->get('users')->result_array();

        //pagination
        $config['base_url'] = base_url() . 'all-users';
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $users_record;
        $data['row'] = $page;
        $data['regen_token'] = $this->security->get_csrf_hash();
        echo json_encode($data);
    }

}