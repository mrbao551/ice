<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Essay extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('BackendEssay_model');
        $this->load->model('BackendReply_model');
        $this->load->model('BackendAsk_model');
        // User login status
        $this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
        if (!$this->isUserLoggedIn) {
            redirect('admin-login');
        }
    }

    public function index()
    {
        $data = array();
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($userId);
        $data['template'] = 'backend/essay/list';
        $this->load->view('backend/index', $data);
    }

    public function formExam($id = '')
    {
        $id = (int)$id;
        $data = array();
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($userId);
        $data['replyList'] = [];
        $data['detail'] = [];
        if ($id > 0) {
            $data['detail'] = $this->BackendEssay_model->show($id);
            $data['action'] = 'edit';
            $data['askList'] = $this->BackendAsk_model->showAsk($id);
//            $data['replyList'] = $this->BackendReply_model->showReplyByAsk($id);

        } else {
            $data['action'] = 'add';
        }

        $data['template'] = 'backend/essay/form';
        $this->load->view('backend/index', $data);
    }

    public function actionForm()
    {
        $_post = $this->input->post(null, true);
        if ($_post['action'] == 'edit') {

            $this->form_validation->set_rules('title', 'Tiêu đề', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                echo json_encode(['status' => false, 'message' => validation_errors()]);
                exit();
            }
            $data1 = array(
                'title' => $_post['title'],
                'content' => $_post['content'],
            );
            $this->BackendEssay_model->update($data1, $_post['id']);
            echo json_encode(['status' => true, 'message' => 'Thay đổi thành công']);
            exit();

        } else {
            if($_post['content'] == ''){
                echo json_encode(['status' => false, 'message' => 'Bạn chưa nhập nội dung bài tự luận']);
                exit();
            }

            if($_post['class_id'] == 0){
                echo json_encode(['status' => false, 'message' => 'Bạn chưa chọn bảng']);
                exit();
            }

            $dataInsert = [
                'des' => $_post['des'],
                'title' => $_post['title'],
                'content' => $_post['content'],
                'class_id' => $_post['class_id'],
                'active' => 1
            ];
            $tu_luan_id = $this->BackendEssay_model->insert($dataInsert);

            if(!$tu_luan_id){
                echo json_encode(['status' => false, 'message' => 'Có lỗi xảy ra']);
                exit();
            }

            $path = 'uploads/';
            require_once APPPATH . "/third_party/PHPExcel.php";
            $config['upload_path'] = $path;
            $config['allowed_types'] = 'xlsx|xls|csv';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('uploadFile')) {
                echo json_encode(['status' => false, 'message' => $this->upload->display_errors()]);
                exit();
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if (!empty($data['upload_data']['file_name'])) {
                $import_xls_file = $data['upload_data']['file_name'];
            } else {
                $import_xls_file = 0;
            }

            $inputFileName = $path . $import_xls_file;
            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
                $flag = true;
                foreach ($allDataInSheet as $value) {
                    if ($flag) {
                        $flag = false;
                        continue;
                    }
                    $inserdataAsk['stt'] = $value['B'];
                    $inserdataAsk['diem'] = $value['I'];
                    $inserdataAsk['class_id'] = $_post['class_id'];
                    $inserdataAsk['title'] = $value['C'];
                    $inserdataAsk['active'] = 1;
                    $inserdataAsk['tu_luan_id'] = $tu_luan_id;

                    $resultAsk = $this->BackendAsk_model->insert($inserdataAsk);

                    $reply_a = trim($value['D']);
                    $reply_b = trim($value['E']);
                    $reply_c = trim($value['F']);
                    $reply_d = trim($value['G']);
                    $reply_true = trim($value['H']);
                    $class_id = $_post['class_id'];
                    $ask_id = $resultAsk;
                    $this->InsertReply('A', $class_id, $ask_id, $reply_a, $reply_true);
                    $this->InsertReply('B', $class_id, $ask_id, $reply_b, $reply_true);
                    $this->InsertReply('C', $class_id, $ask_id, $reply_c, $reply_true);
                    $this->InsertReply('D', $class_id, $ask_id, $reply_d, $reply_true);

                }
                echo json_encode(['status' => true, 'message' => 'Thành công !.']);
                exit();

            } catch (Exception $e) {
//                die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME)
//                    . '": ' . $e->getMessage());

                echo json_encode(['status' => false, 'message' => $e->getMessage()]);
                exit();
            }
        }
    }

    private function InsertReply($title, $class_id, $ask_id, $content, $reply_true)
    {
        $reply_true_active = 0;
        if ($reply_true === $title) {
            $reply_true_active = 1;
        }
        $data['title'] = $title;
        $data['class_id'] = $class_id;
        $data['ask_id'] = $ask_id;
        $data['content'] = $content;
        $data['reply_true'] = $reply_true_active;
        $data['active'] = 1;
        $resultReply = $this->BackendReply_model->insert($data);
    }

    public function updateReply()
    {
        $_post = $this->input->post(NULL, true);
        $this->BackendAsk_model->update(['title' => $_post['value']], $_post['dataId']);
        echo json_encode(['status' => true, 'message' => 'Thay đổi thành công']);
        exit();
    }

    public function loadRecord($page, $rowperpage = 0)
    {
        $rowperpage = $this->input->post('page_size', true);
        $sortBy = $this->input->post('sortBy', true);
        $keyword = $this->input->post('keyword', true);
        $class_id = $this->input->post('class_id', true);

        $rowperpage = !empty($rowperpage) ? $rowperpage : 1;
        if ($page != 0) {
            $page = ($page - 1) * $rowperpage;
        }

        if (!empty($keyword) && $keyword) {
            $this->db->group_start();
            $this->db->like('title', $keyword);
            $this->db->group_end();
        }
        if (isset($class_id) && $class_id != 'all') {
            $this->db->where(['class_id' => $class_id]);
        }

        $allcount = $this->db->count_all_results('tu_luan');

        if (!empty($keyword) && $keyword) {
            $this->db->group_start();
            $this->db->like('title', $keyword);
            $this->db->group_end();
        }
        if (isset($class_id) && $class_id != 'all') {
            $this->db->where(['class_id' => $class_id]);
        }

        $this->db->limit($rowperpage, $page);
        if (!empty($sortBy) && $sortBy) {
            $this->db->order_by('id', $sortBy);
        } else {
            $this->db->order_by('id desc');
        }
        $users_record = $this->db->get('tu_luan')->result_array();

        //pagination
        $config['base_url'] = base_url() . 'backend/essay/loadRecord';
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $users_record;
        $data['row'] = $page;
        $data['regen_token'] = $this->security->get_csrf_hash();
        echo json_encode($data);
    }

    public function showEssayByAsk(){
        $_post = $this->input->post(null, true);
        $replyList = $this->BackendReply_model->showReplyByAsk($_post['askId']);
        $data = [];
        foreach ($replyList as $key => $vl){
            $data[] = [
                'id' => $vl['id'],
                'title' => $vl['title'],
                'content' => $vl['content'],
                'reply_true' => $vl['reply_true'] == 1 ? 'checked' : '',
            ];
        }
        echo json_encode(['status' => true, 'data' => $data]);
        exit();
    }

    public function updateReplyTrue(){
        $_post = $this->input->post(null, true);
        $this->BackendReply_model->updateNotId(['reply_true' => 0], 'ask_id', $_post['askId']);
        $data2 = array(
            'reply_true' => 1
        );
        $this->BackendReply_model->update($data2, $_post['reply_true']);
    }

}