<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hoso extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('Homes_model');
        $this->load->model('BackendSchools_model');
        $this->load->model('BackendHightSchool_model');
        $this->load->model('BackendHoso_model');
        // User login status
        $this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
        if (!$this->isUserLoggedIn) {
            redirect('admin-login');
        }
    }

    public function index()
    {
        $data = array();
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($userId);
        $data['schools'] = $this->BackendSchools_model->show();
        $data['hightSchools'] = $this->BackendHightSchool_model->show();
        $data['template'] = 'backend/hoso/list';
        $this->load->view('backend/index', $data);
    }
    public function formExam($id = '')
    {
        $id = (int)$id;
        $data = array();
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($userId);
        $detailHs = $this->BackendHoso_model->show($id);
        $data['detail'] = $detailHs;
//        $data['provinces'] = $this->Homes_model->getProvinceExam();
        $data['examDetail'] = [];
        if ($id > 0) {
            $data['action'] = 'edit';
        } else {
            $data['action'] = 'add';
            $data['levels'] = [];
            $data['languages'] = [];
            $data['examDateExam'] = [];
            $data['university'] = [];
        }
        $data['template'] = 'backend/hoso/form';
        $this->load->view('backend/index', $data);
    }

    public function loadRecord($page, $rowperpage = 0)
    {
        $rowperpage = $this->input->post('page_size', true);
        $sortBy = $this->input->post('sortBy', true);
        $keyword = $this->input->post('keyword', true);
        $class_id = $this->input->post('class_id', true);
        $hightShools = $this->input->post('hightShools', true);
        $shools = $this->input->post('shools', true);

        $dataResult = $this->loadDataHoSo($page, $rowperpage, $sortBy, $keyword, $class_id, $hightShools, $shools, $action = '');
        //pagination
        $config['base_url'] = base_url() . 'all-ho-so';
        $config['total_rows'] = $dataResult['allcount'];
        $config['per_page'] = $rowperpage;
        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $dataResult['users_record'];
        $data['row'] = $page;
        $data['regen_token'] = $this->security->get_csrf_hash();
        echo json_encode($data);
    }

    private function loadDataHoSo($page, $rowperpage, $sortBy, $keyword, $class_id, $hightShools, $shools, $action = 'export')
    {
        $rowperpage = !empty($rowperpage) ? $rowperpage : 1;
        if ($page != 0) {
            $page = ($page - 1) * $rowperpage;
        }

        $this->db->select('id');
        if (!empty($keyword) && $keyword) {
            $this->db->group_start();
            $this->db->like('fullname', $keyword);
            $this->db->or_like('truong_hoc_ten', $keyword);
            $this->db->or_like('email', $keyword);
            $this->db->or_like('phone', $keyword);
            $this->db->group_end();
        }
        if (isset($class_id) && $class_id != 'all') {
            $this->db->where(['class_id' => $class_id]);
        }

        if (isset($hightShools) && $hightShools != 'all') {
            $this->db->where(['truong_hoc_id' => $hightShools]);
        }
        if (isset($shools) && $shools != 'all') {
            $this->db->where(['truong_hoc_id' => $shools]);
        }
        $this->db->where('is_admin !=', 1);
        $allcount = $this->db->count_all_results('users');
//        echo $this->db->last_query();

        $this->db->select('id, fullname, email, phone, active, class_id, gioi_tinh, ngay_sinh, truong_hoc_ten, tong_diem, user_info');
        if (!empty($keyword) && $keyword) {
            $this->db->group_start();
            $this->db->like('fullname', $keyword);
            $this->db->or_like('truong_hoc_ten', $keyword);
            $this->db->or_like('email', $keyword);
            $this->db->or_like('phone', $keyword);
            $this->db->group_end();
        }

        if (isset($class_id) && $class_id != 'all') {
            $this->db->where(['class_id' => $class_id]);
        }
        if (isset($hightShools) && $hightShools != 'all') {
            $this->db->where(['truong_hoc_id' => $hightShools]);
        }
        if (isset($shools) && $shools != 'all') {
            $this->db->where(['truong_hoc_id' => $shools]);
        }

        if ($action != '') {
            $this->db->where('active', 1);
        }

        $this->db->limit($rowperpage, $page);
        if (!empty($sortBy) && $sortBy) {
            $this->db->order_by('id', $sortBy);
        } else {
            $this->db->order_by('id desc');
        }
        $this->db->where('is_admin !=', 1);
        $users_record = $this->db->get('users')->result_array();

//        echo $this->db->last_query();

        return [
            'allcount' => $allcount,
            'users_record' => $users_record
        ];
    }

    public function exportHoso($page)
    {
        $rowperpage = $this->input->get('page_size', true);
        $sortBy = $this->input->get('sortBy', true);
        $keyword = $this->input->get('keyword', true);
        $class_id = $this->input->get('activeFeeShow', true);
        $exam_id = $this->input->get('exam_id', true);
        $date_exam_id = $this->input->get('date_exam_id', true);
        $dataResult = $this->loadDataHoSo($page, $rowperpage, $sortBy, $keyword, $class_id, $exam_id, $date_exam_id, $action = '');

        require_once APPPATH . "/third_party/PHPExcel.php";
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Document")
            ->setSubject("Office 2007 XLSX Document")
            ->setDescription("Document for Office 2007 XLSX")
            ->setKeywords("Office 2007 openxml")
            ->setCategory("Result file");
        //Cau hinh kich thuoc, mau, size...
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getStyle('A1:M1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Họ tên");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "Email");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "Phone");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "Giới tính");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "Ngày sinh");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "Bảng tham gia");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "Tên trường");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "Lớp/khoa");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I1', "Điểm thi");

        $_list = $dataResult['users_record'];
        if (!empty($_list)) {
            $count = 1;
            foreach ($_list as $key => $vl) {
                $count += 1;
                $l_1 = "A" . "$count";
                $l_2 = "B" . "$count";
                $l_3 = "C" . "$count";
                $l_4 = "D" . "$count";
                $l_5 = "E" . "$count";
                $l_6 = "F" . "$count";
                $l_7 = "G" . "$count";
                $l_8 = "H" . "$count";
                $l_9 = "I" . "$count";

                $user_info = json_decode($vl['user_info'], true);

                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_1, $vl['fullname']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_2, $vl['email']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_3, $vl['phone']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_4, $vl['gioi_tinh']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_5, $vl['ngay_sinh']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_6, $vl['class_id'] == 2 ? 'Bảng A' : 'Bảng B');
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_7, $vl['truong_hoc_ten']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_8, $user_info['lop']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_9, $vl['tong_diem']);
            }
        }

        $objPHPExcel->getActiveSheet()->setTitle('ICE');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ICE-'. date("d/m/Y") . '.xls"');
        $objWriter->save('php://output');
        exit;

    }

    public function calendar()
    {
        $data['template'] = 'backend/hoso/calendar';
        $this->load->view('backend/index', $data);
    }


}