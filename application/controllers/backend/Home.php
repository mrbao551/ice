<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('homes_model');
        $this->load->model('BackendHoso_model');
        // User login status
        $this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
    }
    public function index()
    {
        $data = array();
        if($this->isUserLoggedIn){
            $userId = $this->session->userdata('user_id');
            $data['user'] = $this->user_model->get_user($userId);
//            $data['total_DHNN'] = $this->BackendHoso_model->countAllHoSoDHNN();
//            $data['total_fee_DHNN'] = $this->BackendHoso_model->countAllHoSoDHNN(1);
//
//            $data['total_DHQG'] = $this->BackendHoso_model->countAllHoSoDHQG();
//            $data['total_fee_DHQG'] = $this->BackendHoso_model->countAllHoSoDHQG(1);
//
//            $data['total_Other'] = $this->BackendHoso_model->countAllHoSoOther();
//            $data['total_fee_Other'] = $this->BackendHoso_model->countAllHoSoOther(1);

            $data['template'] = 'backend/home';
            $this->load->view('backend/index', $data);
        }else{
            redirect('admin-login');
        }
    }

    public function login() {

        $this->form_validation->set_rules('username', 'Tài khoản', 'required|trim');
        $this->form_validation->set_rules('password', 'Mật khẩu', 'required|trim');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('backend/login');
        } else {
            $username = $this->input->post('username', true);
            $password = $this->input->post('password', true);
            $user = $this->db->get_where('users',['username' => $username, 'is_admin' => 1])->row();
            if(!$user) {
                $this->session->set_flashdata('login_error', 'Tài khoản hoặc mật khẩu sai', 300);
                redirect(uri_string());
            }
            if(!password_verify($password,$user->password)) {
                $this->session->set_flashdata('login_error', 'Tài khoản hoặc mật khẩu sai', 300);
                redirect(uri_string());
            }

            $data = array(
                'user_id' => $user->id,
                'username' => $user->username,
            );
            $this->session->set_userdata($data);
            $this->session->set_userdata('isUserLoggedIn', TRUE);
            //redirect('/'); // redirect to home
            redirect('list-ho-so');

        }
    }

    public function logout(){
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('user_id');
        $this->session->sess_destroy();
        redirect('admin-login');
    }

    public function updateStatus(){
        $checked = $this->input->post('checked', true);
        $id = $this->input->post('id', true);
        $table = $this->input->post('table', true);
        if($checked == 'true'){
            $dataUpdate = ['active' => 1];
        } else {
            $dataUpdate = ['active' => 0];
        }
        $result = $this->homes_model->update($table, $dataUpdate, $id);
        if($result){
            echo json_encode(['status' => true, 'message' => 'Cập nhật thành công']);
        } else {
            echo json_encode(['status' => false, 'message' => 'Cập nhật thất bại']);
        }

    }
    public function updateStt(){
        $value = $this->input->post('value', true);
        $id = $this->input->post('id', true);
        $table = $this->input->post('table', true);
        $dataUpdate = ['stt' => $value];
        $result = $this->homes_model->update($table, $dataUpdate, $id);
        if($result){
            echo json_encode(['status' => true, 'message' => 'Cập nhật thành công']);
        } else {
            echo json_encode(['status' => false, 'message' => 'Cập nhật thất bại']);
        }

    }

    public function deleteRow(){
        $id = $this->input->post('id', true);
        $table = $this->input->post('table', true);
        $result = $this->homes_model->delete($table, $id);
        if($result){
            echo json_encode(['status' => true, 'message' => 'Xóa thành công']);
        } else {
            echo json_encode(['status' => false, 'message' => 'Xóa thất bại']);
        }
    }
    public function deleteRows(){
        $selectedsId = $this->input->post('selecteds', true);
        $table = $this->input->post('table', true);
        $result = $this->homes_model->deleteMultiple($table, $selectedsId);
        if($result){
            echo json_encode(['status' => true, 'message' => 'Xóa thành công']);
        } else {
            echo json_encode(['status' => false, 'message' => 'Xóa thất bại']);
        }
    }

}