<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Schools extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('BackendSchools_model');
        // User login status
        $this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
        if (!$this->isUserLoggedIn) {
            redirect('admin-login');
        }
    }

    public function index()
    {
        $data = array();
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($userId);
        $data['template'] = 'backend/schools/list';
        $this->load->view('backend/index', $data);
    }

    public function formExam($id = '')
    {
        $id = (int)$id;
        $data = array();
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($userId);
        $data['detail'] = $this->BackendSchools_model->show($id);
        if($id > 0){
            $data['action'] = 'edit';
        } else {
            $data['action'] = 'add';
        }
        $data['template'] = 'backend/schools/form';
        $this->load->view('backend/index', $data);
    }

    public function actionForm(){
        $_post = $this->input->post(null, true);
        $this->form_validation->set_rules('title', 'Tiêu đề', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            echo json_encode(['status' => false, 'message' => validation_errors()]);
            exit();
        }
        $data = array(
            'title' => $_post['title'],
        );
        if($_post['action'] == 'edit'){
            $this->BackendSchools_model->update($data, $_post['id']);
        } else {
            $data = array(
                'title' => $_post['title'],
                'stt' => $this->BackendSchools_model->addMaxStt(),
                'view' => 1000,
                'active' => 1,
            );
            $this->BackendSchools_model->insert($data);
        }
        echo json_encode(['status' => true, 'message' => 'Thành công !.']);
        exit();
    }

    public function loadRecord($page, $rowperpage = 0)
    {
        $rowperpage = $this->input->post('page_size', true);
        $sortBy = $this->input->post('sortBy', true);
        $keyword = $this->input->post('keyword', true);

        $rowperpage = !empty($rowperpage) ? $rowperpage : 1;
        if ($page != 0) {
            $page = ($page - 1) * $rowperpage;
        }

        if (!empty($keyword) && $keyword) {
            $this->db->group_start();
            $this->db->like('title', $keyword);
            $this->db->or_like('des', $keyword);
            $this->db->group_end();
        }
        $this->db->where('active', 1);
        $allcount = $this->db->count_all_results('schools');

        if (!empty($keyword) && $keyword) {
            $this->db->group_start();
            $this->db->like('title', $keyword);
            $this->db->or_like('des', $keyword);
            $this->db->group_end();
        }

        $this->db->limit($rowperpage, $page);
        if (!empty($sortBy) && $sortBy) {
            $this->db->order_by('id', $sortBy);
        } else {
            $this->db->order_by('id desc');
        }
        $this->db->where('active', 1);
        $users_record = $this->db->get('schools')->result_array();

        //pagination
        $config['base_url'] = base_url() . 'all-schools';
        $config['total_rows'] = $allcount;
        $config['per_page'] = $rowperpage;
        $this->pagination->initialize($config);

        $data['pagination'] = $this->pagination->create_links();
        $data['result'] = $users_record;
        $data['row'] = $page;
        $data['regen_token'] = $this->security->get_csrf_hash();
        echo json_encode($data);
    }


}