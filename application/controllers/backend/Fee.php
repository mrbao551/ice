<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Fee extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->model('BackendReply_model');
        $this->load->model('BackendConfig_model');
        $this->load->library('image_lib');
        // User login status
        $this->isUserLoggedIn = $this->session->userdata('isUserLoggedIn');
        if (!$this->isUserLoggedIn) {
            redirect('admin-login');
        }
    }

    public function index($group = 'fee')
    {
        $data = array();
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($userId);

        $data['config'] = $this->db->select('id,label,keyword,val_vi', 'type')->from('config')->where(array('group_conf' => $group, 'public' => 1))->order_by("stt ASC")->get()->result_array();

        $data['template'] = 'backend/fee/index';
        $this->load->view('backend/index', $data);
    }

    public function register($group = 'register')
    {
        $data = [];
        $userId = $this->session->userdata('user_id');
        $data['user'] = $this->user_model->get_user($userId);

        $data['config'] = $this->db->select('id,label,keyword,val_vi', 'type')->from('config')->where(array('group_conf' => $group, 'public' => 1))->order_by("stt ASC")->get()->result_array();

        $data['template'] = 'backend/fee/register';
        $this->load->view('backend/index', $data);
    }

    public function actionForm()
    {
        $_post = $this->input->post(null, false);
        $_data = [];
        foreach ($_post as $keyPost => $valPost) {
            $_data[] = array(
                'keyword' => $keyPost,
                'val_vi' => $valPost,
                'updated' => gmdate('Y-m-d H:i:s', time() + 7 * 3600),
            );
        }
        $result = $this->db->update_batch('config', $_data, 'keyword');
        if ($result) {
            echo json_encode(['status' => true, 'message' => 'Thành công !.']);
            exit();
        } else {
            echo json_encode(['status' => false, 'message' => 'Có lỗi !.']);
            exit();
        }
    }

    public function uploadFileBackend($textType = 'file')
    {
        if ($this->security->xss_clean($_FILES, TRUE) === FALSE) {
            echo json_encode(['location' => 'Upload file bị lỗi']);
            exit();
        } else {
            if ($_FILES[$textType]['size'] > 0) {
                $config['upload_path'] = 'source/backend/images/';
                $config['max_size'] = 1024 * 3; // 3Mb
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);

                if (!$this->upload->do_upload($textType)) {
                    echo json_encode(['location' => $this->upload->display_errors()]);
                    exit();
                } else {
                    $data = $this->upload->data();
                    $configer = array(
                        'image_library' => 'gd2',
                        'source_image' => $data['full_path'],
                        'create_thumb' => FALSE,//tell the CI do not create thumbnail on image
                        'maintain_ratio' => TRUE,
                        'quality' => '60%', //tell CI to reduce the image quality and affect the image size
                        'width' => 540,//new size of image
                        'height' => 380,//new size of image
                    );
                    $this->image_lib->clear();
                    $this->image_lib->initialize($configer);
                    $this->image_lib->resize();
                    $filename = pathinfo($data['full_path']);
                    echo json_encode(['location' => $config['upload_path'] . $filename['basename']]);
                    exit();
                }
            } else {
                echo json_encode(['location' => 'Upload file bị lỗi']);
                exit();
            }
        }
    }


}