<?php
defined('BASEPATH') or exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type');

/**
 * User class.
 *
 * @extends REST_Controller
 */
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class User extends REST_Controller
{

    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Authorization_Token');
        $this->load->model('user_model');
        $this->load->model('UserLuotThi_model');
        $this->load->model('Tests_model');

    }

    /**
     * register function.
     *
     * @access public
     * @return void
     */

    public function checkUsername()
    {
        $userName = $this->security->xss_clean($this->input->post('username'));
        $detail = $this->user_model->checkUserName($userName);
        if ($detail) {
            $this->form_validation->set_message(__FUNCTION__, '%s đã tổn tại');
            return false;
        }
        return true;
    }

    public function checkEmail()
    {
        $email = $this->security->xss_clean($this->input->post('email'));
        $detail = $this->user_model->checkEmail($email);
        if ($detail) {
            $this->form_validation->set_message(__FUNCTION__, '%s đã tổn tại');
            return false;
        }
        return true;
    }

    public function re_register_post()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $_POST['email'] = $data['email'];
        $count = $this->db->from('users')->where(array('email' => $_POST['email']))->count_all_results();
        if ($count > 0) {
            $final = array();
            $final['status'] = false;
            $final['message'] = 'Đã tồn tại email';
            $this->response($final, REST_Controller::HTTP_OK);
        } else {
            $final = array();
            $final['status'] = true;
            $final['message'] = 'Success';
            $this->response($final, REST_Controller::HTTP_OK);
        }
    }


//    public function register_post()
//    {
//        // set validation rules
//
//        $data = json_decode(file_get_contents('php://input'), true);
//        $_POST['username'] = $data['username'];
//        $_POST['email'] = $data['email'];
//        $_POST['password'] = $data['password'];
//        $_POST['fullname'] = $data['fullname'];
//        $_POST['class_id'] = $data['class_id'];
//        $_POST['phone'] = $data['phone'];
//        $_POST['ngay_sinh'] = $data['ngay_sinh'];
//        $_POST['gioi_tinh'] = $data['gioi_tinh'];
//        $_POST['user_info'] = $data['user_info'];
//        $_POST['truong_hoc_id'] = $data['truong_hoc_id'];
//        $_POST['truong_hoc_ten'] = $data['truong_hoc_ten'];
//
//        $this->form_validation->set_rules('username', 'Username', 'trim|required');
//        $this->form_validation->set_rules('email', 'Email', 'trim|required');
//        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
//        //$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
//
//        if ($this->form_validation->run() === false) {
////            echo validation_errors();
//            // validation not ok, send validation errors to the view
//            $this->response([validation_errors()], REST_Controller::HTTP_OK);
//
//        } else {
//
//            // set variables from the form
//            $username = $this->input->post('username');
//            $email = $this->input->post('email');
//            $password = $this->input->post('password');
//            $fullname = $this->input->post('fullname');
//            $class_id = $this->input->post('class_id');
//            $phone = $this->input->post('phone');
//            $gioi_tinh = $this->input->post('gioi_tinh');
//            $ngay_sinh = $this->input->post('ngay_sinh');
//            $user_info = $this->input->post('user_info');
//            $truong_hoc_id = $this->input->post('truong_hoc_id');
//            $truong_hoc_ten = $this->input->post('truong_hoc_ten');
//
//            if ($res = $this->user_model->create_user(
//                $username, $email, $password,
//                $fullname, $class_id, $phone, $ngay_sinh, $gioi_tinh, $user_info,
//                $truong_hoc_id, $truong_hoc_ten
//            )) {
//
//                // user creation ok
//                $token_data['uid'] = $res;
//                $token_data['username'] = $username;
//                $tokenData = $this->authorization_token->generateToken($token_data);
//                $final = array();
//                $final['access_token'] = $tokenData;
//                $final['status'] = true;
//                $final['uid'] = $res;
//                $final['message'] = 'Thank you for registering your new account!';
//                $final['note'] = 'You have successfully register. Please check your email inbox to confirm your email address.';
//
//                $this->response($final, REST_Controller::HTTP_OK);
//
//            } else {
//
//                // user creation failed, this should never happen
//                $this->response(['There was a problem creating your new account. Please try again.'], REST_Controller::HTTP_OK);
//            }
//
//        }
//
//    }


    public function register_post()
    {
        // set validation rules

        $data = json_decode(file_get_contents('php://input'), true);
        // $_POST['username'] = $data['username'];
        $_POST['email'] = $data['email'];
        $_POST['password'] = $data['password'];
        $_POST['username'] = $data['username'];
        $_POST['fullname'] = $data['fullname'];

        // $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[5]');
        //$this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');

        if ($this->form_validation->run() === false) {
//            echo validation_errors();
            // validation not ok, send validation errors to the view
            $this->response([validation_errors()], REST_Controller::HTTP_OK);

        } else {

            // set variables from the form
            // $username = $this->input->post('username');
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            $username = $this->input->post('username');
            $fullname = $this->input->post('fullname');
            // $fullname = $this->input->post('fullname');

            if ($res = $this->user_model->create_user_admin(
                $username, $email, $fullname, $password
            )) {

                // user creation ok
                $token_data['uid'] = $res;
                $tokenData = $this->authorization_token->generateToken($token_data);
                $final = array();
                $final['access_token'] = $tokenData;
                $final['status'] = true;
                $final['uid'] = $res;
                $final['message'] = 'Thank you for registering your new account!';
                $final['note'] = 'You have successfully register. Please check your email inbox to confirm your email address.';

                $this->response($final, REST_Controller::HTTP_OK);

            } else {

                // user creation failed, this should never happen
                $this->response(['There was a problem creating your new account. Please try again.'], REST_Controller::HTTP_OK);
            }

        }

    }

    /**
     * login function.
     *
     * @access public
     * @return void
     */

    public function auth()
    {

    }

    public function login_post()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $_POST['email'] = $data['email'];
        $_POST['password'] = $data['password'];
//        $this->response(array('status' => false, 'message' => 'Hết thời gian tham gia !.'), REST_Controller::HTTP_BAD_REQUEST);
//        exit();
        // set validation rules
        $this->form_validation->set_rules('email', 'Email', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');

        if ($this->form_validation->run() == false) {
            // validation not ok, send validation errors to the view
            //            $this->response(array('status' => false, 'message' => 'Validation rules violated'), REST_Controller::HTTP_OK);
            $this->response(array('status' => false, 'message' => 'Có lỗi xảy ra !.'), REST_Controller::HTTP_BAD_REQUEST);

        } else {

            // set variables from the form
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            if ($this->user_model->resolve_user_login($email, $password)) {

                $user_id = $this->user_model->get_user_id_from_username($email);
                $user = $this->user_model->get_user($user_id);

                // set session user datas
                $_SESSION['user_id'] = (int)$user->id;
                $_SESSION['username'] = (string)$user->username;
                $_SESSION['email'] = (string)$user->email;
                $_SESSION['logged_in'] = (bool)true;
                $_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
                $_SESSION['is_admin'] = (bool)$user->is_admin;

                // user login ok
                $token_data['uid'] = $user_id;
                $token_data['username'] = $user->username;
                $token_data['fullname'] = $user->fullname;
                $token_data['email'] = $user->email;
                $token_data['class_id'] = $user->class_id; // Bảng đã đăng ký thi thử
                $tokenData = $this->authorization_token->generateToken($token_data);
                $final = array();
                $final['access_token'] = $tokenData;
                $final['status'] = true;
                $final['data'] = $token_data;
                $final['message'] = 'Login success!';
                $final['note'] = 'You are now logged in.';
//
//                //Cập nhật lại điểm thi nếu có sai sót
//                $this->updateLuotThiLoi($user_id);

                $this->response($final, REST_Controller::HTTP_OK);

            } else {

                // login failed
                $this->response(array('status' => false, 'message' => 'Email hoặc mật khẩu không đúng'), REST_Controller::HTTP_BAD_REQUEST);

            }

        }

    }

    public function updateLuotThiLoi($userId)
    {
        $dataLuotThi = $this->UserLuotThi_model->getListLuotThi_ByUserId($userId);
        if(count($dataLuotThi) > 0){
            $user_tongdiem = 0;
            $user_diem_trac_nghiem = 0;
            $user_diem_tu_luan = 0;
            foreach ($dataLuotThi as $key => $lt) {
                $diem_trac_nghiem = $this->Tests_model->getDiemLuotThi($lt->id, 0); // trac_nghiem
                $diem_tu_luan = $this->Tests_model->getDiemLuotThi($lt->id, $lt->tu_luan_id); // trac_nghiem
                //Update điểm
                $update['diem_trac_nghiem'] = $diem_trac_nghiem;
                $update['diem_tu_luan'] = $diem_tu_luan;
                $tongdiem = $diem_trac_nghiem + $diem_tu_luan;
                $update['tong_diem'] = $tongdiem;
                $update['diem_bi_loi'] = 0;
                if ($tongdiem > $user_tongdiem) {
                    $user_tongdiem = $tongdiem;
                    $user_diem_trac_nghiem = $diem_trac_nghiem;
                    $user_diem_tu_luan = $diem_tu_luan;
                }

//            $this->UserLuotThi_model->update($update, $lt->id);
            }
            $userData = array(
                'tong_diem' => $user_tongdiem,
                'diem_trac_nghiem' => $user_diem_trac_nghiem,
                'diem_tu_luan' => $user_diem_tu_luan,
            );
            $this->user_model->update($userData, $userId);
        }
        
    }

    public function step_get()
    {
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) {
            $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            if ($decodedToken['status']) {

                // ------- Main Logic part -------
                $data = [];
                $dataUser = (array)$decodedToken['data'];
                $rowUserLuotThi = $this->UserLuotThi_model->getDetail($dataUser['uid']);
                $countLuotThi = count($rowUserLuotThi);
                if (!$rowUserLuotThi || $countLuotThi == 0) {
                    $this->response(null, REST_Controller::HTTP_OK);
                } else {
                    $dataGet = $this->UserLuotThi_model->getLastByUser($dataUser['uid']);
                    $data['user_id'] = $dataGet['user_id'];
                    $data['so_luot'] = $dataGet['so_luot'];
                    $data['trang_thai'] = $dataGet['so_luot'];
                    $this->response($dataGet, REST_Controller::HTTP_OK);
                } // ------------- End -------------

            } else {
                $this->response(array('status' => false, 'message' => 'Error Authorization!..'), REST_Controller::HTTP_BAD_REQUEST);
            }
        }
    }

    /**
     * UPDATE | PUT method.
     *
     * @return Response
     */
    public function stepSubmit_put()
    {
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) {
            $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            if ($decodedToken['status']) {
                $dataUser = (array)$decodedToken['data'];
                // ------- Main Logic part -------
                $data = json_decode(file_get_contents('php://input'), true);
                $id = (int)$data['id'];
                $oUserLuotThi = $this->UserLuotThi_model->getUserLuotThiById($id);
                if ($oUserLuotThi && $oUserLuotThi['user_id'] == $dataUser['uid']) {
                    //Tính tổng điểm
                    $listAskTracNghiem = $this->Tests_model->showReplyMemberResult($oUserLuotThi['id'], 0); // Danh sách câu hỏi trắc nghiệm
                    $listAskTuLuan = $this->Tests_model->showReplyMemberResult($oUserLuotThi['id'], $oUserLuotThi['tu_luan_id']); // Danh sách câu hỏi trắc nghiệm
                    $diem = 0;
                    if ($listAskTracNghiem) {
                        foreach ($listAskTracNghiem as $key => $vl) {
                            if ((int)$vl->reply_id > 0 && (int)$vl->reply_id == (int)$vl->reply_id_true) {
                                $diem += (int)$vl->diem_ask;
                            }
                        }
                    }
                    if ($listAskTuLuan) {
                        foreach ($listAskTuLuan as $key => $vl) {
                            if ((int)$vl->reply_id > 0 && (int)$vl->reply_id == (int)$vl->reply_id_true) {
                                $diem += (int)$vl->diem_ask;
                            }
                        }
                    }
                    //End tính tổng điểm
                    $dataUpdate['tong_diem'] = $diem;
                    $dataUpdate['time_con_lai'] = 0;
                    $dataUpdate['time_ket_thuc_thi'] = time();
                    $dataUpdate['trang_thai'] = 1;
                    $response = $this->UserLuotThi_model->update($dataUpdate, $id);

                    $response > 0 ? $this->response(array('status' => true, 'message' => 'Success'), REST_Controller::HTTP_OK) : $this->response(array('status' => false, 'message' => 'Fail'), REST_Controller::HTTP_BAD_REQUEST);
                } else {
                    $this->response(array('status' => false, 'message' => 'Authentication failed'), REST_Controller::HTTP_BAD_REQUEST);
                }
                // ------------- End -------------
            } else {
                $this->response($decodedToken);
            }
        } else {
            $this->response(['Authentication failed'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    /**
     * UPDATE | PUT method.
     *
     * @return Response
     */
    public function stepUpdate_put()
    {
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) {
            $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            if ($decodedToken['status']) {

                $dataUser = (array)$decodedToken['data'];
                // ------- Main Logic part -------
                $data = json_decode(file_get_contents('php://input'), true);
                $id = (int)$data['id'];
                $oUserLuotThi = $this->UserLuotThi_model->getUserLuotThiById($id);
                if ($oUserLuotThi && $oUserLuotThi['user_id'] == $dataUser['uid']) {
                    $oUserLuotThi['time_con_lai'] = $data['time_con_lai'];
                    if ((int)$data['trang_thai'] == 1) {
                        $oUserLuotThi['time_ket_thuc_thi'] = time();
                    }
                    $oUserLuotThi['trang_thai'] = $data['trang_thai'];
                    $response = $this->UserLuotThi_model->update($oUserLuotThi, $id);
                    $response > 0 ? $this->response(array('status' => true, 'message' => 'Success'), REST_Controller::HTTP_OK) : $this->response(array('status' => false, 'message' => 'Fail'), REST_Controller::HTTP_BAD_REQUEST);
                } else {
                    $this->response(array('status' => false, 'message' => 'Authentication failed'), REST_Controller::HTTP_OK);
                }
                // ------------- End -------------
            } else {
                $this->response($decodedToken);
            }
        } else {
            $this->response(['Authentication failed'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    /**
     * logout function.
     *
     * @access public
     * @return void
     */
    public function logout_post()
    {
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
            // user logout ok
            $this->response(array('status' => true, 'message' => 'Logout success!'), REST_Controller::HTTP_OK);

        } else {
            // there user was not logged in, we cannot logged him out,
            // redirect him to site root
            // redirect('/');
            $this->response(array('status' => false, 'message' => 'There was a problem. Please try again.'), REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function getDetailUser_get()
    {
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) {
            $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            if ($decodedToken['status']) {

                // ------- Main Logic part -------
                $data = [];

                $dataUser = (array)$decodedToken['data'];
                $detail = $this->user_model->get_user($dataUser['uid']);
                $data['username'] = $detail->username;
                $data['fullname'] = $detail->fullname;
                $data['class_id'] = $detail->class_id;
                $data['email'] = $detail->email;
                $data['phone'] = $detail->phone;
                $data['gioi_tinh'] = $detail->gioi_tinh;
                $data['ngay_sinh'] = $detail->ngay_sinh;
                $data['user_info'] = $detail->user_info;

                $this->response($data, REST_Controller::HTTP_OK);
            }

        }
    }

    public function totalRegister_get()
    {
        $detail = $this->user_model->totalRegisterMember();
        $final['status'] = true;
        $final['total'] = $detail;
        $final['message'] = 'Thank you for registering your new account!';
        $final['note'] = 'You have successfully register. Please check your email inbox to confirm your email address.';

        $this->response($final, REST_Controller::HTTP_OK);
    }

    public function totalExam_get()
    {
        $detail = $this->user_model->totalExam();
        $final['status'] = true;
        $final['total'] = $detail;
        $final['message'] = 'Thank you for registering your new account!';
        $final['note'] = 'You have successfully register. Please check your email inbox to confirm your email address.';

        $this->response($final, REST_Controller::HTTP_OK);
    }

    public function editMember_put()
    {
        $data = json_decode(file_get_contents('php://input'), true);
        $user_id = $this->user_model->get_user_id_from_username($data['email']);
        $user = $this->user_model->get_user($user_id);
        if ($user) {
            $datas['fullname'] = $data['ho_ten'];
            $datas['phone'] = $data['phone'];
            $datas['gioi_tinh'] = $data['gioi_tinh'];
            $datas['ngay_sinh'] = $data['ngay_sinh'] . '/' . $data['thang_sinh'] . '/' . $data['nam_sinh'];
            $datas['truong_hoc_id'] = $data['truong_hoc_id'];
            $datas['truong_hoc_ten'] = $data['truong_hoc_ten'];
            $datas['user_info'] = json_encode($data['user_info']);
            $this->user_model->update($datas, $user_id);
        }
        $final['status'] = true;
        $final['total'] = $datas;
        $final['message'] = '';
        $final['note'] = '';

        $this->response($final, REST_Controller::HTTP_OK);
    }

    public function updateAllDiemThi_get()
    {
        die();
        $sql="UPDATE users
            SET tong_diem = (
                SELECT MAX(tong_diem)
                FROM user_luot_thi
                WHERE user_luot_thi.user_id = users.id
            )
            WHERE users.tong_diem = -1 and users.id < 4000";
        $this->db->query($sql);
//        for($i = 0; $i < 1000; $i ++){
//            $iii = ($i + 1) * 1000;
//            $sql="UPDATE users
//            SET tong_diem = (
//                SELECT MAX(tong_diem)
//                FROM user_luot_thi
//                WHERE user_luot_thi.user_id = users.id
//            )
//            WHERE users.tong_diem = -1 and users.id < $iii";
//            $this->db->query($sql);
//        }
        
        
//        echo 'bat dau: '.date("H:i:s");
//        $users = $this->user_model->getAllUserLimit(100);
//        foreach ($users as $key => $vl) {
//            $this->updateLuotThiLoi($vl['id']);
//        }
//        echo 'ket thuc: '.date("H:i:s");
    }
    
    public function danhSachDiemThiBangA_get(){
        $page = $this->input->get('page', true);
        $page_size = $this->input->get('page_size', true);
        $page = $page ? $page : 1;
        $page_size = $page_size ? $page_size : 100;
        $data['list'] = $this->user_model->getAllUsersByClass_page(2, $page_size, $page); // page_size, page tong bản ghi/page_size
        $data['countAll'] = $this->user_model->getAllUsersByClassCouunt(2);
        
        $this->load->view('danhSachDiemBangA', $data);
    }
    
    public function exportExcelBangA_get(){
        require_once APPPATH . "/third_party/PHPExcel.php";
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Document")
            ->setSubject("Office 2007 XLSX Document")
            ->setDescription("Document for Office 2007 XLSX")
            ->setKeywords("Office 2007 openxml")
            ->setCategory("Result file");
        //Cau hinh kich thuoc, mau, size...
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "STT");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "Họ tên");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "Ngày sinh");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "Số điện thoại");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "Email");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "Trường");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "Khoa");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "Tổng điểm");

        $page = $this->input->get('page', true);
        $page_size = $this->input->get('page_size', true);
        $page = $page ? $page : 1;
        $page_size = $page_size ? $page_size : 100;
        $_list = $this->user_model->getAllUsersByClass_page(2, $page_size, $page);

        if (!empty($_list)) {
            $count = 1;
            foreach ($_list as $key => $vl) {
                $userInfo = json_decode($vl['user_info'], true);
                $count += 1;
                $l_1 = "A" . "$count";
                $l_2 = "B" . "$count";
                $l_3 = "C" . "$count";
                $l_4 = "D" . "$count";
                $l_5 = "E" . "$count";
                $l_6 = "F" . "$count";
                $l_7 = "G" . "$count";
                $l_8 = "H" . "$count";
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_1, ($key + 1));
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_2, $vl['fullname']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_3, $vl['ngay_sinh']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_4, $vl['phone']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_5, $vl['email']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_6, $userInfo['ten_truong']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_7, $userInfo['lop']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_8, $vl['tong_diem']);
            }
        }

        $objPHPExcel->getActiveSheet()->setTitle('NET2021');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Bảng A ' . date("d/m/Y") . '.xls"');
        $objWriter->save('php://output');
        exit;
    }
   
    
    public function danhSachDiemThiBangB_get(){
        $page = $this->input->get('page', true);
        $page_size = $this->input->get('page_size', true);
        $page = $page ? $page : 1;
        $page_size = $page_size ? $page_size : 100;
        $data['list'] = $this->user_model->getAllUsersByClass_page(4, $page_size, $page);
        $data['countAll'] = $this->user_model->getAllUsersByClassCouunt(4);
        $this->load->view('danhSachDiemBangB', $data);
    }

    public function exportExcelBangB_get(){
        require_once APPPATH . "/third_party/PHPExcel.php";
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Document")
            ->setSubject("Office 2007 XLSX Document")
            ->setDescription("Document for Office 2007 XLSX")
            ->setKeywords("Office 2007 openxml")
            ->setCategory("Result file");
        //Cau hinh kich thuoc, mau, size...
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');
        $objPHPExcel->getActiveSheet()->getStyle('A1:H1')->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
        $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);

        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "STT");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "Họ tên");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "Ngày sinh");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "Số điện thoại");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "Email");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "Trường");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "Khoa");
        $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H1', "Tổng điểm");

        $page = $this->input->get('page', true);
        $page_size = $this->input->get('page_size', true);
        $page = $page ? $page : 1;
        $page_size = $page_size ? $page_size : 100;
        $_list = $this->user_model->getAllUsersByClass_page(4, $page_size, $page);

        if (!empty($_list)) {
            $count = 1;
            foreach ($_list as $key => $vl) {
                $userInfo = json_decode($vl['user_info'], true);
                $count += 1;
                $l_1 = "A" . "$count";
                $l_2 = "B" . "$count";
                $l_3 = "C" . "$count";
                $l_4 = "D" . "$count";
                $l_5 = "E" . "$count";
                $l_6 = "F" . "$count";
                $l_7 = "G" . "$count";
                $l_8 = "H" . "$count";
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_1, ($key + 1));
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_2, $vl['fullname']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_3, $vl['ngay_sinh']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_4, $vl['phone']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_5, $vl['email']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_6, $userInfo['ten_truong']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_7, $userInfo['lop']);
                $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_8, $vl['tong_diem']);
            }
        }

        $objPHPExcel->getActiveSheet()->setTitle('NET2021');
        $objPHPExcel->setActiveSheetIndex(0);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Bảng B ' . date("d/m/Y") . '.xls"');
        $objWriter->save('php://output');
        exit;
    }


    public function getAllUserLuotThi_get(){
        die();
        $this->db->select('user_id, MAX(tong_diem) AS tong_diem');
        $this->db->where('tong_diem >', 0);
        $this->db->group_by('user_id');
        $result = $this->db->get('user_luot_thi')->result_array();
        foreach ($result as $key => $vl){
//            $this->user_model->update($userData, $userId);
        }
    }

    public function getDiemThiByUser_get(){
        $email = $this->input->get('email', true);
        $this->db->select('tong_diem');
        $this->db->where('email', trim($email));
        $result = $this->db->get('users')->row_array();
//        echo  $result['tong_diem'];
        return $result;
    }

    

}
