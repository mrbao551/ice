<?php

/**
 * Product class.
 *
 * @extends REST_Controller
 */
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Tests extends REST_Controller
{

    /**
     * CONSTRUCTOR | LOAD MODEL
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Authorization_Token');
        $this->load->model('Tests_model');
        $this->load->model('UserLuotThi_model');
        $this->load->model('Config_model');
    }

    /**
     * SHOW | GET method.
     *
     * @return Response
     */
    public function index_get()
    {
        $headers = $this->input->request_headers();
        try {
            if (isset($headers['Authorization'])) {
                $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
                if ($decodedToken['status']) {

                    // ------- Main Logic part -------
                    $data = [];
                    $dataUser = (array)$decodedToken['data'];
                    $bang_thi_id = $dataUser['class_id'];
                    $config = $this->Config_model->getConfig();
                    $rowUserLuotThi = $this->UserLuotThi_model->getDetail($dataUser['uid']);
                    $countLuotThi = count($rowUserLuotThi);
                    if (!$rowUserLuotThi || $countLuotThi == 0) {
                        $this->renderData($dataUser, 1, $bang_thi_id, $config);
                    } elseif ($rowUserLuotThi && $countLuotThi <= (int)$config['so_lan_thi']) {

//                        $this->response(array('status' => false, 'message' => 'Bạn đã hết lượt thi'), REST_Controller::HTTP_BAD_REQUEST);
                        
                        if ($countLuotThi == (int)$config['so_lan_thi'] && $rowUserLuotThi[0]->trang_thai == 1) {
                            $this->response(array('status' => false, 'message' => 'Bạn đã hết lượt thi'), REST_Controller::HTTP_BAD_REQUEST);
                        } else {
                            if ($rowUserLuotThi[0]->trang_thai == 0) {
                                // Lấy ra danh sách

                                $this->getData($dataUser, $bang_thi_id, $rowUserLuotThi);

                            } else {
                                // Tạo mới lượt thi tiếp theo
                                $this->renderData($dataUser, ((int)$rowUserLuotThi[0]->so_luot + 1), $bang_thi_id, $config);
                            }
                        }

                    } else {
                        $this->response(array('status' => false, 'message' => 'Bạn đã hết lượt thi'), REST_Controller::HTTP_BAD_REQUEST);
                    }

                    if (empty($bang_thi_id) || $bang_thi_id == 0) {
                        $this->response(array('status' => false, 'message' => 'Có lỗi.'), REST_Controller::HTTP_BAD_REQUEST);
                    }
                    if (!in_array($bang_thi_id, array(2, 4))) {
                        $this->response(array('status' => false, 'message' => 'Có lỗi.'), REST_Controller::HTTP_BAD_REQUEST);
                    }
                } else {
                    $this->response($decodedToken);
                }
            } else {
                $this->response(['Authentication failed'], REST_Controller::HTTP_BAD_REQUEST);
            }
        } catch (\mysql_xdevapi\Exception $e) {
            $this->response([$e], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    /**
     * SHOW | GET method.
     *
     * @return Response
     */
    public function result_get($id = 0)
    {
        $headers = $this->input->request_headers();
        try {
            if (isset($headers['Authorization'])) {
                $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
                if ($decodedToken['status']) {
                    $dataUser = (array)$decodedToken['data'];
                    //Lay danh sach
                    if ($id == 0) {
                        $data = $this->UserLuotThi_model->getListLuotThiByUserId($dataUser['uid']);
                        foreach ($data as $key => $vl) {
                            if ((int)$vl->time_ket_thuc_thi > 0) {
                                $conlai = $vl->time_ket_thuc_thi - $vl->time_bat_dau_thi; // số giây
                                $vl->time_bat_dau_thi = date('d/m/Y H:i:s', $vl->time_bat_dau_thi);
                                $vl->time_ket_thuc_thi = date('d/m/Y H:i:s', $vl->time_ket_thuc_thi);
                                $vl->thoi_gian_thi = gmdate('H:i:s', $conlai);
                            } else {
                                $vl->time_bat_dau_thi = date('d/m/Y H:i:s', $vl->time_bat_dau_thi);
                            }
                        }

                        $this->response($data, REST_Controller::HTTP_OK);
                    } else //Lay chi tiet
                    {
                        $data = $this->UserLuotThi_model->getDetailLuotThiByUserId($id);
                        //echo $data['trang_thai'];
                        if ((int)$data['user_id'] == (int)$dataUser['uid'] && (int)$data['trang_thai'] == 1) {
                            $listAskTracNghiem = $this->Tests_model->showReplyMemberResult($data['id'], 0); // Danh sách câu hỏi trắc nghiệm
                            $listAskTuLuan = $this->Tests_model->showReplyMemberResult($data['id'], $data['tu_luan_id']); // Danh sách câu hỏi trắc nghiệm
                            $count_tu_luan_true = 0;
                            $count_trac_nghiem_true = 0;
                            $diem = 0;
                            $tong_diem = 0;
                            if ($listAskTracNghiem) {
                                foreach ($listAskTracNghiem as $key => $vl) {
                                    if ((int)$vl->reply_id > 0 && (int)$vl->reply_id == (int)$vl->reply_id_true) {
                                        $count_trac_nghiem_true += 1;
                                        $diem += (int)$vl->diem_ask;
                                    }
                                    $tong_diem += (int)$vl->diem_ask;
                                    $vl->content = $this->Tests_model->getTitleByAskId($vl->ask_id);
                                    $vl->stt = ($key + 1);
                                    $vl->answers = $this->Tests_model->getAnswers($vl->ask_id);
                                }
                            }
                            if ($listAskTuLuan) {
                                foreach ($listAskTuLuan as $key => $vl) {
                                    if ((int)$vl->reply_id > 0 && (int)$vl->reply_id == (int)$vl->reply_id_true) {
                                        $count_tu_luan_true += 1;
                                        $diem += (int)$vl->diem_ask;
                                    }
                                    $tong_diem += (int)$vl->diem_ask;
                                    $vl->content = $this->Tests_model->getTitleByAskId($vl->ask_id);
                                    $vl->stt = ($key + 1);
                                    $vl->answers = $this->Tests_model->getAnswers($vl->ask_id);
                                }
                            }
                            $tu_luan['data'] = $listAskTuLuan;
                            $baiviet = $this->Tests_model->getTuLuanByTuLuanId($data['tu_luan_id']);
                            $tu_luan['content'] = $baiviet['content'];
                            $tu_luan['title'] = $baiviet['title'];
                            $data['trac_nghiem'] = $listAskTracNghiem;
                            $data['tu_luan'] = $tu_luan;
                            $data['count_tu_luan_true'] = $count_tu_luan_true;
                            $data['count_trac_nghiem_true'] = $count_trac_nghiem_true;
                            $data['tong_diem'] = $tong_diem;
                            $data['diem'] = $diem;
                            $conlai = $data['time_ket_thuc_thi'] - $data['time_bat_dau_thi']; // số giây
                            $data['time_bat_dau_thi'] = date('d/m/Y H:i:s', $data['time_bat_dau_thi']);
                            $data['time_ket_thuc_thi'] = date('d/m/Y H:i:s', $data['time_ket_thuc_thi']);
                            $data['con_lai'] = gmdate('H:i:s', $conlai);

                            $this->response($data, REST_Controller::HTTP_OK);
                        } else {
                            $this->response(array('status' => false, 'message' => 'Không tồn tại'), REST_Controller::HTTP_BAD_REQUEST);
                        }

                    }
                } else {
                    $this->response($decodedToken);
                }
            } else {
                $this->response(['Authentication failed'], REST_Controller::HTTP_BAD_REQUEST);
            }
        } catch (\mysql_xdevapi\Exception $e) {
            $this->response([$e], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    private function getData($dataUser, $bang_thi_id, $rowUserLuotThi)
    {
        //Lấy lượt thi
        $luotthi = $this->Tests_model->getLuotThiCurrent($dataUser['uid'], $rowUserLuotThi[0]->so_luot);

        $listAskTracNghiem = $this->Tests_model->showReplyMember($dataUser['uid'], $luotthi['id'], 0); // Danh sách câu hỏi trắc nghiệm
        $listAskTuLuan = $this->Tests_model->showReplyMember($dataUser['uid'], $luotthi['id'], $luotthi['tu_luan_id']); // Danh sách câu hỏi trắc nghiệm
        if ($listAskTracNghiem) {
            foreach ($listAskTracNghiem as $key => $vl) {
                $vl->content = $this->Tests_model->getTitleByAskId($vl->ask_id);
                $vl->stt = ($key + 1);
                $vl->answers = $this->Tests_model->getAnswers($vl->ask_id);
            }
        }
        if ($listAskTuLuan) {
            foreach ($listAskTuLuan as $key => $vl) {
                $vl->content = $this->Tests_model->getTitleByAskId($vl->ask_id);
                $vl->stt = ($key + 1);
                $vl->answers = $this->Tests_model->getAnswers($vl->ask_id);
            }
        }
        $tu_luan['data'] = $listAskTuLuan;
        $baiviet = $this->Tests_model->getTuLuanByTuLuanId($luotthi['tu_luan_id']);
        $tu_luan['content'] = $baiviet['content'];
        $tu_luan['title'] = $baiviet['title'];
        $luotthi['trac_nghiem'] = $listAskTracNghiem;
        $luotthi['tu_luan'] = $tu_luan;
        $this->response($luotthi, REST_Controller::HTTP_OK);
    }

    private function renderData($dataUser, $so_luot, $bang_cuoc_thi, $config)
    {
        $randomTuLuan = $this->Tests_model->creatRandomTuLuan($bang_cuoc_thi);
        $dataLuotThi['user_id'] = $dataUser['uid'];
        $dataLuotThi['so_luot'] = $so_luot;
        $dataLuotThi['trang_thai'] = 0;
        $dataLuotThi['time_bat_dau_thi'] = time();
        $dataLuotThi['time_con_lai'] = (int)$config["thoi_gian"] * 60 * 1000;
        $dataLuotThi['tu_luan_id'] = $randomTuLuan['id'];
        $dataLuotThiId = $this->UserLuotThi_model->insert($dataLuotThi);
        $dataInsert['user_id'] = $dataUser['uid'];
        $randomAsk = $this->Tests_model->creatRandomAskAnswer($bang_cuoc_thi, (int)$config["tong_so_cau_hoi_trac_nghiem"]);

        $listAskTuLuan = $this->Tests_model->getListAskTuLuan($randomTuLuan['id']);

        foreach ($randomAsk as $key => $vl) {
            $reply_id_true = $this->Tests_model->getAnswersTrue($vl->id);
            $dataInsert['class_id'] = $dataUser['class_id'];
            $dataInsert['user_id'] = $dataUser['uid'];
            $dataInsert['user_luot_thi_id'] = $dataLuotThiId;
            $dataInsert['ask_id'] = $vl->id;
            $dataInsert['diem_ask'] = $vl->diem;
            $dataInsert['time_created'] = time();
            $dataInsert['reply_id_true'] = isset($reply_id_true) ? $reply_id_true : 0;
            $dataInsert['active'] = 1;
            $dataInsert['tu_luan_id'] = 0;
            $this->Tests_model->insert($dataInsert);
        }
        foreach ($listAskTuLuan as $key => $vl) {
            $reply_id_true = $this->Tests_model->getAnswersTrue($vl->id);
            $dataInsert['class_id'] = $dataUser['class_id'];
            $dataInsert['user_id'] = $dataUser['uid'];
            $dataInsert['user_luot_thi_id'] = $dataLuotThiId;
            $dataInsert['ask_id'] = $vl->id;
            $dataInsert['diem_ask'] = $vl->diem;
            $dataInsert['time_created'] = time();
            $dataInsert['reply_id_true'] = isset($reply_id_true) ? $reply_id_true : 0;
            $dataInsert['active'] = 1;
            $dataInsert['tu_luan_id'] = $vl->tu_luan_id;
            $this->Tests_model->insert($dataInsert);
        }
        $rowUserLuotThi = $this->UserLuotThi_model->getDetail($dataUser['uid']);
        $this->getData($dataUser, $bang_cuoc_thi, $rowUserLuotThi);
    }

    /**
     * INSERT | POST method.
     *
     * @return Response
     */
    public function index_post()
    {

    }

    /**
     * UPDATE | PUT method.
     *
     * @return Response
     */
    public function index_put($id, $reply_id)
    {
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) {
            $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            if ($decodedToken['status']) {
                // ------- Main Logic part -------
                $UserReplyById = $this->Tests_model->getUserReplyById($id);
                if ($UserReplyById && $UserReplyById['user_id'] == $decodedToken['data']->uid) {
                    $id = (int)$id;
                    $reply_id = (int)$reply_id;
                    $data['reply_id'] = $reply_id;
                    $data['time_reply'] = time();
                    $response = $this->Tests_model->update($data, $id);
                    $response > 0 ? $this->response(array('status' => true, 'message' => 'Success'), REST_Controller::HTTP_OK) : $this->response(array('status' => false, 'message' => 'Fail'), REST_Controller::HTTP_BAD_REQUEST);
                } else {
                    $this->response(array('status' => false, 'message' => 'Authentication failed'), REST_Controller::HTTP_OK);
                }
                // ------------- End -------------
            } else {
                $this->response($decodedToken);
            }
        } else {
            $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
        }
    }

    /**
     * DELETE method.
     *
     * @return Response
     */
    public function index_delete($id)
    {

        $headers = $this->input->request_headers();
        if (isset($headers['Authorization'])) {
            $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
            if ($decodedToken['status']) {
                // ------- Main Logic part -------
                $response = $this->Tests_model->delete($id);

                $response > 0 ? $this->response(['Product deleted successfully.'], REST_Controller::HTTP_OK) : $this->response(['Not deleted'], REST_Controller::HTTP_OK);
                // ------------- End -------------
            } else {
                $this->response($decodedToken);
            }
        } else {
            $this->response(['Authentication failed'], REST_Controller::HTTP_OK);
        }
    }

}
