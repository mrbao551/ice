<?php

/**
 * Product class.
 *
 * @extends REST_Controller
 */
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Config extends REST_Controller
{

    /**
     * CONSTRUCTOR | LOAD MODEL
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('Authorization_Token');
        $this->load->model('Config_model');
    }

    /**
     * SHOW | GET method.
     *
     * @return Response
     */
    public function index_get()
    {
        $headers = $this->input->request_headers();
        try {
            if (isset($headers['Authorization'])) {
                $decodedToken = $this->authorization_token->validateToken($headers['Authorization']);
                if ($decodedToken['status']) {
                    $data=$this->Config_model->getConfig();
                    $this->response($data, REST_Controller::HTTP_OK);
                } else {
                    $this->response($decodedToken);
                }
            } else {
                $this->response(['Authentication failed'], REST_Controller::HTTP_BAD_REQUEST);
            }
        } catch (\mysql_xdevapi\Exception $e) {
            $this->response([$e], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    

}
