<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;


class Home extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url', 'html', 'form'));
    }

    public function thongKeHightShools_get()
    {
        $_get = $this->input->get(NULL, true);
        $data['district'] = $this->db->from('district')->where(array('stt !=' => ''))->order_by('stt ASC')->get()->result_array();
        require_once APPPATH . "/third_party/PHPExcel.php";
        $objPHPExcel = new PHPExcel();
        if (isset($_get['id']) && $_get['id']) {

            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                ->setLastModifiedBy("Maarten Balliauw")
                ->setTitle("Office 2007 XLSX Document")
                ->setSubject("Office 2007 XLSX Document")
                ->setDescription("Document for Office 2007 XLSX")
                ->setKeywords("Office 2007 openxml")
                ->setCategory("Result file");
            //Cau hinh kich thuoc, mau, size...
            $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');
            $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getFont()->setBold(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Họ tên");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "Ngày sinh");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "SĐT");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "email");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "Khoa");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "Lớp");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "Điểm");


            $_list = $this->db->select('fullname, ngay_sinh, phone, email, truong_hoc_ten, tong_diem, user_info')->where(array('truong_hoc_id' => $_get['id'], 'class_id' => 2, 'tong_diem <' => 60))->order_by("tong_diem DESC")->get('users')->result_array();

            if (!empty($_list)) {
                $count = 1;
                foreach ($_list as $key => $vl) {
                    $count += 1;
                    $l_1 = "A" . "$count";
                    $l_2 = "B" . "$count";
                    $l_3 = "C" . "$count";
                    $l_4 = "D" . "$count";
                    $l_5 = "E" . "$count";
                    $l_6 = "F" . "$count";
                    $l_7 = "G" . "$count";
                    $userInfo = json_decode($vl['user_info'], true);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_1, $vl['fullname']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_2, $vl['ngay_sinh']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_3, $vl['phone']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_4, $vl['email']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_5, $vl['truong_hoc_ten']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_6, $userInfo['lop']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_7, $vl['tong_diem']);
                }
            }

            $objPHPExcel->getActiveSheet()->setTitle('NET2021');
            $objPHPExcel->setActiveSheetIndex(0);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . get_where($_get['id'], 'title', 'hightschool') . ' - ' . date("d/m/Y") . '.xls"');
            $objWriter->save('php://output');
            exit;
        }
        $this->load->view('thongke_hightshools', $data);
    }

    public function thongKeShools_get()
    {
        $_get = $this->input->get(NULL, true);
        $data['school'] = $this->db->from('schools')->where(array('active' => 1))->order_by('view DESC')->get()->result_array();
        require_once APPPATH . "/third_party/PHPExcel.php";
        $objPHPExcel = new PHPExcel();
        if (isset($_get['id']) && $_get['id']) {
            $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                ->setLastModifiedBy("Maarten Balliauw")
                ->setTitle("Office 2007 XLSX Document")
                ->setSubject("Office 2007 XLSX Document")
                ->setDescription("Document for Office 2007 XLSX")
                ->setKeywords("Office 2007 openxml")
                ->setCategory("Result file");
            //Cau hinh kich thuoc, mau, size...
            $objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman');
            $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);

            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A1', "Họ tên");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B1', "Ngày sinh");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C1', "SĐT");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D1', "email");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E1', "Khoa");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F1', "Lớp");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G1', "Điểm");


            $_list = $this->db->select('fullname, ngay_sinh, phone, email, truong_hoc_ten, tong_diem, user_info')->where(array('truong_hoc_id' => $_get['id'], 'class_id' => 4, 'tong_diem <' => 60))->order_by("tong_diem DESC")->get('users')->result_array();

            if (!empty($_list)) {
                $count = 1;
                foreach ($_list as $key => $vl) {
                    $count += 1;
                    $l_1 = "A" . "$count";
                    $l_2 = "B" . "$count";
                    $l_3 = "C" . "$count";
                    $l_4 = "D" . "$count";
                    $l_5 = "E" . "$count";
                    $l_6 = "F" . "$count";
                    $l_7 = "G" . "$count";
                    $userInfo = json_decode($vl['user_info'], true);

                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_1, $vl['fullname']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_2, $vl['ngay_sinh']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_3, $vl['phone']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_4, $vl['email']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_5, $vl['truong_hoc_ten']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_6, $userInfo['lop']);
                    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($l_7, $vl['tong_diem']);
                }
            }

            $objPHPExcel->getActiveSheet()->setTitle('NET2021');
            $objPHPExcel->setActiveSheetIndex(0);
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . get_where($_get['id'], 'title', 'schools') . ' - ' . date("d/m/Y") . '.xls"');
            $objWriter->save('php://output');
            exit;
        }
        $this->load->view('thongke_shools', $data);
    }
}
